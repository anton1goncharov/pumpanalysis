package main.java.dts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static main.java.dts.Dts.getStackTrace;

/**
 * Extracted stateful functionality from Dts servlet to make threadsafe
 *
 * @author Cole Markham
 */
public class DtsHandler {

    private final static Logger logger = LoggerFactory.getLogger(DtsHandler.class);

    private Dts dts;
    Connection conn = null;
    WriterWrapper out = null;
    PrintWriter responseOut = null;
    String[] tagNames;
    String prefix;
    boolean useCustomHeader = false;
    String customHeader = null;

    public DtsHandler(Dts dts) {
        this.dts = dts;
    }

    private String removePrefix(String tagName) {
        if (tagName.indexOf(prefix) == 0) {
            return tagName.substring(prefix.length());
        } else {
            return tagName;
        }
    }

    private String makeQuery(String startDate, String endDate) {
        StringBuilder sql = new StringBuilder();
        String newLine = "\n";
        String Q = "\"";
        sql.append("SELECT a.`DATATIME`");
        for (String tag : tagNames) {
            sql.append(newLine);
            sql.append("    , SUM(CASE WHEN a.`TAGNAME` = ");
            sql.append(Q).append(tag).append(Q);
            sql.append(" THEN a.`VALUE` ELSE NULL END) AS ");
            sql.append(removePrefix(tag));
        }
        String[] conditions = new String[3];
        int conditionCount = 0;
        if (startDate != null) {
            conditions[conditionCount] = "`DATATIME` >= '" + startDate + "'";
            conditionCount++;
        }
        if (endDate != null) {
            conditions[conditionCount] = "`DATATIME` <= '" + endDate + "'";
            conditionCount++;
        }
        if (prefix.length() != 0) {
            conditions[conditionCount] = "`TAGNAME` LIKE " + Q + prefix + "%" + Q;
            conditionCount++;
        }
        String whereClause = "";
        if (conditionCount > 0) {
            String separator = "";
            whereClause = "WHERE ";
            for (int i = 0; i < conditionCount; i++) {
                String condition = conditions[i];
                whereClause += separator;
                whereClause += condition;
                separator = " AND ";
            }
        }
        String subTable = "(SELECT `DATATIME`, `TAGNAME`, `VALUE` FROM `tag_data60mins` "
                + whereClause + ") AS a";
        sql.append(newLine).append("FROM ").append(subTable);
        sql.append(newLine).append("GROUP BY a.`DATATIME`");
        sql.append(newLine).append("ORDER BY a.`DATATIME`");
        return sql.toString();
    }

    private void retrieveTagNamesFromRequest(HttpServletRequest request) {
        ArrayList<String> tags = new ArrayList<>();
        int idx = 0;
        while (true) {
            String tagName = request.getParameter("t" + idx);
            idx++;
            if (tagName == null) {
                break;
            } else {
                if (dts.dtsTagNameSet.contains(tagName)) {
                    tags.add(tagName);
                } else {
                    responseOut.println("tag name does not exist: " + tagName);
                    logger.info("Tag name does not exist: {}", tagName);
                }
            }
        }
        tagNames = new String[tags.size()];
        for (int i = 0; i < tagNames.length; i++) {
            tagNames[i] = tags.get(i);
        }
    }

    private void retrieveTagNamesFromProperties(String propFileName) {
        // out.println(sctx.getRealPath(propFileName));
        Properties prop = new Properties();
        // prop.load(new FileInputStream("filename")); in "normal" (non-servlet)
        // circumstances
        try {
            prop.load(dts.sctx.getResourceAsStream(propFileName));
        } catch (IOException e) {
            responseOut.println(getStackTrace(e));
            logger.error("Failed to retrieve tag names from properties: ", e);
        }
        ArrayList<String> tags = new ArrayList<String>();
        int idx = 0;
        while (true) {
            String tagName = prop.getProperty("t" + idx);
            idx++;
            if (tagName == null) {
                break;
            } else {
                if (dts.dtsTagNameSet.contains(tagName)) {
                    tags.add(tagName);
                } else {
                    responseOut.println("tag name does not exist: " + tagName);
                    logger.info("Tag name does not exist: {}", tagName);
                }
            }
        }
        tagNames = new String[tags.size()];
        for (int i = 0; i < tagNames.length; i++) {
            tagNames[i] = tags.get(i);
        }
        prefix = prop.getProperty("prefix");
        String h = prop.getProperty("header");
        if (h != null) {
            useCustomHeader = true;
            customHeader = h;
        }
    }

    abstract class WriterWrapper {
        public abstract void print(String s);

        public abstract void close();

        public void println(String s) {
            this.print(s + "\n");
        }
    }

    private class ServletOnlyWriter extends WriterWrapper {
        PrintWriter out;

        public ServletOnlyWriter(PrintWriter _out) {
            out = _out;
        }

        public void print(String s) {
            out.print(s);
        }

        public void close() {
            out.close();
        }

    }

    private long getSizeOfNewlyWrittenCsvFile(String fileName) throws Exception {
        File csvFile = new File(fileName);
        if (csvFile.exists()) {
            return csvFile.length();
        } else {
            logger.error("New cache file not found");
            throw new Exception("new cache file not found");
        }
    }

    private class ServletAndCacheWriter extends WriterWrapper {
        PrintWriter out = null; // servlet output
        BufferedWriter cacheFileOut = null; // for writing the same stuff to
        // cache file
        String cacheFileName = null;
        String cacheFileNameWithoutPath = null;

        public ServletAndCacheWriter(PrintWriter _out, String callingAppDataDir, int csvManifestNumRows) {
            out = _out;
            cacheFileNameWithoutPath = "" + csvManifestNumRows + ".csv";
            cacheFileName = callingAppDataDir + cacheFileNameWithoutPath;
            //responseOut.println("cache name " + cacheFileNameWithoutPath);
            //responseOut.println("full cache name " + cacheFileName);
            File csvFile = new File(cacheFileName);
            if (!csvFile.exists()) {
                try {
                    csvFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    out.println("couldn't create cache file");
                    out.println(getStackTrace(e));
                    logger.info("Couldn't create cache file: ", e);
                    return;
                }
            }
            try {
                cacheFileOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cacheFileName), "ISO-8859-1"));
            } catch (UnsupportedEncodingException | FileNotFoundException e) {
                e.printStackTrace();
                out.println("couldn't create BufferedWriter to write to cache file");
                out.println(getStackTrace(e));
                logger.error("Couldn't create BufferedWriter to write to cache file", e);
            }
        }

        public void print(String s) {
            out.print(s);
            if (cacheFileOut == null)
                return;
            try {
                cacheFileOut.write(s);
            } catch (IOException e) {
                e.printStackTrace();
                out.println("couldn't write to cache file");
                out.println(getStackTrace(e));
                logger.info("Couldn't write to cache file: ", e);
            }
        }

        public void close() {
            out.close();
            if (cacheFileOut == null)
                return;
            try {
                cacheFileOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                out.println("couldn't close cache file");
                out.println(getStackTrace(e));
                logger.error("Couldn't close cache file", e);
            }
        }

        public void closeCache() {
            try {
                cacheFileOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                out.println("couldn't close cache file");
                out.println(getStackTrace(e));
                logger.error("Couldn't close cache file", e);
            }
        }
    }

    private CsvManifestReader.CsvManifestEntry[] getCsvManifest(String callingAppDataDir) throws Exception {
        String csvManifestFileName = callingAppDataDir + "csvManifest.csv";
        return CsvManifestReader.getFromFile(csvManifestFileName);
    }

    private String updateCsvManifest(String callingAppDataDir, String fileName, String startDateString, String endDateString, long fileSize) {
        // Ignore bad data
        if (fileName == null || fileName.isEmpty()) return "missing fileName";
        String csvManifestFileName = callingAppDataDir + "csvManifest.csv";
        logger.info("Update manifest file {}", csvManifestFileName);
        //responseOut.println(csvManifestFileName);
        //responseOut.println(fileName);
        //responseOut.println(startDateString);
        //responseOut.println(endDateString);
        //responseOut.println(fileSize);
        try (RandomAccessFile fread = new RandomAccessFile(csvManifestFileName, "r");
             FileWriter fr = new FileWriter(csvManifestFileName, true);
             BufferedWriter br = new BufferedWriter(fr);
             PrintWriter pr = new PrintWriter(br)) {
            // Handle files that don't already have a trailing newline
            fread.seek(fread.length() - 1);
            char lastChar = (char) fread.readUnsignedByte();
            if (lastChar != '\n') {
                pr.println();
            }
            pr.print(fileName);
            pr.print(",");
            pr.print(startDateString);
            pr.print(",");
            pr.print(endDateString);
            pr.print(",");
            pr.print(fileSize);
            pr.println();
            pr.close();
            logger.info("Manifest file successfully updated");
        } catch (IOException e) {
            e.printStackTrace();
            responseOut.println(getStackTrace(e));
            return "fail";
        }
        return null;
    }

    private Date databaseMaxDate() {
        String query = "SELECT MAX(`DATATIME`) as maxDate FROM tag_data60mins";
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(query)) {
                while (rs.next()) {
                    return rs.getTimestamp("maxDate");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                responseOut.println(getStackTrace(e));
                responseOut.println("error 3 trying to get max(datatime)");
                logger.error("Error 3 trying to get max(datatime): ", e);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseOut.println(getStackTrace(e));
            responseOut.println("error 1 trying to get max(datatime)");
            logger.error("Error 1 trying to get max(datatime): ", e);
            return null;
        }
        return null;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response, Connection _conn, PrintWriter _responseOut) {
        this.conn = _conn;
        this.responseOut = _responseOut;
        if (request.getParameter("asdownload") != null) {
            response.setContentType("application/x-download");
            response.setHeader("Content-disposition", "attachment; filename=tags.csv");
        }
        Date startDate = null;
        Date endDate;
        Date startDateFromParam;
        Date endDateFromParam;
        String startDateString = null;
        String endDateString = null;
        String startParam = request.getParameter("start");
        String endParam = request.getParameter("end");
        DateFormat mySQLhourlyFormatter = new SimpleDateFormat("yyyy-MM-dd HH");
        if (startParam != null && startParam.length() != 0) {
            try {
                startDateFromParam = mySQLhourlyFormatter.parse(startParam);
            } catch (ParseException invalidStartParam) {
                responseOut.println("ill formed parameter 'start' correct format is yyyy-MM-dd HH");
                logger.error("Ill formed parameter 'start' correct format is yyyy-MM-dd HH", invalidStartParam);
                return;
            }
            startDate = startDateFromParam;
            startDateString = mySQLhourlyFormatter.format(startDate);
        }
        if (endParam != null && endParam.length() != 0) {
            try {
                endDateFromParam = mySQLhourlyFormatter.parse(endParam);
            } catch (ParseException invalidEndParam) {
                responseOut.println("ill formed parameter 'end' correct format is yyyy-MM-dd HH");
                logger.error("Ill formed parameter 'end' correct format is yyyy-MM-dd HH", invalidEndParam);
                return;
            }
            endDate = endDateFromParam;
            endDateString = mySQLhourlyFormatter.format(endDate);
        }
        prefix = request.getParameter("prefix");
        String whatParam = request.getParameter("what");
        Date writeToCacheUpToDate = null;
        boolean writeToCache = false;
        CsvManifestReader.CsvManifestEntry[] csvManifest = null;
        String currentDir = request.getSession().getServletContext().getRealPath("");
        String slash = System.getProperty("file.separator");
        String callingAppDataDir = null;
        if (whatParam == null) {
            this.retrieveTagNamesFromRequest(request);
        } else {
            String propFileName = dts.sc.getInitParameter("propFile_" + whatParam);
            if (propFileName == null) {
                responseOut.println("properties file for " + whatParam + " not found in web.xml");
                logger.error("Properties file for {} not found in web.xml", whatParam);
                this.retrieveTagNamesFromRequest(request);
            } else {
                String appFolder = dts.sc.getInitParameter("appFolder_" + whatParam);
                if (appFolder == null) {
                    responseOut.println("app folder for " + whatParam + " not found");
                    logger.error("App folder for {} not found", whatParam);
                } else {
                    //TODO: check dir path
                    callingAppDataDir = currentDir.replace("/dts10/", slash) + appFolder + slash + "data" + slash;
                }
                this.retrieveTagNamesFromProperties("/WEB-INF/" + propFileName);
                Date dbMaxDate = databaseMaxDate(); // System.out.println
                dts.cacheLock.readLock().lock();
                try {
                    boolean csvManifestReadSucces = false;
                    if (callingAppDataDir != null) {
                        try {
                            logger.info("Read manifest file from {}", callingAppDataDir);
                            csvManifest = getCsvManifest(callingAppDataDir);
                            csvManifestReadSucces = true;
                        } catch (Exception e) {
                            responseOut.println(getStackTrace(e));
                            logger.error("Couldn't read the manifest file: ", e);
                        }
                    }
                    //responseOut.println("db max date: " + mySQLhourlyFormatter.format(dbMaxDate));
                    if (csvManifestReadSucces && (startDate != null) && (dbMaxDate != null)) {
                        int lastRowIdx = csvManifest.length - 1;
                        CsvManifestReader.CsvManifestEntry lastEntry = csvManifest[lastRowIdx];
                        //responseOut.println("last entry name  " + lastEntry.fileName);
                        //responseOut.println("last entry start " + mySQLhourlyFormatter.format(lastEntry.startDate));
                        //responseOut.println("last entry end   " + mySQLhourlyFormatter.format(lastEntry.endDate));
                        //responseOut.println("last entry sisze " + lastEntry.fileSize);
                        Date lastCsvEndDate = lastEntry.endDate;
                        long diff = startDate.getTime() - lastCsvEndDate.getTime();
                        if (diff == 60 * 60 * 1000) {
                            writeToCacheUpToDate = new Date(dbMaxDate.getTime() - 1000 * 60 * 60 * 24 * dts.cacheCommitDelayDays);
                            //responseOut.println("write to cache up to: " + mySQLhourlyFormatter.format(writeToCacheUpToDate));
                            //responseOut.println("cache start date: " + mySQLhourlyFormatter.format(startDate));
                            long minCacheSizeMillisecs = 1000 * 60 * 60 * 24 * dts.cacheChunkSizeDays;
                            long cacheFileTimeSize = writeToCacheUpToDate.getTime() - startDate.getTime();
                            //responseOut.println("cache size min (ms): " + minCacheSizeMillisecs);
                            //responseOut.println("actual cache size (ms) " + cacheFileTimeSize);
                            if (cacheFileTimeSize >= minCacheSizeMillisecs) {
                                writeToCache = true;
                            }
                        }
                    }
                } finally {
                    dts.cacheLock.readLock().unlock();
                }
            }
        }
        if (prefix == null) {
            prefix = "";
        }
        String query = makeQuery(startDateString, endDateString);
        logger.info("Select data from '{}' to '{}'", startDateString, endDateString);

        try {
            // responseOut.println("prefix: " + prefix);
            // responseOut.println("customHeader " + customHeader);
            // responseOut.println(query); responseOut.println();
            // for (int i=0; i<allDtsTagNames.length; i++){
            // responseOut.println(allDtsTagNames[i]); }
            if (writeToCache) {
                // Acquire write lock
                dts.cacheLock.writeLock().lock();
                out = new ServletAndCacheWriter(responseOut, callingAppDataDir, csvManifest.length);
            } else {
                out = new ServletOnlyWriter(responseOut);
            }
            if (useCustomHeader) {
                out.print(customHeader);
            } else {
                out.print("datatime");
                for (String tagName : tagNames) {
                    out.print(",");
                    out.print(removePrefix(tagName));
                }
            }
            long cutOffTime = Long.MAX_VALUE;
            if (writeToCache) {
                cutOffTime = writeToCacheUpToDate.getTime();
            }
            try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
                while (rs.next()) {
                    Date d = rs.getTimestamp("DATATIME");
                    StringBuilder sb = new StringBuilder();
                    sb.append("\n").append(mySQLhourlyFormatter.format(d));
                    for (String tagName : tagNames) {
                        double tagValue = rs.getDouble(removePrefix(tagName));
                        if (rs.wasNull()) {
                            sb.append(",NULL");
                        } else {
                            sb.append(",").append(tagValue);
                        }
                    }
                    if (writeToCache && (d.getTime() <= cutOffTime)) {
                        out.print(sb.toString());
                        logger.info("Write to cache: {}", sb.toString().replace("\n", ""));
                    } else {
                        responseOut.print(sb.toString());
                        logger.info("Response: {}", sb.toString().replace("\n", ""));
                    }

                }
            } catch (SQLException e) {
                responseOut.println(getStackTrace(e));
                logger.error("SQL error occurred: ", e);
            }
            if (writeToCache) {
                long fileSize = -1;
                ServletAndCacheWriter goethe = null;
                try {
                    goethe = (ServletAndCacheWriter) out;
                    logger.info("Write data to cache file {}", goethe.cacheFileName);
                    goethe.closeCache();
                    fileSize = getSizeOfNewlyWrittenCsvFile(goethe.cacheFileName);
                } catch (Exception e) {
                    e.printStackTrace();
                    responseOut.println(getStackTrace(e));
                    logger.error("Couldn't write to cache file: ", e);
                }
                if (fileSize != -1) {
                    if (updateCsvManifest(callingAppDataDir, goethe.cacheFileNameWithoutPath, startDateString, mySQLhourlyFormatter.format(writeToCacheUpToDate), fileSize) != null) {
                        responseOut.println("could not update csv manifest");
                        logger.error("Could not update csv manifest");
                    }
                }
                responseOut.close();
            } else {
                out.close();
            }
        } finally {
            if (writeToCache) {
                dts.cacheLock.writeLock().unlock();
            }
        }
    }

}
