package main.java.dts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.util.TreeMap;

public class CSV {

    public Map<String, Integer> headerMap;
    public String[][] content;
    public int numCols;
    public int numRows;

    private CSV(int _numCols, int _numRows) {
        this.headerMap = new TreeMap<>();
        this.content = new String[_numRows][_numCols];
        this.numCols = _numCols;
        this.numRows = _numRows;
    }

    public static CSV preparse(String fileContents, String columnSeparator) throws Exception {
        String[] lines = fileContents.split("\n");
        int numRows = lines.length - 1;
        if (lines[numRows].length() == 0) {
            numRows--;
        }
        String headerLine = lines[0];
        String[] headerEntries = headerLine.split(columnSeparator);
        int numCols = headerEntries.length;
        CSV result = new CSV(numCols, numRows);
        for (int i = 0; i < numCols; i++) {
            result.headerMap.put(headerEntries[i], i);
        }
        for (int row = 0; row < numRows; row++) {
            String line = lines[row + 1];
            String[] entries = line.split(columnSeparator);
            if (entries.length != numCols) {
                throw new Exception("CSV parse error " + headerLine + "\n" + line);
            }
            System.arraycopy(entries, 0, result.content[row], 0, numCols);
        }
        return result;
    }

    public static CSV preparseFromFile(String filePath, String columnSeparator) throws Exception {
        BufferedReader br;
        FileReader fr;
        String line;
        String lineSep = "";
        StringBuilder sb = new StringBuilder();
        fr = new FileReader(filePath);
        br = new BufferedReader(fr);
        while ((line = br.readLine()) != null) {
            sb.append(lineSep);
            sb.append(line);
            lineSep = "\n";
        }
        br.close();
        return CSV.preparse(sb.toString(), columnSeparator);
    }

}
