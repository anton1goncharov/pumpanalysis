package main.java.dts;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class CsvManifestReader {

    private static DateFormat mySQLhourlyFormatter = new SimpleDateFormat("yyyy-MM-dd HH");

    static class CsvManifestEntry {
        public String fileName;
        public Date startDate;
        public Date endDate;
        public long fileSize;

        public CsvManifestEntry(String _fileName, String startDateString, String endDateString, String fileSizeString) throws ParseException {
            fileName = _fileName;
            startDate = mySQLhourlyFormatter.parse(startDateString);
            endDate = mySQLhourlyFormatter.parse(endDateString);
            fileSize = Long.parseLong(fileSizeString);
        }
    }

    public static CsvManifestEntry[] getFromFile(String filePath) throws Exception {
        CSV csv = CSV.preparseFromFile(filePath, ",");
        Map<String, Integer> hm = csv.headerMap;
        Integer FileNamePosition = hm.get("fileName");
        if (FileNamePosition == null) throw new Exception("file name column not found in csv manifest csv");
        int fileNamePosition = FileNamePosition;
        Integer StartDatePosition = hm.get("startDate");
        if (StartDatePosition == null) throw new Exception("start date column not found in csv manifest csv");
        int startDatePosition = StartDatePosition;
        Integer EndDatePosition = hm.get("endDate");
        if (EndDatePosition == null) throw new Exception("end date column not found in csv manifest csv");
        int endDatePosition = EndDatePosition;
        Integer FileSizePosition = hm.get("fileSize");
        if (FileSizePosition == null) throw new Exception("file size column not found in csv manifest csv");
        int fileSizePosition = FileSizePosition;
        CsvManifestEntry[] result = new CsvManifestEntry[csv.numRows];
        for (int row = 0; row < csv.numRows; row++) {
            String fileName = csv.content[row][fileNamePosition];
            String startDateString = csv.content[row][startDatePosition];
            String endDateString = csv.content[row][endDatePosition];
            String fileSizeString = csv.content[row][fileSizePosition];
            // Skip bad data
            if (fileName == null || fileName.isEmpty()) continue;
            result[row] = new CsvManifestEntry(fileName, startDateString, endDateString, fileSizeString);
        }
        return result;
    }

}
