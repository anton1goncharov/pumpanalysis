// http://localhost:8080/dts/?start=2014-11-23%2000&end=2014-11-28%2016&t0=xxxfoo&t1=xxxbar&t2=xxxbaz&prefix=xxx
// http://valotas.com/get-output-of-jsp-or-servlet-response/
package main.java.dts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Dts extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(Dts.class);

    public static String getStackTrace(Throwable aThrowable) {
        // Also dump to console so it gets logged
        aThrowable.printStackTrace();
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    String[] allDtsTagNames = null;
    Set<String> dtsTagNameSet = null;

    String userName;
    String password;
    String databaseUrlPrefix;

    String externalServer_userName = "developer";
    String externalServer_password = "&U+g<QNS";
    String externalServer_databaseUrlPrefix = "jdbc:mysql://myenergyonline.net:3306/";

    String database; // "dts"
    String extraParams = "";
    long cacheCommitDelayDays;
    long cacheChunkSizeDays;
    ReadWriteLock cacheLock = new ReentrantReadWriteLock(true);

    ServletConfig sc;
    ServletContext sctx = null;

    private String getDatabaseURL() {
        return databaseUrlPrefix + database + extraParams;
    }

    private String getDatabaseURL_external() {
        return externalServer_databaseUrlPrefix + database + extraParams;
    }

    private synchronized void getAllDtsTagNames(Connection conn, PrintWriter responseOut) {
        if (allDtsTagNames != null) return;
        ArrayList<String> temp = new ArrayList<>();
        String query = "SELECT `TAGNAME` FROM tagnames";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String tagName = rs.getString("TAGNAME");
                temp.add(tagName);
            }
        } catch (SQLException e) {
            responseOut.println(getStackTrace(e));
            logger.error("Couldn't get all DTS tag names: ", e);
        }
        allDtsTagNames = new String[temp.size()];
        dtsTagNameSet = new TreeSet<>();
        for (int i = 0; i < allDtsTagNames.length; i++) {
            String tagName = temp.get(i);
            allDtsTagNames[i] = tagName;
            dtsTagNameSet.add(tagName);
        }
    }

    public void init(ServletConfig servletConfig) throws ServletException {
        userName = servletConfig.getInitParameter("mysqlUserName");
        password = servletConfig.getInitParameter("mysqlPassword");
        databaseUrlPrefix = servletConfig.getInitParameter("databaseUrlPrefix");
        database = servletConfig.getInitParameter("database");
        cacheCommitDelayDays = Long.parseLong(servletConfig.getInitParameter("cache_commit_delay_days"));
        cacheChunkSizeDays = Long.parseLong(servletConfig.getInitParameter("cache_chunk_size_days"));
        sctx = servletConfig.getServletContext();
        sc = servletConfig;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        PrintWriter responseOut = response.getWriter();
        response.setContentType("text/plain");
        long expires = new Date().getTime() + 1000;
        response.addDateHeader("Expires", expires);
        DtsHandler handler = new DtsHandler(this);
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            responseOut.println(getStackTrace(e));
            logger.error("Couldn't find MySQL driver: ", e);
        }
        String dbUser;
        String dbPass;
        String url;
        if (request.getParameter("externalServer") != null) {
            url = this.getDatabaseURL_external();
            dbUser = externalServer_userName;
            dbPass = externalServer_password;
        } else {
            url = this.getDatabaseURL();
            dbUser = userName;
            dbPass = password;
        }
        try (Connection conn = DriverManager.getConnection(url, dbUser, dbPass)) {
//            logger.info("Querying database for tag names");
            getAllDtsTagNames(conn, responseOut);

            // Pass off to the thread-safe handler
            handler.doGet(request, response, conn, responseOut);
        } catch (SQLException e) {
            responseOut.println("could not connect to database: " + url);
            responseOut.println(getStackTrace(e));
            logger.error("Couldn't connect to database: " + url, e);
        }
    }

}
