// recommendations.js
/*****************************************************************************************************************
************************************************ RECOMMENDATIONS *************************************************
******************************************************************************************************************/

RECOMMENDATIONS = {
    hasResizeableChart : true,
    showBucketTable : true,
    dotRadius : 2.5,
    pointsOnLine : 111,
    margin : {top: 48, right: 14, bottom: 52, left: 62},
    height0 : 500,
    aspect : 1.7,
    xMouseAxisRectHeight : 18,
    epsilonForExtendingTheLineAlittleBitLeftAndRight : 0.03,
    xAxisLabelVerticalPositionAdjust : 12,
    yAxisLabelHorizontalAdjust : 20,
    xMouseAxisTextVerticalAdjust : 4,
    legendWidth : 195,
    flowBucketStart: 58,
    flowBucketSize: null,
    initialFlowBucketSize: 8,
    flowBucketEnd: 157,
    combosInFlowRange: function (minFlow, maxFlow){
        function isWithinRange(index){
            var median = API.medianFlowFromCode[index];
            return minFlow <= median && median < maxFlow;
        }
        var oneoneone = Math.pow(2, API.numPumps) - 1;
        var comboCodeList = UTIL.range(1, oneoneone).select(isWithinRange);
        var effList = API.meanEffFromCode.pick(comboCodeList);
        return comboCodeList.pick(effList.orderingDESC());
    },
    makePumpRecommendationRowFunc: function(flowStart, bucketSize){
        var step = 0.5 * bucketSize;
        var THIS = this;
        return function(row){
            var minFlow = flowStart + row * step;
            var maxFlow = minFlow + bucketSize;
            var result = [];
            result.push(HtmlGen.img("", {id: "pumpRecoBtn" + row, src: "img/no16.png"}));
            result.push("" + minFlow + "-" + maxFlow);
            var comboCodes = THIS.combosInFlowRange(minFlow, maxFlow);
            var combos = comboCodes.map(API.pumpsListFromCode);
            if ( combos.length == 0 ){
                result.push("None");
                result.push("None");
                result.push("n/a")
            } else {
                result.push(combos[0].toStringCurly());
                if ( combos.length == 1 ){
                    result.push("None");
                } else {
                    result.push(combos.slice(1).map(function(item){ return item.toStringCurly(); }).join(","));
                }
                var bestComboCode = comboCodes[0];
                var effOfBest = API.meanEffFromCode[bestComboCode];
                result.push(effFormat.sprintf(effOfBest));
            }
            return result;
        }
    },
    makeXtrafo : function(){
        return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
    },
    makeYtrafo : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMin, this.yMax]);
    },
    chooseFlowRangeHandler: function(row, minFlow, maxFlow){
        var THIS = this;
        var tmp = row + 1;
        var selector = "#recoList tr:eq(" +tmp+ ")";
        var selAll = "#recoList tr";
        var comboCodes = THIS.combosInFlowRange(minFlow, maxFlow);
        var bestFlows = comboCodes.map(function(comboCode){ return API.formulas.bepFlow[comboCode];});
        var left1 = 2*minFlow - maxFlow;
        var right1 = 2*maxFlow - minFlow;
        var left2 = bestFlows.min();
        var right2 = bestFlows.max();
        var left3 = Math.min(left1, left2);
        var right3 = Math.max(right1, right2);
        var left, right;
        if (left3 == left1){
            left = 1.1 * left3 - 0.1 * right3;
        } else {
            left = 1.3 * left3 - 0.3 * right3;
        }
        if (right3 == right1){
            right = 1.1 * right3 - 0.1 * left3;
        } else {
            right = 1.3 * right3 - 0.3 * left3;
        }
        return function(){
            $(selAll).removeClass("selectedRow");
            $(selector).addClass("selectedRow");
            $(selAll).find("img").attr("src", "img/no16.png");
            $(selector).find("img").attr("src", "img/yes16.png");
            var funs = [];
            var pointsArr = [];
            var funMin = Infinity;
            var funMax = -Infinity;
            var pointMin = Infinity;
            var pointMax = -Infinity;
            for ( var i=0; i<comboCodes.length; i++ ){
                var code = comboCodes[i];
                var pumpsList = API.pumpsListFromCode(code);
                var hydroCoeff = API.formulas.flow2TotalHydro[code];
                var shaftCoeff = API.formulas.flow2TotalShaft[code];
                var hydroFun = makeQuadraticFunction(hydroCoeff);
                var shaftFun = makeLinearFunction(shaftCoeff);
                var effFun = times100(divideFunctions(hydroFun, shaftFun));
                funs.push(effFun);
                var title = pumpsList.toString();
                title += " (" + (i==0 ? "best" : i==1 ? "2nd" : i==2 ? "3rd" : (i+1) + "th") + ")";
                funMin = Math.min(funMin, effFun(left), effFun(right));
                funMax = Math.max(funMax, 100*API.formulas.bepEff[code]);
                // construct points
                var points = API.finalTable.pick(API.formulas.rowsWherePumpsRunOnly[code])
                    .map(function(row){ return [row.flow, 100*row.eff, row];});
                var effs  = points.map(function(p){return p[1];});
                pointMin = Math.min(effs.min(), pointMin);
                pointMax = Math.max(effs.max(), pointMax);
                pointsArr.push(points); 
            }
            var yMin = THIS.yMin = Math.min(pointMin, funMin);
            var yMax = THIS.yMax = Math.max(pointMax, funMax);
            var xMin = THIS.xMin = left;
            var xMax = THIS.xMax = right;
            
            if (!("last_yMin" in THIS) || !THIS.yAxisManual){
            	THIS.last_yMin = yMin;
            }
            if (!("last_yMax" in THIS) || !THIS.yAxisManual){
            	THIS.last_yMax = yMax;
            }
            if (THIS.yAxisManual){
            	THIS.yMin = THIS.last_yMin;
                THIS.yMax = THIS.last_yMax;
            }
            
            THIS.funs = funs;
            THIS.pointsArr = pointsArr;
            THIS.comboCodes = comboCodes;
            var myName = THIS.myName;
            var height0 = THIS.height0;
            var height = THIS.height = height0 * THIS.sizeFactor_Y;
            var width = THIS.width = height0 * THIS.aspect * THIS.sizeFactor_X;
            var container = THIS.container;
            container.html("");
            var svg = THIS.svg = container.append("svg").classed(myName, true).attr("width", width).attr("height", height);
            var margTop   = THIS.margin.top;
            var margBot   = THIS.margin.bottom;
            var margLeft  = THIS.margin.left;
            var margRight = THIS.margin.right;
            var innerWidth = THIS.innerWidth = width - margLeft - margRight;
            var innerHeight = THIS.innerHeight = height - margTop - margBot;
            
            var mainGroup = THIS.mainGroup = svg.append("g").attr("class", "mainGroup").translate(margLeft, margTop);
            THIS.invisiblePlaceholderRect = mainGroup.append("rect").attr("width", width).attr("height", height).attr("stroke", "none").attr("fill", "transparent");
            
            var clipPath = THIS.clipPath = svg.append("clipPath").attr("id", "recommendationsClipPath");
            var clipRect = THIS.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
            var pointGroups = THIS.pointGroups = mainGroup.selectAll(".honestPolitician").data(pointsArr).enter().append("g")
                .attr("class", function(d,i,j) { return "pointGroup rank_" + i; } )
                .style("clip-path", "url(#recommendationsClipPath)");
            var axesGroup = THIS.axesGroup = mainGroup.append("g").classed("axesGroup", true);
            var mouseGroup = THIS.mouseGroup = mainGroup.append("g").classed("mouseGroup", true);
            var x = THIS.x = THIS.makeXtrafo();
            var y = THIS.y = THIS.makeYtrafo();
            var xAxis = THIS.xAxis = d3.svg.axis().scale(x).orient("bottom");
            var yAxis = THIS.yAxis = d3.svg.axis().scale(y).orient("left");
            var xAxisGroup = THIS.xAxisGroup = axesGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
            var xAxisLabel = THIS.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
                .text("flow (MGD)").attr("x", innerWidth/2).attr("y", margBot - THIS.xAxisLabelVerticalPositionAdjust);
            var yAxisGroup = THIS.yAxisGroup = axesGroup.append("g").classed("axis yAxis", true).call(yAxis);
            yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
            var yAxisLabel = THIS.yAxisLabel = yAxisGroup.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
                .text("Efficiency (%)").attr("x", -innerHeight/2).attr("y", -margLeft + THIS.yAxisLabelHorizontalAdjust).style("text-anchor", "middle");
            var mouseOverDivId = THIS.mouseOverDivId;
            pointGroups.selectAll(".honestPolitician").data(function(d){return d;}).enter()
                .append("circle").classed("recoCirc", true)
                .attr("cx", function(d,i,j) {
                    return x(d[0]);
                })
                .attr("cy", function(d) { return y(d[1]); })
                .attr("r", THIS.dotRadius)
                .on("mouseover", function(d, i, j){
                    THIS.hiliteLegendRowForRank(j);
                    var row = d[2];
                    var containerId = mouseOverDivId;
                    var chosenColumnIndex = -1;
                    var tableId = myName + "HintTable";
                    var dragHandleId = myName + "HintHandle";
                    var minimizerId = myName + "Minimizer";
                    var captionId = myName + "MouseOverCaption"
                    makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
                })
                .on("mouseout", function(d, i, j){
                    THIS.hiliteLegendRowForRank(-1);
                });
            var chartCaption = THIS.chartCaption = svg.append("text")
                .text("Operating Efficiency with Flow Range " + minFlow + "-" + maxFlow)
                .attr("x", width/2).attr("y", 22).style("text-anchor", "middle")
                .classed("chartCaption", true)
            // draw the efficiency functions
            var functionsGroup = THIS.functionsGroup = mainGroup.append("g").classed("functionsGroup", true)
                .style("clip-path", "url(#recommendationsClipPath)");
            var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
            var epsilon = THIS.epsilonForExtendingTheLineAlittleBitLeftAndRight;
            var pointsOnLine = THIS.pointsOnLine;
            var xLo = (1+epsilon) * xMin - epsilon * xMax;
            var xHi = (1+epsilon) * xMax - epsilon * xMin;
            var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
            var xxx = d3.range(xLo, xHi, stepSize);
            var pointsOnCurves = d3.range(THIS.funs.length).map(function(rank){
                var plotFun = THIS.funs[rank];
                return xxx.map(function(x){
                    return [x, plotFun(x)];
                });
            });
            var plotFunctionPaths = THIS.plotFunctionPaths = functionsGroup
                .selectAll(".honestPolitician").data(pointsOnCurves).enter().append("path").attr({
                    "fill" : "none", "d" : functionLine
                }).attr("class", function(d,i,j){ return "functionLine rank_" + i; })
                .on("mouseover", function(d,i,j){
                    THIS.hiliteLegendRowForRank(i);
                })
                .on("mouseout", function(d,i,j){
                    THIS.hiliteLegendRowForRank(-1);
                });
            
            // mouse lines
            var hMouseLine = THIS.hMouseLine = mouseGroup.append("line")
                .attr("x1", 0).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
                .attr("id", myName + "_H_MouseLine").classed("mouseLine horizontal inactive", true);
            var vMouseLine = THIS.vMouseLine = mouseGroup.append("line")
                .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
                .attr("id", myName + "_V_MouseLine").classed("mouseLine vertical inactive", true);
            var yMouseAxisRectWidth = margLeft - 5;
            var yMouseAxisRectHeight = THIS.yMouseAxisRectHeight = 18;
            var yMouseAxisRect = THIS.yMouseAxisRect = svg.append("rect").attr("x", margLeft-yMouseAxisRectWidth).attr("width", yMouseAxisRectWidth)
                .attr("height", yMouseAxisRectHeight).classed("yMouseAxisRect mouseAxisRect inactive", true);
            var yMouseAxisText = THIS.yMouseAxisText = svg.append("text").attr("x", margLeft - 6).attr("y", 0)
                .classed("yMouseAxisText mouseAxisText inactive", true);
            var xMouseAxisRectWidth = THIS.xMouseAxisRectWidth = 60;
            var xMouseAxisRectHeight = THIS.xMouseAxisRectHeight;
            var xMouseAxisRect = THIS.xMouseAxisRect = svg.append("rect").attr("y", height-margBot).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
                .classed("xMouseAxisRect mouseAxisRect inactive", true);
            var xMouseAxisText = THIS.xMouseAxisText = svg.append("text").attr("y", height-margBot+xMouseAxisRectHeight-THIS.xMouseAxisTextVerticalAdjust)
                .classed("xMouseAxisText mouseAxisText inactive", true);
            THIS.attachMouseHandler();
            svg.call(makePanDragBehavior(THIS.myName, {groupName : "mouseGroup"}));
            THIS.createLegendTable();
            THIS.legendDiv.draggable({ handle : "tr.recommendationsLegendTableHeaderRow" });
            mouseZoomInit.call(THIS);
        };
    },
    createLegendTable : function(){
        // create legend table
        var THIS = this;
        var legendContainer = d3.select(this.legendDiv[0]);
        legendContainer.html("");
        var legendTable = legendContainer.append("table").classed("recommendationsLegendTable", true);
        var comboCodes = this.comboCodes;
        var headerTr = legendTable.append("tr").classed("recommendationsLegendTableHeaderRow dragHandle", true);
        var contentTr = this.legendContentTr = legendTable.selectAll(".honestPolitician").data(comboCodes).enter().append("tr")
            .attr("class", function(d,i,j) { return "recommendationsLegendTableContentRow rank_" + i; });
        headerTr.append("th").html("").classed("colorColumn", true);
        contentTr.append("td")
            .attr("class", function(d,i,j) { return "colorColumn rank_" + i; })
            .attr("title", "click to bring data points to foreground")
            .on("click", function(d,i,j){
                var lastPointGroup = jQuery(THIS.svg[0][0]).find("g.pointGroup").last();
                var clickedPointGroup = jQuery(THIS.svg.selectAll("g.pointGroup.rank_" + THIS.comboCodes.indexOf(this.__data__)).node());
                clickedPointGroup.insertAfter(lastPointGroup);
            });
        headerTr.append("th").html("&bull;").classed("showDataPointsColumn", true);
        contentTr.append("td").classed("showDataPointsColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
            .classed("recommendationsShowDataPointsCheckbox", true)
            .attr("id", function(d,i,j) { return "recommendationsShowDataPointsOfRank_" + i; })
            .on("change", function(d,i,j){
                var rank = THIS.comboCodes.indexOf(this.__data__);
                var selector = "g.pointGroup.rank_" + rank;
                var pointGroup = THIS.svg.selectAll(selector);
                pointGroup.classed("invisible", !this.checked);
            });
        headerTr.append("th").html(HtmlSymbols.frown).classed("showFunctionColumn", true);
        contentTr.append("td").classed("showFunctionColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
            .classed("recommendationsShowFunctionCheckbox", true)
            .attr("id", function(d,i,j) { return "recommendationsShowFunctionOfRank_" + i; })
            .on("change", function(d,i,j){
                var rank = THIS.comboCodes.indexOf(this.__data__);
                var selector = "path.rank_" + rank;
                var functionPath = THIS.functionsGroup.selectAll(selector);
                functionPath.classed("invisible", !this.checked);
            });
        headerTr.append("th").text("rank").classed("rankColumn", true);
        contentTr.append("td").classed("rankColumn", true).text(function(d,i,j){
            return i + 1;
        });
        headerTr.append("th").text("pumps").classed("pumpsColumn", true);
        contentTr.append("td").classed("pumpsColumn", true).text(function(d,i,j){
            return API.pumpsListFromCode(d).join(", ");
        });
        headerTr.append("th").text("op hrs").classed("opHrsColumn", true);
        contentTr.append("td").classed("opHrsColumn", true).text(function(d,i,j){
            return API.numOpHours[d];
        });
        headerTr.append("th").text("eff").classed("effColumn", true);
        contentTr.append("td").classed("effColumn", true).text(function(d,i,j){
            return "%4.1f%".sprintf(API.meanEffFromCode[d]);
        });
    },
    hiliteLegendRowForRank : function(rank){
        // first, un-hilite all rows of the legend table
        this.legendContentTr.classed("active", false);
        // now, hilite the one with the given rank
        var rowWithThatRank = this.legendContentTr.filter(function(r){ return d3.select(this).classed("rank_" + rank); }).node();
        d3.select(rowWithThatRank).classed("active", true);
    },
    updateChart: function(xMin, xMax, yMin, yMax){
        //DEBUG[0].html("updateChart(%5.1f,%5.1f,%5.1f,%5.1f)".sprintf(xMin, xMax, yMin, yMax));
        var THIS = this;
        var myName = this.myName;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        
        this.last_yMin = yMin;
        this.last_yMax = yMax;
        
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        var svg = this.svg;
        svg.attr("width", width).attr("height", height);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var clipRect = this.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        
        this.mainGroup.translate(margLeft, margTop);
        this.invisiblePlaceholderRect.attr("width", width).attr("height", height);

        var pointGroups = this.pointGroups;
        var axesGroup = this.axesGroup;
        var mouseGroup = this.mouseGroup;
        var x = this.x = THIS.makeXtrafo();
        var y = this.y = THIS.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup;
        xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel;
        xAxisLabel.attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup;
        yAxisGroup.call(yAxis);
        yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        var yAxisLabel = this.yAxisLabel;
        yAxisLabel.attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust);
        pointGroups.selectAll("circle.recoCirc")
            .attr("cx", function(d,i,j) {
                return x(d[0]);
            })
            .attr("cy", function(d) {
                return y(d[1]);
            });
        var chartCaption = this.chartCaption;
        chartCaption.attr("x", width/2);
        // draw the efficiency functions
        var functionsGroup = this.functionsGroup;
        var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var pointsOnCurves = d3.range(this.funs.length).map(function(rank){
            var plotFun = THIS.funs[rank];
            return xxx.map(function(x){
                return [x, plotFun(x)];
            });
        });
        var plotFunctionPaths = this.plotFunctionPaths;
        plotFunctionPaths.data(pointsOnCurves).attr({
            "d" : functionLine
        });
        // mouse lines
        var hMouseLine = this.hMouseLine.attr("x2", innerWidth);
        var vMouseLine = this.vMouseLine.attr("y1", innerHeight);
        var yMouseAxisRectWidth = margLeft - 5;
        var yMouseAxisRect = this.yMouseAxisRect.attr("x", margLeft-yMouseAxisRectWidth);
        var yMouseAxisText = this.yMouseAxisText.attr("x", margLeft - 6);
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect = this.xMouseAxisRect.attr("y", height-margBot);
        var xMouseAxisText = this.xMouseAxisText.attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust);
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = RECOMMENDATIONS;
            var margTop   = OBJ.margin.top;
            var margBot   = OBJ.margin.bottom;
            var margLeft  = OBJ.margin.left;
            var margRight = OBJ.margin.right;
            var m = d3.mouse(OBJ.mouseGroup.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var xRaw_1 = xRaw + margLeft;
            var yRaw_1 = yRaw + margTop;
            var hMouseLine = OBJ.hMouseLine;
            var vMouseLine = OBJ.vMouseLine;
            var yMouseAxisRect = OBJ.yMouseAxisRect;
            var yMouseAxisText = OBJ.yMouseAxisText;
            var xMouseAxisRect = OBJ.xMouseAxisRect;
            var xMouseAxisText = OBJ.xMouseAxisText;
            var innerWidth = OBJ.innerWidth;
            var xInRange = xRaw * (innerWidth - xRaw) > 0;
            var innerHeight = OBJ.innerHeight;
            var yInRange = yRaw * (innerHeight - yRaw) > 0;
            var mouseIsOverChart = xInRange && yInRange;
            hMouseLine.classed("inactive", !mouseIsOverChart);
            vMouseLine.classed("inactive", !mouseIsOverChart);
            yMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yMouseAxisText.classed("inactive", !mouseIsOverChart);
            xMouseAxisRect.classed("inactive", !mouseIsOverChart);
            xMouseAxisText.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hMouseLine.attr("y1", yRaw).attr("y2", yRaw).attr("x2", xRaw);
                vMouseLine.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                yMouseAxisRect.attr("y", yRaw_1 - OBJ.yMouseAxisRectHeight/2);
                yMouseAxisText.attr("y", yRaw_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(OBJ.y.invert(yRaw)));
                xMouseAxisRect.attr("x", xRaw_1 - OBJ.xMouseAxisRectWidth/2);
                xMouseAxisText.attr("x", xRaw_1).text("%5.1f".sprintf(OBJ.x.invert(xRaw)));
            }
        });
    },
    make: function(fbSize){
        var THIS = this;
        this.showBucketTable = true;
        var minBucketSize = 4;
        var maxBucketSize = 12;
        if (arguments.length >= 1){
            this.flowBucketSize = fbSize;
            var firstRun = false;
        } else {
            this.flowBucketSize = this.initialFlowBucketSize;
            var firstRun = true;
        }
        var step = 0.5 * this.flowBucketSize;
        var headings = ["show", "flow<br/>(MGD)", "best", "others", "best<br/>eff(%)"];
        var numBuckets = Math.floor((this.flowBucketEnd - this.flowBucketStart)/step) - 1;
        var recoListHtml = HtmlGen.tableFromRowFunc(numBuckets,
            this.makePumpRecommendationRowFunc(this.flowBucketStart, this.flowBucketSize),
            headings, {id: "recoList", cellSpacing: 0, cellPadding: 1}
        );
        var buttonsHtml = "flow bucket size &nbsp;";
        for (var s=minBucketSize; s<=maxBucketSize; s++){
            var atts = s == fbSize
                ? {id: "recoSetFlowBucketSize" + s, "class": "big chosen"}
                : {id: "recoSetFlowBucketSize" + s, "class": "big"};
            buttonsHtml += HtmlGen.eisBtn(s, atts);
        }
        var buttonsZoomStretchHtml = HtmlGen.table(HtmlGen.tr(
            HtmlGen.td(buttonsHtml, {id: "recommendationsFlowBucketButtonsParentTd"}) +
            HtmlGen.td(HtmlGen.checkbox(this.showBucketTable, {id: "showRecommendationsBucketTableCheckbox"}), {id : "showRecommendationsBucketTableCheckboxParentTd"}) +
            HtmlGen.td("show bucket table", {id: "showRecommendationsBucketTableCheckboxLabel"}) +
            HtmlGen.td("", {"class": "yRangeAutoStuff", id: "recommendationsYRangeAutoStuffParentTd"}) 
        ));
        $("#recommendationsKontent").html(
            HtmlGen.table(
                HtmlGen.tr(HtmlGen.td(buttonsZoomStretchHtml, {colspan: 2})) +
                HtmlGen.tr(
                    HtmlGen.td(recoListHtml, {id: "recommendationsBucketTableParentTd"}) +
                    HtmlGen.td(
                        HtmlGen.table(
                            HtmlGen.tr(HtmlGen.td("", {id: "recommendationsSVGcontainer"})) +
                            HtmlGen.tr(HtmlGen.td(
                                HtmlGen.table(HtmlGen.tr(
                                    HtmlGen.td(HtmlGen.div("", {id: "recommendationsLegendDiv"}), {valign: "top", id: "recommendationsLegendDivParentTd"}) +
                                    HtmlGen.td(HtmlGen.div("", {id: "recommendationsMouseOverDiv"}), {valign: "top", id: "recommendationsMouseOverDivParentTd"})
                                ))
                            ))
                        )
                    )
                )
            )
        );
        this.showBucketTableCB = jQuery("#showRecommendationsBucketTableCheckbox");
        this.bucketTableParentTd = jQuery("#recommendationsBucketTableParentTd");
        this.container = d3.select("#recommendationsSVGcontainer");
        this.legendDiv = jQuery("#recommendationsLegendDiv");
        this.mouseOverDiv = jQuery("#recommendationsMouseOverDiv");
        this.mouseOverDivId = "recommendationsMouseOverDiv";
        $("table#recoList img").each(function(){
            var par = $(this).parent(); par.addClass("firstColumn");
            var ch = par.parent().children();
            $(ch[ch.length-1]).addClass("fifthColumn");
        });
        $("table#recoList tr:eq(0) th:eq(0)").addClass("firstColumn");
        $("table#recoList tr:eq(0) th:eq(4)").addClass("fifthColumn");
        $("table#recoList tr:odd").addClass("oddRow");
        $("table#recoList tr:even:not(:eq(0))").addClass("evenRow");
        $("table#recoList tr:eq(0)").addClass("headerRow");
        
        
        makeYAxisRangeStuff(jQuery("#recommendationsYRangeAutoStuffParentTd"), this.myName, this, {}, true, "yAxisManual");
        
        for (var row=0; row<numBuckets; row++){
            var btn = $("#pumpRecoBtn" + row);
            var minFlow = this.flowBucketStart + row*step;
            var maxFlow = minFlow + this.flowBucketSize;
            btn.click(this.chooseFlowRangeHandler(row, minFlow, maxFlow));
        }
        for (var s=minBucketSize; s<=maxBucketSize; s++){
            var id = "recoSetFlowBucketSize" + s;
            var btn = $("#" + id);
            btn.click((function(size){
                return function(){
                    RECOMMENDATIONS.make(size);
                }
            })(s));
        }
        $("#pumpRecoBtn0").trigger("click");
        if ( firstRun ){
            $("#recoSetFlowBucketSize" + this.initialFlowBucketSize).addClass("chosen");
        }
        this.showBucketTableCB.on("change", function(){
            THIS.showBucketTable = this.checked;
            THIS.bucketTableParentTd.toggle(this.checked);
        });
    },
    setYaxisAuto: function(args){
        this.yAxisManual = false;
        console.log(this.myName + " set y axis auto");
    },
    setYaxisManual: function(args){
        this.yAxisManual = true;
        console.log(this.myName + " set y axis manual");
    },
    setYaxisAction: function(args){
        console.log(this.myName + " set y axis action");
    },
    onActivate : function(){
        this.attachMouseHandler();
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        this.chartResizerElt.style("display", "none");
    }
};
