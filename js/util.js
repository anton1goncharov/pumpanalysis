// util.js
// mostly utilities for functional programming in JavaScript.

// example usage of Array.reduce:
// var a = [1,2,3]; var average = a.reduce(function(a, b) { return a + b; }, 0) /  a.length;
// in the following Array.prototype mixins, using .reduce might have been better
// than using .map. Whatever.

function affineCombineDates(date1, date2, epsilon){
    // returns (1 - epsilon) * date1 + epsilon  * date2
    return new Date((1 - epsilon) * date1.getTime() + epsilon * date2.getTime());
}
function NOW(){
    return new Date().getTime();
}
function __ascendingSortAux__(x, y){
    return x - y;
}
function __descendingSortAux__(x, y){
    return y - x;
}
function __ascendingSortAux1__(x, y){
    return x.item - y.item;
}
function __descendingSortAux1__(x, y){
    return y.item - x.item;
}

Array.prototype.sortDESC = function(){
    return this.sort(__descendingSortAux__);
};
Array.prototype.sortASC = function(){
    return this.sort(__ascendingSortAux__);
};
Array.prototype.sortCopy = function(compareFun){
    return this.concat([]).sort(compareFun);
};
Array.prototype.sortASCCopy = function(){
    return this.concat([]).sort(__ascendingSortAux__);
};
Array.prototype.sortDESCCopy = function(){
    return this.concat([]).sort(__descendingSortAux__);
};
Array.prototype.mapSortCopy = function(compareFun){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sortCopy(compareFun) : null; });
};
Array.prototype.mapSort = function(compareFun){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sort(compareFun) : null; });
};
Array.prototype.mapSortASCCopy = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sortASCCopy() : null; });
};
Array.prototype.mapSortDESCCopy = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sortDESCCopy() : null; });
};
Array.prototype.mapSortASC = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sortASC() : null; });
};
Array.prototype.mapSortDESC = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.sortDESC() : null; });
};
if (!Array.prototype.map){
    Array.prototype.map = function(f){
        var result = [];
        for (var i=0; i<this.length; i++) {
            result.push(f(this[i], i));
        }
        return result;
    };
}
Array.prototype.roundAll = function(){
    return this.map(function(x){return Math.round(x);});
};
Array.prototype.mapConsecutive = function(f){
    var result = [];
    for (var i=0; i<this.length-1; i++){
        result.push(f(this[i], this[i+1], i));
    }
    return result;
};
if (!Array.prototype.reduce){
    Array.prototype.reduce = function(fun, first){
        var result = first;
        for (var i=0; i<this.length; i++){
            result = fun(result, this[i], i);
        }
        return result;
    };
}
if (!Array.prototype.indexOf){
    Array.prototype.indexOf = function(itemToFind){
        for (var i=0; i<this.length; i++){
            if (this[i] == itemToFind) return i;
        }
        return -1;
    };
}
Array.prototype.sum = function(){
    var sum = 0;
    this.map(function(item){
        sum += item;
    });
    return sum;
};
Array.prototype.accumulate = function(){
    var partialSums = [];
    var sum = 0;
    for (var i=0; i<this.length; i++){
        partialSums.push(sum);
        sum += this[i];
    }
    partialSums.push(sum);
    return partialSums;
};
Array.prototype.differences = function(){
    var result = [];
    var l = this.length - 1;
    for (var i=0; i<l; i++){
        result.push(this[i+1] - this[i]);
    }
    return result;
};
Array.prototype.linTrafo = function(slope, offset){
	if (arguments.length <= 1) {
		offset = 0;
	}
    return this.map(function(x){ return slope*x + offset;});
};
Array.prototype.mean = function(){
    var sum = 0;
    this.map(function(item){
        sum += item;
    });
    return sum/this.length;
};
Array.prototype.medianFromSorted = function(){
    if ( this.length == 0 ) return NaN;
    if ( this.length % 2 == 0 ){
        var temp = this.length/2;
        return 0.5 * (this[temp] + this[temp-1]);
    } else {
        return this[Math.floor(this.length/2)];
    }
};
Array.prototype.median = function(sortFun){
    return this.sortCopy(sortFun).medianFromSorted();
};
Array.prototype.medianNumerical = function(){
    return this.median(__ascendingSortAux__);
};
Array.prototype.stdDev = function(){
    var m = this.mean();
    var variance =  this.map(function(item) {var tmp = item-m; return tmp*tmp;}).mean();
    return Math.sqrt(variance);
};
Array.prototype.stdDevE = function(){
    // empirical standard deviation
    var m = this.mean();
    var variance =  this.map(function(item) {var tmp = item-m; return tmp*tmp;}).sum() / (this.length-1);
    return Math.sqrt(variance);
};
Array.prototype.min = function(){
    return this.reduce(function(old, item){ return Math.min(old, item); }, Infinity);
};
Array.prototype.min1 = function(){
    // same as min, but ignoring entries which are NaN
    return this.reduce(function(old, item){ return isNaN(item) ? old : Math.min(old, item); }, Infinity);
};
Array.prototype.max1 = function(){
    // same as max, but ignoring entries which are NaN
    return this.reduce(function(old, item){ return isNaN(item) ? old : Math.max(old, item); }, -Infinity);
};
Array.prototype.max = function(){
    return this.reduce(function(old, item){ return Math.max(old, item); }, -Infinity);
};
Array.prototype.mapMean = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.mean() : null; });
};
Array.prototype.mapMedian = function(sortFun){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.median(sortFun) : null; });
};
Array.prototype.mapMedianNumerical = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.medianNumerical() : null; });
};
Array.prototype.mapMedianFromSorted = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.medianFromSorted() : null; });
};
Array.prototype.mapStdDev = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.stdDev() : null; });
};
Array.prototype.mapStdDevE = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.stdDevE() : null; });
};
Array.prototype.mapMin = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.min() : null; });
};
Array.prototype.mapMax = function(){
    return this.map(function(item){ return item && typeof item == "object" && item.constructor == Array ? item.max() : null; });
};
Array.prototype.orderingASCold = function(){
    var wrapper = this.map(function(item, index) { return {item: item, index: index}; });
    var sortedWrapper = wrapper.sort(function(a, b){ return a.item > b.item ? 1 : -1; });
    return sortedWrapper.map(function(item){ return item.index});
};
Array.prototype.orderingDESCold = function(){
    var wrapper = this.map(function(item, index) { return {item: item, index: index}; });
    var sortedWrapper = wrapper.sort(function(a, b){ return a.item < b.item ? 1 : -1; });
    return sortedWrapper.map(function(item){ return item.index});
};
Array.prototype.orderingASC = function(){
    var wrapper = this.map(function(item, index) { return {item: item, index: index}; });
    var sortedWrapper = wrapper.sort(__ascendingSortAux1__);
    return sortedWrapper.map(function(item){ return item.index});
};
Array.prototype.orderingDESC = function(){
    var wrapper = this.map(function(item, index) { return {item: item, index: index}; });
    var sortedWrapper = wrapper.sort(__descendingSortAux1__);
    return sortedWrapper.map(function(item){ return item.index});
};
Array.prototype.inversePermutation = function(){
    var result = new Array(this.length);
    for (var i=0; i<this.length; i++){
        result[this[i]] = i;
    }
    return result;
}
Array.prototype.pick = function(picker){
    var THIS = this;
    return picker.map(function(index){ return THIS[index]; })
};
Array.prototype.takeWhile = function(predicate){
    var result = [];
    for (var i=0; i<this.length; i++){
        if (predicate(this[i])){
            result.push(this[i])
        } else {
            return result;
        }
    }
    return result;
};
Array.prototype.select = function(predicateFun, startIndex, afterEndIndex){
    if (arguments.length < 2) startIndex = 0;
    if (arguments.length < 3) afterEndIndex = this.length;
    var result = [];
    for (var i=startIndex; i<afterEndIndex; i++){
        if (predicateFun(this[i], i)){
            result.push(this[i]);
        }
    }
    return result;
};
Array.prototype.selectBasedOneBooleanArray = function(booleanArray, startIndex, afterEndIndex){
    if (arguments.length < 2) startIndex = 0;
    if (arguments.length < 3) afterEndIndex = this.length;
    var result = [];
    for (var i=startIndex; i<afterEndIndex; i++){
        if (booleanArray[i]){
            result.push(this[i]);
        }
    }
    return result;
};
Array.prototype.or = function(predicateFun){
    for (var i=0; i<this.length; i++){
        if (predicateFun(this[i], i)) return true;
    }
    return false;
};
Array.prototype.and = function(predicateFun){
    for (var i=0; i<this.length; i++){
        if (!predicateFun(this[i], i)) return false;
    }
    return true;
};
Array.prototype.toStringCurly = function(){
    return "{" + this.toString() + "}";
};
Array.prototype.differences = function(){
    var result = [];
    if (!this.length) return [];
    for (var i=0; i<this.length - 1; i++){
        result.push(this[i+1] - this[i]);
    }
    return result;
};
Array.prototype.nGroups = function(n){
    var result = new Array();
    var size = Math.ceil(this.length / n);
    var targetIndex = 0;
    var sourceIndex = 0;
    while (true){
        result[targetIndex] = new Array();
        while(result[targetIndex].length < size){
            result[targetIndex].push(this[sourceIndex]);
            sourceIndex++;
            if (sourceIndex >= this.length){
                return result;
            } 
        }
        targetIndex++;
    }
}
Array.prototype.shallowClone = function(){
    var result = new Array(this.length);
    for (var i=0; i<this.length; i++){
        result[i] = this[i];
    }
    return result;
}
Array.prototype.equalShallow = function(that){
    if (!that){
        return false;
    }
    if (!(typeof that == "object")){
        return false;
    }
    if (!that.constructor == Array){
        return false;
    }
    if (this.length != that.length) return false;
    for (var i=0; i<this.length; i++){
        if (this[i] != that[i]) return false;
    }
    return true;
}
UTIL = {
    range: function(start, end){
        var result = [];
        for (var entry = start; entry <= end; entry++){
            result.push(entry);
        }
        return result;
    },
    table: function(size, fun){
        var result = [];
        for (var i=0; i<size; i++){
            result.push(fun(i));
        }
        return result;
    },
    table0: function(size, fun){
        var result = [];
        result.push(0);
        for (var i=1; i<=size; i++){
            result.push(fun(i));
        }
        return result;
    },
    zip: function(array1, array2){
        // arrays should have the same length
        var result = [];
        for (var i=0; i<array1.length; i++){
            result.push([array1[i], array2[i]]);
        }
        return result;
    },
    GET: function(doc){
        // retrieve get parameters from document search string,
        // result is an object (associative array) with the parameters as entries
        // or null, if the querystring is absent.
        if ( arguments.length < 1 ){
            doc = document;
        }
        // full search string
        var s = doc.location.search;
        // omit question mark
        var str = s.substr(1, s.length);
        if ( !str ) return null;
        var result = {};
        // split into name value pairs
        var arr = str.split('&');
        var numParams = arr.length;
        for ( var i=0; i<numParams; i++ ){
            // split into name and value
            var subArr = arr[i].split('=');
            var paramName = unescape(subArr[0]);
            var value = '';
            if ( subArr.length > 1 ){
                value = unescape(subArr[1]);
            }
            // store what we have retrieved
            result[paramName] = value;
        }
        return result;
    },
    nest : function(fun, start, stopFun){
        var result = [start];
        var current = start;
        while (true){
            var next = fun(current);
            if (stopFun(next)) return result;
            current = next;
            result.push(current);
        }
    }    
};
UTIL.dateAdd = function(date, add){
    // date, a JavaScript date, add, a time interval in milliseconds, positive or negative
    return new Date((date - 0) + add);
};
function fitDataLinear(data){
    /* data[i][0] = xi, data[i][1] = yi
    returns object with fields abjCoe and linCoe, such that the function
    x -> linCoe*x + absCoe is the least squares linear fit to the data
    solution:
    F = (1  1  ... 1 )
        (x1 x2 ... xN)
        
    FT = (1 x1)
         (1 x2)
          ...
         (1 xN)
         
    Y = (y1 ... yN)     
    then
    
    (F FT)^(-1) (F y)
    then
    F FT = (N sx)
           (sx sxx)
           
    F Y = (sy)
          (sxy)
          
    where sx = sum of x
          sy = sum of y
          sxy sum of x*y
          sxx = sum of x*x  
    */
    var sx = 0, sy = 0, sxy = 0, sxx = 0, n = data.length;
    for (var i=0; i<n; i++){
        var point = data[i];
        var x = point[0], y = point[1];
        sx += x;
        sy += y;
        sxy += (x*y);
        sxx += (x*x);
    }
    var deter = n*sxx - sx*sx;
    if (deter == 0) return null; // maybe better raise error here
    return {
        absCoe : (sxx * sy - sx * sxy) / deter,
        linCoe : (-sx * sy + n * sxy) / deter
    };
}
function det33(a, b, c){
    return a[0]*b[1]*c[2] + b[0]*c[1]*a[2] + c[0]*a[1]*b[2] - c[0]*b[1]*a[2] - b[0]*a[1]*c[2] - a[0]*c[1]*b[2]; 
}
function fitDataQuadratic(data){
    /* data[i][0] = xi, data[i][1] = yi
    returns object with fields abjCoe and linCoe and quadCoe, such that the function
    x -> quadCoe*x^2 + linCoe*x + absCoe is the least squares linear fit to the data
    with basis functions 1, x, x^2
    */
    var sx = 0, sxx = 0, sxxx = 0, sxxxx = 0, sy = 0, sxy = 0, sxxy = 0, n = data.length;
    for (var i=0; i<n; i++){
        var point = data[i];
        var x = point[0], y = point[1];
        var xx = x*x;
        var xxx = x*xx;
        var xxxx = xx*xx;
        var xy = x*y;
        var xxy = xx*y;
        sx += x;
        sxx += xx;
        sxxx += xxx;
        sxxxx += xxxx;
        sy += y;
        sxy += xy;
        sxxy += xxy;
    }
    var a = [n, sx, sxx];
    var b = [sx, sxx, sxxx];
    var c = [sxx, sxxx, sxxxx];
    var deter = det33(a, b, c);
    if (deter == 0) return null; // maybe better raise error here
    var rhs = [sy, sxy, sxxy];
    return {
        absCoe :  det33(rhs, b, c)/ deter,
        linCoe :  det33(a, rhs, c)/ deter,
        quadCoe :  det33(a, b, rhs)/ deter
    };
}
function invertLinearTrafo(trafo){
	// y = m x + b
	// x = (1/m)y +(-b/m)
	return {linCoe: 1/trafo.linCoe, absCoe: -trafo.absCoe / trafo.linCoe};
}
function composeLinearTrafos(left, right){
	// left = x -> m1 x + b1
	// right = x -> m2 x + b2
	// left o right = x -> m1 (m2 x + b2) + b1 = (m1 m2) x + (m1 b2 + b1)
	return {linCoe: left.linCoe * right.linCoe, absCoe: left.linCoe * right.absCoe + left.absCoe};
}
function linTrafoTimesConst(trafo, constant){
	return {linCoe: trafo.linCoe * constant, absCoe: trafo.absCoe * constant};
}
function linTrafoTimesLinTrafo(trafo1, trafo2){
	// (m1 x + b1) * (m2 x + b2) = (m1 m2) x^2   +   (m1 b2 + m2 b1) x +    (b1 b2)
	return {
		quadCoe: trafo1.linCoe * trafo2.linCoe,
		linCoe: trafo1.linCoe * trafo2.absCoe + trafo2.linCoe * trafo1.absCoe,
		absCoe: trafo1.absCoe * trafo2.absCoe
	};
}
function quadTrafoTimesConst(trafo, constant){
	return {quadCoe: trafo.quadCoe * constant, linCoe: trafo.linCoe * constant, absCoe: trafo.absCoe * constant};
}
function addQuadTrafos(trafo1, trafo2){
	return {
		quadCoe: trafo1.quadCoe + trafo2.quadCoe,
		linCoe: trafo1.linCoe + trafo2.linCoe,
		absCoe: trafo1.absCoe + trafo2.absCoe
	};
}
function quadTrafoAddSecondToFirst(trafo1, trafo2){
	trafo1.quadCoe += trafo2.quadCoe;
	trafo1.linCoe += trafo2.linCoe;
	trafo1.absCoe += trafo2.absCoe;
}
function linTrafoAddSecondToFirst(trafo1, trafo2){
	trafo1.linCoe += trafo2.linCoe;
	trafo1.absCoe += trafo2.absCoe;
}
function zeroQuadTrafo(){
	return {quadCoe: 0, linCoe: 0, absCoe: 0};
}
function isNumberDigit(str){
    var t = typeof str;
    if (t == "number"){
        var temp = str + "";
        if (temp.length != 1) return false;
        return 0 <= str && str <= 9;
    }
    if (t == "string"){
        if (str.length != 1) return false;
        return "0" <= str && str <= "9"
    }
    return false;
}
function numberAtEndOfString(str){
    var temp = UTIL.range(1,str.length).takeWhile(function(i){ return isNumberDigit(str[str.length-i]); });
    if (temp.length == 0) return -1;
    return str.slice(-temp.length);
}
function mapFormat(format, arr){
    return arr.map(function(item){ return format.sprintf(item)});
}
function entry0(arr){ return arr[0]; }
function entry1(arr){ return arr[1]; }
function entry2(arr){ return arr[2]; }
function entry3(arr){ return arr[3]; }
function entry4(arr){ return arr[4]; }
function entry5(arr){ return arr[5]; }
function entry6(arr){ return arr[6]; }
function entry7(arr){ return arr[7]; }
function entry8(arr){ return arr[8]; }

////////////////////////////// canvas stuff /////////////////////////
var CP = window.CanvasRenderingContext2D && CanvasRenderingContext2D.prototype;
if (CP.lineTo) {
    CP.dashedLine = function(x, y, x2, y2, da) {
        if (!da) da = [10,5];
        this.save();
        var dx = (x2-x), dy = (y2-y);
        var len = Math.sqrt(dx*dx + dy*dy);
        var rot = Math.atan2(dy, dx);
        this.translate(x, y);
        this.moveTo(0, 0);
        this.rotate(rot);       
        var dc = da.length;
        var di = 0, draw = true;
        x = 0;
        while (len > x) {
            x += da[di++ % dc];
            if (x > len) x = len;
            draw ? this.lineTo(x, 0): this.moveTo(x, 0);
            draw = !draw;
        }       
        this.restore();
    };
    CP.straightLine = function(x, y, x2, y2, da) {
        this.beginPath();
        this.moveTo(x, y);
        this.lineTo(x2, y2);
        this.stroke();
    };
    CP.dashedLine2 = function(x, y, x2, y2, da){
        this.beginPath();
        var totalDx = x2 - x;
        var totalDy = y2 - y;
        var totalLenSquared = totalDx*totalDx + totalDy*totalDy;
        var totalLength = Math.sqrt(totalLenSquared);
        var cos = totalDx / totalLength;
        var sin = totalDy / totalLength;
        var curX = x;
        var curY = y;
        var dashIndex = 0;
        var draw = true;
        this.moveTo(x, y);
        while (true){
            var dashLen = da[dashIndex];
            var dx = cos * dashLen;
            var dy = sin * dashLen;
            var nextX = curX + dx;
            var nextY = curY + dy;
            if (draw){
                this.lineTo(nextX, nextY);
            } else {
                this.moveTo(nextX, nextY);
            }
            var tmpX = nextX - x;
            var tmpY = nextY - y;
            var curSquaredLength = tmpX*tmpX + tmpY*tmpY;
            if (curSquaredLength >= totalLenSquared) break;
            curX = nextX;
            curY = nextY;
            draw = !draw;
            dashIndex++;
            if (dashIndex == da.length) dashIndex = 0;
        }
        this.stroke();
    };
    CP.dashedLine3 = function(x, y, x2, y2, da){
        this.beginPath();
        var totalDx = x2 - x;
        var totalDy = y2 - y;
        var totalLenSquared = totalDx*totalDx + totalDy*totalDy;
        var totalLength = Math.sqrt(totalLenSquared);
        var cos = totalDx / totalLength;
        var sin = totalDy / totalLength;
        var factor = 1;
        function calcCountAndFactor(){
            var count = 0;
            var curX = x;
            var curY = y;
            var dashIndex = 0;
            var draw = true;
            while (true){
                var dashLen = da[dashIndex];
                var dx = cos * dashLen;
                var dy = sin * dashLen;
                var nextX = curX + dx;
                var nextY = curY + dy;
                var tmpX = nextX - x;
                var tmpY = nextY - y;
                var curSquaredLength = tmpX*tmpX + tmpY*tmpY;
                if (curSquaredLength >= totalLenSquared){
                    if (draw){
                        len = Math.sqrt(curSquaredLength);
                        factor = totalLength / len;
                        return count + 1;
                    } else {
                        tmpX = curX-x; tmpY = curY-y;
                        len = Math.sqrt(tmpX*tmpX + tmpY*tmpY);
                        factor = totalLength / len;
                        return count;
                    }
                }
                curX = nextX;
                curY = nextY;
                draw = !draw;
                dashIndex++;
                if (dashIndex == da.length) dashIndex = 0;
                count++;
            }
        }
        var curX = x;
        var curY = y;
        var dashIndex = 0;
        var draw = true;
        this.moveTo(x, y);
        var numRuns = calcCountAndFactor();
        cos *= factor;
        sin *= factor;
        for (var run=0; run<numRuns; run++){
            var dashLen = da[dashIndex];
            var dx = cos * dashLen;
            var dy = sin * dashLen;
            curX += dx;
            curY += dy;
            if (draw){
                this.lineTo(curX, curY);
            } else {
                this.moveTo(curX, curY);
            }
            draw = !draw;
            dashIndex++;
            if (dashIndex == da.length) dashIndex = 0;
        }
        this.stroke();
    }
}
CANVAS = {
    draw : function(symbolIndex, ctx, x0, y0, size, color, lineWidth){
        symbolIndex = symbolIndex % this.functions.length;
        this.functions[symbolIndex](ctx, x0, y0, size, color, lineWidth);
    },
    cross : function(ctx, x0, y0, size, color, lineWidth){
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 - size, y0);
        ctx.lineTo(x0 + size, y0);
        ctx.stroke();
        ctx.moveTo(x0, y0 - size);
        ctx.lineTo(x0, y0 + size);
        ctx.stroke();
    }, 
    diagCross : function(ctx, x0, y0, size, color, lineWidth){
        size /= 1.4142;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 - size, y0 - size);
        ctx.lineTo(x0 + size, y0 + size);
        ctx.stroke();
        ctx.moveTo(x0 + size, y0 - size);
        ctx.lineTo(x0 - size, y0 + size);
        ctx.stroke();
    }, 
    circle : function(ctx, x0, y0, radius, color, lineWidth, N){
        if (arguments.length < 7 ){
            N = 8;
        }
        var ang = 2 * Math.PI / N;
        var c = Math.cos(ang);
        var s = Math.sin(ang);
        var Radius = radius * Math.sqrt(2*(1-c)) / s;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 + radius, y0);
        for (var i=1; i<=N; i++){
            var angCntrl = (i-0.5) * ang;
            var angPoint = i * ang;
            var x = radius * Math.cos(angPoint);
            var y = radius * Math.sin(angPoint);
            var xCntrl = Radius * Math.cos(angCntrl);
            var yCntrl = Radius * Math.sin(angCntrl);
            ctx.quadraticCurveTo(x0 + xCntrl, y0 + yCntrl, x0 + x, y0 + y);
        }
        ctx.stroke();
    },
    square : function(ctx, x0, y0, size, color, lineWidth){
        size /= 1.4142;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 - size, y0 - size);
        ctx.lineTo(x0 + size, y0 - size);
        ctx.lineTo(x0 + size, y0 + size);
        ctx.lineTo(x0 - size, y0 + size);
        ctx.lineTo(x0 - size, y0 - size);
        ctx.stroke();
    },
    squareRot45 : function(ctx, x0, y0, size, color, lineWidth){
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0       , y0 - size);
        ctx.lineTo(x0 + size, y0       );
        ctx.lineTo(x0       , y0 + size);
        ctx.lineTo(x0 - size, y0       );
        ctx.lineTo(x0       , y0 - size);
        ctx.stroke();
    },
    downTriag : function(ctx, x0, y0, size, color, lineWidth){
        var s = 0.8660254; // half square root of 3
        var r = size;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0     , y0 + r);
        ctx.lineTo(x0 -s*r, y0 - r/2);
        ctx.lineTo(x0 +s*r, y0 - r/2);
        ctx.lineTo(x0     , y0 + r);
        ctx.stroke();
    },
    upTriag : function(ctx, x0, y0, size, color, lineWidth){
        var s = 0.8660254; // half square root of 3
        var r = size;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0     , y0 - r);
        ctx.lineTo(x0 -s*r, y0 + r/2);
        ctx.lineTo(x0 +s*r, y0 + r/2);
        ctx.lineTo(x0     , y0 - r);
        ctx.stroke();
    },
    rightTriag : function(ctx, x0, y0, size, color, lineWidth){
        var s = 0.8660254; // half square root of 3
        var r = size;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 + r  , y0     );
        ctx.lineTo(x0 - r/2, y0 -s*r);
        ctx.lineTo(x0 - r/2, y0 +s*r);
        ctx.lineTo(x0 + r  , y0     );
        ctx.stroke();
    },
    leftTriag : function(ctx, x0, y0, size, color, lineWidth){
        var s = 0.8660254; // half square root of 3
        var r = size;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(x0 - r  , y0     );
        ctx.lineTo(x0 + r/2, y0 -s*r);
        ctx.lineTo(x0 + r/2, y0 +s*r);
        ctx.lineTo(x0 - r  , y0     );
        ctx.stroke();
    },
    weird : function(ctx, x0, y0, size, color, lineWidth){
        var s = 0.8660254; // half square root of 3
        var r = size * 1.8;
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        var yy0 = y0 - r;
        var yy1 = y0 + r/2;
        var yy2 = yy1;
        var xx0 = x0;
        var xx1 = x0 - s*r;
        var xx2 = x0 + s*r;
        ctx.moveTo(xx0, yy0);
        ctx.quadraticCurveTo(xx1, yy1, xx2, yy2);
        ctx.quadraticCurveTo(xx0, yy0, xx1, yy1);
        ctx.quadraticCurveTo(xx2, yy2, xx0, yy0);
        ctx.stroke();
    }
};
CANVAS.functions = [
    CANVAS.circle, CANVAS.square, CANVAS.cross, CANVAS.diagCross, CANVAS.squareRot45,
    CANVAS.downTriag, CANVAS.upTriag, CANVAS.leftTriag, CANVAS.rightTriag, CANVAS.weird
];


/*******************************************************
********************************************************
color conversion from
http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
********************************************************
*******************************************************/


/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b){
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, l];
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  Array           The RGB representation
 */
function hslToRgb(h, s, l){
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [r * 255, g * 255, b * 255];
}
function hslToRgbString(h, s, l){
    var rgb = hslToRgb(h, s, l);
    var r = Math.round(rgb[0]);
    var g = Math.round(rgb[1]);
    var b = Math.round(rgb[2]);
    return "rgb(" + r + "," + g + "," + b + ")";
}
/**
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
 */
function rgbToHsv(r, g, b){
    r = r/255, g = g/255, b = b/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, v = max;

    var d = max - min;
    s = max == 0 ? 0 : d / max;

    if(max == min){
        h = 0; // achromatic
    }else{
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, v];
}

/**
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  v       The value
 * @return  Array           The RGB representation
 */
function hsvToRgb(h, s, v){
    var r, g, b;

    var i = Math.floor(h * 6);
    var f = h * 6 - i;
    var p = v * (1 - s);
    var q = v * (1 - f * s);
    var t = v * (1 - (1 - f) * s);

    switch(i % 6){
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }

    return [r * 255, g * 255, b * 255];
}
/////////////////////// end color conversion ////////////////////////////////

function grey(value){
    return "rgb(" +value+ "," +value+ "," +value+ ")";
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////


function linSolve(mat, rhs){
    var N = mat.length;
    var result = new Array(N);
    // mat regular n x n matrix, as array of rows
    // rhs n dim vector
    function pivotIndex(col, startRow){
        // find argmax { |mat[s][c]|, |mat[s+1][c]|, ..., |mat[N-1][c]}|}
        var maxVal = 0;
        var index = -1;
        for (var row = startRow; row < N; row++){
            var val = Math.abs(mat[row][col]);
            if (val > maxVal){
                maxVal = val;
                index = row;
            }
        }
        return index;
    }
    function swapRows(row1, row2, startCol){
        // swap rows with indices row1, row2, starting at colmun index start 
        // also swap corresponding entries in rhs
        for (var col = startCol; col < N; col++){
            temp = mat[row1][col];
            mat[row1][col] = mat[row2][col];
            mat[row2][col] = temp;
        }
        temp = rhs[row1];
        rhs[row1] = rhs[row2];
        rhs[row2] = temp;
    }
    function addLinComb(pivotRow, targetRow, coeff, startCol){
        // adds coeff * pivotRow to targetRow, starting at startCol
        // also do it with rhs
        for (var col = startCol; col < N; col++){
            mat[targetRow][col] += coeff * mat[pivotRow][col]; 
        }
        rhs[targetRow] += coeff * rhs[pivotRow];
    }
    function triag(){
        // generate zeros below the main diagonal
        for (var k = 0; k < N; k++){
            var pIndex = pivotIndex(k, k);
            if ( k!=pIndex ){
                swapRows(k, pIndex, k); // always use absolute biggest possible as pivot, for numerical stability
            }
            for (var r = k+1; r < N; r++){
                var lambda = -mat[r][k]  * 1 / mat[k][k];
                addLinComb(k, r, lambda, k+1);
                mat[r][k] = 0;
            }
        }
    }
    function step2(){
        for (var row = N-1; row >= 0; row--){
            var tmp = rhs[row];
            for (var s = row+1; s < N; s++){
                tmp -= mat[row][s] * result[s];
            }
            result[row] = tmp / mat[row][row];
        }
    }
    triag();
    step2();
    return result;
}

function linearRegression(basisFunctions, data){
    // basisFunctions = f_0, f_1,... , f_(k-1) are the k basis functions
    // data = array of length N
    // 0 <= i < N:    data[i].x -> independent variable (can be ANY OBJECT; only condition is that all k basis functions must accept that kind of object as argument, and return a scalar)
    //                data[i].y -> dependent variable (must be scalar)
    // mat1[r][i] = f_r(data[i].x)         0 <= r < k,  0 <= i < N
    var k = basisFunctions.length;
    var N = data.length;
    var mat1 = new Array(k);
    for (var r = 0; r < k; r++){
        var fr = basisFunctions[r];
        mat1[r] = new Array(N);
        for (var i = 0; i < N; i++){
            mat1[r][i] = fr(data[i].x);
        }
    }
    //mat[r][c] = sum_i mat1[r][i]*mat1[c][i], 0 <= i < N
    //rhs[r] = sum_i mat1[r][i] * data[i].y,  0 <= i < N
    var mat = new Array(k);
    for (var r = 0; r < k; r++){
        mat[r] = new Array(k);
    }
    var rhs = new Array(k);
    for (var r = 0; r < k; r++){
        for (var c = 0 ; c < k; c++){
            mat[r][c] = 0;
            for (var i = 0; i < N; i++){
                mat[r][c] += mat1[r][i]*mat1[c][i];
            }
        }
        rhs[r] = 0;
        for (var i = 0; i < N; i++){
            rhs[r] += mat1[r][i] * data[i].y;
        }
    }
    return linSolve(mat, rhs);
}
