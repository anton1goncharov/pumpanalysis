//constants.js


var usualKWvalues = [2300, 2200, 1200, 2300, 2300, 800, 1000, 1000];
var minimumHeadRequiredForNonError = 92;
var pump7StartValidDate = new Date(2013, 3, 24, 12);

var flowFormat      = "%7.1f";
var flowDiffFormat  = "%7.2f";
var shaftFormat     = "%7.1f";
var hydroFormat     = "%7.1f";
var tdhFormat       = "%7.1f";
var effFormat       = "%6.2f";
var tankLevelFormat = "%7.1f";
var opHoursFormat   = "%d";
var minEffFormat    = effFormat;
var meanEffFormat   = effFormat;
var maxEffFormat    = effFormat;
var stdDevEffFormat = effFormat;
var minFlowFormat   = flowFormat;
var meanFlowFormat  = flowFormat;
var maxFlowFormat   = flowFormat;
var meanShaftFormat = shaftFormat;
var minHeadFormat   = tdhFormat;
var meanHeadFormat  = tdhFormat;
var maxHeadFormat   = tdhFormat;
var bepEffFormat    = effFormat;
var bepFlowFormat   = flowFormat;

var chartSizeChangeFactor = 1.1;

pumpGroupInfoTableHeadings = [
    "pumps", "op<span id='numOpHoursOrdering'></span><br/>hours",
    "min<br/>eff (%)", "mean<span id='meanEffOrdering'></span><br/>eff (%)", "max<br/>eff (%)", "std<br/>Dev eff", 
    "min<br/>flow (MGD)", "mean<span id='meanFlowOrdering'></span><br/>flow (MGD)", "max<br/>flow (MGD)",
    "mean<span id='meanShaftOrdering'></span><br/>Shaft (KW)",
    "min<br/>TDH (psi)", "mean<span id='meanHeadOrdering'></span><br/>TDH (psi)", "max<br/>TDH (psi)",
    "eff at<span id='effAtOptimumOrdering'></span><br/>opt (%)", "opt<span id='optFlowOrdering'></span><br/>flow (MGD)"
];

pumpGroupInfoTableHeadings2 = [
    "pumps", "op hours", "min eff (%)", "mean eff (%)", "max eff (%)", "std dev eff", 
    "min flow (MGD)", "mean flow (MGD)", "max flow (MGD)", "mean shaft (KW)", "min TDH (psi)", 
    "mean TDH (psi)", "max TDH (psi)", "BEP eff(%)", "BEP flow (MGD)"
];

function pmpGroupInfo(code){
    return [
        API.pumpsListFromCode(code), API.numOpHours[code], API.minEffFromCode[code], API.meanEffFromCode[code],
        API.maxEffFromCode[code], API.stdDevEffFromCode[code], API.minFlowFromCode[code], API.meanFlowFromCode[code],
        API.maxFlowFromCode[code], API.meanShaftFromCode[code], API.minHeadFromCode[code], API.meanHeadFromCode[code],
        API.maxHeadFromCode[code], 100*API.formulas.bepEff[code], API.formulas.bepFlow[code]
    ];
}
function formattedPmpGroupInfo(code){
    var p = pmpGroupInfo(code);
    return [
        p[0],
        opHoursFormat.sprintf(p[1]),   // op hours
        minEffFormat.sprintf(p[2]),    // min eff
        meanEffFormat.sprintf(p[3]),   // mean eff
        maxEffFormat.sprintf(p[4]),    // max eff
        stdDevEffFormat.sprintf(p[5]), // std dev eff
        minFlowFormat.sprintf(p[6]),   // min flow
        meanFlowFormat.sprintf(p[7]),  // mean flow
        maxFlowFormat.sprintf(p[8]),   // max flow
        meanShaftFormat.sprintf(p[9]), // mean shaft
        minHeadFormat.sprintf(p[10]),  // min head
        meanHeadFormat.sprintf(p[11]), // mean head
        maxHeadFormat.sprintf(p[12]),  // max head
        bepEffFormat.sprintf(p[13]),   // bep eff
        bepFlowFormat.sprintf(p[14])   // bep flow
    ];
}
Config = {
    URLs : {
        CSVmanifest : "data/csvManifest.csv",
        SQL         : thisIsThomasKloeckersLocalMachine ? "../dts10" : "../../dts10"
    },
    minHours : 3,
    ophrsEffByFlowRange : {
        defaultFlowBuckets : [58, 70, 80, 87, 92, 102, 112, 122, 142, 160],
        bucketsName        : "potoEffD3_ophrsEffByFlowRange_flowBuckets",
        summary1: {
            margin  : {top: 28, right: 14, bottom: 54, left: 70},
            height0 : 311,
            aspect  : 1.8,
            preferredNumYaxisTicks : 7,
            xAxisLabelVerticalPositionAdjust : 10
        },
        summary2: {
            margin  : {top: 24, right: 11, bottom: 34, left: 28},
            extraWidthAndLeftMarginForFirst : 32,
            height0 : 220,
            aspect  : 1.08,
            preferredNumXaxisTicks : 4,
            xAxisLabelVerticalPositionAdjust : 10
        },
        bucketBars : {
            margin  : {top: 31, right: 14, bottom: 48, left: 70},
            height0 : 311,
            aspect  : 1.8,
            preferredNumYaxisTicks : 7,
            xAxisLabelVerticalPositionAdjust : 10
        },
        flowHeadCanvas : {
            margin  : {top: 14, right: 12, bottom: 48, left: 58},
            height0 : 450,
            aspect  : 1.4,
            preferredNumXaxisTicks : 7,
            preferredNumYaxisTicks : 6,
            xAxisLabelVerticalPositionAdjust : 10,
            yAxisLabelHorizontalAdjust : 20
        }

    }
};