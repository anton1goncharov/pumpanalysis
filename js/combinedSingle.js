/********************************************************************************************************************
************************************************ COMBINED_AND_SINGLE ************************************************
*********************************************************************************************************************/
COMBINED_AND_SINGLE = {
    hasResizeableChart : true,
    //defaultMinimumPlotEfficiencyPercent : 65,
    //defaultMaximumPlotEfficiencyPercent : 90,
    //defaultMinimumTDHpsi : 96,
    //defaultMaximumTDHpsi : 132,
    allowNegativeY_left  : true,
    allowNegativeY_right : true,
    allowNegativeX       : true,
    defaultMinimumPlotEfficiencyPercent : 50,
    defaultMaximumPlotEfficiencyPercent : 120,
    defaultMinimumTDHpsi : 0,
    defaultMaximumTDHpsi : 140,
    verticalAxisLabelGap : 6,
    dotRadius : 4,
    pointsOnLine_char : 2,
    pointsOnLine_eff  : 99,
    zoomEpsilon  : 0.05,
    margin : {top: 18, right: 62, bottom: 52, left: 62},
    height0 : 550,
    aspect : 1.7,
    xMouseAxisRectHeight : 18,
    epsilonForExtendingTheLineAlittleBitLeftAndRight : 0.002,
    xAxisLabelVerticalPositionAdjust : 12,
    yAxisLabelHorizontalAdjustLeft  : 20,
    yAxisLabelHorizontalAdjustRight : 12,
    xMouseAxisTextVerticalAdjust : 4,
    code: null,
    btnCode: null,
    makeXtrafo : function(){
        return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
    },
    makeYtrafoLeft : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMinLeft, this.yMaxLeft]);
    },
    makeYtrafoRight : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMinRight, this.yMaxRight]);
    },
    make: function(){
        var THIS = this;
        this.svgContainer = d3.select("#combinedAndSingleChartTd");
        this.legendContainer = d3.select("#combinedSingleLegendDiv");
        this.mouseOverContainer = d3.select("#combinedSingleMouseOverDiv");
        this.mouseOverSuperContainer = d3.select("#combinedSingleMouseOver_Super_Div");
        makeComboChooserInterface("combinedAndSingleControls", "cas", this, "code", "btnCode");
        var zoomOuterDiv = this.zoomOuterDiv = jQuery("#combinedSingleZoomOuterDiv");
        zoomOuterDiv.html(HtmlGen.table(
            HtmlGen.tr(HtmlGen.td("Zooming and y-axes Adjustment"), {id: "combinedSingleZoomAndPanDragHandle", "class": "dragHandle combinedSingleZoomAndPanDragHandle"}) +
            HtmlGen.tr(HtmlGen.td(HtmlGen.div("", {id: "combinedSingleZoomInnerDiv"}))) +
            HtmlGen.tr(HtmlGen.td(
                HtmlGen.table(
                    HtmlGen.tr(HtmlGen.td("&bull; (characteristic curve)") + HtmlGen.td("", {id: "combinedSingleCharCurveSpecialParentTd"})) +
                    HtmlGen.tr(HtmlGen.td(HtmlSymbols.blackSquare + " (efficiency curve)") + HtmlGen.td("", {id: "combinedSingleEffCurveSpecialParentTd"}))
                )
            ))
        ));
        var zoomInnerDiv = this.zoomInnerDiv = jQuery("#combinedSingleZoomInnerDiv");
        zoomInnerDiv.html(jQuery("#charCurveZoomTableParentTd").html().replace(/charCurve/g, "combinedSingle"));
        var zoomInButton = this.zoomInButton = jQuery("#combinedSingleZoomIn");
        var zoomOutButton = this.zoomOutButton = jQuery("#combinedSingleZoomOut");
        var charCurveSpecialParent = this.charCurveSpecialParent = jQuery("#combinedSingleCharCurveSpecialParentTd");
        var effCurveSpecialParent  = this.effCurveSpecialParent  = jQuery("#combinedSingleEffCurveSpecialParentTd");
        var idPrePrefix = "combinedSingle";
        var charSpecialBtns = zoomPanBtnAux(charCurveSpecialParent, idPrePrefix + "CharCurve");
        var effSpecialBtns = zoomPanBtnAux(effCurveSpecialParent, idPrePrefix + "EffCurve");
        charSpecialBtns[0].on("click", function(){
            THIS.charCurve_compress();
        });
        charSpecialBtns[1].on("click", function(){
            THIS.charCurve_stretch();
        });
        charSpecialBtns[2].on("click", function(){
            THIS.charCurve_up();
        });
        charSpecialBtns[3].on("click", function(){
            THIS.charCurve_down();
        });
        effSpecialBtns[0].on("click", function(){
            THIS.effCurve_compress();
        });
        effSpecialBtns[1].on("click", function(){
            THIS.effCurve_stretch();
        });
        effSpecialBtns[2].on("click", function(){
            THIS.effCurve_up();
        });
        effSpecialBtns[3].on("click", function(){
            THIS.effCurve_down();
        });
        zoomOutButton.on("click", function(){
            THIS.zoomOut();
        });
        zoomInButton.on("click", function(){
            THIS.zoomIn();
        });
        zoomOuterDiv.draggable({ handle : "tr.combinedSingleZoomAndPanDragHandle" });
        // show first combination on startup
        $("#casBtnCombo" + API.common[0]).trigger("click");
    },
    charCurve_compress : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        if (!this.allowNegativeY_left){
            if (yLo < 0){
                yHi -= yLo; yLo = 0;
            }
        }
        var options = {
            yMinLeft  : yLo,
            yMaxLeft  : yHi
        };
        this.updateChart(options);
    },
    charCurve_stretch : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        var options = {
            yMinLeft  : yLo,
            yMaxLeft  : yHi
        };
        this.updateChart(options);
    },
    charCurve_up : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        if (!this.allowNegativeY_left){
            if (yLo < 0){
                yHi -= yLo; yLo = 0;
            }
        }
        var options = {
            yMinLeft  : yLo,
            yMaxLeft  : yHi
        };
        this.updateChart(options);
    },
    charCurve_down : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        var options = {
            yMinLeft  : yLo,
            yMaxLeft  : yHi
        };
        this.updateChart(options);
    },
    effCurve_compress : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinRight;
        var y1 = this.yMaxRight;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        if (!this.allowNegativeY_right){
            if (yLo < 0){
                yHi -= yLo; yLo = 0;
            }
        }
        var options = {
            yMinRight  : yLo,
            yMaxRight  : yHi
        };
        this.updateChart(options);
    },
    effCurve_stretch : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinRight;
        var y1 = this.yMaxRight;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        var options = {
            yMinRight  : yLo,
            yMaxRight  : yHi
        };
        this.updateChart(options);
    },
    effCurve_up : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinRight;
        var y1 = this.yMaxRight;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        if (!this.allowNegativeY_right){
            if (yLo < 0){
                yHi -= yLo; yLo = 0;
            }
        }
        var options = {
            yMinRight  : yLo,
            yMaxRight  : yHi
        };
        this.updateChart(options);
    },
    effCurve_down : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var y0 = this.yMinRight;
        var y1 = this.yMaxRight;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        var options = {
            yMinRight  : yLo,
            yMaxRight  : yHi
        };
        this.updateChart(options);
    },
    zoomIn : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 + zoomEpsilon * dx;
        var xHi = x1 - zoomEpsilon * dx;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        var _y0 = this.yMinRight;
        var _y1 = this.yMaxRight;
        var _dy = _y1 - _y0;
        var _yLo = _y0 + zoomEpsilon * _dy;
        var _yHi = _y1 - zoomEpsilon * _dy;
        var options = {
            xMin      : xLo,
            xMax      : xHi,
            yMinLeft  : yLo,
            yMaxLeft  : yHi,
            yMinRight : _yLo,
            yMaxRight : _yHi
        };
        this.updateChart(options);
    },
    zoomOut : function(){
        var zoomEpsilon = this.zoomEpsilon;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 - zoomEpsilon * dx;
        var xHi = x1 + zoomEpsilon * dx;
        var y0 = this.yMinLeft;
        var y1 = this.yMaxLeft;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        var _y0 = this.yMinRight;
        var _y1 = this.yMaxRight;
        var _dy = _y1 - _y0;
        var _yLo = _y0 - zoomEpsilon * _dy;
        var _yHi = _y1 + zoomEpsilon * _dy;
        if (!this.allowNegativeX){
            if (xLo < 0){
                xHi -= xLo; xLo = 0;
            }
        }
        if (!this.allowNegativeY_left){
            if (yLo < 0){
                yHi -= yLo; yLo = 0;
            }
        }
        if (!this.allowNegativeY_right){
            if (_yLo < 0){
                _yHi -= _Lo; _yLo = 0;
            }
        }
        var options = {
            xMin      : xLo,
            xMax      : xHi,
            yMinLeft  : yLo,
            yMaxLeft  : yHi,
            yMinRight : _yLo,
            yMaxRight : _yHi
        };
        this.updateChart(options);
    },
    comboChoiceDone: function(code){
        this.makeChart(code);
        this.showTable(code);
    },
    showTable: function(code){// generate the info table to the right of the chart
        // first of all, delet the table that occurs when mouse overing data points (since the pump combination has changed, that table is now outdated)
        // $("#combinedAndSingleMouseOverInfo").html(""); element no longer exists (it's real tooltip where the mouse is, not to the right, now)
        // call makeComboInfoTable to do the actual work
        makeComboInfoTable(code, "combinedAndSingleInfo", "casStatsHandle", "casStatsMinimizer");
    },
    updateChart : function(options){
        // options should contain zero or more of: xMin, xMax, yMinLeft, yMaxLeft, yMinRight, yMaxRight, and nothing else
        // (empty options possible - when sizeFactor has changed, that makes sense)
        if (options){
            for (var key in options){
                this[key] = options[key];
            }
        }
        var THIS = this;
        var code = this.code;
        var xMin = this.xMin;
        var xMax = this.xMax;
        var yMinLeft = this.yMinLeft;
        var yMaxLeft = this.yMaxLeft;
        var yMinRight = this.yMinRight;
        var yMaxRight = this.yMaxRight;
        var totalHydroCoeff = API.formulas.flow2TotalHydro[code];
        var fc2fCoeffArr = this.fc2fCoeffArr;
        var hydroCoeffArr = this.hydroCoeffArr;
        var pumpsList = this.pumpsList;
        var pumpsInd = this.pumpsInd;
        var comboCharCurve = this.charFun;
        var totalEffFun = this.effFun;
        var myName = this.myName;
        var svg = this.svg;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup;
        var clipRect = this.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        var x = this.x = this.makeXtrafo();
        var yLeft  = this.yLeft  = this.makeYtrafoLeft();
        var yRight = this.yRight = this.makeYtrafoRight();
        var xAxis = this.xAxis; xAxis.scale(x);
        var yAxis_left  = this.yAxis_left; yAxis_left.scale(yLeft);
        var yAxis_right = this.yAxis_right; yAxis_right.scale(yRight);
        var xAxisGroup = this.xAxisGroup; xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel; xAxisLabel.attr("x", innerWidth/2);
        var yAxisGroup_left  = this.yAxisGroup_left; yAxisGroup_left.call(yAxis_left);
        var yAxisLabel_left = this.yAxisLabel_left; yAxisLabel_left.attr("x", -innerHeight/2);
        var yAxisGroup_right = this.yAxisGroup_right; yAxisGroup_right.translate(innerWidth, 0).call(yAxis_right);
        yAxisGroup_right.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        var yAxisLabel_right = this.yAxisLabel_right; yAxisLabel_right.attr("x", -innerHeight/2);
        // data points for total char curve
        var charTotDataGroup = this.charTotDataGroup;
        charTotDataGroup.selectAll("circle")
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return yLeft(d[1]); });
        // function line of total char curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_char;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        if (comboCharCurve(lastX) < 0){
        	var absCoeff = comboCharCurve(0);
        	var linCoeff = comboCharCurve(1) - absCoeff;
        	xxx[xxx.length-1] = -absCoeff/linCoeff;
        }
        var pointsOnCurve = xxx.map(function(x){
        	return [x, comboCharCurve(x)];
        });
        var comboCharCurveLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yLeft(d[1]); });
        mainGroup.selectAll("path.comboCharCurveLine").data([pointsOnCurve]).attr({
            "d" : comboCharCurveLine
        });
        // data points for single char curves
        var singleCharDataGroups = this.singleCharDataGroups;
        singleCharDataGroups.selectAll("circle")
            .attr("cx", function(d,i,j) {
                return x(d[0]);
            })
            .attr("cy", function(d) {
                return yLeft(d[1]);
            });
        // function lines of single char curves
        var singleCharCurvesGroup = this.singleCharCurvesGroup;
        var charCurveFunctionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yLeft(d[1]); });
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_char;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        var pointsOnCurves = d3.range(this.charFuns.length).map(function(pumpIdx){
            xxx[xxx.length-1] = lastX;
            var plotFun = THIS.charFuns[pumpIdx];
            var rightVal = plotFun(lastX);
            if (rightVal < 0){
                var absCoeff = plotFun(0);
                var linCoeff = plotFun(1) - absCoeff;
                var intersectionWithXaxis = -absCoeff/linCoeff;
                xxx[xxx.length-1] = intersectionWithXaxis;
            }
            return xxx.map(function(x){
                return [x, plotFun(x)];
            });
        });
        var singleCharCurvesPaths = this.singleCharCurvesPaths = singleCharCurvesGroup
            .selectAll("path.singleCharCurve").data(pointsOnCurves).attr({
                d : charCurveFunctionLine
            });
        // data points for total eff curve
        var effTotDataGroup = this.effTotDataGroup;
        effTotDataGroup.selectAll("rect")
            .attr("x", function(d) { return x(d[0]) - THIS.dotRadius; })
            .attr("y", function(d) { return yRight(d[1]) - THIS.dotRadius; });
        // function line of total eff curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_eff;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var z = zerosOfQuadraticFunction(totalHydroCoeff);
        xLo = Math.max(z[0], xLo);
        xHi = Math.min(z[1], xHi);
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        var pointsOnCurve = xxx.map(function(x){
            return [x, totalEffFun(x)];
        });
        totalEffFunLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yRight(d[1]); });
        mainGroup.selectAll("path.totalEffFunLine").data([pointsOnCurve]).attr({
            d : totalEffFunLine
        });
        // data points for single eff curves
        var singleEffDataGroups = this.singleEffDataGroups;
        singleEffDataGroups.selectAll("rect")
            .attr("x", function(d,i,j) {
                return x(d[0]) - THIS.dotRadius;
            })
            .attr("y", function(d) {
                return yRight(d[1]) - THIS.dotRadius;
            });
        // function lines of single eff curves
        var singleEffCurvesGroup = this.singleEffCurvesGroup;
        var effCurveFunctionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yRight(d[1]); });
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_eff;
        var pointsOnCurves = d3.range(this.effFuns.length).map(function(pumpIdx){
            var xLo = (1+epsilon) * xMin - epsilon * xMax;
            xLo = Math.max(0, xLo);
            var xHi = (1+epsilon) * xMax - epsilon * xMin;
            var z = zerosOfQuadraticFunction(hydroCoeffArr[pumpIdx]);
            //console.log("pumpIdx " + pumpIdx + " pump " + pumpsList[pumpIdx]);
            //console.log(hydroCoeffArr[pumpIdx]);
            //console.log("z " + z.map(function(x){return "%5.1f".sprintf(x);}).join(","));
            var innerCoeffs = fc2fCoeffArr[pumpIdx];
            var leftZero = (z[0] - innerCoeffs.absCoe)/innerCoeffs.linCoe;
            var rightZero = (z[1] - innerCoeffs.absCoe)/innerCoeffs.linCoe;
            //console.log("left " + leftZero + " right " + rightZero);
            xLo = Math.max(leftZero, xLo);
            xHi = Math.min(rightZero, xHi);
            //console.log("xLo " + xLo + " xHi " + xHi;
            var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
            var xxx = d3.range(xLo, xHi, stepSize);
            var plotFun = THIS.effFuns[pumpIdx];
            return xxx.map(function(x){
                return [x, plotFun(x)];
            });
        });
        var singleEffCurvesPaths = this.singleEffCurvesPaths = singleEffCurvesGroup
            .selectAll("path.singleEffCurve").data(pointsOnCurves).attr({
                d : effCurveFunctionLine
            });
        // mouse lines
        var valg = this.verticalAxisLabelGap;
        var hMouseLineTDH = this.hMouseLineTDH.attr("x2", innerWidth).classed("inactive", true);
        var vMouseLineTotal = this.vMouseLineTotal.attr("y1", innerHeight).classed("inactive", true);
        var hMouseLineTotalEff = this.hMouseLineTotalEff.attr("x1", innerWidth).attr("x2", innerWidth).classed("inactive", true);
        var yMouseAxisRectWidth = this.yMouseAxisRectWidth;
        var yMouseAxisRectHeight = this.yMouseAxisRectHeight;
        //var yMouseAxisRect = this.yMouseAxisRect.attr("x", margLeft-yMouseAxisRectWidth).classed("inactive", true);
        var xMouseAxisRectWidth = this.xMouseAxisRectWidth;
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect_Y = height-margBot;
        var xMouseAxisRect_Y_aboveAxis = xMouseAxisRect_Y - xMouseAxisRectHeight;
        var xMouseAxisRectTotal = this.xMouseAxisRectTotal.attr("y", xMouseAxisRect_Y).classed("inactive", true);
        var xMouseAxisText_Y = xMouseAxisRect_Y + xMouseAxisRectHeight - this.xMouseAxisTextVerticalAdjust;
        var xMouseAxisText_Y_aboveAxis = xMouseAxisText_Y - xMouseAxisRectHeight;
        var xMouseAxisText = this.xMouseAxisText.attr("y", xMouseAxisText_Y).classed("inactive", true);        
        var rightAxisY = margLeft + innerWidth;
        var yRightMouseAxisRect = this.yRightMouseAxisRect.attr("x", rightAxisY).classed("inactive", true);
        var yRightMouseAxisText = this.yRightMouseAxisText.attr("x", rightAxisY + valg).classed("inactive", true);

        if (pumpsList.length > 1){
            var vMouseLinesSingle = this.vMouseLinesSingle;
            var hMouseLinesSingle = this.hMouseLinesSingle;
            var xMouseAxisTextSingle = this.xMouseAxisTextSingle;
            var xMouseAxisRectSingle = this.xMouseAxisRectSingle;
            var yMouseAxisRectSingle = this.yMouseAxisRectSingle;
            var yMouseAxisTextSingle = this.yMouseAxisTextSingle;
            var pumpRankByFlow = this.pumpRankByFlow;
            for (var pumpIdx=0; pumpIdx<pumpsList.length; pumpIdx++){
                var vml = vMouseLinesSingle[pumpIdx].attr("y1", innerHeight).classed("inactive", true);
                var hml = hMouseLinesSingle[pumpIdx].attr("x1", innerWidth).attr("x2", innerWidth).classed("inactive", true);
                var ry, rty;
                if (((pumpsList.length-pumpRankByFlow[pumpIdx]) % 2) == 0){
                    ry = xMouseAxisRect_Y;
                    rty = xMouseAxisText_Y;
                } else {
                    ry = xMouseAxisRect_Y_aboveAxis;
                    rty = xMouseAxisText_Y_aboveAxis;
                }
                var xmar = xMouseAxisRectSingle[pumpIdx].attr("y", ry);
                var xTxt = xMouseAxisTextSingle[pumpIdx].attr("y", rty);
                var ymarX = rightAxisY - (pumpsList.length-pumpRankByFlow[pumpIdx]) * yMouseAxisRectWidth;
                var yTxt_X = ymarX + yMouseAxisRectWidth - valg;
                var ymar = yMouseAxisRectSingle[pumpIdx].attr("x", ymarX);
                var yTxt = yMouseAxisTextSingle[pumpIdx].attr("x", yTxt_X);
            }
        } else {
        }


    },
    makeChart: function(code){
        var THIS = this;
        var pumpsList = API.pumpsListFromCode(code);
        var pumpsInd = pumpsList.map(function(p){ return p-1});
        this.pumpsList = pumpsList;
        this.pumpsInd = pumpsInd;
        this.code = code;
        // construct functions: totals
        var totalHeadCoeff = API.formulas.flow2Head[code];
        var totalHydroCoeff = API.formulas.flow2TotalHydro[code];
        var totalShaftCoeff = API.formulas.flow2TotalShaft[code];
        var comboCharCurve = makeLinearFunction(totalHeadCoeff);
        var totalHydroFun = makeQuadraticFunction(totalHydroCoeff);
        var totalShaftFun = makeLinearFunction(totalShaftCoeff);
        var totalEffFun = times100(divideFunctions(totalHydroFun, totalShaftFun));
        this.charFun = comboCharCurve;
        this.invCharFun = makeLinearFunction(API.formulas.head2Flow[code]);
        this.effFun = totalEffFun;
        // construct functions: singles
        if (pumpsList.length > 1){
            //
            var f2fcCoesArr = API.formulas.flow2FlowContrib[code].pick(pumpsInd);
            var f2fcFuns = f2fcCoesArr.map(makeLinearFunction);
            this.f2fcFuns = f2fcFuns;
            var headCoeffArr = API.formulas.flow2Heads[code].pick(pumpsInd);
            var hydroCoeffArr = this.hydroCoeffArr = API.formulas.flow2SingleHydro[code].pick(pumpsInd);
            var shaftCoeffArr = API.formulas.flow2SingleShaft[code].pick(pumpsInd);
            var singleCharCurves1 = headCoeffArr.map(makeLinearFunction);
            var singleHydroFuns = hydroCoeffArr.map(makeQuadraticFunction);
            var singleShaftFuns = shaftCoeffArr.map(makeLinearFunction);
            var singleEffFuns1 = UTIL.range(0, pumpsList.length-1).map(function(i){ return times100(divideFunctions(singleHydroFuns[i], singleShaftFuns[i]))});
            // singleEffFuns1 depend on total flow, but we want them to depend on the respective single flow contributions
            var fc2fCoeffArr = this.fc2fCoeffArr = API.formulas.flowContrib2Flow[code].pick(pumpsInd);
            var fc2fFuns = fc2fCoeffArr.map(makeLinearFunction);
            var singleEffFuns = UTIL.range(0, pumpsList.length-1).map(function(i){ return comp(singleEffFuns1[i], fc2fFuns[i])});
            // also for char curves
            var singleCharCurves = UTIL.range(0, pumpsList.length-1).map(function(i){ return comp(singleCharCurves1[i], fc2fFuns[i]); });
            this.charFuns = singleCharCurves;
            this.effFuns = singleEffFuns;
        } else {
            this.charFuns = [];
            this.effFuns = [];
            this.f2fcFuns = [];
        }
        // construct points
        var rows = API.finalTable.pick(API.formulas.rowsWherePumpsRunOnly[code]);
        // points for totals:
        var charPoints = rows.map(function(row){ return [row.flow, row.avgHead, row];});
        var effPoints = rows.map(function(row){ return [row.flow, 100*row.eff, row];});
        var singleCharPointsArr;
        var singleEffPointsArr;
        if (pumpsList.length > 1){
            // points for singles:
            singleCharPointsArr = pumpsInd.map(function(pump, i){ return rows.map(function(row){ return [row.absFlowContrib(pump), row.head[pump], i, row];})});
            singleEffPointsArr = pumpsInd.map(function(pump, i){ return rows.map(function(row){ return [row.absFlowContrib(pump), 100*row.singleEff(pump), i, row];})});
        } else {
            singleCharPointsArr = [];
            singleEffPointsArr = [];
        }
        var maxPlotFlow = Math.max(Math.min(1.5 * API.formulas.bepFlow[code], 180), 100);
        var xMin = this.xMin = 0;
        var xMax = this.xMax = maxPlotFlow;
        var yMinLeft  = this.yMinLeft  = this.defaultMinimumTDHpsi;
        var yMaxLeft  = this.yMaxLeft  = this.defaultMaximumTDHpsi;
        var yMinRight = this.yMinRight = this.defaultMinimumPlotEfficiencyPercent;
        var yMaxRight = this.yMaxRight = this.defaultMaximumPlotEfficiencyPercent;
        // make chart
        var svgContainer = this.svgContainer;
        var myName = this.myName;
        svgContainer.html("");
        var svg = this.svg = svgContainer.append("svg");
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height).classed(myName, true);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup = svg.append("g").translate(margLeft, margTop).classed("mainGroup", true);
        var clipPath = this.clipPath = mainGroup.append("clipPath").attr("id", "combinedSingleClipPath");
        var clipRect = this.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
        var x = this.x = this.makeXtrafo();
        var yLeft  = this.yLeft  = this.makeYtrafoLeft();
        var yRight = this.yRight = this.makeYtrafoRight();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis_left  = this.yAxis_left  = d3.svg.axis().scale(yLeft).orient("left");
        var yAxis_right = this.yAxis_right = d3.svg.axis().scale(yRight).orient("right");
        var xAxisGroup = this.xAxisGroup = mainGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text("flow (MGD)").attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);

        var yAxisGroup_left  = this.yAxisGroup_left  = mainGroup.append("g").classed("axis yAxis", true).call(yAxis_left);
        var yAxisLabel_left = this.yAxisLabel_left = yAxisGroup_left.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
            .text("TDH (psi)").attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjustLeft).style("text-anchor", "middle");

        var yAxisGroup_right = this.yAxisGroup_right = mainGroup.append("g").classed("axis yAxis", true).translate(innerWidth, 0).call(yAxis_right);
        yAxisGroup_right.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        var yAxisLabel_right = this.yAxisLabel_right = yAxisGroup_right.append("text").classed("axisLabel yAxisLabel right", true).rotate(-90)
            .text("efficiency (%)").attr("x", -innerHeight/2).attr("y", margRight - this.yAxisLabelHorizontalAdjustRight).style("text-anchor", "middle");

        // data points for total char curve
        var charTotDataGroup = this.charTotDataGroup = mainGroup.append("g").classed("charTotDataGroup pointGroup", true)
            .style("clip-path", "url(#combinedSingleClipPath)");
        var mouseOverDivId = this.mouseOverContainer.attr("id");
        charTotDataGroup.selectAll(".honestPolitician").data(charPoints).enter().append("circle")
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return yLeft(d[1]); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d,i,j){
                var row = d[2];
                var containerId = mouseOverDivId;
                var chosenColumnIndex = -1;
                var tableId = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId = myName + "Minimizer";
                var captionId = myName + "MouseOverCaption"
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
                pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
            });
        // function line of total char curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_char;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        if (comboCharCurve(lastX) < 0){
            var absCoeff = comboCharCurve(0);
            var linCoeff = comboCharCurve(1) - absCoeff;
            xxx[xxx.length-1] = -absCoeff/linCoeff;
        }
        var pointsOnCurve = xxx.map(function(x){
            return [x, comboCharCurve(x)];
        });
        var comboCharCurveLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yLeft(d[1]); });
        mainGroup.selectAll(".honestPolitician").data([pointsOnCurve]).enter().append("path").attr({
            "fill" : "none", "d" : comboCharCurveLine
        }).classed("comboCharCurveLine", true)
        .style("clip-path", "url(#combinedSingleClipPath)");
        // data points for single char curves
        var singleCharDataGroups = this.singleCharDataGroups = mainGroup.selectAll(".honestPolitician").data(singleCharPointsArr).enter().append("g")
            .attr("class", function(d,i,j) { return "singleCharDataGroup pointGroup pumpIdx_" + i; } )
            .style("clip-path", "url(#combinedSingleClipPath)");
        singleCharDataGroups.selectAll(".honestPolitician").data(function(d){return d;}).enter().append("circle")
            .attr("cx", function(d,i,j) {
                return x(d[0]);
            })
            .attr("cy", function(d) {
                return yLeft(d[1]);
            })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d,i,j){
                var row = d[3];
                var containerId = mouseOverDivId;
                var chosenColumnIndex = j + 2;
                var tableId = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId = myName + "Minimizer";
                var captionId = myName + "MouseOverCaption"
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
                pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
            });
        // function lines of single char curves
        var singleCharCurvesGroup = this.singleCharCurvesGroup = mainGroup.append("g").classed("singleCharCurvesGroup", true)
            .style("clip-path", "url(#combinedSingleClipPath)");
        var charCurveFunctionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yLeft(d[1]); });
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_char;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        var pointsOnCurves = d3.range(this.charFuns.length).map(function(pumpIdx){
            xxx[xxx.length-1] = lastX;
            var plotFun = THIS.charFuns[pumpIdx];
            var rightVal = plotFun(lastX);
            if (rightVal < 0){
                var absCoeff = plotFun(0);
                var linCoeff = plotFun(1) - absCoeff;
                var intersectionWithXaxis = -absCoeff/linCoeff;
                xxx[xxx.length-1] = intersectionWithXaxis;
            }
            return xxx.map(function(x){
                return [x, plotFun(x)];
            });
        });
        var singleCharCurvesPaths = this.singleCharCurvesPaths = singleCharCurvesGroup
            .selectAll(".honestPolitician").data(pointsOnCurves).enter().append("path").attr({
                "fill" : "none", "d" : charCurveFunctionLine
            }).attr("class", function(d,i,j){ return "singleCharCurve pumpIdx_" + i; })
            .on("mouseover", function(d,i,j){
            })
            .on("mouseout", function(d,i,j){
            });
        // data points for total eff curve
        var effTotDataGroup = this.effTotDataGroup = mainGroup.append("g").classed("effTotDataGroup pointGroup", true)
            .style("clip-path", "url(#combinedSingleClipPath)");
        effTotDataGroup.selectAll(".honestPolitician").data(effPoints).enter().append("rect")
            .attr("x", function(d) { return x(d[0]) - THIS.dotRadius; })
            .attr("y", function(d) { return yRight(d[1]) - THIS.dotRadius; })
            .attr("width", 2*this.dotRadius)
            .attr("height", 2*this.dotRadius)
            .on("mouseover", function(d,i,j){
                var row = d[2];
                var containerId = mouseOverDivId;
                var chosenColumnIndex = -1;
                var tableId = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId = myName + "Minimizer";
                var captionId = myName + "MouseOverCaption"
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
                pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
            });

        // function line of total eff curve
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_eff;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        xLo = Math.max(0, xLo);
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var z = zerosOfQuadraticFunction(totalHydroCoeff);
        xLo = Math.max(z[0], xLo);
        xHi = Math.min(z[1], xHi);
        var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var lastX = xxx[xxx.length-1];
        var pointsOnCurve = xxx.map(function(x){
            return [x, totalEffFun(x)];
        });
        totalEffFunLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yRight(d[1]); });
        mainGroup.selectAll(".honestPolitician").data([pointsOnCurve]).enter().append("path").attr({
            "fill" : "none", "d" : totalEffFunLine
        }).classed("totalEffFunLine", true)
        .style("clip-path", "url(#combinedSingleClipPath)");
        // data points for single eff curves
        var singleEffDataGroups = this.singleEffDataGroups = mainGroup.selectAll(".honestPolitician").data(singleEffPointsArr).enter().append("g")
            .attr("class", function(d,i,j) { return "singleEffDataGroup pointGroup pumpIdx_" + i; } )
            .style("clip-path", "url(#combinedSingleClipPath)");
        singleEffDataGroups.selectAll(".honestPolitician").data(function(d){return d;}).enter().append("rect")
            .attr("x", function(d,i,j) {
                return x(d[0]) - THIS.dotRadius;
            })
            .attr("y", function(d) {
                return yRight(d[1]) - THIS.dotRadius;
            })
            .attr("width", 2*this.dotRadius)
            .attr("height", 2*this.dotRadius)
            .on("mouseover", function(d,i,j){
                var row = d[3];
                var containerId = mouseOverDivId;
                var chosenColumnIndex = j + 2;
                var tableId = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId = myName + "Minimizer";
                var captionId = myName + "MouseOverCaption"
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
                pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
            });
        // function lines of single eff curves
        var singleEffCurvesGroup = this.singleEffCurvesGroup = mainGroup.append("g").classed("singleEffCurvesGroup", true)
            .style("clip-path", "url(#combinedSingleClipPath)");
        var effCurveFunctionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return yRight(d[1]); });
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointCount = this.pointsOnLine_eff;
        var pointsOnCurves = d3.range(this.effFuns.length).map(function(pumpIdx){
            var xLo = (1+epsilon) * xMin - epsilon * xMax;
            xLo = Math.max(0, xLo);
            var xHi = (1+epsilon) * xMax - epsilon * xMin;
            var z = zerosOfQuadraticFunction(hydroCoeffArr[pumpIdx]);
            //console.log("pumpIdx " + pumpIdx + " pump " + pumpsList[pumpIdx]);
            //console.log(hydroCoeffArr[pumpIdx]);
            //console.log("z " + z.map(function(x){return "%5.1f".sprintf(x);}).join(","));
            var innerCoeffs = fc2fCoeffArr[pumpIdx];
            var leftZero = (z[0] - innerCoeffs.absCoe)/innerCoeffs.linCoe;
            var rightZero = (z[1] - innerCoeffs.absCoe)/innerCoeffs.linCoe;
            //console.log("left " + leftZero + " right " + rightZero);
            xLo = Math.max(leftZero, xLo);
            xHi = Math.min(rightZero, xHi);
            //console.log("xLo " + xLo + " xHi " + xHi;
            var stepSize = (xHi - xLo)/(pointCount-1)*0.9999999;
            var xxx = d3.range(xLo, xHi, stepSize);
            var plotFun = THIS.effFuns[pumpIdx];
            return xxx.map(function(x){
                return [x, plotFun(x)];
            });
        });
        var singleEffCurvesPaths = this.singleEffCurvesPaths = singleEffCurvesGroup
            .selectAll(".honestPolitician").data(pointsOnCurves).enter().append("path").attr({
                "fill" : "none", "d" : effCurveFunctionLine
            }).attr("class", function(d,i,j){ return "singleEffCurve pumpIdx_" + i; })
            .on("mouseover", function(d,i,j){
            })
            .on("mouseout", function(d,i,j){
            });
        // mouse lines
        var valg = this.verticalAxisLabelGap;
        var hMouseLineTDH = this.hMouseLineTDH = mainGroup.append("line")
            .attr("x1", 0).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
            .attr("id", "combinedSingle_H_MouseLine_TDH").classed("mouseLine horizontal inactive", true);
        var vMouseLineTotal = this.vMouseLineTotal = mainGroup.append("line")
            .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
            .attr("id", "combinedSingle_V_MouseLine_Total").classed("mouseLine vertical inactive", true);
        var hMouseLineTotalEff = this.hMouseLineTotalEff = mainGroup.append("line")
            .attr("y1", innerHeight).attr("x1", innerWidth).attr("x2", innerWidth)
            .attr("id", "combinedSingle_H_MouseLine_TotalEff").classed("mouseLine horizontal inactive", true);
        var yMouseAxisRectWidth = this.yMouseAxisRectWidth = margLeft - 5;
        var yMouseAxisRectHeight = this.yMouseAxisRectHeight = 18;
        var yMouseAxisRect = this.yMouseAxisRect = svg.append("rect").attr("x", margLeft-yMouseAxisRectWidth).attr("width", yMouseAxisRectWidth)
            .attr("height", yMouseAxisRectHeight).classed("yMouseAxisRect mouseAxisRect inactive", true);
        var yMouseAxisText = this.yMouseAxisText = svg.append("text").attr("x", margLeft - valg).attr("y", 0)
            .classed("yMouseAxisText mouseAxisText inactive", true);
        var xMouseAxisRectWidth = this.xMouseAxisRectWidth = 60;
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect_Y = height-margBot;
        var xMouseAxisRect_Y_aboveAxis = xMouseAxisRect_Y - xMouseAxisRectHeight;
        var xMouseAxisRectTotal = this.xMouseAxisRectTotal = svg.append("rect").attr("y", xMouseAxisRect_Y).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
            .classed("xMouseAxisRect mouseAxisRect inactive", true);
        var xMouseAxisText_Y = xMouseAxisRect_Y + xMouseAxisRectHeight - this.xMouseAxisTextVerticalAdjust;
        var xMouseAxisText_Y_aboveAxis = xMouseAxisText_Y - xMouseAxisRectHeight;
        var xMouseAxisText = this.xMouseAxisText = svg.append("text").attr("y", xMouseAxisText_Y)
            .classed("xMouseAxisText mouseAxisText inactive", true);
        
        var rightAxisY = margLeft + innerWidth;
        var yRightMouseAxisRect = this.yRightMouseAxisRect = svg.append("rect").attr("x", rightAxisY)
            .attr("width", yMouseAxisRectWidth).attr("height", yMouseAxisRectHeight)
            .classed("yMouseAxisRect mouseAxisRect inactive", true);
        var yRightMouseAxisText = this.yRightMouseAxisText = svg.append("text").attr("x", rightAxisY + valg).attr("y", 0)
            .classed("rightMouseAxisText mouseAxisText inactive", true);

        var vMouseLinesSingle = this.vMouseLinesSingle = [];
        var hMouseLinesSingle = this.hMouseLinesSingle = [];
        var xMouseAxisTextSingle = this.xMouseAxisTextSingle = [];
        var xMouseAxisRectSingle = this.xMouseAxisRectSingle = [];
        var yMouseAxisRectSingle = this.yMouseAxisRectSingle = [];
        var yMouseAxisTextSingle = this.yMouseAxisTextSingle = [];
        if (pumpsList.length > 1){
            //var pumpOrdering = API.formulas.meanAbsoluteFlowContrib.pick(pumpsList).orderingASC();
            var pumpOrdering = API.formulas.indivBepFlow.pick(pumpsInd).orderingASC();
            var pumpRankByFlow = this.pumpRankByFlow = pumpOrdering.inversePermutation();
            for (var pumpIdx=0; pumpIdx<pumpsList.length; pumpIdx++){
                var vml = mainGroup.append("line")
                    .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
                    .attr("id", "combinedSingle_V_MouseLine_Single_" + pumpIdx).classed("mouseLine vertical inactive", true);
                vMouseLinesSingle.push(vml);
                var hml = mainGroup.append("line")
                    .attr("x1", innerWidth).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
                    .attr("id", "combinedSingle_H_MouseLine_Single_" + pumpIdx).classed("mouseLine horizontal inactive", true);
                hMouseLinesSingle.push(hml);
                var ry, rty;
                if (((pumpsList.length-pumpRankByFlow[pumpIdx]) % 2) == 0){
                    ry = xMouseAxisRect_Y;
                    rty = xMouseAxisText_Y;
                } else {
                    ry = xMouseAxisRect_Y_aboveAxis;
                    rty = xMouseAxisText_Y_aboveAxis;
                }
                var xmar = svg.append("rect").attr("y", ry).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
                    .attr("class", function(d,i,j){ return "xMouseAxisRect mouseAxisRect inactive pumpIdx_" + pumpIdx; });
                xMouseAxisRectSingle.push(xmar);
                var xTxt = svg.append("text").attr("y", rty)
                    .attr("class", function(d,i,j){ return "xMouseAxisText mouseAxisText inactive pumpIdx_" + pumpIdx; });
                xMouseAxisTextSingle.push(xTxt);
                var ymarX = rightAxisY - (pumpsList.length-pumpRankByFlow[pumpIdx]) * yMouseAxisRectWidth;
                var yTxt_X = ymarX + yMouseAxisRectWidth - valg;
                var ymar = svg.append("rect").attr("x", ymarX)
                    .attr("width", yMouseAxisRectWidth).attr("height", yMouseAxisRectHeight)
                    .attr("class", function(d,i,j){ return "yMouseAxisRect mouseAxisRect inactive pumpIdx_" + pumpIdx; });
                yMouseAxisRectSingle.push(ymar);
                var yTxt = svg.append("text").attr("x", yTxt_X).attr("y", 0)
                    .attr("class", function(d,i,j){ return "yMouseAxisText mouseAxisText inactive pumpIdx_" + pumpIdx; });
                yMouseAxisTextSingle.push(yTxt);
            }
        } else {
        }
        this.attachMouseHandler();
        this.createLegendTable();
        jQuery(this.legendContainer.node()).draggable({ handle : "tr.combinedSingleLegendTableHaderRow" });
        svg.call(makeCombinedSinglePanDragBehavior());
        pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
    },
    createLegendTable : function(){
        var THIS = this;
        var myName = this.myName;
        var pumpsList = this.pumpsList;
        var container = this.legendContainer;
        container.html("");
        var legendTable = container.append("table").classed(myName + "LegendTable", true);
        var headerTr = legendTable.append("tr").classed(myName + "LegendTableHaderRow dragHandle", true);
        var legendData = [];
        if (pumpsList.length > 1){
            legendData = pumpsList.map(function(pmp){
                return {
                    pump : pmp
                };
            });
        }
        legendData[legendData.length] = {
            pump : "total"
        };
        var contentTr = this.legendContentTr = legendTable.selectAll(".unicorn").data(legendData).enter().append("tr")
            .attr("class", function(d,i,j){
                if (d.pump == "total"){
                    return myName + "LegendTableContentRow total";
                }
                return myName + "LegendTableContentRow pumpIdx_" + i;
            });
        headerTr.append("th").html("").classed("colorColumn", true);
        contentTr.append("td")
        	.attr("class", function(d,i,j) {
                if (d.pump == "total"){
                    return "colorColumn total";
                }
        	   return "colorColumn pumpIdx_" + i;
            });
        headerTr.append("th").text("pump").classed("pumpColumn", true);
        contentTr.append("td").text(function(d,i,j){return d.pump;}).classed("pumpColumn", true);
        headerTr.append("th").html("&bull;").classed("showTDHDataPointsColumn", true);
        contentTr.append("td").classed("showTDHDataPointsColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
        	.classed("combinedSingleShowTDHDataPointsCheckbox", true)
        	.attr("id", function(d,i,j) { return "combinedSingleShowTDHDataPointsCheckboxOfPumpIdx_" + i; })
        	.on("change", function(d,i,j){
        	   if (d.pump == "total"){
        	       THIS.svg.selectAll("g.mainGroup>g.charTotDataGroup").classed("invisible", !this.checked);
        	   } else {
        	       THIS.svg.selectAll("g.mainGroup>g.singleCharDataGroup.pumpIdx_" + i).classed("invisible", !this.checked);
        	   }
        	});
        headerTr.append("th").html(HtmlSymbols.blackSquare).classed("showEffDataPointsColumn", true);
        contentTr.append("td").classed("showEffDataPointsColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
        	.classed("combinedSingleShowEffDataPointsCheckbox", true)
        	.attr("id", function(d,i,j) { return "combinedSingleShowEffDataPointsCheckboxOfPumpIdx_" + i; })
        	.on("change", function(d,i,j){
        	   if (d.pump == "total"){
        	       THIS.svg.selectAll("g.mainGroup>g.effTotDataGroup").classed("invisible", !this.checked);
        	   } else {
        	       THIS.svg.selectAll("g.mainGroup>g.singleEffDataGroup.pumpIdx_" + i).classed("invisible", !this.checked)
        	   }
        	});
        headerTr.append("th").html("\\").classed("showCharCurveColumn", true);
        contentTr.append("td").classed("showCharCurveColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
        	.classed("combinedSingleShowCharCurveCheckbox", true)
        	.attr("id", function(d,i,j) { return "combinedSingleShowCharCurveCheckboxOfPumpIdx_" + i; })
        	.on("change", function(d,i,j){
        	   if (d.pump == "total"){
        	       THIS.svg.selectAll("g.mainGroup>path.comboCharCurveLine").classed("invisible", !this.checked);
        	   } else {
        	       THIS.svg.selectAll("g.mainGroup>g.singleCharCurvesGroup>path.singleCharCurve.pumpIdx_" + i).classed("invisible", !this.checked);
        	   }
        	});
        headerTr.append("th").html(HtmlSymbols.frown).classed("showEffCurveColumn", true);
        contentTr.append("td").classed("showEffCurveColumn", true).append("input").attr("type", "checkbox").attr("checked", "checked")
        	.classed("combinedSingleShowEffCurveCheckbox", true)
        	.attr("id", function(d,i,j) { return "combinedSingleShowEffCurveCheckboxOfPumpIdx_" + i; })
        	.on("change", function(d,i,j){
        	   if (d.pump == "total"){
        	       THIS.svg.selectAll("g.mainGroup>path.totalEffFunLine").classed("invisible", !this.checked);
        	   } else {
        	       THIS.svg.selectAll("g.mainGroup>g.singleEffCurvesGroup>path.singleEffCurve.pumpIdx_" + i).classed("invisible", !this.checked);
        	   }
        	});

    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = COMBINED_AND_SINGLE;
            var pumpsList = OBJ.pumpsList;
            var margTop   = OBJ.margin.top;
            var margBot   = OBJ.margin.bottom;
            var margLeft  = OBJ.margin.left;
            var margRight = OBJ.margin.right;
            var m = d3.mouse(OBJ.mainGroup.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var mouseTDH = OBJ.yLeft.invert(yRaw);
            var mouseTotalFlow = OBJ.invCharFun(mouseTDH);
            var mouseTotalFlowPixel = OBJ.x(mouseTotalFlow);
            var totalEff = OBJ.effFun(mouseTotalFlow);
            var totalEffPixel = OBJ.yRight(totalEff);
            var xRaw_1 = xRaw + margLeft;
            var yRaw_1 = yRaw + margTop;
            var mouseTotalFlowPixel_1 = mouseTotalFlowPixel + margLeft;
            var totalEffPixel_1 = totalEffPixel + margTop;
            var hMouseLineTDH = OBJ.hMouseLineTDH;
            var hMouseLineTotalEff = OBJ.hMouseLineTotalEff;
            var vMouseLineTotal = OBJ.vMouseLineTotal;
            var yMouseAxisRect = OBJ.yMouseAxisRect;
            var yMouseAxisText = OBJ.yMouseAxisText;
            var xMouseAxisRectTotal = OBJ.xMouseAxisRectTotal;
            var xMouseAxisText = OBJ.xMouseAxisText;
            var yRightMouseAxisRect = OBJ.yRightMouseAxisRect;
            var yRightMouseAxisText = OBJ.yRightMouseAxisText;
            var innerWidth = OBJ.innerWidth;
            var xInRange = xRaw * (innerWidth - xRaw) > 0;
            var innerHeight = OBJ.innerHeight;
            var yInRange = yRaw * (innerHeight - yRaw) > 0;
            var mouseIsOverChart = xInRange && yInRange;
            hMouseLineTDH.classed("inactive", !mouseIsOverChart);
            hMouseLineTotalEff.classed("inactive", !mouseIsOverChart);
            vMouseLineTotal.classed("inactive", !mouseIsOverChart);
            yMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yMouseAxisText.classed("inactive", !mouseIsOverChart);
            xMouseAxisRectTotal.classed("inactive", !mouseIsOverChart);
            xMouseAxisText.classed("inactive", !mouseIsOverChart);
            yRightMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yRightMouseAxisText.classed("inactive", !mouseIsOverChart);
            if (pumpsList.length > 1){
            	for (var i=0; i<pumpsList.length; i++){
            		var vml = OBJ.vMouseLinesSingle[i];
            		var hml = OBJ.hMouseLinesSingle[i];
                    vml.classed("inactive", !mouseIsOverChart);
                    hml.classed("inactive", !mouseIsOverChart);
                    var xTxt = OBJ.xMouseAxisTextSingle[i];
                    xTxt.classed("inactive", !mouseIsOverChart);
                    var xmar = OBJ.xMouseAxisRectSingle[i];
                    xmar.classed("inactive", !mouseIsOverChart);
                    var ymar = OBJ.yMouseAxisRectSingle[i];
                    ymar.classed("inactive", !mouseIsOverChart);
                    var yTxt = OBJ.yMouseAxisTextSingle[i];
                    yTxt.classed("inactive", !mouseIsOverChart);
            	}
            }
            //DEBUG[0].html(mouseTotalFlow);
            //DEBUG[1].html(totalEff);
            if (mouseIsOverChart){
                hMouseLineTDH.attr("y1", yRaw).attr("y2", yRaw).attr("x2", mouseTotalFlowPixel);
                hMouseLineTotalEff.attr("y1", totalEffPixel).attr("y2", totalEffPixel).attr("x1", mouseTotalFlowPixel);
                vMouseLineTotal.attr("x1", mouseTotalFlowPixel).attr("x2", mouseTotalFlowPixel).attr("y2", yRaw);
                yMouseAxisRect.attr("y", yRaw_1 - OBJ.yMouseAxisRectHeight/2);
                yMouseAxisText.attr("y", yRaw_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(mouseTDH));
                xMouseAxisRectTotal.attr("x", mouseTotalFlowPixel_1 - OBJ.xMouseAxisRectWidth/2);
                xMouseAxisText.attr("x", mouseTotalFlowPixel_1).text("%5.1f".sprintf(mouseTotalFlow));
                yRightMouseAxisRect.attr("y", totalEffPixel_1 - OBJ.yMouseAxisRectHeight/2);
                yRightMouseAxisText.attr("y", totalEffPixel_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(totalEff));
                if (pumpsList.length > 1){
                	for (var i=0; i<pumpsList.length; i++){
                		var vml = OBJ.vMouseLinesSingle[i];
                        var flowContribution = OBJ.f2fcFuns[i](mouseTotalFlow);
                        var effSingle = OBJ.effFuns[i](flowContribution);
                        var singleTDH = OBJ.charFuns[i](flowContribution);
                        var singleTDHPixel = OBJ.yLeft(singleTDH)
                        var flowContribPixel = OBJ.x(flowContribution);
                        var flowContribPixel_1 = flowContribPixel + margLeft;
                        var effSinglePixel = OBJ.yRight(effSingle);
                        var effSinglePixel_1 = effSinglePixel + margTop;
               			vml.attr("x1", flowContribPixel).attr("x2", flowContribPixel).attr("y2", singleTDHPixel);
                        var hml = OBJ.hMouseLinesSingle[i];
                        hml.attr("y1", effSinglePixel).attr("y2", effSinglePixel).attr("x1", flowContribPixel);
                        var xTxt = OBJ.xMouseAxisTextSingle[i];
                        xTxt.attr("x", flowContribPixel_1).text("%5.1f".sprintf(flowContribution));
                        var xmar = OBJ.xMouseAxisRectSingle[i];
                        xmar.attr("x", flowContribPixel_1 - OBJ.xMouseAxisRectWidth/2);
                        var ymar = OBJ.yMouseAxisRectSingle[i];
                        ymar.attr("y", effSinglePixel_1 - OBJ.yMouseAxisRectHeight/2);
                        var yTxt = OBJ.yMouseAxisTextSingle[i];
                        yTxt.attr("y", effSinglePixel_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(effSingle));
                        //DEBUG[2+i].html("%5.1f".sprintf(effSingle));
                	}
                }
            }
        });
    },
    onActivate : function(){
        this.attachMouseHandler();
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        this.chartResizerElt.style("display", "none");
    }
    /*sizeChanged : function(){
        this.updateChart();
        pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
    }*/
};
// for combined / single
function pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG(){
    var OBJ = COMBINED_AND_SINGLE;
    var mosc = jQuery(OBJ.mouseOverSuperContainer.node());
    var moscWidth = mosc.width();
    var svg = OBJ.svg;
    var jsvg = jQuery(svg.node());
    var osvg = jsvg.offset();
    var svgLeft = osvg.left;
    var svgTop = osvg.top;
    var svgWidth = jsvg.width();
    var moscTargetRight = svgLeft + svgWidth - OBJ.margin.right;
    var moscTargetLeft = moscTargetRight - moscWidth;
    mosc.css({
        left   : moscTargetLeft,
        //right  : moscTargetRight,
        top    : svgTop
    });
}
function makeCombinedSinglePanDragBehavior(){
    var OBJ = COMBINED_AND_SINGLE;
    var behavior = d3.behavior.drag();
    behavior.on("dragstart", function(){
        var mouseStartXY = d3.mouse(OBJ.mainGroup.node());
        var startXraw = mouseStartXY[0];
        var startYraw = mouseStartXY[1];
        var xInv = OBJ.makeXtrafo().invert;
        var yInvLeft  = OBJ.makeYtrafoLeft().invert;
        var yInvRight = OBJ.makeYtrafoRight().invert;
        OBJ.dragStartData = {
            xMin       : OBJ.xMin,
            xMax       : OBJ.xMax,
            yMinLeft   : OBJ.yMinLeft,
            yMaxLeft   : OBJ.yMaxLeft,
            yMinRight  : OBJ.yMinRight,
            yMaxRight  : OBJ.yMaxRight,
            xInv       : xInv,
            yInvLeft   : yInvLeft,
            yInvRight  : yInvRight,
            startX     : xInv(startXraw),
            startYLeft : yInvLeft (startYraw),
            startYRight: yInvRight(startYraw)
        };
    });
    behavior.on("drag", function(){
        mouseXY = d3.mouse(OBJ.mainGroup.node());
        xRaw = mouseXY[0];
        yRaw = mouseXY[1];
        var dsd = OBJ.dragStartData;
        var xInv = dsd.xInv;
        var yInvLeft  = dsd.yInvLeft;
        var yInvRight = dsd.yInvRight;
        var currentX = xInv(xRaw);
        var currentYLeft  = yInvLeft (yRaw);
        var currentYRight = yInvRight(yRaw);
        var dX = currentX - dsd.startX;
        var dY_left  = currentYLeft  - dsd.startYLeft;
        var dY_right = currentYRight - dsd.startYRight;
        var xLo = dsd.xMin - dX;
        var xHi = dsd.xMax - dX;
        var yLoLeft  = dsd.yMinLeft  - dY_left;
        var yHiLeft  = dsd.yMaxLeft  - dY_left;
        var yLoRight = dsd.yMinRight - dY_right;
        var yHiRight = dsd.yMaxRight - dY_right;
        if (!OBJ.allowNegativeX){
            if (xLo < 0){
                xHi -= xLo; xLo = 0;
            }
        }
        if (!OBJ.allowNegativeY_left){
            if (yLoLeft < 0){
                yHiLeft -= yLoLeft; yLoLeft = 0;
            }
        }
        if (!OBJ.allowNegativeY_right){
            if (yLoRight < 0){
                yHiRight -= yLoRight; yLoRight = 0;
            }
        }
        var options = {
            xMin      : xLo,
            xMax      : xHi,
            yMinLeft  : yLoLeft,
            yMaxLeft  : yHiLeft,
            yMinRight : yLoRight,
            yMaxRight : yHiRight
        };
        OBJ.updateChart(options);
    });
    return behavior;
}
function zoomPanBtnAux(container, idPrefix){
    var id1 = idPrefix + "_verticalCompress";
    var id2 = idPrefix + "_verticalStretch";
    var id3 = idPrefix + "_up";
    var id4 = idPrefix + "_down";
    var myTableHtml = HtmlGen.table(
        HtmlGen.tr(
            HtmlGen.td(HtmlGen.img("", {src: "img/verticalCompressButton.png", "class": "zoomPanImg"}), {"class": "zoomPanImgTd", id: id1}) +
            HtmlGen.td(HtmlGen.img("", {src: "img/verticalStretchButton.png", "class": "zoomPanImg"}), {"class": "zoomPanImgTd", id: id2}) +
            HtmlGen.td(HtmlGen.img("", {src: "img/upButton.png", "class": "zoomPanImg"}), {"class": "zoomPanImgTd", id: id3}) +
            HtmlGen.td(HtmlGen.img("", {src: "img/downButton.png", "class": "zoomPanImg"}), {"class": "zoomPanImgTd", id: id4})
        )
    );
    container.html(myTableHtml);
    var elt1 = jQuery("#"+id1);
    var elt2 = jQuery("#"+id2);
    var elt3 = jQuery("#"+id3);
    var elt4 = jQuery("#"+id4);
    return [elt1, elt2, elt3, elt4];
}
