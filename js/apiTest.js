// apiTest.js

bodyOnload = function (){
    Elts = {};
    Elts.code = jQuery("#code");
    //Elts.result = jQuery("#result");
    Elts.commentary = jQuery("#commentary");
    Elts.exampleCounter = jQuery("#exampleCounter");
    jQuery.ajax({
        url: "data/mfgrCharCurveData.json",
        dataType: "json",
        success: function(response) {
			mfgrCharCurve = response;
            //console.log("mfgrCharCurveData.json loaded");
        }, 
        error: function(response, error) {
            $("#loadInfo").html("error loading mfgrCharCurve");
            console.log("error");
        }
    });
    jQuery.ajax({
        url: "data/mfgrEffCurveData.json",
        dataType: "json",
        success: function(response) {
			mfgrEffCurve = response;
            //console.log("mfgrEffCurveData.json loaded");
        }, 
        error: function(response, error) {
            $("#loadInfo").html("error loading mfgrEffCurve");
            console.log("error");
        }
    });
    jQuery.ajax({
        //url: "../pa/",
        url: "data/finalTable.json",
        dataType: "json",
        success: function(response) {
            API.init(response);
            API.init1(API.finalTableAll, 12, 50);
            nextTest();
        },
        error: function(response, error) {
            $("#loadInfo").html("error");
            console.log("error");
        }
    });
}

Tests = [
    {
        code : "API.pumpsListFromCode(137)",
        commentary : "code 137 = (2^1 + 2^4 + 2^8)/2 is used to represent pump combination {1,4,8} as a single integer, here 137"
    },
    {
        code : "API.finalTable[12345]",
        commentary : "entry # 12345 of the hourly data downloaded from the server and 'beefed up' on the client (in API.init and API.init1). to do add description of fields"
    },
    {
        code: "API.formulas.bepEff[137]",
        commentary : "predicted efficiency of pump combination {1,4,8} at the best efficiency point of that combination"
    },
    {
        code: "API.numOpHours[137]",
        commentary: "if the result is 6, that means that pump combination {1,4,8} has been running 6 hours total in the interval in question. API.numOpHours is an array of length 256 (because there are 8 pumps, and hence 2^8 = 256 possible pump combinations (not all of which have been running of course, but they all have a code.)"
    },
    {
        code: "[pumpComboCode = API.opHoursOrder[0], API.numOpHours[pumpComboCode], API.pumpsListFromCode(pumpComboCode)]",
        commentary : "pump 4 running alone is the most frequent pump combination, with 1937 hours"
    },
    {
        code: "[pumpComboCode = API.opHoursOrder[1], API.numOpHours[pumpComboCode], API.pumpsListFromCode(pumpComboCode)]",
        commentary: "pump 5 alone is next with 1636 hours"
    },
    {
        code: "[pumpComboCode = API.opHoursOrder[45], API.numOpHours[pumpComboCode], API.pumpsListFromCode(pumpComboCode)]",
        commentary: "{1,4,8} is at rank 45 in terms of numbers of hours in operation"
    },
    {
        code: "API.opHoursOrder.inversePermutation()[137]",
        commentary: "this is how you can find out the rank of each pump combination in terms of the number of hours in operation, starting with the (code of the) pump combination."
    }

];


var testIndex = -1;

function nextTest(){
    ++testIndex;
    testIndex = testIndex % Tests.length;
    runTest();
}
function prevTest(){
    --testIndex;
    testIndex += Tsts.length;
    testIndex = testIndex % Tests.length;
    runTest();
}

function runTest(){
    Elts.exampleCounter.html("example # " + testIndex);
    var currentTest = Tests[testIndex];
    var code = currentTest.code;
    Elts.code.html(code);
    Elts.commentary.html(currentTest.commentary);
    var result = eval(code);
    //Elts.result.html(result);
    console.log(code);
    console.log(result);
}