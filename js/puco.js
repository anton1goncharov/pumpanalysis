/***************************************************************************************************************
******************************************** PUMPCOMB_ANALYSIS *************************************************
****************************************************************************************************************/
PUMPCOMB_ANALYSIS = {
    hasResizeableChart : true,
    dotRadius : 2.5,
    pointsOnLine : 111,
    margin : {top: 18, right: 14, bottom: 52, left: 62},
    height0 : 600,
    aspect : 1.7,
    xMouseAxisRectHeight : 18,
    epsilonForExtendingTheLineAlittleBitLeftAndRight : 0.03,
    xAxisLabelVerticalPositionAdjust : 12,
    yAxisLabelHorizontalAdjust : 20,
    xMouseAxisTextVerticalAdjust : 4,
    code : null,
    btnCode : null,
    pumpsList : null,
    onVerticalAxis : "eff", // will be "eff", "head", "shaft", or "hydro"
    make: function(){
        this.container = d3.select("#tdPumpCombAnaChart");
        this.code = API.common[0];
        this.btnCode = this.code;
        makeComboChooserInterface("pumpCombinationAnalysisControls", "pca", this, "code", "btnCode");
        var id = "pcaBtnCombo" + this.code;
        var elt = document.getElementById(id);
        $(elt).addClass("chosen");
        setCheckboxesToCode("pcaPumpComboChooserCheckbox", this.code);
        this.makeVerticalAxisButtons();
        var pumpComboAnalysisYRangeAutoStuffParentTd = jQuery("#pumpComboAnalysisYRangeAutoStuffParentTd");
        pumpComboAnalysisYRangeAutoStuffParentTd.html(
            HtmlGen.table(HtmlGen.tr(
                HtmlGen.td("", {id: "pumpComboAnalysisYRange_eff_parent"}) +
                HtmlGen.td("", {id: "pumpComboAnalysisYRange_head_parent"}) +
                HtmlGen.td("", {id: "pumpComboAnalysisYRange_shaft_parent"}) +
                HtmlGen.td("", {id: "pumpComboAnalysisYRange_hydro_parent"})
            ))
        );
        var td_eff   = jQuery("#pumpComboAnalysisYRange_eff_parent");
        var td_head  = jQuery("#pumpComboAnalysisYRange_head_parent");
        var td_shaft = jQuery("#pumpComboAnalysisYRange_shaft_parent");
        var td_hydro = jQuery("#pumpComboAnalysisYRange_hydro_parent");
        this.yRangeParent_eff   = td_eff  ;
        this.yRangeParent_head  = td_head ;
        this.yRangeParent_shaft = td_shaft;
        this.yRangeParent_hydro = td_hydro;
        makeYAxisRangeStuff(td_eff  , this.myName + "_eff"  , this, {what: "eff"  }, true , "yAxisManual_eff");
        makeYAxisRangeStuff(td_head , this.myName + "_head" , this, {what: "head" }, false, "yAxisManual_head");
        makeYAxisRangeStuff(td_shaft, this.myName + "_shaft", this, {what: "shaft"}, false, "yAxisManual_shaft");
        makeYAxisRangeStuff(td_hydro, this.myName + "_hydro", this, {what: "hydro"}, false, "yAxisManual_hydro");
        td_head.hide();
        td_shaft.hide();
        td_hydro.hide();
        this.makeChart();
    },
    setYaxisAuto: function(args){
        this["yAxisManual_" + args.what] = false;
        console.log(this.myName + " set y axis auto: " + args.what);
    },
    setYaxisManual: function(args){
        this["yAxisManual_" + args.what] = true;
        console.log(this.myName + " set y axis manual: " + args.what);
    },
    setYaxisAction: function(args){
        console.log(this.myName + " set y axis action "  + args.what);
    },
    makeVerticalAxisButtons: function(){
        PUCO.makeVerticalAxisButtons("pca", "pumpCombAnaVerticalAxisButtons", PUMPCOMB_ANALYSIS);
    },
    comboChoiceDone: function(code){
        this.makeChart(code);
    },
    makeXtrafo : function(){
        return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
    },
    makeYtrafo : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMin, this.yMax]);
    },
    updateChart: function(xMin, xMax, yMin, yMax){
        var container = this.container;
        var pumpsList = this.pumpsList;
        var myName = this.myName;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        
        var lastYminName = "last_yMin_" + this.onVerticalAxis;
        var lastYmaxName = "last_yMax_" + this.onVerticalAxis;
        this[lastYminName] = yMin;
        this[lastYmaxName] = yMax;
        
        var plotFun = this.plotFun;
        var svg = this.svg;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup;
        mainGroup.translate(margLeft, margTop);
        this.invisiblePlaceholderRect.attr("width", width).attr("height", height);

        var clipRect = this.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        
        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup;
        xAxisGroup.selectAll("g.tick").remove();        //  not really necessary, but doesn't hurt either
        xAxisGroup.selectAll("path.domain").remove();   // ditto
        xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel;
        xAxisLabel.attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup;
        yAxisGroup.selectAll("g.tick").remove();        //  not really necessary, but doesn't hurt either
        yAxisGroup.selectAll("path.domain").remove();   // ditto
        yAxisGroup.call(yAxis);
        if (this.onVerticalAxis == "eff"){
            yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        }
        var yAxisLabel = this.yAxisLabel;
        yAxisLabel.attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust);
        var clipGroup = this.clipGroup;
        clipGroup.selectAll("circle.pucoAna")
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return y(d[1]); });
        // update the function plot
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var pointsOnCurve = xxx.map(function(x){
            return [x, plotFun(x)];
        });
        var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll("path.functionLine").data([pointsOnCurve]).attr({
            "d" : functionLine
        });
        this.vMouseLine.attr("y1", innerHeight);
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        this.xMouseAxisRect.attr("y", height-margBot);
        this.xMouseAxisText.attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust);
    },
    makeChart: function(code){
        var container = this.container;
        if (!code){
            code = this.code;
        }
        if (arguments.length == 0){
            this.yRangeParent_eff.hide();
            this.yRangeParent_head.hide();
            this.yRangeParent_shaft.hide();
            this.yRangeParent_hydro.hide();
            this["yRangeParent_" + this.onVerticalAxis].show();
        }
        var pumpsList = API.pumpsListFromCode(code);
        this.pumpsList = pumpsList;
        makeComboInfoTable(code, "pumpCombAnaRightTop", "pcaStatsHandle", "pcaStatsMinimizer");
        var myName = this.myName;
        var obj = PUCO.makeChartAssist(code, this.onVerticalAxis, this);
        var xMin = this.xMin = obj.xMin;
        var xMax = this.xMax = obj.xMax;
                   this.yMin = obj.yMin;
                   this.yMax = obj.yMax;
        /*
        var lastYminName = "last_yMin_" + this.onVerticalAxis;
        var lastYmaxName = "last_yMax_" + this.onVerticalAxis;
        var yManualName = "yAxisManual_" + this.onVerticalAxis;
        if (!(lastYminName in this) || !this[yManualName]){
        	this[lastYminName] = yMin;
        }
        if (!(lastYmaxName in this) || !this[yManualName]){
        	this[lastYmaxName] = yMax;
        }
        if (this[yManualName]){
            yMin = this.yMin = this[lastYminName];
            yMax = this.yMax = this[lastYmaxName];
        } else {
        }
        */
        PUCO.yRangeMemory(this);
        var yMin = this.yMin;
        var yMax = this.yMax;
        
        var plotFun = this.plotFun = obj.plotFun;
        var verticalAxisCaption = PUCO.captions[this.onVerticalAxis];
        var horizontalAxisCaption = "flow (MGD)";
        container.html("");
        var svg = this.svg = container.append("svg");
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height).classed(myName, true);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup = svg.append("g").translate(margLeft, margTop).classed("mainGroup", true);
		this.invisiblePlaceholderRect = mainGroup.append("rect").attr("width", width).attr("height", height).attr("stroke", "none").attr("fill", "transparent");

        var clipPath = this.clipPath = mainGroup.append("clipPath").attr("id", "pumpComboAnalysisClipPath");
        var clipRect = this.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
        var clipGroup = this.clipGroup = mainGroup.append("g").classed("clipGroup", true)
            .style("clip-path", "url(#pumpComboAnalysisClipPath)");

        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup = mainGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text(horizontalAxisCaption).attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup = mainGroup.append("g").classed("axis yAxis", true).call(yAxis);
        if (this.onVerticalAxis == "eff"){
            yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        }
        var yAxisLabel = this.yAxisLabel = yAxisGroup.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
            .text(verticalAxisCaption).attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust).style("text-anchor", "middle");
        clipGroup.selectAll(".honestPolitician").data(obj.points).enter().append("circle").classed("pucoAna", true)
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return y(d[1]); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d){
                var row = d[2];
                var containerId = "pumpCombAnaRightBottom";
                var chosenColumnIndex = -1;
                var tableId      = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId  = myName + "Minimizer";
                var captionId    = myName + "MouseOverCaption";
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            });
        // draw the selected function
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var pointsOnCurve = xxx.map(function(x){
            return [x, plotFun(x)];
        });
        var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll(".honestPolitician").data([pointsOnCurve]).enter().append("path").attr({
            "fill" : "none", "d" : functionLine
        }).classed("functionLine " + this.onVerticalAxis, true);
        // zoom and pan
        var THIS = this;
        svg.call(makePanDragBehavior(this.myName));
        // mouse lines
        var hMouseLine = this.hMouseLine = mainGroup.append("line")
            .attr("x1", 0).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
            .attr("id", myName + "_H_MouseLine").classed("mouseLine horizontal inactive", true);
        var vMouseLine = this.vMouseLine = mainGroup.append("line")
            .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
            .attr("id", myName + "_V_MouseLine").classed("mouseLine vertical inactive", true);
        var yMouseAxisRectWidth = margLeft - 5;
        var yMouseAxisRectHeight = this.yMouseAxisRectHeight = 18;
        var yMouseAxisRect = this.yMouseAxisRect = svg.append("rect").attr("x", margLeft-yMouseAxisRectWidth).attr("width", yMouseAxisRectWidth)
            .attr("height", yMouseAxisRectHeight).classed("yMouseAxisRect mouseAxisRect inactive", true);
        var yMouseAxisText = this.yMouseAxisText = svg.append("text").attr("x", margLeft - 6).attr("y", 0)
            .classed("yMouseAxisText mouseAxisText inactive", true);
        var xMouseAxisRectWidth = this.xMouseAxisRectWidth = 60;
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect = this.xMouseAxisRect = svg.append("rect").attr("y", height-margBot).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
            .classed("xMouseAxisRect mouseAxisRect inactive", true);
        var xMouseAxisText = this.xMouseAxisText = svg.append("text").attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust)
            .classed("xMouseAxisText mouseAxisText inactive", true);
        this.attachMouseHandler();
        mouseZoomInit.call(this);
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = PUMPCOMB_ANALYSIS;
            var margTop   = OBJ.margin.top;
            var margBot   = OBJ.margin.bottom;
            var margLeft  = OBJ.margin.left;
            var margRight = OBJ.margin.right;
            var m = d3.mouse(OBJ.mainGroup.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var xRaw_1 = xRaw + margLeft;
            var yRaw_1 = yRaw + margTop;
            var hMouseLine = OBJ.hMouseLine;
            var vMouseLine = OBJ.vMouseLine;
            var yMouseAxisRect = OBJ.yMouseAxisRect;
            var yMouseAxisText = OBJ.yMouseAxisText;
            var xMouseAxisRect = OBJ.xMouseAxisRect;
            var xMouseAxisText = OBJ.xMouseAxisText;
            var innerWidth = OBJ.innerWidth;
            var xInRange = xRaw * (innerWidth - xRaw) > 0;
            var innerHeight = OBJ.innerHeight;
            var yInRange = yRaw * (innerHeight - yRaw) > 0;
            var mouseIsOverChart = xInRange && yInRange;
            hMouseLine.classed("inactive", !mouseIsOverChart);
            vMouseLine.classed("inactive", !mouseIsOverChart);
            yMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yMouseAxisText.classed("inactive", !mouseIsOverChart);
            xMouseAxisRect.classed("inactive", !mouseIsOverChart);
            xMouseAxisText.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hMouseLine.attr("y1", yRaw).attr("y2", yRaw).attr("x2", xRaw);
                vMouseLine.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                yMouseAxisRect.attr("y", yRaw_1 - OBJ.yMouseAxisRectHeight/2);
                yMouseAxisText.attr("y", yRaw_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(OBJ.y.invert(yRaw)));
                xMouseAxisRect.attr("x", xRaw_1 - OBJ.xMouseAxisRectWidth/2);
                xMouseAxisText.attr("x", xRaw_1).text("%5.1f".sprintf(OBJ.x.invert(xRaw)));
            }
        });
    },
    onActivate : function(){
        this.attachMouseHandler();
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        this.chartResizerElt.style("display", "none");
    }
};
PUCO = {
    // object for usage in pump combination analysis and pump combination comparison
    captions : {
        eff : "efficiency (%)",
        head : "TDH (psi)",
        shaft : "shaft power (KW)",
        hydro : "hydro power (KW)"
    },
    funLegendTitles : {
        eff : "efficiency function",
        head : "TDH function",
        shaft : "shaft power function",
        hydro : "hydro power function"
    },
    hintFormatter : {
        eff : function(title, x, y){
        	return "<label>" + title + "</label><br/>" +
                "Flow: " + flowFormat.sprintf(x) + " MGD<br/>" +
                "Efficiency: " + effFormat.sprintf(y) + " %";
        },
        head : function(title, x, y){
        	return "<label>" + title + "</label><br/>" +
                "Flow: " + flowFormat.sprintf(x) + " MGD<br/>" +
                "TDH: " + tdhFormat.sprintf(y) + " psi";
        },
        shaft : function(title, x, y){
        	return "<label>" + title + "</label><br/>" +
                "Flow: " + flowFormat.sprintf(x) + " MGD<br/>" +
                "Shaft Power: " + shaftFormat.sprintf(y) + " KW";
        },
        hydro : function(title, x, y){
        	return "<label>" + title + "</label><br/>" +
                "Flow: " + flowFormat.sprintf(x) + " MGD<br/>" +
                "Hydro Power: " + hydroFormat.sprintf(y) + " KW";
        }
    },
    makeVerticalAxisButtons: function(prefix, containerId, host){
		var pre = "#" + prefix;
        var btn1 = HtmlGen.eisBtn("efficiency", {id: prefix + "VAeff", "class": "big chosen"});
        var btn2 = HtmlGen.eisBtn("TDH", {id: prefix + "VAhead", "class": "big"});
        var btn3 = HtmlGen.eisBtn("shaft power", {id: prefix + "VAshaft", "class": "big"});
        var btn4 = HtmlGen.eisBtn("hydro power", {id: prefix + "VAhydro", "class": "big"});
        $("#" + containerId).html("vertical axis: &nbsp; " + btn1 + btn2 + btn3 + btn4);
        $(pre + "VAeff").click(function(){
            host.onVerticalAxis = "eff"; host.makeChart();
            $(pre + "VAeff").addClass("chosen"); $(pre + "VAhead").removeClass("chosen");
            $(pre + "VAshaft").removeClass("chosen"); $(pre + "VAhydro").removeClass("chosen");
        });
        $(pre + "VAhead").click(function(){
            host.onVerticalAxis = "head"; host.makeChart();
            $(pre + "VAeff").removeClass("chosen"); $(pre + "VAhead").addClass("chosen");
            $(pre + "VAshaft").removeClass("chosen"); $(pre + "VAhydro").removeClass("chosen");
        });
        $(pre + "VAshaft").click(function(){
            host.onVerticalAxis = "shaft"; host.makeChart();
            $(pre + "VAeff").removeClass("chosen"); $(pre + "VAhead").removeClass("chosen");
            $(pre + "VAshaft").addClass("chosen"); $(pre + "VAhydro").removeClass("chosen");
        });
        $(pre + "VAhydro").click(function(){
            host.onVerticalAxis = "hydro"; host.makeChart();
            $(pre + "VAeff").removeClass("chosen"); $(pre + "VAhead").removeClass("chosen");
            $(pre + "VAshaft").removeClass("chosen"); $(pre + "VAhydro").addClass("chosen");
        });
        
    },
    makeChartAssist : function(code, onVertAxis){
        var pumpsList = API.pumpsListFromCode(code);
        // prepare functions
        var pumpsInd = pumpsList.map(function(p){ return p-1});
        this.pumpsInd = pumpsInd;
        var headCoeff = API.formulas.flow2Head[code];
        var hydroCoeff = API.formulas.flow2TotalHydro[code];
        var shaftCoeff = API.formulas.flow2TotalShaft[code];
        var charCurve = makeLinearFunction(headCoeff);
        var hydroFun = makeQuadraticFunction(hydroCoeff);
        var shaftFun = makeLinearFunction(shaftCoeff);
        var effFun = times100(divideFunctions(hydroFun, shaftFun));
        // take care of horizontal range
        var minFlow = API.minFlowFromCode[code];
        var maxFlow = API.maxFlowFromCode[code];
        var xMin = Math.max(0, 2.5*minFlow - 1.5*maxFlow);
        var xMax = 2.5*maxFlow - 1.5*minFlow;
        var yMin, yMax, plotFun, d, points;
        var bf = API.formulas.bepFlow[code];
        xMax = Math.max(bf + 16, xMax);
        xMin = Math.max(0, Math.min(bf-16, xMin));
        // prepare data points
        var rows = API.finalTable.pick(API.formulas.rowsWherePumpsRunOnly[code]);
        function getFunctionRange(fun){ // helper for vertical range
            var table = UTIL.range(xMin, xMax).map(fun);
            yMin = table.min();
            yMax = table.max();
        }
        switch(onVertAxis){
            case "eff":
                plotFun = effFun;
                getFunctionRange(plotFun);
                d = 0.2;
                points = rows.map(function(row){ return [row.flow, 100*row.eff, row];});
                break;
            case "head":
                plotFun = charCurve;
                yMin = plotFun(xMax);
                yMax = plotFun(xMin);
                d = 0.3;
                points = rows.map(function(row){ return [row.flow, row.avgHead, row];});
                break;
            case "shaft":
                plotFun = shaftFun;
                var temp1 = plotFun(xMax);
                var temp2 = plotFun(xMin);
                yMin = Math.min(temp1, temp2);
                yMax = Math.max(temp1, temp2);
                d = 5;
                points = rows.map(function(row){ return [row.flow, row.totalShaft, row];});
                break;
            case "hydro":
                plotFun = hydroFun;
                getFunctionRange(plotFun);
                points = rows.map(function(row){ return [row.flow, row.hydroPow, row];});
                d = 6;
                break;
            default:
                debugger; // we should never get here!
        }
        var ys = points.map(entry1);
        var yDataMax = ys.max();
        var yDataMin = ys.min();
        yMax = Math.max(yMax, yDataMax);
        yMin = Math.min(yMin, yDataMin);
        var diff = yMax - yMin;
        yMax += ( 0.1 * diff + d );
        yMin -= ( 0.1 * diff + d );
        return {
            xMin: xMin,
            xMax: xMax,
            yMin: yMin,
            yMax: yMax,
            plotFun : plotFun,
            points: points
        };
    },
    yRangeMemory : function(OBJ){
        var lastYminName = "last_yMin_" + OBJ.onVerticalAxis;
        var lastYmaxName = "last_yMax_" + OBJ.onVerticalAxis;
        var yManualName = "yAxisManual_" + OBJ.onVerticalAxis;
        if (!(lastYminName in OBJ) || !OBJ[yManualName]){
        	OBJ[lastYminName] = OBJ.yMin;
        }
        if (!(lastYmaxName in OBJ) || !OBJ[yManualName]){
        	OBJ[lastYmaxName] = OBJ.yMax;
        }
        if (OBJ[yManualName]){
            OBJ.yMin = OBJ[lastYminName];
            OBJ.yMax = OBJ[lastYmaxName];
        } else {
        }
    }
};
/***************************************************************************************************************
******************************************* PUMPCOMB_COMPARISON *************************************************
****************************************************************************************************************/
PUMPCOMB_COMPARISON = {
    hasResizeableChart : true,
    dotRadius : 2.5,
    pointsOnLine : 111,
    margin : {top: 18, right: 14, bottom: 52, left: 62},
    height0 : 600,
    aspect : 1.7,
    xMouseAxisRectHeight : 18,
    epsilonForExtendingTheLineAlittleBitLeftAndRight : 0.03,
    xAxisLabelVerticalPositionAdjust : 12,
    yAxisLabelHorizontalAdjust : 20,
    xMouseAxisTextVerticalAdjust : 4,
    legendWidth : 195,
    legendLine_1_fromTop : 20,
    legendLine_2_fromTop : 42,
    code1 : 2 + 4 + 128,  // 2, 3, 8
    code2 : 2 + 8,        // 2, 4
    onVerticalAxis : "eff", // will be "eff", "head", "shaft", or "hydro"
    comboChoiceDone: function(code, which){
        this.makeChart();
    },
    make : function(){
        this.container = d3.select("#tdPumpCombCompChart");
        /*
        <div id="pccControls">
            <div id="pcc1stComboControls"></div>
            <div id="pcc2ndComboControls"></div>
        </div>
        <div id="pumpCombCompVerticalAxisButtons"></div>
        */
        this.btnCode1 = this.code1;
        this.btnCode2 = this.code2;
        makeComboChooserInterface("pcc1stComboControls", "pcc1st", this, "code1", "btnCode1", 1);
        makeComboChooserInterface("pcc2ndComboControls", "pcc2nd", this, "code2", "btnCode2", 2);
        var id1 = "pcc1stBtnCombo" + this.code1; var elt1 = document.getElementById(id1);
        if (elt1){
            $(elt1).addClass("chosen");
            this.btnCode1 = this.code1;
        } else {
            this.btnCode1 = "";
        }
        var id2 = "pcc2ndBtnCombo" + this.code2; var elt2 = document.getElementById(id2);
        if (elt2){
            $(elt2).addClass("chosen");
            this.btnCode2 = this.code2;
        } else {
            this.btnCode2 = "";
        }
        setCheckboxesToCode("pcc1stPumpComboChooserCheckbox", this.code1);
        setCheckboxesToCode("pcc2ndPumpComboChooserCheckbox", this.code2);
        this.makeVerticalAxisButtons();
        
        var pumpComboComparisonYRangeAutoStuffParentTd = jQuery("#pumpComboComparisonYRangeAutoStuffParentTd");
        pumpComboComparisonYRangeAutoStuffParentTd.html(
            HtmlGen.table(HtmlGen.tr(
                HtmlGen.td("", {id: "pumpComboComparisonYRange_eff_parent"}) +
                HtmlGen.td("", {id: "pumpComboComparisonYRange_head_parent"}) +
                HtmlGen.td("", {id: "pumpComboComparisonYRange_shaft_parent"}) +
                HtmlGen.td("", {id: "pumpComboComparisonYRange_hydro_parent"})
            ))
        );
        var td_eff   = jQuery("#pumpComboComparisonYRange_eff_parent");
        var td_head  = jQuery("#pumpComboComparisonYRange_head_parent");
        var td_shaft = jQuery("#pumpComboComparisonYRange_shaft_parent");
        var td_hydro = jQuery("#pumpComboComparisonYRange_hydro_parent");
        this.yRangeParent_eff   = td_eff  ;
        this.yRangeParent_head  = td_head ;
        this.yRangeParent_shaft = td_shaft;
        this.yRangeParent_hydro = td_hydro;
        makeYAxisRangeStuff(td_eff  , this.myName + "_eff"  , this, {what: "eff"  }, true , "yAxisManual_eff");
        makeYAxisRangeStuff(td_head , this.myName + "_head" , this, {what: "head" }, false, "yAxisManual_head");
        makeYAxisRangeStuff(td_shaft, this.myName + "_shaft", this, {what: "shaft"}, false, "yAxisManual_shaft");
        makeYAxisRangeStuff(td_hydro, this.myName + "_hydro", this, {what: "hydro"}, false, "yAxisManual_hydro");
        td_head.hide();
        td_shaft.hide();
        td_hydro.hide();
        
        this.makeChart();
    },
    setYaxisAuto: function(args){
        this["yAxisManual_" + args.what] = false;
        console.log(this.myName + " set y axis auto: " + args.what);
    },
    setYaxisManual: function(args){
        this["yAxisManual_" + args.what] = true;
        console.log(this.myName + " set y axis manual: " + args.what);
    },
    setYaxisAction: function(args){
        console.log(this.myName + " set y axis action "  + args.what);
    },
    UNUSED_makeComboChosenFunction_UNUSED : function(code, wind, labelId, varName){
        // was used in conjunction with pop up window
        return function(){
            var theCode;
            if ( code == "check"){
                theCode = getCodeFromCheckboxes("customComboCheck", wind);
                if (theCode == 0) return;
            } else {
                theCode = code;
            }
            $("#" + labelId).html(API.pumpsListFromCode(theCode).toString());
            wind.close();
            PUMPCOMB_COMPARISON[varName] = theCode;
            PUMPCOMB_COMPARISON.makeChart();
        }
    },
    UNUSED_makeComboChangeHandler_UNUSED : function(labelId, windName, varName){
        // was used in conjunction with pop up window
        return function(){
            var wind = window.open("", windName, "width=800,height=600,menubar=no,location=no,status=no,titlebar=no,toolbar=no");
            wind.document.write(makePumpComboChooserWindowHTML());
            wind.focus();
            for (var i=0; i<API.common.length; i++){
                var code = API.common[i];
                $(wind.document.getElementById("pumpComboChoiceButton" + code)).click(PUMPCOMB_COMPARISON.makeComboChosenFunction(code, wind, labelId, varName));
            }
            customBtn = $(wind.document.getElementById("pickCustomCombo")); 
            customBtn.click(PUMPCOMB_COMPARISON.makeComboChosenFunction("check", wind, labelId, varName));
        }
    },
    makeVerticalAxisButtons: function(){
        PUCO.makeVerticalAxisButtons("pcc", "pumpCombCompVerticalAxisButtons", PUMPCOMB_COMPARISON);
    },
    makeXtrafo : function(){
        return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
    },
    makeYtrafo : function(){
        return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMin, this.yMax]);
    },
    updateChart: function(xMin, xMax, yMin, yMax){
        var container = this.container;
        var pumpsList1 = this.pumpsList1;
        var pumpsList2 = this.pumpsList2;
        var myName = this.myName;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        
        var lastYminName = "last_yMin_" + this.onVerticalAxis;
        var lastYmaxName = "last_yMax_" + this.onVerticalAxis;
        this[lastYminName] = yMin;
        this[lastYmaxName] = yMax;
        
        var plotFun1 = this.plotFun1;
        var plotFun2 = this.plotFun2;
        var svg = this.svg;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup;
        mainGroup.translate(margLeft, margTop);
        this.invisiblePlaceholderRect.attr("width", width).attr("height", height);

        var clipRect = this.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        
        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup;
        xAxisGroup.selectAll("g.tick").remove();        //  not really necessary, but doesn't hurt either
        xAxisGroup.selectAll("path.domain").remove();   // ditto
        xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel;
        xAxisLabel.attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup;
        yAxisGroup.selectAll("g.tick").remove();        //  not really necessary, but doesn't hurt either
        yAxisGroup.selectAll("path.domain").remove();   // ditto
        yAxisGroup.call(yAxis);
        if (this.onVerticalAxis == "eff"){
            yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        }
        var yAxisLabel = this.yAxisLabel;
        yAxisLabel.attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust);
        var clipGroup = this.clipGroup;
        clipGroup.selectAll("circle.pucoComp")
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return y(d[1]); });
        // update the function plot
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var pointsOnCurve_1 = xxx.map(function(x){
            return [x, plotFun1(x)];
        });
        var pointsOnCurve_2 = xxx.map(function(x){
            return [x, plotFun2(x)];
        });
        var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll("path.functionLine.yin").data([pointsOnCurve_1]).attr({
            "d" : functionLine
        });
        clipGroup.selectAll("path.functionLine.yang").data([pointsOnCurve_2]).attr({
            "d" : functionLine
        });
        this.legendGroup.translate(width - this.legendWidth, 0);
        this.vMouseLine.attr("y1", innerHeight);
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        this.xMouseAxisRect.attr("y", height-margBot);
        this.xMouseAxisText.attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust);
    },
    makeChart: function(){
        this.yRangeParent_eff.hide();
        this.yRangeParent_head.hide();
        this.yRangeParent_shaft.hide();
        this.yRangeParent_hydro.hide();
        this["yRangeParent_" + this.onVerticalAxis].show();
        var myName = this.myName;
        var container = this.container;
        var pumpsList1 = API.pumpsListFromCode(this.code1);
        var pumpsList2 = API.pumpsListFromCode(this.code2);
        this.pumpsList1 = pumpsList1;
        this.pumpsList2 = pumpsList2;
        makeTwoCombosInfoTable(this.code1, this.code2, "pumpCombCompRightTop", "pccStatsHandle", "pccStatsMinimizer");
        //$("#pumpCombCompChart").html("combo1: " + pumpsList1.toString() + " &nbsp; combo2: " + pumpsList2.toString() + " va: " + this.onVerticalAxis);
        var obj1 = PUCO.makeChartAssist(this.code1, this.onVerticalAxis, this);
        var obj2 = PUCO.makeChartAssist(this.code2, this.onVerticalAxis, this);
        var xMin = this.xMin = Math.min(obj1.xMin, obj2.xMin);
        var xMax = this.xMax = Math.max(obj1.xMax, obj2.xMax);
                   this.yMin = Math.min(obj1.yMin, obj2.yMin);
                   this.yMax = Math.max(obj1.yMax, obj2.yMax);
        /*
        var lastYminName = "last_yMin_" + this.onVerticalAxis;
        var lastYmaxName = "last_yMax_" + this.onVerticalAxis;
        var yManualName = "yAxisManual_" + this.onVerticalAxis;
        if (!(lastYminName in this) || !this[yManualName]){
        	this[lastYminName] = yMin;
        }
        if (!(lastYmaxName in this) || !this[yManualName]){
        	this[lastYmaxName] = yMax;
        }
        if (this[yManualName]){
            yMin = this.yMin = this[lastYminName];
            yMax = this.yMax = this[lastYmaxName];
        } else {
        }
        */
        PUCO.yRangeMemory(this);
        var yMin = this.yMin;
        var yMax = this.yMax;
        
        var plotFun1 = this.plotFun1 = obj1.plotFun;
        var plotFun2 = this.plotFun2 = obj2.plotFun;
        var verticalAxisCaption = PUCO.captions[this.onVerticalAxis];
        var horizontalAxisCaption = "flow (MGD)";
        container.html("");
        var svg = this.svg = container.append("svg");
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        svg.attr("width", width).attr("height", height).classed(myName, true);
        var margTop   = this.margin.top;
        var margBot   = this.margin.bottom;
        var margLeft  = this.margin.left;
        var margRight = this.margin.right;
        var innerWidth = this.innerWidth = width - margLeft - margRight;
        var innerHeight = this.innerHeight = height - margTop - margBot;
        var mainGroup = this.mainGroup = svg.append("g").translate(margLeft, margTop).classed("mainGroup", true);
		this.invisiblePlaceholderRect = mainGroup.append("rect").attr("width", width).attr("height", height).attr("stroke", "none").attr("fill", "transparent");

        var clipPath = this.clipPath = mainGroup.append("clipPath").attr("id", "pumpComboComparisonClipPath");
        var clipRect = this.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
        var clipGroup = this.clipGroup = mainGroup.append("g").classed("clipGroup", true)
            .style("clip-path", "url(#pumpComboComparisonClipPath)");


        var x = this.x = this.makeXtrafo();
        var y = this.y = this.makeYtrafo();
        var xAxis = this.xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = this.yAxis = d3.svg.axis().scale(y).orient("left");
        var xAxisGroup = this.xAxisGroup = mainGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
        var xAxisLabel = this.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text(horizontalAxisCaption).attr("x", innerWidth/2).attr("y", margBot - this.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = this.yAxisGroup = mainGroup.append("g").classed("axis yAxis", true).call(yAxis);
        if (this.onVerticalAxis == "eff"){
            yAxisGroup.selectAll("g.tick").filter(function(d){ return d > 100; }).remove();
        }
        var yAxisLabel = this.yAxisLabel = yAxisGroup.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
            .text(verticalAxisCaption).attr("x", -innerHeight/2).attr("y", -margLeft + this.yAxisLabelHorizontalAdjust).style("text-anchor", "middle");
        clipGroup.selectAll(".honestPolitician").data(obj1.points).enter().append("circle").classed("pucoComp yin", true)
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return y(d[1]); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d){
                var row = d[2];
                var containerId = "pumpCombCompRightBottom";
                var chosenColumnIndex = -1;
                var tableId      = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId  = myName + "Minimizer";
                var captionId    = myName + "MouseOverCaption";
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            });
        clipGroup.selectAll(".honestPolitician").data(obj2.points).enter().append("circle").classed("pucoComp yang", true)
            .attr("cx", function(d) { return x(d[0]); })
            .attr("cy", function(d) { return y(d[1]); })
            .attr("r", this.dotRadius)
            .on("mouseover", function(d){
                var row = d[2];
                var containerId = "pumpCombCompRightBottom";
                var chosenColumnIndex = -1;
                var tableId      = myName + "HintTable";
                var dragHandleId = myName + "HintHandle";
                var minimizerId  = myName + "Minimizer";
                var captionId    = myName + "MouseOverCaption";
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            });
        // draw the selected function
        var epsilon = this.epsilonForExtendingTheLineAlittleBitLeftAndRight;
        var pointsOnLine = this.pointsOnLine;
        var xLo = (1+epsilon) * xMin - epsilon * xMax;
        var xHi = (1+epsilon) * xMax - epsilon * xMin;
        var stepSize = (xHi - xLo)/(pointsOnLine-1)*0.9999999;
        var xxx = d3.range(xLo, xHi, stepSize);
        var pointsOnCurve_1 = xxx.map(function(x){
            return [x, plotFun1(x)];
        });
        var pointsOnCurve_2 = xxx.map(function(x){
            return [x, plotFun2(x)];
        });
        var functionLine = d3.svg.line().x(function(d,i,j){ return x(d[0]); }).y(function(d,i,j){ return y(d[1]); });
        clipGroup.selectAll(".honestPolitician").data([pointsOnCurve_1]).enter().append("path").attr({
            "fill" : "none", "d" : functionLine
        }).classed("functionLine " + this.onVerticalAxis + " yin", true);
        clipGroup.selectAll(".honestPolitician").data([pointsOnCurve_2]).enter().append("path").attr({
            "fill" : "none", "d" : functionLine
        }).classed("functionLine " + this.onVerticalAxis + " yang", true);
        // zoom and pan
        var THIS = this;
        svg.call(makePanDragBehavior(this.myName));
        // mouse lines
        var hMouseLine = this.hMouseLine = mainGroup.append("line")
            .attr("x1", 0).attr("x2", innerWidth).attr("y1", 0).attr("y2", 0)
            .attr("id", myName + "_H_MouseLine").classed("mouseLine horizontal inactive", true);
        var vMouseLine = this.vMouseLine = mainGroup.append("line")
            .attr("y1", innerHeight).attr("x1", 0).attr("x2", 0)
            .attr("id", myName + "_V_MouseLine").classed("mouseLine vertical inactive", true);
        var yMouseAxisRectWidth = margLeft - 5;
        var yMouseAxisRectHeight = this.yMouseAxisRectHeight = 18;
        var yMouseAxisRect = this.yMouseAxisRect = svg.append("rect").attr("x", margLeft-yMouseAxisRectWidth).attr("width", yMouseAxisRectWidth)
            .attr("height", yMouseAxisRectHeight).classed("yMouseAxisRect mouseAxisRect inactive", true);
        var yMouseAxisText = this.yMouseAxisText = svg.append("text").attr("x", margLeft - 6).attr("y", 0)
            .classed("yMouseAxisText mouseAxisText inactive", true);
        var xMouseAxisRectWidth = this.xMouseAxisRectWidth = 60;
        var xMouseAxisRectHeight = this.xMouseAxisRectHeight;
        var xMouseAxisRect = this.xMouseAxisRect = svg.append("rect").attr("y", height-margBot).attr("width", xMouseAxisRectWidth).attr("height", xMouseAxisRectHeight)
            .classed("xMouseAxisRect mouseAxisRect inactive", true);
        var xMouseAxisText = this.xMouseAxisText = svg.append("text").attr("y", height-margBot+xMouseAxisRectHeight-this.xMouseAxisTextVerticalAdjust)
            .classed("xMouseAxisText mouseAxisText inactive", true);
        this.attachMouseHandler();
        // legend
        var legendGroup = this.legendGroup = svg.append("g").classed("legendGroup", true).attr("id", "pumpComboComparisonLegendGroup").translate(width - this.legendWidth, 0);
        legendGroup.append("text").text("first combo " + this.pumpsList1.join(",")).classed("yin pumpComboComparisonText", true).attr("x", 0).attr("y", this.legendLine_1_fromTop);
        legendGroup.append("text").text("second combo " + this.pumpsList2.join(",")).classed("yang pumpComboComparisonText", true).attr("x", 0).attr("y", this.legendLine_2_fromTop);
        mouseZoomInit.call(this);
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = PUMPCOMB_COMPARISON;
            var margTop   = OBJ.margin.top;
            var margBot   = OBJ.margin.bottom;
            var margLeft  = OBJ.margin.left;
            var margRight = OBJ.margin.right;
            var m = d3.mouse(OBJ.mainGroup.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var xRaw_1 = xRaw + margLeft;
            var yRaw_1 = yRaw + margTop;
            var hMouseLine = OBJ.hMouseLine;
            var vMouseLine = OBJ.vMouseLine;
            var yMouseAxisRect = OBJ.yMouseAxisRect;
            var yMouseAxisText = OBJ.yMouseAxisText;
            var xMouseAxisRect = OBJ.xMouseAxisRect;
            var xMouseAxisText = OBJ.xMouseAxisText;
            var innerWidth = OBJ.innerWidth;
            var xInRange = xRaw * (innerWidth - xRaw) > 0;
            var innerHeight = OBJ.innerHeight;
            var yInRange = yRaw * (innerHeight - yRaw) > 0;
            var mouseIsOverChart = xInRange && yInRange;
            hMouseLine.classed("inactive", !mouseIsOverChart);
            vMouseLine.classed("inactive", !mouseIsOverChart);
            yMouseAxisRect.classed("inactive", !mouseIsOverChart);
            yMouseAxisText.classed("inactive", !mouseIsOverChart);
            xMouseAxisRect.classed("inactive", !mouseIsOverChart);
            xMouseAxisText.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hMouseLine.attr("y1", yRaw).attr("y2", yRaw).attr("x2", xRaw);
                vMouseLine.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                yMouseAxisRect.attr("y", yRaw_1 - OBJ.yMouseAxisRectHeight/2);
                yMouseAxisText.attr("y", yRaw_1 + OBJ.yMouseAxisRectHeight/2-3).text("%5.1f".sprintf(OBJ.y.invert(yRaw)));
                xMouseAxisRect.attr("x", xRaw_1 - OBJ.xMouseAxisRectWidth/2);
                xMouseAxisText.attr("x", xRaw_1).text("%5.1f".sprintf(OBJ.x.invert(xRaw)));
            }
        });
    },
    onActivate : function(){
        this.attachMouseHandler();
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        this.chartResizerElt.style("display", "none");
    }
};
