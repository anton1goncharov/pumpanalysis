// ophrsEff.js

var minEff = 79;
var maxEff = 85;
        var minEff1 = minEff / 100;
        var maxEff1 = (1+maxEff) / 100;

function FlowRange(from, to, comboList){
    this.from = from;                 // from from MGD to to MGD is the range in question
    this.to = to;
    this.comboList = comboList;       // list of pump combinations (code (binary rep)) the mean flow of which is in that range
    this.inited = false;
    this.calcEffBinCounts();          // used in "summary2"
    this.visible = comboList.map(function(){ return true;});  // visible[i] means that pump combination with index i (from this.comboList) is currently visible (in flow-head chart) 
    this.chartInited = false;
    this.chartVisible = false;
}
FlowRange.prototype.calcEffBinCounts = function(){
    this.effBinCountDetails = [];
    this.effBinCount = [];
    for (var eff = minEff; eff <= maxEff; eff++){
        this.effBinCountDetails[eff] = this.comboList.map(function(combo){
            return API.effBinsFromCode[combo][eff];
        });
        this.effBinCount[eff] = this.effBinCountDetails[eff].sum();
    }
    // effBinCount[e] = n means that for n hours, the pump combos in the flow range "this" had an efficiency between e-0.5 and e+0.5
    // effBinCount[e][i] = n means that this.comboList[i] (one of the combos in the flow range "this") had n operating hours where it had an efficiency between e-0.5 and e+0.5
}


OPHRSEFF = {
    hasResizeableChart : false  // (this is for the tabs which have ONE resizeable chart)
    , headingText : "Potomac Main Zone - Operating Hours &amp; Efficiencies by Flow Range"
    , showFlowHeadBtnCaption :  "show flow-head chart"
    , hideFlowHeadBtnCaption :  "hide flow-head chart"
    , epsilon : 0.3
    , SLIDER : {
        // everything that has to do with the "multi-range-slider" on top, determining the flow ranges is here in sub-object slider
          numHandles : null
        , mini : null
        , maxi : null
        , values0 : null
        , values : null
        , hhw : 10 // "half handle width", namely width attribute of class .rangeGrip in file multiRangeSliderTest.css, and half the width (in pixels) of the image referred to in the same class
        , refreshCallDue2WindowResizeEvt : null
        , registerWindowResizeHandler : function(){
            $(window).bind("resize", this.windowResize);
        }
        , windowResize : function(evt){
            if (OPHRSEFF.SLIDER.refreshCallDue2WindowResizeEvt){
                window.clearTimeout(OPHRSEFF.SLIDER.refreshCallDue2WindowResizeEvt);
            }
            OPHRSEFF.SLIDER.refreshCallDue2WindowResizeEvt = window.setTimeout(OPHRSEFF.SLIDER.refresh, 80);
        }
        , refresh : function(){
            OPHRSEFF.SLIDER.refreshCallDue2WindowResizeEvt = null;
            OPHRSEFF.SLIDER.updateInfo();
            OPHRSEFF.SLIDER.addButtons();
            var toolHeading = $("#FlowRangeSizingToolHeader");
            var top = Math.round($("#ophrsEffKontent").position().top - 10);
            toolHeading.css("top", top);
        }
        , init : function(values, containerId, topId, parentId, labelsId, buttonsId, tableId){
            
            //console.log(values.join(" - "));
            
            var numHandles, numTds, mini, maxi, values0, communism;
            if (typeof values == "number"){
                mini = 0; maxi = 1;
                numHandles = values - 0;
                numTds = numHandles + 1;
                values0 = UTIL.range(0, numTds).map(function(i){return i/numTds;});
                values = values0;
                communism = true;
            } else {
                mini = values[0]; maxi = values[values.length - 1];
                numHandles = values.length - 2;
                numTds = numHandles + 1;
                var width = maxi - mini;
                values0 = values.map(function(entry){return (entry-mini)/width;});
                communism = false;
            }
            this.mini = mini; this.maxi = maxi; this.values0 = values0; this.values = values; // this.numHandles = numHandles;
            if (containerId){ this.containerId = containerId; } else { containerId = this.containerId; }
            if (topId){ this.topId = topId;} else { topId = this.topId; }
            if (parentId) { this.parentId = parentId; } else {parentId = this.parentId; }
            if (labelsId) { this.labelsId = labelsId; } else {labelsId = this.labelsId; }
            if (buttonsId) { this.buttonsId = buttonsId; } else { buttonsId = this.buttonsId; }
            if (tableId) { this.tableId = tableId; } else {
                tableId = this.tableId;
                jQuery("#" + tableId).colResizable({disable:true});
            }
            // fieldset and legend don't behave well in all browsers, so the frame is done "manually".
            // remnants of how it would be with fieldset are left below (ineffective calls for demonstration.)
            // [replace HtmlGenFieldset by HtmlGen.fieldset to see what it would look like (after removing the manual solution)]
            var HtmlGenFieldset = function(content){ return content; };
            var HtmlGenLegend = function(){ return ""; };
            var html = HtmlGenFieldset(
                HtmlGenLegend("Flow Range Sizing Tool") +
                HtmlGen.eisBtn("configure flow ranges", {id: "configureFlowBucketsBtn", "class" : "big"}) +
                HtmlGen.div(
                    HtmlGen.div("", {id: parentId}) + HtmlGen.div("", {id: labelsId}) + HtmlGen.div("", {id: buttonsId})
                    , {id: topId}
                ) + HtmlGen.div("", {id: "bucketExtra"})
                + HtmlGen.div(HtmlGen.span("", {id: "warningAboutTooLargeFlowRange"}) + HtmlGen.eisBtn("done", {id: "doneConfiguringFlowBucketsBtn", "class" : "big"}), {id: "doneConfiguringFlowBucketsBtnParentDiv"})
            );
            var container = jQuery("#" + containerId);
            container.empty();
            container.html(html);
            jQuery("#configureFlowBucketsBtn").hide();
            jQuery("#" + topId).addClass("multiRangeSliderContainer");
            jQuery("#" + parentId).addClass("multiRangeSlider");
            var tableHtml = HtmlGen.table(HtmlGen.tr(UTIL.range(0, numHandles).map(function(){ return "<td></td>";}).join("")), {id: tableId});
            jQuery("#" + parentId).html(tableHtml);
            jQuery("#" + tableId).addClass("multiRangeSliderTable");
            if (communism){
                var widthPortion = 100/numTds;
                widthPortion += "%";
                jQuery("#" + tableId + " td").css("width", widthPortion);
            } else {
                var lastVal = values0[numTds];
                var totalWidth = $("#" + tableId).width();
                var naivePercentages = values0.map(function(x){ return x/lastVal; }).differences();
                var reducedWidth = totalWidth - numTds*this.hhw - 1;
                var percentages = naivePercentages.map(function(p, i){ return (100*(p*reducedWidth + OPHRSEFF.SLIDER.hhw + (i == 0 ? 1 : 0))/totalWidth) + "%";});
                var tds = $("#" + tableId + " td");
                for (var i=0; i<tds.length; i++){
                    var td = $(tds[i]);
                    td.css("width", percentages[i]);
                }
            }
            $("#" + tableId).colResizable({
                liveDrag:true, 
                draggingClass:"rangeDrag", 
                gripInnerHtml:"<div class='rangeGrip'></div>", 
                onDrag:this.onSliderSlide,
                onResize:this.onSliderStop,
                minWidth:8   // seems to be 2 less than width of .rangeGrip, see file multiRangeSliderTest.css
            });
            this.updateInfo();
            this.addButtons();
            OPHRSEFF.updateExtra();
            jQuery("#doneConfiguringFlowBucketsBtn").click(function(){OPHRSEFF.bucketConfigDone.call(OPHRSEFF)});
            // now, put the heading on the frame around the tool
            var containerElt = $("#" + this.containerId);
            containerElt.append(HtmlGen.div(" &nbsp; Flow Range Sizing Tool", {id: "FlowRangeSizingToolHeader"}));
            var toolHeading = $("#FlowRangeSizingToolHeader");
            var top = Math.round($("#ophrsEffKontent").position().top - 10);
            toolHeading.css("top", top);
        }
        , onSliderSlide : function(evt){
            OPHRSEFF.SLIDER.retrieveSetting(evt);
            OPHRSEFF.SLIDER.updateInfo();
            OPHRSEFF.updateExtra();
        }
        , onSliderStop : function(evt){
            OPHRSEFF.SLIDER.addButtons();
        }
        , getPosList: function(){
            var grips = $("div.JCLRgrip, div.JCLRLastGrip");
            return [0].concat(UTIL.range(0, grips.length-1).map(function(index){ var grip = $(grips[index]); return parseInt(grip.css("left")); }));
        }
        , retrieveSetting: function(evt){
            // evt.currentTarget = on what you called .colResizable
            if (!evt){
                evt = {currentTarget: document.getElementById(this.tableId)};
            }
        	var columns = $(evt.currentTarget).find("td");
        	var widths = [], total = 0, i, width;
        	for(i = 0; i<columns.length; i++){
        		width = columns.eq(i).width()-this.hhw - (i==0?1:0);
        		widths.push(width);
        		total+=width;
       	    }
            this.widths = widths;
            var normalizedResult = widths.accumulate().map(function(arg){ return arg/total;});
            this.values0 = normalizedResult;
            //this.values = normalizedResult.linTrafo(this.maxi - this.mini, this.mini);
            this.values = this.denormalize(normalizedResult).roundAll();
            return this.values;
        }
        , denormalize : function(v){
            return v.linTrafo(this.maxi - this.mini, this.mini);
        }
        , updateInfo: function(){
            var posList = this.getPosList();
            var labelParent = $("#" + this.labelsId);
            labelParent.empty();
            var backgroundClasses = ["evenTdInfo", "oddTdInfo"];
            var sliderTable = $("#" + this.tableId); 
            var top = sliderTable.position().top + 0.4 * sliderTable.height();
            var left0 = sliderTable.position().left;
            for (var i=0; i<posList.length-1; i++){
                labelParent.append(HtmlGen.div("%5.0f - %5.0f".sprintf(this.values[i], this.values[i+1]),{id: "infoForTd" + i}));
                var elt = $("#infoForTd" + i);
                var left = left0 + posList[i];
                elt.css({
                    position: "absolute",
                    left: left + "px",
                    top: top + "px" 
                })
                elt.addClass(backgroundClasses[i % backgroundClasses.length]);
                elt.width(posList[i+1] - posList[i]);
            }
        }
        , makeSplitButtonHandler : function(index){
            return function(){
                var old = OPHRSEFF.SLIDER.values0;
                var toBeInserted = 0.5  *(old[index] + old[index+1]);
                var values = old.slice(0, index + 1).concat([toBeInserted]).concat(old.slice(index+1));
                OPHRSEFF.SLIDER.init(OPHRSEFF.SLIDER.denormalize(values).roundAll());
            };
        }
        , makeSplitButtonMouseOverHandler : function(index){
            return function(){
                var ip1 = index + 1;
                var tds = $("#bucketExtra>table>tbody>tr:eq(" +ip1+ ")").find("td");
                var leftTd = $(tds[0]);
                var rightTd = $(tds[1]);
                var thickBlack = "3px black solid";
                tds.css("border-top", thickBlack);
                tds.css("border-bottom", thickBlack);
                leftTd.css("border-left", thickBlack);
                rightTd.css("border-right", thickBlack);
                //$("#FlowRangeSizingToolHeader").offset({top: $("#FlowRangeSizingToolHeader").offset().top - 4});
            }
        }
        , makeSplitButtonMouseOutHandler : function(index){
            return function(){
                var ip1 = index + 1;
                var tds = $("#bucketExtra>table>tbody>tr:eq(" +ip1+ ")").find("td");
                tds.css("border", "");
                //$("#FlowRangeSizingToolHeader").offset({top: $("#FlowRangeSizingToolHeader").offset().top + 4});
            }
        }
        , makeJoinButtonHandler : function(index){
            return function(){
                var old = OPHRSEFF.SLIDER.values0;
                var values = old.slice(0, index).concat(old.slice(index+1));
                OPHRSEFF.SLIDER.init(OPHRSEFF.SLIDER.denormalize(values).roundAll());
            };
        }
        , makeJoinButtonMouseOverHandler : function(index){
            return function(){
                var ip1 = index + 1;
                var tdsAAA = $("#bucketExtra>table>tbody>tr:eq(" +index+ ")").find("td");
                var tdsZZZ = $("#bucketExtra>table>tbody>tr:eq(" + ip1 + ")").find("td");
                var leftTdAAA = $(tdsAAA[0]);
                var rightTdAAA = $(tdsAAA[1]);
                var leftTdZZZ = $(tdsZZZ[0]);
                var rightTdZZZ = $(tdsZZZ[1]);
                var thickBlack = "3px black solid";
                tdsAAA.css("border-top", thickBlack);
                tdsZZZ.css("border-bottom", thickBlack);
                leftTdAAA.css("border-left", thickBlack);
                leftTdZZZ.css("border-left", thickBlack);
                rightTdAAA.css("border-right", thickBlack);
                rightTdZZZ.css("border-right", thickBlack);
                //$("#FlowRangeSizingToolHeader").offset({top: $("#FlowRangeSizingToolHeader").offset().top - 4});
            }
        }
        , makeJoinButtonMouseOutHandler : function(index){
            return function(){
                var ip1 = index + 1;
                var tdsAAA = $("#bucketExtra>table>tbody>tr:eq(" +index+ ")").find("td");
                var tdsZZZ = $("#bucketExtra>table>tbody>tr:eq(" + ip1 + ")").find("td");
                tdsAAA.css("border", "");
                tdsZZZ.css("border", "");
                //$("#FlowRangeSizingToolHeader").offset({top: $("#FlowRangeSizingToolHeader").offset().top + 4});
            }
        }
        , addButtons : function(){
            var imgWidth = 24;
            var imgHeight = 13;
            var halfImgWidth = 0.5 * imgWidth;
            var posList = this.getPosList();
            var btnParent = $("#bucketButtons");
            btnParent.empty();
            var td0 = $("#infoForTd0");
            if (!td0 || !td0.position()){
                //console.log("bad");
                return;
            }
            var top = td0.position().top + td0.height() + 3;
            var sliderTable = $("#" + this.tableId); 
            var left0 = sliderTable.position().left;
            // the split buttons look like <==> and are depicted in the middle of each range, they split the range in two halves.
            for (var i=0; i<posList.length-1; i++){
                var middleOfSplitBtn = left0 + 0.5*(posList[i] + posList[i+1]);
                var leftOfSplitBtn = middleOfSplitBtn - halfImgWidth;
                var btnId = "splitBtn" + i;
                btnParent.append(HtmlGen.img("", {
                    src : "img/split48x26.png",
                    width: imgWidth, height: imgHeight,
                    id: btnId,
                    title: "split range in half",
                    "class": "splitButton"
                }));
                var btn = $("#" + btnId);
                btn.css({
                    left: leftOfSplitBtn,
                    top: top
                });
                btn.tooltip();
                btn.click(this.makeSplitButtonHandler(i));
                btn.mouseover(this.makeSplitButtonMouseOverHandler(i));
                btn.mouseout(this.makeSplitButtonMouseOutHandler(i));
            }
            // the join button joins 2 adjacent ranges together and looks like this ==><== . those buttons are depicted where two different ranges meet.
            for (var i=1; i<posList.length-1; i++){
                var middleOfJoinBtn = left0 + posList[i];
                var leftOfJoinBtn = middleOfJoinBtn - halfImgWidth;
                var btnId = "joinBtn" + i;
                btnParent.append(HtmlGen.img("", {
                    src : "img/join48x26.png",
                    width: imgWidth, height: imgHeight,
                    id: btnId,
                    title: "join ranges into one",
                    "class": "joinButton"
                }));
                var btn = $("#" + btnId);
                btn.css({
                    left: leftOfJoinBtn,
                    top: top
                });
                btn.tooltip();
                btn.click(this.makeJoinButtonHandler(i));
                btn.mouseover(this.makeJoinButtonMouseOverHandler(i));
                btn.mouseout(this.makeJoinButtonMouseOutHandler(i));
            }
        }
    } // end of SLIDER
/*
<div id="ophrsEffByFlowRangeTab">
    <h3>Potomac Main Zone - Operating Hours &amp; Efficiencies by Flow Range</h3>
    <div id="ophrsEffKontent">
        <div id="ophrsEffMultiSlider"></div>
        <div id="ophrsEffMain"></div>
    </div>
</div>*/
    , SAVINGS : {
        // everything having to do with the expected savings calculation (at the top in each individual sub-tab for each flow range) is in this sub-object SAVINGS
        tdhToFlowIncreasePortionFunction : null, // a function which, given the tdh, will give back a number between 0 and 1, indicating which portion
                                                 // of an increase in hydraulic power will generate an increase in flow (the "other" portion, i.e. 1-result, gives increase in TDH)
                                                 // this function will be computed/generated in init
        init : function(){
            var changes = UTIL.range(0, API.finalTable.length-2).select(function(i){      // %%%
                var row1 = API.finalTable[i];
                var row2 = API.finalTable[i+1];
                return row1.runCode != row2.runCode;
            });
            var data = changes.map(function(i){
                var row1 = API.finalTable[i];
                var row2 = API.finalTable[i+1];
                var x = 0.5*(row1.avgHead + row2.avgHead);
                var y = Math.log(row2.flow/row1.flow) / Math.log(row2.hydroPow/row1.hydroPow);
                return [x, y];        
            });
            var dataWithoutOutliers = data.select(function(point){ return 0.5 <= point[1] && point[1] < 1});
            this.tdhToFlowIncreasePortionFunction = makeQuadraticFunction(fitDataQuadratic(dataWithoutOutliers));
        }
        , estimateSavingsAux: function(replacee, replacer){
            var oldHydro = API.meanHydroFromCode[replacee];
            var newHydro = API.meanHydroFromCode[replacer];
            var oldFlow = API.meanFlowFromCode[replacee];
            var logIncHydro = Math.log(newHydro/oldHydro);
            var oldTDH = API.meanHeadFromCode[replacee];
            var newTDH = API.meanHeadFromCode[replacer];
            var tdh = 0.5*(oldTDH + newTDH);
            var flowIncreasePortion = OPHRSEFF.SAVINGS.tdhToFlowIncreasePortionFunction(tdh);
            var flowIncreaseFactor = Math.exp(flowIncreasePortion * logIncHydro);
            var newFlow = oldFlow * flowIncreaseFactor;
            var oldOpHours = API.numOpHours[replacee];
            var newOpHours = oldOpHours / flowIncreaseFactor;
            var oldShaftPower = API.meanShaftFromCode[replacee];
            var newShaftPower = API.meanShaftFromCode[replacer];
            var oldShaftEnergy = oldShaftPower * oldOpHours / 1000;
            var newShaftEnergy = newShaftPower * newOpHours / 1000;
            var savings = oldShaftEnergy - newShaftEnergy;
            var oldVolume = oldFlow * oldOpHours / 24;
            // newVolume = newFlow * newOpHours / 24; is the same as old volume by design
            var oldEnergyPerVolume = 1000 * oldShaftEnergy/oldVolume;
            var newEnergyPerVolume = 1000 * newShaftEnergy/oldVolume;
            return {
                oldOpHours : oldOpHours,
                newOpHours : newOpHours,
                oldShaftEnergy : oldShaftEnergy,
                newShaftEnergy : newShaftEnergy,
                volume : oldVolume,
                oldEnergyPerVolume : oldEnergyPerVolume,
                newEnergyPerVolume : newEnergyPerVolume,
                savings : savings
            };
        }
        , estimateSavingsHtml: function(frIndex, mix){
            var linco = function(before, after){ return (1 - mix) * before + mix * after; }
            var fr = OPHRSEFF.flowRanges[frIndex];
            var replaceeList = fr.comboList.slice(1);
            var replacer = fr.comboList[0];
            var bestOpHrs = API.numOpHours[replacer];                    // h
            var bestShaftPower = API.meanShaftFromCode[replacer];        // KW
            var bestShaftEnergy = bestShaftPower * bestOpHrs / 1000; // MWh
            var bestFlow = API.meanFlowFromCode[replacer];               // MGD
            var bestVolume = bestFlow * bestOpHrs / 24;              // MG
            var bestEnergyPerVolume = 1000*bestShaftEnergy / bestVolume; // KWh/MG
            // make and add up replacee stats
            var items = replaceeList.map(function(r){ return OPHRSEFF.SAVINGS.estimateSavingsAux(r, replacer)});
            var getOldOpHours         = function(obj){ return obj.oldOpHours; };
            var getNewOpHours         = function(obj){ return obj.newOpHours; };
            var getOldShaftEnergy     = function(obj){ return obj.oldShaftEnergy; };
            var getNewShaftEnergy     = function(obj){ return obj.newShaftEnergy; };
            var getOldEnergyPerVolume = function(obj){ return obj.oldEnergyPerVolume; };
            var getNewEnergyPerVolume = function(obj){ return obj.newEnergyPerVolume; };
            var getVolume             = function(obj){ return obj.volume; };
            var getSavings            = function(obj){ return obj.savings; };
            var replItems = items.select(function(item) { return item.savings > 0});
            var keepItems = items.select(function(item) { return item.savings <= 0});
            //
            var replOpHrs = replItems.map(getOldOpHours).sum();
            var replShaftEnergy = replItems.map(getOldShaftEnergy).sum();
            var replVolume = replItems.map(getVolume).sum();
            var replEnergyPerVolume = 1000 * replShaftEnergy / replVolume;
            var totalMaxSavings = replItems.map(getSavings).sum();
            //
            var keepOpHrs = keepItems.map(getOldOpHours).sum();
            var keepShaftEnergy = keepItems.map(getOldShaftEnergy).sum();
            var keepVolume = keepItems.map(getVolume).sum();
            var keepEnergyPerVolume = 1000 * keepShaftEnergy / keepVolume;
            //
            var othersOpHrs           = keepOpHrs + replOpHrs;
            var othersShaftEnergy     = keepShaftEnergy + replShaftEnergy;
            var othersVolume          = keepVolume + replVolume;
            var othersEnergyPerVolume = 1000 * othersShaftEnergy / othersVolume;
            /*stats for replacing 100% of old by new (i.e. the case mix = 1)
            this relates to the replacer combination replacing the replacees,
            and does not include the original run of replacer */
            var fullRepOpHrs = replItems.map(getNewOpHours).sum();           // h
            var fullRepShaftEnergy = replItems.map(getNewShaftEnergy).sum(); // MWh
            // totals
            var totalOpHrs = bestOpHrs + othersOpHrs;
            var totalShaftEnergy = bestShaftEnergy + othersShaftEnergy;
            var totalVolume = bestVolume + othersVolume;
            var totalEnergyPerVolume = 1000 * totalShaftEnergy / totalVolume;
            // make mix stats
            var mixBestOpHrs = bestOpHrs + linco(0, fullRepOpHrs);
            var mixBestShaftEnergy = bestShaftEnergy + linco(0, fullRepShaftEnergy);
            var mixBestVolume = bestVolume + linco(0, replVolume);
            var mixBestEnergyPerVolume = 1000*mixBestShaftEnergy / mixBestVolume;
            var mixOthersOpHrs = keepOpHrs + linco(replOpHrs, 0);
            var mixOthersShaftEnergy = keepShaftEnergy + linco(replShaftEnergy, 0);
            var mixOthersVolume = keepVolume + linco(replVolume, 0);
            var mixOthersEnergyPerVolume = 1000 * mixOthersShaftEnergy / mixOthersVolume;
            var mixTotalOpHrs = mixBestOpHrs + mixOthersOpHrs;
            var mixTotalShaftEnergy = mixBestShaftEnergy + mixOthersShaftEnergy;
            var mixTotalVolume = mixBestVolume + mixOthersVolume;
            var mixTotalEnergyPerVolume = 1000*mixTotalShaftEnergy / mixTotalVolume;
            //
            var rowAux = function(tdArr, trAtts){ return HtmlGen.tr(
                tdArr.map(function(td){ return HtmlGen.td(isNaN(td) ? "-" : "%9.0f".sprintf(td)); }).join("")
                , trAtts);
            };
            var rowAuxH = function(heading, tdArr, trAtts){ return HtmlGen.tr(HtmlGen.th(heading) +
                tdArr.map(function(td){ return HtmlGen.td(isNaN(td) ? "-" : "%9.0f".sprintf(td)); }).join("")
                , trAtts);
            };
            var headingAux = function(thArr, trAtts){ return HtmlGen.tr(thArr.map(function(th){ return HtmlGen.th(th); }).join(""), trAtts); };
            var oddAtts = {"class" : "oddRow"};
            var evenAtts = {"class" : "evenRow"};
            var headAtts = {"class" : "topHeadRow"};
            var beforeBest = rowAuxH("best", [bestOpHrs, bestShaftEnergy, bestVolume, bestEnergyPerVolume], oddAtts);
            var beforeOthers = rowAuxH("others", [othersOpHrs, othersShaftEnergy, othersVolume, othersEnergyPerVolume], evenAtts);
            var beforeTotal = rowAuxH("total", [totalOpHrs, totalShaftEnergy, totalVolume, totalEnergyPerVolume], oddAtts);
            var afterBest = rowAux([mixBestOpHrs, mixBestShaftEnergy, mixBestVolume, mixBestEnergyPerVolume], oddAtts);
            var afterOthers = rowAux([mixOthersOpHrs, mixOthersShaftEnergy, mixOthersVolume, mixOthersEnergyPerVolume], evenAtts);
            var afterTotal = rowAux([mixTotalOpHrs, mixTotalShaftEnergy, mixTotalVolume, mixTotalEnergyPerVolume], oddAtts);
            //
            var headings = headingAux([     "hrs", "MWh", "MG", "KWh/MG"], headAtts);
            var headings1 = headingAux(["", "hrs", "MWh", "MG", "KWh/MG"], headAtts);
            var tableAux = function(headi, best, others, total){ return HtmlGen.table(headi + best + others + total, {"class": "savingsTableInner", cellSpacing: 0}); };
            var before = tableAux(headings1, beforeBest, beforeOthers, beforeTotal);
            var after = tableAux(headings, afterBest, afterOthers, afterTotal);
            var replacedHours = linco(0, replOpHrs);
            var replacedByHours = linco(0, fullRepOpHrs);
            var savings = linco(0, totalMaxSavings);
            var savingsStr = "Replacing " + "%7.1f".sprintf(replacedHours) + " non-best hours by an additional "
                + "%7.1f".sprintf(replacedByHours) + " best hours gives estimated savings of " + "%7.1f".sprintf(savings) + " MWh";
            var savingsSpan = HtmlGen.span(savingsStr);
            var leftTable = HtmlGen.table(
                HtmlGen.tr(HtmlGen.td(
                    HtmlGen.table(
                        HtmlGen.tr(HtmlGen.td(before) + HtmlGen.td(after))
                        ,{"class": "savingsTableUpper", cellSpacing: 0, cellPadding: 0}
                    )
                ))
                + HtmlGen.tr(HtmlGen.td(savingsStr))
                , {id: "savingsTable" + frIndex, "class": "savingsTable"}
            );
            return leftTable;
        }
        , estimateSavingsHtmlWithSlider: function(frIndex, mix){
            var leftTable = this.estimateSavingsHtml(frIndex, mix);
            return HtmlGen.table(HtmlGen.tr(
                HtmlGen.td(leftTable) +
                HtmlGen.td(HtmlGen.div("", {id: "savingsSlider" + frIndex, "class": "savingsSliderDiv"})) +
                HtmlGen.td(HtmlGen.div("", {id: "savingsSliderValue" + frIndex, "class": "savingsSliderValueDiv"}))
            ), {id: "uberSavingsTable" + frIndex, "class" : "uberSavingsTable"});
        }
    } // end of SAVINGS
    , updateExtra : function(){
        var extra = $("#bucketExtra");
        //extra.css("margin-top", 20);
        var recoListList = this.SLIDER.values.mapConsecutive(RECOMMENDATIONS.combosInFlowRange);
        var recoList2Html = function(recoList){
            return HtmlGen.div(recoList.map(function(reco){ return HtmlGen.span(API.pumpsListFromCode(reco).toStringCurly()); }).join(","));
        }
        var col1 = this.SLIDER.values.mapConsecutive(function(from, to){ return "%5.0f - %5.0f".sprintf(from, to)});
        var col2 = recoListList.map(recoList2Html);
        var html = HtmlGen.twoColTable(col1, col2);
        extra.html(html);
        var rows = $("#bucketExtra>table>tbody>tr");
        var numberOfWarnings = 0;
        for (var i=0; i<rows.length; i++){
            //$(rows[i]).attr({id: "bucketExtraRow" + i});
            var from = this.SLIDER.values[i];
            var to = this.SLIDER.values[i+1];
            var rangeSize = to - from;
            if (rangeSize < 9) continue;
            var numCombos = RECOMMENDATIONS.combosInFlowRange(from, to).length;
            if (numCombos <= 1)continue;
            var row = $(rows[i]);
            var warnLevel = Math.min(11, Math.max(1, Math.round(0.5 * ( rangeSize - 7 ))));
            row.addClass("warn" + warnLevel);
            numberOfWarnings++;
        }
        $("#bucketExtra table").attr("cellpadding", 1).attr("cellspacing", 0);
        $("#bucketExtra table tbody").prepend(jQuery(HtmlGen.tr(HtmlGen.th("Flow Range (MGD)") + HtmlGen.th("Pump Combinations"))));
        var warner = $("#warningAboutTooLargeFlowRange");
        if (numberOfWarnings > 0){
            warner.html("orange background indicates possibly too large a flow range");
        } else {
            warner.html("");
        }
    }
    , bucketConfigDone : function(){
        var THIS = this;
        $("#ophrsEffMultiSlider").removeClass("active");
        this.flowRanges = this.SLIDER.values.mapConsecutive(function(from, to, index){
            return new FlowRange(from, to, RECOMMENDATIONS.combosInFlowRange(from, to));
        });
        
        localStorage.setItem(this.bucketsName, JSON.stringify(this.SLIDER.values));
        //$.removeCookie(bucketDividersCookieName);
        //$.cookie(bucketDividersCookieName, this.SLIDER.values, { expires : cookieValidForThatManyDays });
        
        var mainContent = $("#ophrsEffMain");
        var hrefs = ["ophrsEffFirstSummary", "ophrsEffSecondSummary"];
        var tabTitles = ["Summary1", "Summary2"];
        var tabContents = [this.prepareSummary1BarChart(), this.prepareSummary2BarCharts()];
        for (var i=0; i<this.flowRanges.length; i++){
            var fr = this.flowRanges[i];
            if (fr.comboList.length == 0) continue;
            hrefs.push("flowRange" + i);
            tabTitles.push("%5.0f - %5.0f".sprintf(fr.from, fr.to));
            //tabContents.push("flow range " + i + " goes here");
            tabContents.push(this.prepareBucketTab(i));
        }
        var makeLi = function(i){
            return HtmlGen.li(HtmlGen.a(tabTitles[i], {href: "#" + hrefs[i]}))
        };
        var makeDiv = function(i){
            return HtmlGen.div(tabContents[i], {id: hrefs[i]});
        }
        var tabsHtmlPart1 = HtmlGen.ul(UTIL.range(0, hrefs.length-1).map(makeLi).join(""));
        var tabsHtmlPart2 = UTIL.range(0, hrefs.length-1).map(makeDiv).join("");
        var mainHtml = HtmlGen.div(tabsHtmlPart1 + tabsHtmlPart2, {id: "ophrsEffMainTabs"});
        mainContent.html(mainHtml);
        this.makeSummary1BarChart();
        this.makeSummary2BarCharts();
        // http://stackoverflow.com/questions/4565128/set-default-tab-in-jquery-ui-tabs
        jQuery("#ophrsEffMainTabs").tabs({
            activate: function(event, ui){
                console.log("acitivating : " + ui.newPanel.selector)
                jQuery("img.flowHeadChartResizer").hide();
                var sel = ui.newPanel.selector;
                if (sel == "#ophrsEffFirstSummary"){
                    THIS.putSummary1ResizerEltInPlace();
                } else {
                    THIS.summary1Resizer.style("display", "none")
                }
                var index = numberAtEndOfString(sel);
                for (var k=0; k<THIS.flowRanges.length; k++){
                    if (k == index){
                        continue;
                    }
                    d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_bucket_" + k).style("display", "none"); 
                }
                // important that "ophrsEffFirstSummary", "ophrsEffSecondSummary" don't end with a number
                if (index == -1){
                    OPHRSEFF.oneOfTheBucketTabsIsVisible = false;
                    return;
                }
                OPHRSEFF.oneOfTheBucketTabsIsVisible = true;
                OPHRSEFF.indexOfVisibleBucketTab = index;
                var bucket = OPHRSEFF.flowRanges[index];
                var virgin = !bucket.inited;
                if (virgin || (!("charts" in bucket))
                           || (bucket.charts.sizeFactor_X != OPHRSEFF.getSizeFactor_X())
                           || (bucket.charts.sizeFactor_Y != OPHRSEFF.getSizeFactor_Y())
                ){
                    OPHRSEFF.makeBucketTab(index);
                    OPHRSEFF.putBucketResizerEltInPlace(index);
                }
                if (virgin){
                    bucket.inited = true;
                    var container = $("#bucket" + index + "flowHead");
                    var theChart =  $("#bucket" + index + "flowHead>table");
                    theChart.hide();
                    var showHideBtnId = "showFlowHeadChart" + index;
                    container.append(HtmlGen.eisBtn(OPHRSEFF.showFlowHeadBtnCaption, {id: showHideBtnId, "class" : "big"}));
                    $("#" + showHideBtnId).click(OPHRSEFF.makeShowHideFlowHeadBtnHandler(index));
                } else {
                    //if (("FHC" in bucket) && (bucket.FHC.sizeFactor != OPHRSEFF.sizeFactor)){ OPHRSEFF.updateFlowHeadCanvas(index); }
                    if (("FHC" in bucket)){
                        bucket.FHC.chartResizerElt.style("display", null);
                    }
                }
            }, // end activate
            active : 0
        }); // end tabs
        var configBtn = jQuery("#configureFlowBucketsBtn").detach();
        var heading = jQuery("#ophrsEffByFlowRangeTab h3");
        heading.html(this.headingText);
        heading.append(configBtn);
        configBtn.show();
        jQuery("#" + this.SLIDER.containerId).hide();
        jQuery("#configureFlowBucketsBtn").click(function(){
            $("#ophrsEffMultiSlider").addClass("active");
            jQuery(this).remove();
            jQuery("#" + OPHRSEFF.SLIDER.containerId).show();
            $("#ophrsEffMain").empty();
            OPHRSEFF.SLIDER.init(OPHRSEFF.SLIDER.values);
        });
        this.add2ndSummarySizeChangeBtns();
    }
    , add2ndSummarySizeChangeBtns : function(){
        var container = $("div#ophrsEffSecondSummary");
        var idTrunk = "secondSummary";
        var plusId = idTrunk + "Plus";
        var minusId = idTrunk + "Minus";
        var plusMinusId = idTrunk + "PlusMinus";
        //var btns = HtmlGen.span(HtmlGen.eisBtn("+", {id: plusId}) + "<br/>" + HtmlGen.eisBtn("-", {id: minusId}), {id: plusMinusId});
        var btns = HtmlGen.span(HtmlGen.table(
            HtmlGen.tr(HtmlGen.td(HtmlGen.eisBtn("+", {id: plusId}))) + HtmlGen.tr(HtmlGen.td(HtmlGen.eisBtn("&ndash;", {id: minusId})))
        ), {id: plusMinusId});
        container.prepend(btns);
        var THIS = this;
        $("#" + plusId).click(function(){
            THIS.secondSummarySizeChange(chartSizeChangeFactor);
        });
        $("#" + minusId).click(function(){
            THIS.secondSummarySizeChange(1/chartSizeChangeFactor);
        });
    }
    , makeShowHideFlowHeadBtnHandler: function(index){
        return function(){
            var theChart =  $("#bucket" + index + "flowHead>table");
            var theButton = $("#showFlowHeadChart" + index + " span");
            var bucket = OPHRSEFF.flowRanges[index];
            
            
            
            console.log(bucket.FHC ? bucket.FHC.myName + " " + index : " N U L L ");
            
            
            
            
            jQuery("img.flowHeadChartResizer").hide();
            if (bucket.chartVisible){
                theChart.hide();
                theButton.html(OPHRSEFF.showFlowHeadBtnCaption);
                if (!bucket.FHC){
                    debugger;
                }
                
                bucket.FHC.chartResizerElt.style("display", "none");
            } else {
                theChart.show();
                theButton.html(OPHRSEFF.hideFlowHeadBtnCaption);
                if ("FHC" in bucket){
                    bucket.FHC.chartResizerElt.style("display", null);
                }
            }
            bucket.chartVisible = !bucket.chartVisible;
            if (bucket.chartInited){
                if (bucket.chartVisible && (OPHRSEFF.sizeFactor != bucket.FHC.sizeFactor)){
                    
                    
                    //OPHRSEFF.updateFlowHeadCanvas(index);
                    
                    
                    
                }
            } else {
                OPHRSEFF.flowHeadChartInit(index);
            }
        }
    }
    , prepareSummary2BarCharts : function(){
        var THIS = this;
        return UTIL.table(this.flowRanges.length, function(frIndex){
                return THIS.flowRanges[frIndex].comboList.length ? HtmlGen.span(HtmlGen.div("", {id: "hrsAtEffChart" + frIndex, "class" : "ophrsEffSmallRotatedChart"})) : "";
            }).join("");
    }
    , makeSummary2BarChart : function(frIndex){
        var fr = this.flowRanges[frIndex];
        if (!fr.comboList.length) return;
        var id = "hrsAtEffChart" + frIndex;
        var title = "" + fr.from + "-" + fr.to + " MGD";
        var data = UTIL.range(minEff, maxEff).map(function(e){
            return {
                eff       : e,
                count     : fr.effBinCount[e],
                comboList : fr.comboList,
                details   : fr.effBinCountDetails[e]
            };
        });
        var extraWidth = Config.ophrsEffByFlowRange.summary2.extraWidthAndLeftMarginForFirst
        var margin  = Config.ophrsEffByFlowRange.summary2.margin;
        var margTop   = margin.top;
        var margBot   = margin.bottom;
        var margLeft  = margin.left;
        var margRight = margin.right;
        var aspect  = Config.ophrsEffByFlowRange.summary2.aspect;
        var height0 = Config.ophrsEffByFlowRange.summary2.height0;
        var preferredNumYaxisTicks = Config.ophrsEffByFlowRange.summary2.preferredNumYaxisTicks;
        var xAxisLabelVerticalPositionAdjust = Config.ophrsEffByFlowRange.summary2.xAxisLabelVerticalPositionAdjust;
        var height = height0 * this.secondSummarySizeFactor;
        var width = height * aspect;
        if (frIndex == 0){
            width += extraWidth;
            margLeft += extraWidth;
        }
        var innerWidth = width - margLeft - margRight;
        var innerHeight = height - margTop - margBot;
        var S = {};

        S.y = d3.scale.ordinal().rangeRoundBands([innerHeight, 0], .1);
        S.x = d3.scale.linear().range([0, innerWidth]);
        S.xAxis = d3.svg.axis().scale(S.x).orient("bottom").ticks(5);
        S.yAxis = d3.svg.axis().scale(S.y).orient("left");
        S.container = d3.select("#" + id);
        S.container.html("");
        S.svg = S.container.append("svg").attr("width", width).attr("height", height).classed("ophrsEffByFlowRange summary2", true);
        S.mainGroup = S.svg.append("g").translate(margLeft, margTop).classed("mainGroup", true);
        S.y.domain(data.map(function(d) { return d.eff; }));
        S.x.domain([0, d3.max(data, function(d) { return d.count; })]);
        S.xAxisGroup = S.mainGroup.append("g").attr("class", "x axis").translate(0, innerHeight).call(S.xAxis);
        S.xAxisGroup.append("text").classed("axisLabel", true).text("op hrs").style("text-anchor", "middle")
            .attr("x", innerWidth/2).attr("y", margBot-4);
        S.mainGroup.append("text").classed("chartTitle", true).text(title).style("text-anchor", "middle")
            .attr("x", innerWidth/2 - 14).attr("y", 0);
        
        var yAxisLabel = "efficiency (%)";
        S.yAxisGroup = S.mainGroup.append("g").attr("class", "y axis").call(S.yAxis);
        if (frIndex == 0){
            S.yAxisGroup
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", -margLeft + 10)
                .attr("x", -innerHeight/2)
                .attr("dy", ".71em")
                .style("text-anchor", "middle")
                .text(yAxisLabel)
                .classed("axisLabel", true);
        }
        
        var mouseOverContainer = this.summary2MouseOverContainer;
        S.bars = S.mainGroup.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("y", function(d) { return S.y(d.eff); })
            .attr("height", S.y.rangeBand())
            .attr("x", function(d) { return S.x(0); })
            .attr("width", function(d) { return S.x(d.count); })
            .on("mouseover", function(d,i,j){
                //console.log(d);
                //console.log("frIndex, i, j ", frIndex, i, j);
                var comboList = d.comboList;
                var hrsList = d.details;
                var totalHours = d.count;
                var html = HtmlGen.twoColTable(comboList.map(function(code){ return API.pumpsListFromCode(code).toStringCurly(); }).concat("total"), hrsList.concat(totalHours), ["pumps", "hrs"]);
                html = "efficiency: " +d.eff+ "%<br/>" + html;
                mouseOverContainer.html(html);
                var hostSVG = this.parentNode.parentNode;
                var hostDiv = jQuery(hostSVG.parentNode);
                var o = hostDiv.offset();
                var hostLeft = o.left;
                var hostTop = o.top;
                var hostWidth = hostDiv.width();
                var hostRight = hostLeft + hostWidth;
                mouseOverContainer.show();
                var containerWidth = mouseOverContainer.width();
                var left = hostRight - containerWidth;
                mouseOverContainer.css("left", left);
                mouseOverContainer.css("top", hostTop);
            })
            .on("mouseout", function(d,i,j){
                mouseOverContainer.hide();
            });
        return S;
    }
    , makeSummary2BarCharts : function(){
        this.summary2Charts = [];
        for (var i=0; i<this.flowRanges.length; i++){
            this.summary2Charts[i] = this.makeSummary2BarChart(i);
        }
    }
    , prepareSummary1BarChart : function(){
        return HtmlGen.table(
            HtmlGen.tr(
                HtmlGen.td("", {id: "ophrsAtFlowRangeChartTd"}) +
                HtmlGen.td("", {id: "meanEffsAtFlowRangeChartTd"})
            )
        );
    }
    , makeSummary1BarChart : function(){
        function fixLabels(xag){ // adjusts formatting of the labels of the bars
            xag.selectAll("g.tick>text").each(function(d,i,j){
            	var elt = d3.select(this);
            	var y = elt.attr("y");
            	var dy = parseFloat(elt.attr("dy"));
            	elt.text("");
            	elt.selectAll("tspan").data(elt.datum()).enter().append("tspan").text(function(dd,ii,jj){
            		return dd + (ii == 0 ? "-" : "");
            	})
            	.attr("x", 0)
            	.attr("y", y)
            	.attr("dy", function(dd,ii,jj){
            		return ii * 1.1 + dy + "em";
            	});
            });
        }
        // prepare data
        var flowRanges = this.flowRanges;
        var minEff = Infinity;
        var maxEff = - Infinity;
        flowRanges.forEach(function(flowRange){
            var combos = flowRange.comboList;
        	flowRange.totalNumOpHours = combos.map(function(comboCode){
        		return API.numOpHours[comboCode];
        	}).sum();
            var enumerator  = combos.map(function(comboCode){return API.meanEffFromCode[comboCode] * API.numOpHours[comboCode]; }).sum();
            var denominator = combos.map(function(comboCode){return                                  API.numOpHours[comboCode]; }).sum();
            flowRange.eff =  enumerator / denominator;
            if (!isNaN(flowRange.eff)){
                minEff = Math.min(minEff, flowRange.eff);
                maxEff = Math.max(maxEff, flowRange.eff);
            }
        	flowRange.barLabel = [flowRange.from, flowRange.to];
        });
        var margin  = Config.ophrsEffByFlowRange.summary1.margin;
        var margTop   = margin.top;
        var margBot   = margin.bottom;
        var margLeft  = margin.left;
        var margRight = margin.right;
        var aspect  = Config.ophrsEffByFlowRange.summary1.aspect;
        var height0 = Config.ophrsEffByFlowRange.summary1.height0;
        var preferredNumYaxisTicks = Config.ophrsEffByFlowRange.summary1.preferredNumYaxisTicks;
        var xAxisLabelVerticalPositionAdjust = Config.ophrsEffByFlowRange.summary1.xAxisLabelVerticalPositionAdjust;
        var height = height0 * this.getSizeFactor_Y();
        var width = height0 * aspect * this.getSizeFactor_X();
        var innerWidth = width - margin.left - margin.right;
        var innerHeight = height - margin.top - margin.bottom;
        var S1H = this.SUMMARY1H = {};
        var S1E = this.SUMMARY1E = {};
        // left chart (hours)
        S1H.width = width;
        S1H.height = height;
        S1H.innerWidth = innerWidth;
        S1H.innerHeight = innerHeight;
        S1H.x = d3.scale.ordinal().rangeRoundBands([0, innerWidth], .1);
        S1H.y = d3.scale.linear().range([innerHeight, 0]);
        S1H.xAxis = d3.svg.axis().scale(S1H.x).orient("bottom");
        S1H.yAxis = d3.svg.axis().scale(S1H.y).orient("left").ticks(preferredNumYaxisTicks);
        S1H.container = d3.select("td#ophrsAtFlowRangeChartTd");
        S1H.container.html("");
        S1H.svg = S1H.container.append("svg")
        	.attr("width", width).attr("height", height).classed("ophrsEffByFlowRange summary1 hours", true);
        S1H.mainGroup = S1H.svg.append("g").translate(margin.left, margin.top).attr("class", "mainGroup");

        S1H.x.domain(flowRanges.map(function(d) { return d.barLabel; }));
        S1H.y.domain([0, d3.max(flowRanges, function(d) { return d.totalNumOpHours; })]);
        
        S1H.xAxisGroup = BarChartTools.makeXaxisGroup(S1H, S1H.xAxis);
        S1H.yAxisGroup = BarChartTools.makeYaxisGroupWithCenteredLabel(S1H, "hours", S1H.yAxis, margLeft);
        
        var idPreH = "ophrsEffByFlowRangeSummary1Hours_";
        S1H.bars = S1H.mainGroup.selectAll(".bar")
            .data(flowRanges)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return S1H.x(d.barLabel); })
            .attr("width", S1H.x.rangeBand())
            .attr("y", function(d) { return S1H.y(d.totalNumOpHours); })
            .attr("height", function(d) { return innerHeight - S1H.y(d.totalNumOpHours); })
            .on("mouseover", function(d, i){
                S1H.tooltip.classed("invisible", true);
                S1H.svg.selectAll("text#" + idPreH + i).classed("invisible", false);
            })
            .on("mouseout", function(d, i){
                S1H.tooltip.classed("invisible", true);
            });
        
        S1H.tooltip = BarChartTools.attachTooltips(S1H, "totalNumOpHours", idPreH, flowRanges, "%d");

        fixLabels(S1H.xAxisGroup);
        S1H.xAxisLabel = BarChartTools.makeCenteredXaxisLabel(S1H, "flow (MGD)", margBot, xAxisLabelVerticalPositionAdjust);            
        S1H.heading = BarChartTools.makeHeading(S1H, "hours of operation at flow range");
                        
        // right chart (eff)
        S1E.width = width;
        S1E.height = height;
        S1E.innerWidth = innerWidth;
        S1E.innerHeight = innerHeight;
        S1E.x = d3.scale.ordinal().rangeRoundBands([0, innerWidth], .1);
        S1E.y = d3.scale.linear().range([innerHeight, 0]);
        S1E.xAxis = d3.svg.axis().scale(S1E.x).orient("bottom");
        S1E.yAxis = d3.svg.axis().scale(S1E.y).orient("left").ticks(preferredNumYaxisTicks);
        S1E.container = d3.select("td#meanEffsAtFlowRangeChartTd");
        S1E.container.html("");
        S1E.svg = S1E.container.append("svg")
        	.attr("width", width).attr("height", height).classed("ophrsEffByFlowRange summary1 eff", true);
        S1E.mainGroup = S1E.svg.append("g").translate(margin.left, margin.top).attr("class", "mainGroup");
        var minEff_1 = 1.05 * minEff -0.05 * maxEff;
        S1E.x.domain(S1H.x.domain());
        S1E.y.domain([minEff_1, maxEff]);
        
        S1E.xAxisGroup = BarChartTools.makeXaxisGroup(S1E, S1E.xAxis);
        S1E.yAxisGroup = BarChartTools.makeYaxisGroupWithCenteredLabel(S1E, "efficiency (%)", S1E.yAxis, margLeft);

        var idPreE = "ophrsEffByFlowRangeSummary1Eff_";
        var nonZeroOpHourFlowRanges = flowRanges.filter(function(d){ return !isNaN(d.eff); });
        S1E.bars = S1E.mainGroup.selectAll(".bar")
            .data(nonZeroOpHourFlowRanges)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return S1E.x(d.barLabel); })
            .attr("width", S1E.x.rangeBand())
            .attr("y", function(d) { return S1E.y(d.eff); })
            .attr("height", function(d) {
                return innerHeight - S1E.y(d.eff);
            })
            .on("mouseover", function(d, i){
                S1E.tooltip.classed("invisible", true);
                S1E.svg.selectAll("text#" + idPreE + i).classed("invisible", false);
            })
            .on("mouseout", function(d, i){
                S1E.tooltip.classed("invisible", true);
            });

        S1E.tooltip = BarChartTools.attachTooltips(S1E, "eff", idPreE, nonZeroOpHourFlowRanges, "%4.1f");

        fixLabels(S1E.xAxisGroup);
        S1E.xAxisLabel = BarChartTools.makeCenteredXaxisLabel(S1E, "flow (MGD)", margBot, xAxisLabelVerticalPositionAdjust);
        S1E.heading = BarChartTools.makeHeading(S1E, "efficiency at flow range");
        
        
        var resizerId = "chartResizerImg_ophrsEffByFlowRange_summary1";
        if (d3.selectAll("#" + resizerId).node() == null){
            this.summary1Resizer = theBody.append("img").attr("id", resizerId).attr("src", "img/corner.png")
                .style("position", "absolute").style("cursor", "se-resize").style("display", "none");
        }
    }
    , getSizeFactor_X : function(){
        var varName = makeLocalStorageVarName_X(this.myName);
        var varValue = localStorage.getItem(varName);
        if (varValue == null || isNaN(varValue)){
            localStorage.setItem(varName, 1);
            return 1;
        } else {
            return parseFloat(varValue);
        }
    }
    , getSizeFactor_Y : function(){
        var varName = makeLocalStorageVarName_Y(this.myName);
        var varValue = localStorage.getItem(varName);
        if (varValue == null || isNaN(varValue)){
            localStorage.setItem(varName, 1);
            return 1;
        } else {
            return parseFloat(varValue);
        }
    }
    , putSummary1ResizerEltInPlace : function(){
        var THIS = this;
        var svgElt = this.SUMMARY1H.svg.node();
        var $svgElt = jQuery(svgElt);
        var o = $svgElt.offset();
        var svgLeft = o.left;
        var svgTop = o.top;
        var svgWidth = $svgElt.width();
        var svgHeight = $svgElt.height();
        var svgRight = svgLeft + svgWidth;
        var svgBottom = svgTop + svgHeight;
        var eltLeft = svgRight - 19;
        var eltTop = svgBottom - 19;
        var delt = this.summary1Resizer;
        var resizerElt = jQuery(this.summary1Resizer.style("display", null).node());
        resizerElt.css({
            left     : eltLeft,
            top      : eltTop
        });
        var behavior = d3.behavior.drag();
        behavior.on("dragstart", function(){
            var svgElt = THIS.SUMMARY1H.svg.node();
            var mouseStartXY = d3.mouse(svgElt);
            //DEBUG[0].html(mouseStartXY.join(", "));
            THIS.summary1_chartResizeStartX = mouseStartXY[0];
            THIS.summary1_chartResizeStartY = mouseStartXY[1];
            THIS.summary1_chartResizeStartWidth = THIS.SUMMARY1H.width;
            THIS.summary1_chartResizeStartHeight = THIS.SUMMARY1H.height;
            THIS.summary1_chartResizeResizerEltStartLeft = parseFloat(resizerElt.css("left"));
            THIS.summary1_chartResizeResizerEltStartTop = parseFloat(resizerElt.css("top"));
        });
        behavior.on("drag", function(){
            var svgElt = THIS.SUMMARY1H.svg.node();
            var mouseXY = d3.mouse(svgElt);
            //DEBUG[1].html(mouseXY.join(", "));
            var mouseX = mouseXY[0];
            var mouseY = mouseXY[1];
            var dx = mouseX - THIS.summary1_chartResizeStartX;
            var dy = mouseY - THIS.summary1_chartResizeStartY;
            var targetWidth = THIS.summary1_chartResizeStartWidth + dx;
            var targetHeight = THIS.summary1_chartResizeStartHeight + dy;
            var height0 = Config.ophrsEffByFlowRange.summary1.height0;
            var width0 = height0 * Config.ophrsEffByFlowRange.summary1.aspect;
            var factor_X = targetWidth / width0;
            var factor_Y = targetHeight / height0;
            THIS.sizeFactor_X = factor_X;
            THIS.sizeFactor_Y = factor_Y;
            //DEBUG[7].html("drag sizefa " + factor_X + " , " + factor_Y);
            localStorage.setItem(makeLocalStorageVarName_X(THIS.myName), factor_X);
            localStorage.setItem(makeLocalStorageVarName_Y(THIS.myName), factor_Y);
            THIS.makeSummary1BarChart();
            resizerElt.css({
                left : THIS.summary1_chartResizeResizerEltStartLeft + dx,
                top  : THIS.summary1_chartResizeResizerEltStartTop + dy
            })
        });
        delt.call(behavior);

        
    }
    , prepareFlowHeadCanvas : function(index){
        // makes the html for the flow head chart. First step. No handlers, no canvas drawing
        var controlsTr = HtmlGen.tr(
            HtmlGen.td("", {id: "ophrsEffByFlowRangeFlowHeadChartControlsParentTd_" + index, colspan: 2})
        )
        var mainTr = HtmlGen.tr(
            HtmlGen.td("", {id: "ophrsEffByFlowRangeFlowHeadChartParentTd_" + index, valign: "top"}) +
            HtmlGen.td("", {id: "ophrsEffByFlowRangeFlowHeadChartLegendParentTd_" + index, valign: "top"})
        );
        var result = HtmlGen.table(controlsTr + mainTr, {"class": "flowHeadCanvasTable"});
        return result;
    }
    , flowHeadChartInit : function(index){
        // just before first use of flow head chart in question, after prepareFlowHeadCanvas. (executed when subtab associated with flow range is first chosen)
        var bucket = OPHRSEFF.flowRanges[index];
        OPHRSEFF.drawToFlowHeadCanvas(index);
        bucket.myName = "ophrsEffByFlowRange_Bucket_" + index;
        OPHRSEFF.flowHeadRankTable(index);
        bucket.chartInited = true;
        var FHC = OPHRSEFF.flowRanges[index].FHC;
        mouseZoomInit.call(FHC);
    }
    , updateFlowHeadCanvas : function(index, options){
        /*DEBUG[2].html("updateFlowHeadCanvas index = " + index + " options : " + (options ? (d3.keys(options).map(function(optionKey){
            return optionKey + " -> " + options[optionKey]
        }).join(", ")) : "null"));*/
        var bucket;
        if (typeof index == "number"){
            bucket = this.flowRanges[index];
        } else {
            bucket = index.bucket;
        }
            
        var combos = bucket.comboList;
        var FHC = bucket.FHC;
        if (!FHC){
            debugger;
        }
        if (!("sizeFactor_X" in FHC) || isNaN(FHC.sizeFactor_X)){
            debugger;
        }
        if (!("sizeFactor_Y" in FHC) || isNaN(FHC.sizeFactor_Y)){
            debugger;
        }
        //DEBUG[5].html("sizefactor  x " + FHC.sizeFactor_X);
        //DEBUG[6].html("sizefactor  y " + FHC.sizeFactor_Y);
        var sizeFactor_X = FHC.sizeFactor_X;// = this.getSizeFactor_X_forFlowHeadChart(index);
        var sizeFactor_Y = FHC.sizeFactor_Y;// = this.getSizeFactor_Y_forFlowHeadChart(index);
        if (options){
            for (var key in options){
                FHC[key] = options[key];
            }
        }
        var effWidth = maxEff1 - minEff1;
        var effToHue = function(eff){ return (eff - minEff1) / effWidth; };
        var conf = Config.ophrsEffByFlowRange.flowHeadCanvas;
        var margTop   = conf.margin.top;
        var margBot   = conf.margin.bottom;
        var margLeft  = conf.margin.left;
        var margRight = conf.margin.right;
        var height0 = conf.height0;
        var height = FHC.height = height0 * sizeFactor_Y;
        var width = FHC.width = height0 * conf.aspect * sizeFactor_X;
        var innerWidth  = width - margLeft - margRight;
        var innerHeight = height - margTop - margBot;
        FHC.innerWidth = innerWidth;
        FHC.innerHeight = innerHeight;
        var svg = FHC.svg;
        svg.attr("width", width).attr("height", height);
        var mainGroup = FHC.mainGroup;
        FHC.invisiblePlaceholderRect.attr("width", width).attr("height", height);
        var clipRect = FHC.clipRect;
        clipRect.attr("width", innerWidth).attr("height", innerHeight);
        var clipGroup = FHC.clipGroup;
        var x = FHC.x = FHC.makeXtrafo();
        var y = FHC.y = FHC.makeYtrafo();
        var xAxis = FHC.xAxis = d3.svg.axis().scale(x).ticks(conf.preferredNumXaxisTicks + Math.max(0, (innerWidth-300)/140)).orient("bottom");
        var yAxis = FHC.yAxis = d3.svg.axis().scale(y).ticks(conf.preferredNumYaxisTicks + Math.max(0, (innerHeight-300)/140)).orient("left");
        var xAxisGroup = FHC.xAxisGroup;
        xAxisGroup.translate(0, innerHeight).call(xAxis);
        var xAxisLabel = FHC.xAxisLabel;
        xAxisLabel.attr("x", innerWidth/2);
        var yAxisGroup = FHC.yAxisGroup;
        yAxisGroup.call(yAxis);
        var yAxisLabel = FHC.yAxisLabel;
        yAxisLabel.attr("x", -innerHeight/2);
        var points = FHC.points;
        points.attr("transform", function(d,i,j){ return trans(x(d.flow), y(d.avgHead)); })

    }
    , drawToFlowHeadCanvas : function(index){
        var svgContainer = d3.select("#ophrsEffByFlowRangeFlowHeadChartParentTd_" + index);
        var legendContainer = d3.select("#ophrsEffByFlowRangeFlowHeadChartLegendParentTd_" + index);
        svgContainer.html("");
        legendContainer.html("");
        
        var bucket = this.flowRanges[index];
        var combos = bucket.comboList;
        var minFlow = combos.map(function(combo) { return API.minFlowFromCode[combo]; }).min() - 0.5;
        var maxFlow = combos.map(function(combo) { return API.maxFlowFromCode[combo]; }).max() + 0.2;
        var minTDH = combos.map(function(combo) { return API.minAvgHeadFromCode[combo]; }).min() - 0.5;
        var maxTDH = combos.map(function(combo) { return API.maxAvgHeadFromCode[combo]; }).max() + 0.35;
        bucket.minFlow = minFlow;
        bucket.maxFlow = maxFlow;
        bucket.minTDH = minTDH;
        bucket.maxTDH = maxTDH;
        var effWidth = maxEff1 - minEff1;
        var effToHue = function(eff){ return (eff - minEff1) / effWidth; };

        var conf = Config.ophrsEffByFlowRange.flowHeadCanvas;
        var margTop   = conf.margin.top;
        var margBot   = conf.margin.bottom;
        var margLeft  = conf.margin.left;
        var margRight = conf.margin.right;
        var sizeFactor_X = this.getSizeFactor_X_forFlowHeadChart(index);
        var sizeFactor_Y = this.getSizeFactor_Y_forFlowHeadChart(index);
        var height0 = conf.height0;
        var height = height0 * sizeFactor_Y;
        var width  = height0 * conf.aspect * sizeFactor_X;
        var innerWidth  = width - margLeft - margRight;
        var innerHeight = height - margTop - margBot;
        var FHC = {
            myName : this.nameOfFlowHeadChart(index),
            sizeFactor_X : sizeFactor_X,
            sizeFactor_Y : sizeFactor_Y,
            aspect       : conf.aspect,
            height0      : height0,
            zoomEpsilon  : 0.05,
            yAxisLabelHorizontalAdjust : conf.yAxisLabelHorizontalAdjust,
            xAxisLabelVerticalPositionAdjust : conf.xAxisLabelVerticalPositionAdjust,
            xMin            : minFlow,
            xMax            : maxFlow,
            yMin            : minTDH,
            yMax            : maxTDH,
            width           : width,
            height          : height,
            innerWidth      : innerWidth,
            innerHeight     : innerHeight,
            svgContainer    : svgContainer,
            legendContainer : legendContainer,
            makeXtrafo : function(){
                return d3.scale.linear().range([0, this.innerWidth]).domain([this.xMin, this.xMax]);
            },
            makeYtrafo : function(){
                return d3.scale.linear().range([this.innerHeight, 0]).domain([this.yMin, this.yMax]);
            },
            updateChart: function(_xMin, _xMax, _yMin, _yMax){
                //DEBUG[4].html(+(DEBUG[4].html())+1);
                OPHRSEFF.updateFlowHeadCanvas(this, {
                    xMin : _xMin,
                    xMax : _xMax,
                    yMin : _yMin,
                    yMax : _yMax
                })
            }
        };
        bucket.FHC = FHC;
        bucket.index = index;
        FHC.bucket = bucket;
        zoomInit.call(FHC);
        var svg = FHC.svg = svgContainer.append("svg").attr("width", width).attr("height", height).attr("class", "ophrsEffByFlowRange flowHeadChart");
        var mainGroup = FHC.mainGroup = svg.append("g").attr("class", "mainGroup").translate(margLeft, margTop);
        
        
        FHC.invisiblePlaceholderRect = mainGroup.append("rect").attr("width", width).attr("height", height).attr("stroke", "none").attr("fill", "transparent");

        
        var clipPathId = FHC.clipPathId = "ophrsEffByFlowRangeFlowHeadChart_" +index+ "_ClipPath";
        var clipPath = FHC.clipPath = mainGroup.append("clipPath").attr("id", clipPathId);
        var clipRect = FHC.clipRect = clipPath.append("rect").attr("width", innerWidth).attr("height", innerHeight).attr("x", 0).attr("y", 0);
        var clipGroup = FHC.clipGroup = mainGroup.append("g").attr("class", "clipGroup").style("clip-path", "url(#" + clipPathId + ")");
        
        var mouseGroup = FHC.mouseGroup = mainGroup.append("g").classed("mouseGroup", true);
        var x = FHC.x = FHC.makeXtrafo();
        var y = FHC.y = FHC.makeYtrafo();
        var xAxis = FHC.xAxis = d3.svg.axis().scale(x).ticks(conf.preferredNumXaxisTicks + Math.max(0, (innerWidth-300)/140)).orient("bottom");
        var yAxis = FHC.yAxis = d3.svg.axis().scale(y).ticks(conf.preferredNumYaxisTicks + Math.max(0, (innerHeight-300)/140)).orient("left");
        var xAxisGroup = FHC.xAxisGroup = mainGroup.append("g").classed("axis xAxis", true).translate(0, innerHeight).call(xAxis);
        var xAxisLabel = FHC.xAxisLabel = xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text("flow (MGD)").attr("x", innerWidth/2).attr("y", margBot - FHC.xAxisLabelVerticalPositionAdjust);
        var yAxisGroup = FHC.yAxisGroup = mainGroup.append("g").classed("axis yAxis", true).call(yAxis);
        var yAxisLabel = FHC.yAxisLabel = yAxisGroup.append("text").classed("axisLabel yAxisLabel", true).rotate(-90)
            .text("TDH (psi)").attr("x", -innerHeight/2).attr("y", -margLeft + FHC.yAxisLabelHorizontalAdjust).style("text-anchor", "middle");
            
        var pointData = combos.map(function(comboCode){
            return {
                code:  comboCode,
                rows : API.finalTable.pick(API.formulas.rowsWherePumpsRunOnly[comboCode])
            };
        });
        var pointGroups = FHC.pointGroups = clipGroup.selectAll(".unicorn").data(pointData).enter().append("g").attr("class", "pointGroup");
        
        var points = FHC.points = pointGroups.selectAll(".unicorn").data(function(d){return d.rows;}).enter()
            .append("g")
            .attr("transform", function(d,i,j){
                return trans(x(d.flow), y(d.avgHead));
            })
            .on("mouseover", function(row, i, whichComboIdx){
                jQuery("#flowHeadRankTable" + index + " tr").removeClass("chosen");
                jQuery(jQuery("#flowHeadRankTable" + index + " tr")[whichComboIdx+1]).addClass("chosen");
                jQuery("#mousePos" + index).html( 
                    makeMouseOverTable(row, -1, "flowHeadMouseoverLegend" + index)
                );
            })
            .on("mouseout", function(row, i, whichComboIdx){
                jQuery("#flowHeadRankTable" + index + " tr").removeClass("chosen");
            });
        var renderFunctions = [];
        renderFunctions[0] = function(row, elt, color){
            elt.append("circle").attr("cx", 0).attr("cy", 0).attr("r", 5).style("stroke", color).style("fill", "transparent");
        }
        renderFunctions[1] = function(row, elt, color){
            elt.append("rect").attr("x", -4.5).attr("y", -4.5).attr("width", 9).attr("height", 9)
                .style("stroke", color).style("fill", "transparent");
        }
        renderFunctions[2] = function(row, elt, color){
            elt.append("line").attr("x1", 0).attr("x2", 0).attr("y1", -5.5).attr("y2", 5.5).style("stroke", color).style("fill", "transparent");
            elt.append("line").attr("y1", 0).attr("y2", 0).attr("x1", -5.5).attr("x2", 5.5).style("stroke", color).style("fill", "transparent");
        }
        renderFunctions[3] = function(row, elt, color){
            var d = 4.5;
            elt.append("line").attr("x1", -d).attr("y1", -d).attr("x2",  d).attr("y2",  d).style("stroke", color).style("fill", "transparent");
            elt.append("line").attr("x1", -d).attr("y1",  d).attr("x2",  d).attr("y2", -d).style("stroke", color).style("fill", "transparent");
        }
        renderFunctions[4] = function(row, elt, color){
            function first(arg){ return arg[0]; }
            function second(arg){ return arg[1]; }
            var d = 5;
            var data = [
                [ 0,-d],
                [-d, 0],
                [ 0, d],
                [ d, 0],
                [ 0,-d]
            ];
            var line = d3.svg.line().x(first).y(second);
            elt.selectAll(".unicorn").data([data]).enter().append("path").attr("fill", "transparent").attr("d", line).style("stroke", color);
        }
        function pointsOnCircle(radius, numPoints, startAngle){
            var angleStep = 2 * Math.PI / numPoints;
            return d3.range(numPoints + 1).map(function(i){
                var angle = startAngle + i * angleStep;
                return [radius * Math.cos(angle), radius * Math.sin(angle)];
            });
        }
        function makeTriangleRenderFunction(startAngle){
            return function(row, elt, color){
                function first(arg){ return arg[0]; }
                function second(arg){ return arg[1]; }
                var d = 5;
                var data = pointsOnCircle(6, 3, startAngle);
                var line = d3.svg.line().x(first).y(second);
                elt.selectAll(".unicorn").data([data]).enter().append("path").attr("fill", "transparent").attr("d", line).style("stroke", color);
            }
        }
        renderFunctions[5] = makeTriangleRenderFunction( Math.PI/2);
        renderFunctions[6] = makeTriangleRenderFunction(-Math.PI/2);
        renderFunctions[7] = makeTriangleRenderFunction( Math.PI);
        renderFunctions[8] = makeTriangleRenderFunction( 0);
        points.each(function(row, i, whichComboIdx){
            var efficiency = row.eff;
            var hue = effToHue(efficiency);
            var color = hslToRgbString(hue, 1, 0.5);
            renderFunctions[whichComboIdx % renderFunctions.length](row, d3.select(this), color);
        });
        var behavior = d3.behavior.drag();
        behavior.on("dragstart", function(){
            var mouseStartXY = d3.mouse(FHC.mainGroup.node());
            var startXraw = mouseStartXY[0];
            var startYraw = mouseStartXY[1];
            var xInv = FHC.makeXtrafo().invert;
            var yInv = FHC.makeYtrafo().invert;
            FHC.dragStartData = {
                xMinExt : FHC.xMin,
                xMaxExt : FHC.xMax,
                yMinExt : FHC.yMin,
                yMaxExt : FHC.yMax,
                xInv    : xInv,
                yInv    : yInv,
                startX  : xInv(startXraw),
                startY  : yInv(startYraw)
            };
        });
        behavior.on("drag", function(){
            var mouseXY = d3.mouse(FHC.mainGroup.node());
            var xRaw = mouseXY[0];
            var yRaw = mouseXY[1];
            var dsd = FHC.dragStartData;
            var xInv = dsd.xInv;
            var yInv = dsd.yInv;
            var currentX = xInv(xRaw);
            var currentY = yInv(yRaw);
            var dX = currentX - dsd.startX;
            var dY = currentY - dsd.startY;
            var xLo = dsd.xMinExt - dX;
            var xHi = dsd.xMaxExt - dX;
            var yLo = dsd.yMinExt - dY;
            var yHi = dsd.yMaxExt - dY;
            FHC.updateChart(xLo, xHi, yLo, yHi);
        });
        svg.call(behavior);
        resizeableObjects[FHC.myName] = FHC;
        var resizerElt = putResizerElementInPlace(FHC, FHC.myName);
        attachResizingHandler(FHC, resizerElt);
        jQuery("img.flowHeadChartResizer").hide();
        resizerElt.show();   
    }
    , flowHeadRankTable : function(index){
        var size = 16;
        //var container = jQuery("#flowHeadRight" + index);
        var container = jQuery("#ophrsEffByFlowRangeFlowHeadChartLegendParentTd_" + index);
        
        var bucket = this.flowRanges[index];
        var combos = bucket.comboList;
        var headerRow = HtmlGen.tr(HtmlGen.th("") + HtmlGen.th("") + HtmlGen.th("hours") + HtmlGen.th("eff") + HtmlGen.th(""));
        //                              symbol              pumps          hours                  eff                visibility           
        var tableContent = headerRow;
        var littleCanvasId, eyeId;
        for (var comboIndex=0; comboIndex<combos.length; comboIndex++){
            var combo = combos[comboIndex];
            var trContent = "";
            littleCanvasId = "littleCanvas" + index + "combo" + comboIndex;
            eyeId = "eye" + index + "combo" + comboIndex;
            trContent += HtmlGen.td(HtmlGen.canvas({width: size, height: size, id: littleCanvasId}), {"class": "_littleCanvasParent"});    // symbol
            trContent += HtmlGen.td(API.pumpsListFromCode(combo).toStringCurly());                           // pumps
            trContent += HtmlGen.td(API.numOpHours[combo]);                                                  // hours
            trContent += HtmlGen.td("%4.1f".sprintf(API.meanEffFromCode[combo]) + "%");                      // efficiency         
            trContent += HtmlGen.td(HtmlGen.div("", {id : eyeId, "class": "openEye"}), {"class": "eyeParent"});  // visibility
            tableContent+= HtmlGen.tr(trContent);
        }
        container.html(HtmlGen.table(tableContent, {id: "flowHeadRankTable" + index, "class": "flowHeadRankTable", cellPadding: 2, cellSpacing: 0}));
        var numberOfRenderFunctionsIn_DrawToFlowHeadCanvas = 9;
        for (var comboIndex=0; comboIndex<combos.length; comboIndex++){
            var combo = combos[comboIndex];
            littleCanvasId = "littleCanvas" + index + "combo" + comboIndex;
            var canv = jQuery("#" + littleCanvasId);
            var ctx = canv[0].getContext("2d");
            CANVAS.draw(comboIndex % numberOfRenderFunctionsIn_DrawToFlowHeadCanvas, ctx, size/2, size/2, size/2.3, "black", 1);
            eyeId = "eye" + index + "combo" + comboIndex;
            $("#" + eyeId).click(this.makeEyeClickHandler(index, comboIndex));
            $("#" + littleCanvasId).parent().click(this.makeLittleCanvasClickHandler(index, comboIndex));
        }
        // to do : remove code duplication here
        var effWidth = maxEff1 - minEff1;
        var effToHue = function(eff){ return (eff - minEff1) / effWidth; };
        var makeColorLegendSpan = function(eff){
            var hue = effToHue(eff/100);
            var color = hslToRgbString(hue, 1, 0.5);
            return HtmlGen.span(eff + "%", {style: "color:" + color});
        } 
        //var s = HtmlGen.span("80%", {style: "color: rgb(0, 80, 222)"});
        container.append(HtmlGen.div(UTIL.range(minEff, maxEff).map(makeColorLegendSpan).join(" "), {"class": "colorLegend"}));
        container.append(HtmlGen.div("", {id: "mousePos" + index, "class": "flowHeadMouse"}));
    }
    , makeLittleCanvasClickHandler : function(flowRangeIndex, comboIndex){
        return function(){
        }
    }
    , makeEyeClickHandler : function(flowRangeIndex, comboIndex){
        return function(){
            //console.log("flowRangeIndex " + flowRangeIndex + " comboIndex " + comboIndex);
            var flowRange = OPHRSEFF.flowRanges[flowRangeIndex];
            var visibleNow = flowRange.visible[comboIndex];
            if (visibleNow){
                $(this).removeClass("openEye");
                $(this).addClass("closedEye");
                flowRange.visible[comboIndex] = false;
            } else {
                $(this).removeClass("closedEye");
                $(this).addClass("openEye");
                flowRange.visible[comboIndex] = true;
            }
            visibleNow = !visibleNow;
            d3.select(flowRange.FHC.pointGroups[0][comboIndex]).classed("invisible", !visibleNow);
        }
    }
    , makeFlowTDHhideFun : function(index, combo){
        return function(){
            alert("hide: index: " + index + " combo: " + combo);
        }
    }
    , makeFlowTDHshowFun : function(index, combo){
        return function(){
            alert("show: index: " + index + " combo: " + combo);
        }
    }
    , prepareBucketTab : function(index){
        // there is a tab for each flow range, and this method is step 1 in generating one of those
        // (makeBucketTab will be called after the html made here is already inserted into the dom tree)
        var idSavings = "bucket" + index + "savings";
        var idLeft = "bucket" + index + "effs";
        var idRight = "bucket" + index + "hrs";
        var idFlowHeadChart = "bucket" + index + "flowHead";
        var savingsHTML = HtmlGen.div(this.SAVINGS.estimateSavingsHtmlWithSlider(index, 1), {id: idSavings});
        var barCharts = HtmlGen.table(HtmlGen.tr(
                HtmlGen.td(HtmlGen.div("", {id: idLeft, "class": "effsOfCombosInBucket"})) +
                HtmlGen.td(HtmlGen.div("", {id: idRight, "class": "hrsOfCombosInBucket" }))
        ));
        var flowHeadChart = HtmlGen.div(this.prepareFlowHeadCanvas(index), {id: idFlowHeadChart});
        return barCharts + savingsHTML + flowHeadChart;
        //return flowHeadChart + barCharts + savingsHTML;
    }
    , makeBucketTab : function(index){
        // there is a tab for each flow range, and this method is step 2 in generating one of those
        // (prepareBucketTab had previously made the html that has been inserted into the dom tree, and now we're doing more stuff, mostly chart related)
        var bucket = this.flowRanges[index];
        var effContainer = d3.select(document.getElementById("bucket" + index + "effs"));
        var hourContainer = d3.select(document.getElementById("bucket" + index + "hrs"));
        // left chart (efficiencies)
        var effData = bucket.comboList.map(function(combo){
            return {
                barLabel  : API.pumpsListFromCode(combo).toStringCurly(),
                eff       : Math.round(100*API.meanEffFromCode[combo])/100
            };
        });
        // X = array with the labels used for the x-axis (with the pump combo in it, one under each bar)
        var X = effData.map(function(d) { return d.barLabel; });
        var Yeff = effData.map(function(d){ return d.eff; });
        var minEff, maxEff;
        if (bucket.comboList.length == 1){
            minEff = Math.round(Yeff[0] - 1.5);
            maxEff = Math.round(Yeff[0] + 1.5);
        } else {
            var _minEff = Yeff.min();
            var _maxEff = Yeff.max();
            var eps = 0.11;
            var minEff = (1 + eps) * _minEff - eps * _maxEff;
            var maxEff = (1 + eps) * _maxEff - eps * _minEff;
        }

        var margin  = Config.ophrsEffByFlowRange.bucketBars.margin;
        var margTop   = margin.top;
        var margBot   = margin.bottom;
        var margLeft  = margin.left;
        var margRight = margin.right;
        var aspect  = Config.ophrsEffByFlowRange.bucketBars.aspect;
        var height0 = Config.ophrsEffByFlowRange.bucketBars.height0;
        var preferredNumYaxisTicks = Config.ophrsEffByFlowRange.bucketBars.preferredNumYaxisTicks;
        var xAxisLabelVerticalPositionAdjust = Config.ophrsEffByFlowRange.bucketBars.xAxisLabelVerticalPositionAdjust;
        
        var sfx = this.getSizeFactor_X();
        var sfy = this.getSizeFactor_Y();
        
        var height = height0 * sfy;
        var width = height0 * aspect * sfx;

        var innerWidth = width - margin.left - margin.right;
        var innerHeight = height - margin.top - margin.bottom;

        var x = d3.scale.ordinal().rangeRoundBands([0, innerWidth], .1);
        var y = d3.scale.linear().range([innerHeight, 0]);
        var xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = d3.svg.axis().scale(y).orient("left").ticks(preferredNumYaxisTicks);
        var EffBars = {
            x           : x,
            y           : y,
            width       : width,
            height      : height,
            innerWidth  : innerWidth,
            innerHeight : innerHeight,
            container   : effContainer,
            idPrefix    : "ophrsEffByFlowRangeBucket_" + index + "_Eff_"
        };
        effContainer.html("");
        EffBars.svg = effContainer.append("svg")
        	.attr("width", width).attr("height", height).classed("ophrsEffByFlowRange bucketBarChart efficiencies", true);
        EffBars.mainGroup = EffBars.svg.append("g").translate(margin.left, margin.top).attr("class", "mainGroup");
        x.domain(X);
        y.domain([minEff, maxEff]);
        EffBars.xAxisGroup = BarChartTools.makeXaxisGroup(EffBars, xAxis);
        EffBars.xAxisLabel = BarChartTools.makeCenteredXaxisLabel(EffBars, "pumps", margBot, xAxisLabelVerticalPositionAdjust);
        EffBars.yAxisGroup = EffBars.mainGroup.append("g").attr("class", "y axis").call(yAxis);
        EffBars.bars = EffBars.mainGroup.selectAll(".bar")
        	.data(effData)
        	.enter().append("rect")
        	.attr("class", "bar")
        	.attr("x", function(d) { return x(d.barLabel); })
        	.attr("width", x.rangeBand())
        	.attr("y", function(d) { return y(d.eff); })
        	.attr("height", function(d) { return innerHeight - y(d.eff); })
        	.on("mouseover", function(d, i){
        		EffBars.tooltip.classed("invisible", true);
        		EffBars.svg.selectAll("text#" + EffBars.idPrefix + i).classed("invisible", false);
        	})
        	.on("mouseout", function(d, i){
        		EffBars.tooltip.classed("invisible", true);
        	});
        EffBars.tooltip = BarChartTools.attachTooltips(EffBars, "eff", EffBars.idPrefix, effData, "%4.1f");
        EffBars.heading = BarChartTools.makeHeading(EffBars, "Efficiencies");

        // right chart (hours)
        var Yhrs1 = bucket.comboList.map(function(combo){ return API.numOpHours[combo]; });
        var ordering = Yhrs1.orderingDESC();
        var reorderedComboList = bucket.comboList.pick(ordering);
        var hourData = reorderedComboList.map(function(combo){
            return {
                barLabel  : API.pumpsListFromCode(combo).toStringCurly(),
                hours     : API.numOpHours[combo]
            };
        });
        var X = hourData.map(function(d){ return d.barLabel; });
        var maxY = Yhrs1.max();
        var x = d3.scale.ordinal().rangeRoundBands([0, innerWidth], .1);
        var y = d3.scale.linear().range([innerHeight, 0]);
        var xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = d3.svg.axis().scale(y).orient("left").ticks(preferredNumYaxisTicks);
        var HourBars = {
            x           : x,
            y           : y,
            width       : width,
            height      : height,
            innerWidth  : innerWidth,
            innerHeight : innerHeight,
            container   : hourContainer,
            idPrefix    : "ophrsEffByFlowRangeBucket_" + index + "_Hours_"
        };
        hourContainer.html("");
        HourBars.svg = hourContainer.append("svg")
        	.attr("width", width).attr("height", height).classed("ophrsEffByFlowRange bucketBarChart hours", true);
        HourBars.mainGroup = HourBars.svg.append("g").translate(margin.left, margin.top).attr("class", "mainGroup");
        x.domain(X);
        y.domain([0, maxY]);
        HourBars.xAxisGroup = BarChartTools.makeXaxisGroup(HourBars, xAxis);
        HourBars.xAxisLabel = BarChartTools.makeCenteredXaxisLabel(HourBars, "pumps", margBot, xAxisLabelVerticalPositionAdjust);
        HourBars.yAxisGroup = HourBars.mainGroup.append("g").attr("class", "y axis").call(yAxis);
        HourBars.bars = HourBars.mainGroup.selectAll(".bar")
        	.data(hourData)
        	.enter().append("rect")
        	.attr("class", "bar")
        	.attr("x", function(d) { return x(d.barLabel); })
        	.attr("width", x.rangeBand())
        	.attr("y", function(d) { return y(d.hours); })
        	.attr("height", function(d) { return innerHeight - y(d.hours); })
        	.on("mouseover", function(d, i){
        		HourBars.tooltip.classed("invisible", true);
        		HourBars.svg.selectAll("text#" + HourBars.idPrefix + i).classed("invisible", false);
        	})
        	.on("mouseout", function(d, i){
        		HourBars.tooltip.classed("invisible", true);
        	});
        HourBars.tooltip = BarChartTools.attachTooltips(HourBars, "hours", HourBars.idPrefix, hourData, "%d");
        HourBars.heading = BarChartTools.makeHeading(HourBars, "Hours in Operation");
        
        bucket.charts = {
            effBars    : EffBars,
            hourBars   : HourBars,
            sizeFactor_X : sfx,
            sizeFactor_Y : sfy
        };

        // fix width of savings table info text bottom left of the savings table
        var elt = $("#savingsTable" + index);
        var topHalf = elt.find(".savingsTableUpper");
        var topWidth = topHalf.width();
        elt.width(topWidth);
        //uberSavingsTable
        // fix slider height of savings slider
        var height = $("#savingsTable" + index).height();
        var sliderElt = $("#savingsSlider" + index);
        var THIS = this;
        function aux(value){
			var mix = value / 1000;
            var html = THIS.SAVINGS.estimateSavingsHtml(index, mix);
            var elt = $("#savingsTable" + index);
            var td = elt.parent();
            td.empty();
            td.html(html);
            elt = $("#savingsTable" + index);
            var topHalf = elt.find(".savingsTableUpper");
            var topWidth = topHalf.width();
            elt.width(topWidth);
        }
        sliderElt.slider({
            orientation: "vertical",
			min: 0,
			max: 1000,
			value: 1000,
			slide: function( event, ui ) {
                var infoStr = "%4.1f".sprintf(ui.value/10) + "%<br/>replacement"; 
                $("#savingsSliderValue" + index).html(infoStr);
                aux(ui.value);
			},
			stop: function( event, ui ) {
                aux(ui.value);
			}
        });
        sliderElt.height(height - 25);
        
        var resizerId = "chartResizerImg_ophrsEffByFlowRange_bucket_" + index;
        if (d3.selectAll("#" + resizerId).node() == null){
            this["bucketChartResizer_" + index] = theBody.append("img").attr("id", resizerId).attr("src", "img/corner.png")
                .style("position", "absolute").style("cursor", "se-resize").style("display", "none");
        }
    }
    , putBucketResizerEltInPlace : function(index){;
        var THIS = this;
        var svgElt = this.flowRanges[index].charts.effBars.svg.node();
        var $svgElt = jQuery(svgElt);
        var o = $svgElt.offset();
        var svgLeft = o.left;
        var svgTop = o.top;
        var svgWidth = $svgElt.width();
        var svgHeight = $svgElt.height();
        var svgRight = svgLeft + svgWidth;
        var svgBottom = svgTop + svgHeight;
        var eltLeft = svgRight - 19;
        var eltTop = svgBottom - 19;
        var delt = this["bucketChartResizer_" + index];
        var resizerElt = jQuery(delt.style("display", null).node());
        resizerElt.css({
            left     : eltLeft,
            top      : eltTop
        });
        var behavior = d3.behavior.drag();
        behavior.on("dragstart", function(){
            var svgElt = THIS.flowRanges[index].charts.effBars.svg.node();
            var mouseStartXY = d3.mouse(svgElt);
            //DEBUG[0].html(mouseStartXY.join(", "));
            THIS.flowRanges[index].chartResizeStartX = mouseStartXY[0];
            THIS.flowRanges[index].chartResizeStartY = mouseStartXY[1];
            THIS.flowRanges[index].chartResizeStartWidth = THIS.flowRanges[index].charts.effBars.width;
            THIS.flowRanges[index].chartResizeStartHeight = THIS.flowRanges[index].charts.effBars.height;
            THIS.flowRanges[index].chartResizeResizerEltStartLeft = parseFloat(resizerElt.css("left"));
            THIS.flowRanges[index].chartResizeResizerEltStartTop = parseFloat(resizerElt.css("top"));
        });
        behavior.on("drag", function(){
            var svgElt = THIS.flowRanges[index].charts.effBars.svg.node();
            var mouseXY = d3.mouse(svgElt);
            //DEBUG[1].html(mouseXY.join(", "));
            var mouseX = mouseXY[0];
            var mouseY = mouseXY[1];
            var dx = mouseX - THIS.flowRanges[index].chartResizeStartX;
            var dy = mouseY - THIS.flowRanges[index].chartResizeStartY;
            var targetWidth = THIS.flowRanges[index].chartResizeStartWidth + dx;
            var targetHeight = THIS.flowRanges[index].chartResizeStartHeight + dy;

            var height0 = Config.ophrsEffByFlowRange.summary1.height0;
            var width0 = height0 * Config.ophrsEffByFlowRange.summary1.aspect;
            var factor_X = targetWidth / width0;
            var factor_Y = targetHeight / height0;
            THIS.sizeFactor_X = factor_X;
            THIS.sizeFactor_Y = factor_Y;
            //DEBUG[7].html("drag sizefa " + factor_X + " , " + factor_Y);
            localStorage.setItem(makeLocalStorageVarName_X(THIS.myName), factor_X);
            localStorage.setItem(makeLocalStorageVarName_Y(THIS.myName), factor_Y);
            THIS.makeBucketTab(index);
            resizerElt.css({
                left : THIS.flowRanges[index].chartResizeResizerEltStartLeft + dx,
                top  : THIS.flowRanges[index].chartResizeResizerEltStartTop  + dy
            })
        });
        delt.call(behavior);
    }
    , sizeChanged: function(){
        console.log("sizeChanged called");
        //this.makeSummary1BarChart();
        this.makeSummary2BarCharts();
        /*if (this.oneOfTheBucketTabsIsVisible){
            this.makeBucketTab(this.indexOfVisibleBucketTab);
            //var bucket = this.flowRanges[this.indexOfVisibleBucketTab];
        }*/
    }
    , make : function(){
        this.makeComplete = false;
        this.secondSummarySizeFactor = this.getSecondSummarySizeFactor_fromLocalStorage();
        this.summary2MouseOverContainer = jQuery("#ophrsEffByFlowRangeSummary2MouseOverContainer");
        var bucketsName = this.bucketsName = Config.ophrsEffByFlowRange.bucketsName;
        var flowBuckets = localStorage.getItem(bucketsName);
        if (flowBuckets == null){
            flowBuckets = Config.ophrsEffByFlowRange.defaultFlowBuckets;
            localStorage.setItem(bucketsName, JSON.stringify(flowBuckets));
        } else {
            flowBuckets = JSON.parse(flowBuckets);
        }
        var buckets0 = this.initialBucketDividers = flowBuckets;
        jQuery("#ophrsEffByFlowRangeTab h3").html(this.headingText);
        var containerId = "ophrsEffMultiSlider";
        var topId = "bucketTop";
        var parentId = "bucketParent";
        var labelsId = "bucketLabels";
        var buttonsId = "bucketButtons";
        var tableId = "buckets";
        var smallestRelevantMeanFlow = API.meanFlowFromCode.pick(API.common).min();
        smallestRelevantMeanFlow = Math.floor(smallestRelevantMeanFlow-0.05);
        buckets0[0] = Math.min(buckets0[0], smallestRelevantMeanFlow);
        this.SLIDER.init(buckets0, containerId, topId, parentId, labelsId, buttonsId, tableId);
        this.SLIDER.registerWindowResizeHandler();
        this.SAVINGS.init();
        jQuery("#doneConfiguringFlowBucketsBtn").trigger("click");
        this.makeComplete = true;
    }
    , nameOfFlowHeadChart : function(i){
        var objName = "ophrsEffByFlowRange_flowHeadChart_" + i;
        return objName;
    }
    , flowHeadChartLocalStorageVarNameForSizeFactor_X : function(i){
        var objName = this.nameOfFlowHeadChart(i);
        return makeLocalStorageVarName_X(objName);
    }
    , flowHeadChartLocalStorageVarNameForSizeFactor_Y : function(i){
        var objName = this.nameOfFlowHeadChart(i);
        return makeLocalStorageVarName_Y(objName);
    }
    , makeChartResizerIdForFlowHeadChart : function(i){
        var objName = this.nameOfFlowHeadChart(i);
        return "chartResizerImg_" + objName;
    }
    , makeSecondSummarySizeLocalStorageVarName : function(){
        return makeLocalStorageVarName("ophrsEffByFlowRange_2ndSummary");
    }
    , getSizeFactor_X_forFlowHeadChart : function(i){
        var varName = this.flowHeadChartLocalStorageVarNameForSizeFactor_X(i);
        var varValue = localStorage.getItem(varName);
        if (varValue == null || isNaN(varValue)){
            localStorage.setItem(varName, 1);
            return 1;
        } else {
            return parseFloat(varValue);
        }
    }
    , getSizeFactor_Y_forFlowHeadChart : function(i){
        var varName = this.flowHeadChartLocalStorageVarNameForSizeFactor_Y(i);
        var varValue = localStorage.getItem(varName);
        if (varValue == null || isNaN(varValue)){
            localStorage.setItem(varName, 1);
            return 1;
        } else {
            return parseFloat(varValue);
        }
    }
    , getSecondSummarySizeFactor_fromLocalStorage : function(){
        var varName = this.makeSecondSummarySizeLocalStorageVarName();
        var varValue = localStorage.getItem(varName);
        if (varValue == null || isNaN(varValue) || varValue < 0){
            localStorage.setItem(varName, 1);
            return 1;
        }
        return parseFloat(varValue);
    }
    , secondSummarySizeChange : function(factor){
        var newSizeFactor = this.secondSummarySizeFactor * factor;
        this.secondSummarySizeFactor = newSizeFactor;
        var varName = this.makeSecondSummarySizeLocalStorageVarName();
        localStorage.setItem(varName, newSizeFactor);
        this.makeSummary2BarCharts();
    }
    , onActivate : function(){
        this.summary1Resizer.style("display", "none");
        for (var k=0; k<this.flowRanges.length; k++){
            d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_bucket_" + k).style("display", "none");
            d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_flowHeadChart_" + k).style("display", "none");
        }
        var activeTabIdx = jQuery("#ophrsEffMainTabs>ul>li.ui-state-active").index();
        if (activeTabIdx == 0){
            this.putSummary1ResizerEltInPlace();
        } else {
            if (activeTabIdx != 1){
                var bucketIdx = activeTabIdx - 2;
                this.putBucketResizerEltInPlace(bucketIdx);
                d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_flowHeadChart_" + bucketIdx).style("display", null);
            }
        }
        
    }
    , onDeactivate : function(){
        this.summary1Resizer.style("display", "none");
        for (var k=0; k<this.flowRanges.length; k++){
            d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_bucket_" + k).style("display", "none");
            d3.selectAll("#chartResizerImg_ophrsEffByFlowRange_flowHeadChart_" + k).style("display", "none");
        }
    }

};


BarChartTools = {
    attachTooltips: function (OBJ, fieldName, idPrefix, data, format){
    	return OBJ.mainGroup.selectAll(".unicorn")
    		.data(data)
    		.enter().append("text")
    		.attr("class", "tooltip invisible")
    		.attr("x", function(d) { return OBJ.x(d.barLabel) + 0.5*OBJ.x.rangeBand(); })
    		.style("text-anchor", "middle")
    		.attr("y", function(d) { return Math.min(OBJ.innerHeight - 8, OBJ.y(d[fieldName]) + 20); })
    		.attr("id", function(d, i) { return idPrefix + i; } )
    		.text(function(d) { return format.sprintf(d[fieldName]); });
    },
    makeXaxisGroup: function (OBJ, xAxis){
        return OBJ.mainGroup.append("g").attr("class", "x axis").translate(0, OBJ.innerHeight).call(xAxis);
    },
    makeCenteredXaxisLabel: function (OBJ, label, margBot, verticalPositionAdjust){
        return OBJ.xAxisGroup.append("text").classed("axisLabel xAxisLabel", true)
            .text(label).attr("x", OBJ.innerWidth/2).attr("y", margBot - verticalPositionAdjust);
    },
    makeYaxisGroupWithCenteredLabel: function (OBJ, label, yAxis, margLeft, positionAdjust){
        if (arguments.length < 5){
            positionAdjust = 10;
        }
        return OBJ.mainGroup.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -margLeft + positionAdjust)
            .attr("x", -OBJ.innerHeight/2)
            .attr("dy", ".71em")
            .style("text-anchor", "middle")
            .text(label)
            .classed("axisLabel", true);
    },
    makeHeading: function (OBJ, theText, y){
        if (arguments.length < 3){
            y = 16;
        }
        return OBJ.svg.append("text").style("text-anchor", "middle").attr("x", OBJ.width/2).attr("y", y)
            .text(theText).classed("heading", true);
    }

};

