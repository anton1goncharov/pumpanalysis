// api.js

DURATION = {};
DURATION.SECOND = 1000;
DURATION.MINUTE = DURATION.SECOND * 60;
DURATION.HOUR = DURATION.MINUTE * 60;
DURATION.DAY = DURATION.HOUR * 24;
DURATION.WEEK = DURATION.DAY * 7;
DURATION.MONTH = DURATION.DAY * 30;
DURATION.YEAR = DURATION.DAY * 365;
DURATION.LEAPYEAR = DURATION.DAY * 366;

TIME_RESOLUTION = DURATION.HOUR;

API = {
    headRatios : [1,0.9971889378253678, 0.9895318014809256, 0.990575829691033, 0.9850563843250921, 1.0041224128816983, 1.0301979963770005, 1.043369585705431],
    meanShaftsTable : [2387.25, 2219.11, 1175.7, 2332.29, 2287.95, 722.539, 1035.24, 1028.21],
    tankNames : ["AVST_STLEV", "BLST_STLEV1", "BLST_STLEV2", "NWST_STLEV", "WHWP_RTLEV1", "WHWP_RTLEV2", "WHWP_RTLEV3", "WHWP_RTLEV4", "WLST_STLEV"],
    hydroFactor  :  0.302077,
    ftToPsiFactor : 0.433528,
    finalTable : null,
    formulas   : null,
    linearDDCCCoefficients : null,
    absoluteDDCCCoefficients : null,
    numPumps : -1
};

function leftPadWithZero(arg){
    if (arg<10){
        return "0" + arg; 
    } else {
        return arg;
    }
}
API.isErrorRow = function(row){
    return row.errors.length > 0;
}
API.isntErrorRow = function (row){
    return row.errors.length == 0;
}
API.pumpsListFromCode = function (code){
    var pump = 0;
    var result = [];
    while (code > 0){
        pump++;
        if (code % 2){
            result.push(pump);
        }
        code = Math.floor(0.5 * code);
    }
    return result;
}
API.runBitsFromCode = function (code){
    var result = [];
    while (code > 0){
        if (code % 2){
            result.push(1);
        } else {
            result.push(0);
        }
        code = Math.floor(0.5 * code);
    }
    while (result.length < API.numPumps){
        result.push(0);
    }
    return result;
}

API.calcFlowsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].flow });} 
API.calcShaftsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].totalShaft }); }
API.calcMinHeadsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].minHead }); }
API.calcMaxHeadsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].maxHead }); }
API.calcAvgHeadsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].avgHead }); }
API.calcEffsFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return 100*API.finalTable[x].eff }); }
API.calcHydrosFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].map(function(x){ return API.finalTable[x].hydroPow }); }
API.calcNumOpHoursFromCode = function(code){ return API.formulas.rowsWherePumpsRunOnly[code].length; }

API.calcMeanAbsoluteFlowContrib = function (pump){
    //var code = Math.round(Math.pow(2, pump-1));
    //var rows = finalTable.pick(API.formulas.rowsWherePumpsRun[code]);
	var rows = API.finalTable.pick(API.formulas.rowsWherePumpRuns[pump]);
    return rows.map(function(row){ return row.absFlowContrib(pump-1); }).mean();
}
API.calcMeanSingleEff = function (pump){
	var rows = API.finalTable.pick(API.formulas.rowsWherePumpRuns[pump]);
    return 100*rows.map(function(row) { return row.singleEff(pump-1);}).mean();    
}
API.calcEffBinsFromCode = function (code){
    var result = UTIL.table(101, function(){ return 0;});
    var rows = API.formulas.rowsWherePumpsRunOnly[code];
    for (var i=0; i<rows.length; i++){
        var row = API.finalTable[rows[i]];
        var e = 100 * row.eff;
        var re = Math.round(e);
        result[re]++;
    }
    return result;
}
function dateFromString(sqlDateString){
	// YYYY-MM-DD HH:MM:SS
	// 0123456789012345678
	var year   = parseInt(sqlDateString.substring(0,4));
	var month  = parseInt(sqlDateString.substring(5,7));
	var day    = parseInt(sqlDateString.substring(8,10));
	var hour   = parseInt(sqlDateString.substring(11,13));
	//var minute = parseInt(sqlDateString.substring(14,16));
	//var second = parseInt(sqlDateString.substring(17,19));
	return {
		year: year,
		month: month,
		day: day,
		hour: hour,
		minute: 0,
		second: 0
	};
}
function finalTableEntry(){
    // the entries of finalTable, i.e., finalTable[i] for integer i
    // are initially plain objects, since they are loaded from JSON
    // We change their prototype (constructor) later on (see init)
    // to finalTableEntry, in order to have some functions associated
    // with the objects, also.
    // entries which are there in the JSON follow
    // avgHead, minHead, maxHead
    // head (zero based array - head[0] is for pump 1 etc)
    // date.year
    // date.month
    // date.day
    // date.hour
    // date.minute
    // date.second
    // flow
    // eff (between 0 and 1)
    // hydroPow
    // pumpsList (zero based array of pumps running, pumps numbering starting at 1)
    // relativeFlowContrib (zero based array (same way as for head) of flow contributions. 0 if pump isn't running. Whole array always adds up to 1)
    // runBits (zero based array (same way as for head) for running or not. entries are 0 or 1)
    // runCode: binary number composed out of the bits in runBits
    // shaft : zero based array, same way as for head, shaft in KW for pump
    // totalShaft: sum of shaft
    // tankLevels: array
    // units: psi for head, MGD for flow, KW for power, probably feet for tank levels
}

function toDateString(dateObj){
    return dateObj.year + "-" + leftPadWithZero(dateObj.month) + "-" + leftPadWithZero(dateObj.day) + " " + leftPadWithZero(dateObj.hour) + ":" + leftPadWithZero(dateObj.minute);
}
function toJsDate(d){
    return new Date(d.year, d.month-1, d.day, d.hour, d.minute, d.second);
}
finalTableEntry.prototype.absFlowContrib = function(i){
    return this.flow * this.relativeFlowContrib[i];
}
finalTableEntry.prototype.singleEff = function(i){
    var floContrib = this.flow * this.relativeFlowContrib[i];
    var tdh = this.head[i];
    var shaft = this.shaft[i];
    return API.hydroFactor * floContrib * tdh / shaft;
}
finalTableEntry.prototype.jsDate = function(){
    return toJsDate(this.date);
}
finalTableEntry.prototype.jsTime = function(){
    return this.jsDate().getTime();
}
finalTableEntry.prototype.simpleAxisDate = function(){
    return this.date.year + "-" + this.date.month + "-" + this.date.day;
}
finalTableEntry.prototype.fullAxisDate = function(){
    return this.date.year + "-" + leftPadWithZero(this.date.month) + "-" + leftPadWithZero(this.date.day) + "<br/>"
    + leftPadWithZero(this.date.hour) + ":" + leftPadWithZero(this.date.minute);
}
API.init0 = function(data){
    /* init0 runs exactly once at startup
       init1 below at startup after init0 AND after the user changes the time frame to be examined
    */
    var ppp = [1,2,3,4,5,6,7,8];
    var kwNames = ppp.map(function(p){
        return "POWF_M" + p + "_KW";
    });
    var headNames = ppp.map(function(p){
        return "POWF_M" + p + "_DIFF";
    });
    var runBitNames = ppp.map(function(p){
        return "POWF_M" + p + "PMPRN";
    });
    data.forEach(function(d){
        var head = [];
        var runBits = [];
        var shaft = [];
        var tankLevels = [];
        var errors = [];
        var seriousError = false;
        var numRunningPumps = 0;
        var totalShaft = 0;
        var powerOf2 = 1;
        var runCode = 0;
        for (var i=0; i<8; i++){
            var ii = i+1;
            var kwName = kwNames[i];
            var kwVal = +d[kwName];
            shaft[i] = kwVal;
            var headName = headNames[i];
            var headVal = +d[headName];
            head[i] = headVal;
            var runBitName = runBitNames[i];
            var runBitVal_raw = d[runBitName];
            var runBitVal = NaN;
            var runBitError = false;
            var runPartOfHour = false;
            if (runBitVal_raw == "NULL"){
                if (kwVal < 1){
                    runBitVal = 0;
                } else {
                    if (kwVal > 0.9 * usualKWvalues[i]){
                        runBitVal = 1;
                    } else {
                        runBitError = true;
                    }
                }
            } else {
                runBitVal = +runBitVal_raw;
                if (isNaN(runBitVal)){
                    runBitError = true;
                    debugger;
                }
                if ( !((runBitVal == 0) || (runBitVal == 1)) ){
                    runPartOfHour = true;
                }
            }
            if (runBitError){
                errors.push(runBitName + " invalid");
                seriousError = true;
            } else {
                if (runPartOfHour){
                    errors.push("pump " + ii + " running part of hour");
                    seriousError = true;
                } else {
                    runBits[i] = runBitVal;
                }
            }
            if (runBitVal == 1){
                numRunningPumps++;
                totalShaft += kwVal;
                runCode += powerOf2;
                if (isNaN(headVal)){
                    errors.push("pump " + ii + " has invalid TDH measurement.")
                } else {
                    if (headVal <= minimumHeadRequiredForNonError){
                        errors.push("pump " + ii + " has unrealistically low TDH measurement.")
                    }
                }
                if (isNaN(kwVal)){
                    errors.push("pump " + ii + " has invalid shaft power measurement.")
                } else {
                    if (kwVal <= usualKWvalues[i] * 0.7){
                        errors.push("pump " + ii + "has unrealistically low power consumption measurement.")
                    }
                }
            }
            if (runBitVal == 0){
                head[i] = 0;
                if (isNaN(kwVal)){
                    shaft[i] = 0;
                }
                if (kwVal > 1){
                    errors.push("pump " + ii + " uses power but has run bit not set.")
                }
            }
            powerOf2 *= 2;
        }
        if (numRunningPumps == 0){
            errors.push("no pump is running");
            seriousError = true;
        }
        var flowName = "POWF_MZFLOW"
        var flowVal_raw = d[flowName];
        var flowVal = +flowVal_raw;
        if (isNaN(flowVal)){
            errors.push("invalid flow value");
            seriousError = true;
        } else {
            d.flow = flowVal;
        }
        for (var t=0; t<API.tankNames.length; t++){
            tankLevels[t] = +d[API.tankNames[t]];
            if (isNaN(tankLevels[t])){
                tankLevels[t] = 0;
            }
        }
        d.head = head;
        d.shaft = shaft;
        d.runBits = runBits;
        d.runCode = runCode;
        d.totalShaft = totalShaft;
        d.tankLevels = tankLevels;
        d.errors = errors;
        d.seriousError = seriousError;
        if (errors.length == 0 && (runBits[6] == 1 && (dateFromString(d.datatime) - pump7StartValidDate) < 0)){
            debugger;
        }
    });
    var response = data;
    var finalTableAll = API.finalTableAll = response;
    // calc or format date, max head, min head, pumpsList
    API.numPumps = finalTableAll[0].head.length;
	for (var i=0; i<finalTableAll.length; i++){     // %%%
		var row = finalTableAll[i];
		row.date = dateFromString(row.datatime);
		var hh = row.head;
		var bits = row.runBits;
		var maxH = -Infinity;
		var minH = Infinity;
		var pList = [];
		for (var pmp = 0; pmp < API.numPumps; pmp++){
			if (bits[pmp] == 1){
				pList.push(pmp + 1);
				var h = hh[pmp];
				if (h > maxH) maxH = h;
				if (h < minH) minH = h;
			} else {
				hh[pmp] = 0;
			}
		}
		row.maxHead = maxH;
		row.minHead = minH;
		row.pumpsList = pList;
	}
    
    for (var i=0; i<finalTableAll.length; i++){ // %%%
        // finalTableAll[i].__proto__ = finalTableEntry; --> doesn't work in IE8
        // finalTableAll[i].absFlowContrib = finalTableEntry.absFlowContrib; --> works in IE8, but this way, would have to repeat for every single function separately, which is not good
        // the preceding two methods would have required to define absFlowContrib and the like directly under finalTableEntry, rather than finalTableEntry.prototype
        finalTableAll[i] = jQuery.extend(new finalTableEntry(), finalTableAll[i]);
    }
}
API.init1 = function (currentTable, maxError, minHours){  // maxError is max SQUARED error
    //API.finalTable = currentTable.shallowClone();
    API.finalTable = currentTable.filter(function(row){
        return !row.seriousError;
    });
	for (var i=0; i<API.finalTable.length; i++){
		API.finalTable[i].rowId = i;
	}
    API.calcRowsWhere();
    API.pumpsWithValidData = UTIL.range(1, API.numPumps).select(function(pump){ return API.formulas.rowsWherePumpRuns[pump].length > 0});
    // calculate the data derived characteristic curve
	var ddcc = API.makeDDCCcleaned(maxError);
    API.linearDDCCCoefficients = ddcc.linCoes;
    API.absoluteDDCCCoefficients = ddcc.absCoes;
    
    // calculate average head, predicted flow, hydraulic power, and efficiency (needs data derived char curves)
	for (var i=0; i<API.finalTable.length; i++){   // %%%
		var row = API.finalTable[i];
		var hh = row.head;
		var bits = row.runBits;
		var flowContrib = new Array(API.numPumps);
		var predictedFlow = 0;
		for (var pmp = 0; pmp < API.numPumps; pmp++){
			if (bits[pmp] == 0){
				hh[pmp] = 0;
				flowContrib[pmp] = 0;
			} else {
				var h = hh[pmp];
				flowContrib[pmp] = API.linearDDCCCoefficients[pmp] * h + API.absoluteDDCCCoefficients[pmp];
			}
			predictedFlow += flowContrib[pmp];
		}
		var avgHead = 0;
		for (var pmp = 0; pmp < API.numPumps; pmp++){
			flowContrib[pmp] /= predictedFlow;
			avgHead += ( flowContrib[pmp] * hh[pmp] );
		}
		row.avgHead = avgHead;
		row.predictedFlow = predictedFlow;
		row.relativeFlowContrib = flowContrib;
		row.hydroPow = API.hydroFactor * avgHead * row.flow;
		row.eff = row.hydroPow / row.totalShaft;
	}

    //  repeat this here, because now we got the ddcc errors, which we didn't have before.
    API.calcRowsWhere();
    API.pumpsWithValidData = UTIL.range(1, API.numPumps).select(function(pump){ return API.formulas.rowsWherePumpRuns[pump].length > 0});
    var pumpValid = API.pumpValid = new Array(API.numPumps + 1);
    for (var i = 0; i < pumpValid.length; i++){
        pumpValid[i] = false;
    }
    for (var i = 0; i < API.pumpsWithValidData.length; i++){
        pumpValid[API.pumpsWithValidData[i]] = true;
    }

	API.makeFormulas();

    var oneoneone = Math.pow(2, API.numPumps) - 1;
    var flowsFromCode          = API.flowsFromCode          = UTIL.table0(oneoneone, API.calcFlowsFromCode); 
    var shaftsFromCode         = API.shaftsFromCode         = UTIL.table0(oneoneone, API.calcShaftsFromCode);
    var minHeadsFromCode       = API.minHeadsFromCode       = UTIL.table0(oneoneone, API.calcMinHeadsFromCode);
    var maxHeadsFromCode       = API.maxHeadsFromCode       = UTIL.table0(oneoneone, API.calcMaxHeadsFromCode);
    var avgHeadsFromCode       = API.avgHeadsFromCode       = UTIL.table0(oneoneone, API.calcAvgHeadsFromCode);
    var effsFromCode           = API.effsFromCode           = UTIL.table0(oneoneone, API.calcEffsFromCode);
    var hydrosFromCode         = API.hydrosFromCode         = UTIL.table0(oneoneone, API.calcHydrosFromCode);
    var numOpHours             = API.numOpHours             = UTIL.table0(oneoneone, API.calcNumOpHoursFromCode);
    var effBinsFromCode        = API.effBinsFromCode        = UTIL.table0(oneoneone, API.calcEffBinsFromCode);
    var flowsFromCodeSorted    = API.flowsFromCodeSorted    = flowsFromCode.mapSortASCCopy();
    var shaftsFromCodeSorted   = API.shaftsFromCodeSorted   = shaftsFromCode.mapSortASCCopy();
    var minHeadsFromCodeSorted = API.minHeadsFromCodeSorted = minHeadsFromCode.mapSortASCCopy();
    var maxHeadsFromCodeSorted = API.maxHeadsFromCodeSorted = maxHeadsFromCode.mapSortASCCopy();
    var avgHeadsFromCodeSorted = API.avgHeadsFromCodeSorted = avgHeadsFromCode.mapSortASCCopy();
    var effsFromCodeSorted     = API.effsFromCodeSorted     = effsFromCode.mapSortASCCopy();
    var hydrosFromCodeSorted   = API.hydrosFromCodeSorted   = hydrosFromCode.mapSortASCCopy();
    var meanHydroFromCode      = API.meanHydroFromCode      = hydrosFromCode.mapMean();
    var medianHydroFromCode    = API.medianHydroFromCode    = hydrosFromCodeSorted.mapMedianFromSorted();
    var minHydroFromCode       = API.minHydroFromCode       = hydrosFromCode.mapMin();
    var maxHydroFromCode       = API.maxHydroFromCode       = hydrosFromCode.mapMax();
    var stdDevHydroFromCode    = API.stdDevHydroFromCode    = hydrosFromCode.mapStdDevE();
    var meanFlowFromCode       = API.meanFlowFromCode       = flowsFromCode.mapMean();
    var medianFlowFromCode     = API.medianFlowFromCode     = flowsFromCodeSorted.mapMedianFromSorted();
    var minFlowFromCode        = API.minFlowFromCode        = flowsFromCode.mapMin();
    var maxFlowFromCode        = API.maxFlowFromCode        = flowsFromCode.mapMax();
    var stdDevFlowFromCode     = API.stdDevFlowFromCode     = flowsFromCode.mapStdDevE();
    var meanShaftFromCode      = API.meanShaftFromCode      = shaftsFromCode.mapMean();
    var medianShaftFromCode    = API.medianShaftFromCode    = shaftsFromCodeSorted.mapMedianFromSorted();
    var minShaftFromCode       = API.minShaftFromCode       = shaftsFromCode.mapMin();
    var maxShaftFromCode       = API.maxShaftFromCode       = shaftsFromCode.mapMax();
    var stdDevShaftFromCode    = API.stdDevShaftFromCode    = shaftsFromCode.mapStdDevE();
    var meanHeadFromCode       = API.meanHeadFromCode       = avgHeadsFromCode.mapMean();
    var medianHeadFromCode     = API.medianHeadFromCode     = avgHeadsFromCodeSorted.mapMedianFromSorted();
    var minHeadFromCode        = API.minHeadFromCode        = minHeadsFromCode.mapMin();
    var maxHeadFromCode        = API.maxHeadFromCode        = maxHeadsFromCode.mapMax();
    var minAvgHeadFromCode     = API.minAvgHeadFromCode     = avgHeadsFromCode.mapMin();
    var maxAvgHeadFromCode     = API.maxAvgHeadFromCode     = avgHeadsFromCode.mapMax();
    var stdDevHeadFromCode     = API.stdDevHeadFromCode     = avgHeadsFromCode.mapStdDevE();
    var meanEffFromCode        = API.meanEffFromCode        = effsFromCode.mapMean();
    var medianEffFromCode      = API.medianEffFromCode      = effsFromCodeSorted.mapMedianFromSorted();
    var minEffFromCode         = API.minEffFromCode         = effsFromCode.mapMin();
    var maxEffFromCode         = API.maxEffFromCode         = effsFromCode.mapMax();
    var stdDevEffFromCode      = API.stdDevEffFromCode      = effsFromCode.mapStdDevE();

    
    var opHoursOrder          = API.opHoursOrder         = numOpHours.orderingDESC();
    var common                = API.common               = opHoursOrder.takeWhile(function(item){ return numOpHours[item] >= minHours; });
    var numCommon             = API.numCommon            = common.length;
    var allCombos             = API.allCombos            = opHoursOrder.takeWhile(function(item){ return numOpHours[item] >= 1; });

    var meanFlowOrdering      = API.meanFlowOrdering     = meanFlowFromCode.pick(common).orderingASC();
    var meanShaftOrdering     = API.meanShaftOrdering    = meanShaftFromCode.pick(common).orderingASC();
    var meanHeadOrdering      = API.meanHeadOrdering     = meanHeadFromCode.pick(common).orderingASC();
    var meanEffOrdering       = API.meanEffOrdering      = meanEffFromCode.pick(common).orderingDESC();
    var numOpHoursOrdering    = API.numOpHoursOrdering   = UTIL.table(numCommon, function(x){ return x; });
    var effAtOptimumOrdering  = API.effAtOptimumOrdering = API.formulas.bepEff.pick(common).orderingDESC();
    var optFlowOrdering       = API.optFlowOrdering      = API.formulas.bepFlow.pick(common).orderingASC();

    //for (var i=0; i<finalTable.length; i++){ // %%%
        // finalTable[i].__proto__ = finalTableEntry; --> doesn't work in IE8
        // finalTable[i].absFlowContrib = finalTableEntry.absFlowContrib; --> works in IE8, but this way, would have to repeat for every single function separately, which is not good
        // the preceding two methods would have required to define absFlowContrib and the like directly under finalTableEntry, rather than finalTableEntry.prototype
    //    finalTable[i] = jQuery.extend(new finalTableEntry(), finalTable[i]);
    //}
}
API.makeFormulas = function(){
    var hydroFactor = API.hydroFactor;
    var pumpValid = API.pumpValid;
	var shaftEquations = new Array(API.numPumps);
	var singleCharCurves = new Array(API.numPumps);
	var singleCharCurvesFlow2Head = new Array(API.numPumps);
	var singleShutOffHead = new Array(API.numPumps);
	var indivFlow2Shaft = new Array(API.numPumps);
	var indivFlow2Hydro = new Array(API.numPumps);
	var indivBepFlow = new Array(API.numPumps);
	var indivBepEff = new Array(API.numPumps);
	for (var pmp = 0; pmp < API.numPumps; pmp++){
        if (!pumpValid[pmp + 1]){
            continue;
        }
		var rowIdList = API.formulas.rowsWherePumpRuns[pmp + 1];
		var data = rowIdList.map(function(item){var row = API.finalTable[item]; return [row.head[pmp], row.shaft[pmp]];});
		shaftEquations[pmp] = fitDataLinear(data);
		singleCharCurves[pmp] = {linCoe: API.linearDDCCCoefficients[pmp], absCoe: API.absoluteDDCCCoefficients[pmp]};
		singleCharCurvesFlow2Head[pmp] = {linCoe: 1/API.linearDDCCCoefficients[pmp], absCoe: (-API.absoluteDDCCCoefficients[pmp])/API.linearDDCCCoefficients[pmp]};
		singleShutOffHead[pmp] = (-API.absoluteDDCCCoefficients[pmp]) / API.linearDDCCCoefficients[pmp];
		indivFlow2Shaft[pmp] = composeLinearTrafos(shaftEquations[pmp], singleCharCurvesFlow2Head[pmp]);
		var scfh = singleCharCurvesFlow2Head[pmp];
		indivFlow2Hydro[pmp] = {quadCoe: scfh.linCoe * hydroFactor, linCoe: scfh.absCoe * hydroFactor, absCoe: 0};
		
		var a = indivFlow2Hydro[pmp].quadCoe;
		var b = indivFlow2Hydro[pmp].linCoe;
		var c = indivFlow2Hydro[pmp].absCoe;
		var r = indivFlow2Shaft[pmp].linCoe;
		var s = indivFlow2Shaft[pmp].absCoe;
		var w = b*s/r - a*s*s/(r*r)-c;
		var u = -a/r;
		var best = (Math.sqrt(w*r/u)-s)/r;		
		indivBepFlow[pmp] = best;
		indivBepEff[pmp] = (a*best*best + b*best + c)/(r*best + s);
		
	}
	API.formulas.shaftEquations = shaftEquations;
	API.formulas.singleCharCurves = singleCharCurves;
	API.formulas.singleCharCurvesFlow2Head = singleCharCurvesFlow2Head;
	API.formulas.singleShutOffHead = singleShutOffHead;
	API.formulas.indivFlow2Shaft = indivFlow2Shaft;
	API.formulas.indivFlow2Hydro = indivFlow2Hydro;
	API.formulas.indivBepFlow = indivBepFlow;
	API.formulas.indivBepEff = indivBepEff;
	
	var oneoneone = Math.round(Math.pow(2, API.numPumps)) - 1;
	var flowFactor = new Array(oneoneone + 1);
	for (var code = 1; code <= oneoneone; code++){
		var rowIdList = API.formulas.rowsWherePumpsRunOnly[code];
		if (rowIdList.length <= 1){
			flowFactor[code] = 1;
		} else {
			var factorList = rowIdList.map(function(id){
				var row = API.finalTable[id];
				return row.flow / row.predictedFlow;
			});
			var mean = factorList.mean();
			var std = factorList.stdDevE();
			var hoursInOperation = rowIdList.length;
			var stdErr = std / Math.sqrt(hoursInOperation);
			if (mean > 1){
				flowFactor[code] = ( mean - 1 > 2 * stdErr ? Math.min(1.02, mean - 1.3 * stdErr) : 1 );
			} else {
				flowFactor[code] = ( 1 - mean > 2 * stdErr ? Math.max(0.98, mean + 1.3 * stdErr) : 1 );
			}
		}
	}
	var headFactors = new Array(oneoneone + 1);
	for (var code = 1; code <= oneoneone; code++){
		var pmps = API.pumpsListFromCode(code);
		var enumerator = 0;
		var denominator = 0;
		for (var i=0; i<pmps.length; i++){
			var pmp = pmps[i] - 1;   // pumps numbering starts with 1, array index start with 0, hence subtract 1
			enumerator += API.headRatios[pmp] * API.meanShaftsTable[pmp];
			denominator += API.meanShaftsTable[pmp];
		}
		// enumerator / denominator is the shaft power weighted mean of the running pumps of that code
		headFactors[code] = API.headRatios.linTrafo(denominator / enumerator);
	}/*
	head2Flow has only one head as input, and we want the flow at that head
	we have to make several heads out of that one head, according to usual ratios between the heads of the different pumps.
	In addition, we multiply with "flowfactor" for some correction.
	*/
	var head2Flow = new Array(oneoneone + 1);
	var head2Flows = new Array(oneoneone + 1);
	var flow2Head = new Array(oneoneone + 1);
	var flow2Heads = new Array(oneoneone + 1);
	var head2Shaft = new Array(oneoneone + 1);
	var head2SingleHydro = new Array(oneoneone + 1);
	var head2TotalHydro = new Array(oneoneone + 1);
	var flowContrib2Flow = new Array(oneoneone + 1);
	var flow2SingleHydro = new Array(oneoneone + 1);
	var flow2TotalHydro = new Array(oneoneone + 1);
	var flow2SingleShaft = new Array(oneoneone + 1);
	var flow2TotalShaft = new Array(oneoneone + 1);
	var shutOffFlow = new Array(oneoneone + 1);
	var hiFlow = new Array(oneoneone + 1);
	var bepFlow = new Array(oneoneone + 1);
	var bepEff = new Array(oneoneone + 1);
	//var head2SingleEff = new Array(oneoneone + 1);
	//var head2TotalEff = new Array(oneoneone + 1);
	var flow2FlowContrib = new Array(oneoneone + 1);
	for (var code = 1; code <= oneoneone; code++){
		var ff = flowFactor[code];
		head2Flows[code] = new Array(API.numPumps);
		head2SingleHydro[code] = new Array(API.numPumps);
		flow2Heads[code] = new Array(API.numPumps);
		flow2FlowContrib[code] = new Array(API.numPumps);
		flowContrib2Flow[code] = new Array(API.numPumps);
		flow2SingleHydro[code] = new Array(API.numPumps);
		flow2SingleShaft[code] = new Array(API.numPumps);
		//head2SingleEff = new Array(API.numPumps);
		for (var i=0; i<API.numPumps; i++){
			head2Flows[code][i] = {linCoe: 0, absCoe: 0};
			head2SingleHydro[code][i] = {quadCoe: 0, linCoe: 0, absCoe: 0};
			flow2Heads[code][i] = {linCoe: 0, absCoe: 0};
			flow2FlowContrib[code][i] = {linCoe: 0, absCoe: 0};
			flowContrib2Flow[code][i] = {linCoe: 0, absCoe: 0};
			flow2SingleHydro[code][i] = {quadCoe: 0, linCoe: 0, absCoe: 0};
			flow2SingleShaft[code][i] = {linCoe: 0, absCoe: 0};
		}
		head2TotalHydro[code] = {quadCoe: 0, linCoe: 0, absCoe: 0};
		var pmps = API.pumpsListFromCode(code);
		var hfs = headFactors[code];
		var lin = 0; var abs = 0;    // for char curve
		var linShaft = 0; var absShaft = 0;
		var quadHyd = 0; var linHyd = 0;
		for (var i=0; i<pmps.length; i++){
			var pmp = pmps[i] - 1;
            if (!pumpValid[pmp + 1]){
                continue;
            }
			var headFactor = hfs[pmp];
			var scc = singleCharCurves[pmp];
			lin += headFactor * scc.linCoe;
			abs += scc.absCoe;
			head2Flows[code][pmp] = {linCoe: ff * headFactor * scc.linCoe, absCoe: ff * scc.absCoe};
			var sheq = shaftEquations[pmp];
			linShaft += headFactor * sheq.linCoe;
			absShaft += sheq.absCoe;
			// hydro = const * flow * head; hence, the hydro functions come from multiplying the flow functions with h,
			// lifting the coefficients by 1 degree, so to speak.
			head2SingleHydro[code][pmp].quadCoe = hydroFactor * headFactor * head2Flows[code][pmp].linCoe;
			head2SingleHydro[code][pmp].linCoe = hydroFactor * headFactor * head2Flows[code][pmp].absCoe;
			quadHyd += head2SingleHydro[code][pmp].quadCoe;
			linHyd += head2SingleHydro[code][pmp].linCoe;
			//head2SingleEff = ... don't have data structure for rational polynomials right now
		}
		lin *= ff; abs *= ff;
		head2Flow[code] = {linCoe: lin, absCoe: abs};
		hiFlow[code] = abs;
		flow2Head[code] = invertLinearTrafo(head2Flow[code]);
		head2Shaft[code] = {linCoe: linShaft, absCoe: absShaft};
		head2TotalHydro[code].quadCoe = quadHyd;
		head2TotalHydro[code].linCoe = linHyd;
		//head2TotalEff[code] = head2TotalHydro[code] / head2Shaft[code];  ... don't have data structure for rational polynomials right now
		flow2TotalHydro[code] = zeroQuadTrafo();
		flow2TotalShaft[code] = {linCoe: 0, absCoe: 0};
		var shutOffFlowSoFar = 0;
		for (var i=0; i<pmps.length; i++){
			var pmp = pmps[i] - 1;
            if (!pumpValid[pmp + 1]){
                continue;
            }
			var headFactor = hfs[pmp];
			flow2Heads[code][pmp] = {linCoe: flow2Head[code].linCoe * headFactor, absCoe: flow2Head[code].absCoe * headFactor};
			flow2FlowContrib[code][pmp] = linTrafoTimesConst(composeLinearTrafos(singleCharCurves[pmp], flow2Heads[code][pmp]), ff);
			flowContrib2Flow[code][pmp] = invertLinearTrafo(flow2FlowContrib[code][pmp]);
			flow2SingleHydro[code][pmp] = quadTrafoTimesConst(linTrafoTimesLinTrafo(flow2FlowContrib[code][pmp], flow2Heads[code][pmp]), hydroFactor);
			quadTrafoAddSecondToFirst(flow2TotalHydro[code], flow2SingleHydro[code][pmp]);
			flow2SingleShaft[code][pmp] = composeLinearTrafos(shaftEquations[pmp], flow2Heads[code][pmp]);
			linTrafoAddSecondToFirst(flow2TotalShaft[code], flow2SingleShaft[code][pmp]);
			var shutOffFlowCandidate = (singleShutOffHead[pmp] - flow2Heads[code][pmp].absCoe) / flow2Heads[code][pmp].linCoe;
			shutOffFlowSoFar = Math.max(shutOffFlowSoFar, shutOffFlowCandidate);
		}
		shutOffFlow[code] = shutOffFlowSoFar;
	}
	var shutOffHead = new Array(oneoneone + 1);
	for (var code = 1; code <= oneoneone; code++){
		var pmps = API.pumpsListFromCode(code);
		var hfs = headFactors[code];
		var shutOff = Infinity;
		for (var i=0; i<pmps.length; i++){
			var pmp = pmps[i] - 1;
            if (!pumpValid[pmp + 1]){
                continue;
            }
			var headFactor = hfs[pmp];
			var h = (singleShutOffHead[pmp]) / headFactor;
			shutOff = Math.min(shutOff, h);
		}
		
		var sh = flow2TotalShaft[code];
		var hy = flow2TotalHydro[code];
		var a = hy.quadCoe;
		var b = hy.linCoe;
		var c = hy.absCoe;
		var r = sh.linCoe;
		var s = sh.absCoe;
		var w = b*s/r - a*s*s/(r*r)-c;
		var u = -a/r;
		var best = (Math.sqrt(w*r/u)-s)/r;		
		bepFlow[code] = best;
		bepEff[code] = (a*best*best + b*best + c)/(r*best + s);
		
		shutOffHead[code] = shutOff;
	}
	
	API.formulas.flowFactor = flowFactor;
	API.formulas.headFactors = headFactors;
	API.formulas.head2Flow = head2Flow;
	API.formulas.head2Flows = head2Flows;
	API.formulas.head2Shaft = head2Shaft;
	API.formulas.head2SingleHydro = head2SingleHydro;
	API.formulas.head2TotalHydro = head2TotalHydro;
	API.formulas.flow2FlowContrib = flow2FlowContrib;
	API.formulas.flowContrib2Flow = flowContrib2Flow;
	API.formulas.flow2SingleHydro = flow2SingleHydro;
	API.formulas.flow2TotalHydro = flow2TotalHydro;
	API.formulas.flow2SingleShaft = flow2SingleShaft;
	API.formulas.flow2TotalShaft = flow2TotalShaft;
	//API.formulas.head2SingleEff = head2SingleEff;
	//API.formulas.head2TotalEff = head2TotalEff;
	API.formulas.flow2Head = flow2Head;
	API.formulas.flow2Heads = flow2Heads;
	API.formulas.shutOffHead = shutOffHead;
	API.formulas.shutOffFlow = shutOffFlow;
	API.formulas.hiFlow = hiFlow;
	API.formulas.bepFlow = bepFlow;
	API.formulas.bepEff = bepEff;
    API.formulas.meanAbsoluteFlowContrib = [];
    var pumpsWithValidData = API.pumpsWithValidData;
    for (var i = 0; i < pumpsWithValidData.length; i++){
        var pmp = pumpsWithValidData[i];
        API.formulas.meanAbsoluteFlowContrib[pmp] = API.calcMeanAbsoluteFlowContrib(pmp);
    }
}
API.calcRowsWhere = function (){
    // later: have hasError function as a parameter, to cope with rows being put back in with the errors tab.
    function hasError(row){
        return row.errors.length > 0;
    }
    var twoPowNumPumps = Math.round(Math.pow(2, API.numPumps));
    var rowsWherePumpsRunOnly = new Array(twoPowNumPumps);                     // only without errors
    var rowsWherePumpRuns = new Array(API.numPumps);                           //        "
    
    var rowsWherePumpsRunOnlyError = new Array(twoPowNumPumps);                // only with errors
    var rowsWherePumpRunsError = new Array(API.numPumps);                      //        "
    
    var rowsWherePumpsRunOnlyAll = new Array(twoPowNumPumps);                  // all
    var rowsWherePumpRunsAll = new Array(API.numPumps);                        //        "
    
    for (var c = 1; c < twoPowNumPumps; c++){
        rowsWherePumpsRunOnly[c] = new Array();
        rowsWherePumpsRunOnlyError[c] = new Array();
        rowsWherePumpsRunOnlyAll[c] = new Array();
    }
    for (var pmp = 1; pmp <= API.numPumps; pmp++){
        rowsWherePumpRuns[pmp] = new Array();
        rowsWherePumpRunsError[pmp] = new Array();
        rowsWherePumpRunsAll[pmp] = new Array();
    }
    
	for (var i=0; i<API.finalTable.length; i++){              // %%%
		var row = API.finalTable[i];
        if (row.seriousError){
            continue;
        }
        rowsWherePumpsRunOnlyAll[row.runCode].push(i);
        if (hasError(row)){
            rowsWherePumpsRunOnlyError[row.runCode].push(i);
        } else {
            rowsWherePumpsRunOnly[row.runCode].push(i);
        }
        for (var p = 0; p < API.numPumps; p++){
            if (row.runBits[p]){
                rowsWherePumpRunsAll[p+1].push(i);
                if (hasError(row)){
                    rowsWherePumpRunsError[p+1].push(i);
                } else {
                    rowsWherePumpRuns[p+1].push(i);
                }
            }
        }
	}
    API.formulas = {};
    API.formulas.rowsWherePumpsRunOnly = rowsWherePumpsRunOnly;
    API.formulas.rowsWherePumpsRunOnlyError = rowsWherePumpsRunOnlyError;
    API.formulas.rowsWherePumpsRunOnlyAll = rowsWherePumpsRunOnlyAll;
    API.formulas.rowsWherePumpRuns = rowsWherePumpRuns;
    API.formulas.rowsWherePumpRunsError = rowsWherePumpRunsError;
    API.formulas.rowsWherePumpRunsAll = rowsWherePumpRunsAll;
}
API.makeDDCCcleaned = function(maxError){
    var ddccErrTooBig = "char curve mean squared error too big";
    for (var i=0; i<API.finalTable.length; i++){
        var row = API.finalTable[i];
        row.forsaken = false;
        row.errors = row.errors.select(function(err){
            return err != ddccErrTooBig;
        });
    }
    var numIterations = 0;
    // param maxError: if the error of 
    // the the least squares fit for the char curves using makeDDCC, then thorw out some outliers
    // (data points where the error (arrording to this curve) is a lot higher than the average)
    // and then repeat the process with the outliers removed from the data, until no outliers are there any more
    while (true){
        numIterations++;
        var ddcc = API.makeDDCC();
        //var linCoes = ddcc.slice(0, API.numPumps);
        //var absCoes = ddcc.slice(API.numPumps);
        var linCoes = ddcc.linCoes;
        var absCoes = ddcc.absCoes;
        var badApples = [];   // data rows exceeding maxError
        var maxOccurringError = 0;   // error of data row with worst error
        for (var rowIndex = 0; rowIndex < API.finalTable.length; rowIndex++){              // %%%
            var row = API.finalTable[rowIndex];
            var prediction = 0;
            for (var p = 0; p < row.pumpsList.length; p++){
                var pump = row.pumpsList[p] - 1;
                prediction += linCoes[pump] * row.head[pump] + absCoes[pump];
            }
            var factual = row.flow;
            var diff = factual - prediction;
            var squaredDiff = diff * diff;
            row.pred = prediction;
            row.err = squaredDiff;
            if (squaredDiff >= maxError){
                badApples.push(rowIndex);
                if (squaredDiff > maxOccurringError && row.errors.length == 0){
                    maxOccurringError = squaredDiff;           // max error among the data rows which are still "in the race"
                }
            }
        }
        if (maxOccurringError <= maxError){
            return ddcc;
        }
        var outlierThreshold = 0.5 * (maxOccurringError + maxError);         // the error above which we throw out the data row in this iteration
        for (var badAppleIndex = 0; badAppleIndex < badApples.length; badAppleIndex++){
            var badRow = API.finalTable[badApples[badAppleIndex]];
            if (badRow.err >= outlierThreshold){
                if (!badRow.forsaken){
                    badRow.errors.push(ddccErrTooBig);
                    badRow.forsaken = true;    // this if statement here is just so that the row doesn't get marked with "mse too big" multiple times
                }
            }
        }
    }
}
API.makeDDCC = function(){
    // perform least squares fit of characteristic curves, using all data with no errors
    function makeLin(index){
    	return function(row){
    		if (row.runBits[index] == 1){
    			return row.head[index]
    		} else {
    			return 0;
    		}
    	}
    }
    function makeAbs(index){
    	return function(row){
    		return row.runBits[index];
    	}
    }
    var pumpind = API.pumpsWithValidData.map(function(p) { return p-1; });
    //var pumpind = [0,1,2,3,4,5,6,7];
    var baseFunsLin = pumpind.map(makeLin);
    var baseFunsAbs = pumpind.map(makeAbs);
    var baseFuns = baseFunsLin.concat(baseFunsAbs);
    var noErrorRows = API.finalTable.select(function(item) {
    	return item.errors.length == 0;
    });
    var theData = noErrorRows.map(function(item){ return {x: item, y: item.flow}});
    
    var result = linearRegression(baseFuns, theData);
    
    var linCoes = UTIL.range(0, API.numPumps - 1);
    var absCoes = UTIL.range(0, API.numPumps - 1);
    
    for (var i=0; i<pumpind.length; i++){
        linCoes[pumpind[i]] = result[i];
        absCoes[pumpind[i]] = result[i + pumpind.length];
    }
    return {
        linCoes: linCoes,
        absCoes: absCoes
    };
}
function makeLinearFunction(coeObj){  // used in charCurves
    return function(arg){
        return coeObj.linCoe * arg + coeObj.absCoe;
    }
}
function makeQuadraticFunction(coeObj){
    return function(arg){
        return (coeObj.quadCoe*arg + coeObj.linCoe) * arg + coeObj.absCoe;
    }
}
function zerosOfQuadraticFunction(coeObj){
    var a = coeObj.quadCoe;
    var b = coeObj.linCoe;
    var c = coeObj.absCoe;
    var disc = b*b - 4*a*c;
    if (disc < 0){
        return [];
    }
    var denom = 2*a;
    if (Math.abs(disc) < 0.00000000001){
        return [-b/denom] 
    }
    var s = Math.sqrt(disc);
    var sol_1 = (-b + s) / denom;
    var sol_2 = (-b - s) / denom;
    var smaller = Math.min(sol_1, sol_2);
    var bigger  = Math.max(sol_1, sol_2);
    return [smaller, bigger];
}
function divideFunctions(fun1, fun2){
    return function(arg){
        return fun1(arg) / fun2(arg);
    }
}
function times100(fun){
    return function(arg){
        return 100 * fun(arg);
    }
}
