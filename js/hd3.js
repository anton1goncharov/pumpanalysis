function TAG_FACTORY(tagName){
    return function(f, attr, style){
        var o = {
            tag   : tagName,
            isTag : true
        };
        if (attr){
            o.attr = attr;
        }
        if (style){
            o.style = style;
        }
        if (f){
            if (typeof f == "object"){
                if (f.isTag){
                    o.kids = [f];
                    return o;
                }
                if (f.constructor == Array){
                    o.kids = f;
                    return o;
                }
            }
            if (typeof f == "string"){
                o.html = f;
                return o;
            }
            if (typeof f == "number"){
                o.text = f;
                return o;
            }
            throw new Error("illegal argument");
        }
        return o;
    }
}
function TAG_FACTORY_EMPTY(tagName){
    return function(attr, style){
        var o = {
            tag   : tagName,
            isTag : true
        };
        if (attr){
            o.attr = attr;
        }
        if (style){
            o.style = style;
        }
        return o;
    }
}
function HTML(o){
    function nameEqVal(name, val){
        return name + "=\"" + val + "\"";
    }
    function attAux(att, key){
        return nameEqVal(key, att[key]);
    }
    function styAux(sty, key){
        return key + ":" + sty[key];
    }
    if (!(typeof o == "object")){
        throw new Error("illegal argument");
    }
    if (o.isTag){
        var pre = "<" + o.tag;
        if ("attr" in o){
            var ATT = o.attr;
            var K = d3.keys(ATT);
            if (K.length > 0){
                pre += " " + K.map(function(key){ return attAux(ATT, key); }).join(" ");
            }
        }
        if ("style" in o){
            var STY = o.style;
            var K = d3.keys(STY);
            if (K.length > 0){
                pre += " " + nameEqVal("style", K.map(function(key){ return styAux(STY, key); }).join(";") + ";");
            }
        }
        pre +=  ">";
        var post = "</" + o.tag + ">";
        if ("kids" in o){
            return pre + o.kids.map(HTML).join("") + post;
        } else {
            if ("html" in o){
                return pre + o.html + post;
            } else {
                if ("text" in o){
                    return pre + o.text + post;
                }
            }
        }
    }
    if (o.constructor == Array){
        return o.map(function(entry){return HTML(entry);}).join("");
    }
    throw new Error("illegal argument");
}
(function(){
    var tagNames = ["ul", "li", "div", "span", "table", "tr", "td", "a", "button", "i"];
    for (var i=0; i<tagNames.length; i++){
        var tagName = tagNames[i];
        window[tagName.toUpperCase()] = TAG_FACTORY(tagName);
    }
    var emptyTagNames = ["input", "br", "img", "canvas"];
    for (var i=0; i<emptyTagNames.length; i++){
        var tagName = emptyTagNames[i];
        window[tagName.toUpperCase()] = TAG_FACTORY_EMPTY(tagName);
    }
})();

function APPEND(container, content){
    if (typeof content != "object"){
        throw new Error("illegal second argument: " + content);
    }
    if ( (typeof content == "object") && (content.constructor == Array) ){
        return content.map(function(c){
            return APPEND(container, c);
        });
    }
    var elt = container.append(content.tag);
    if ("kids" in content){
        for (var i=0; i<content.kids.length; i++){
            APPEND(elt, content.kids[i]);
        }
    } else {
        if ("html" in content){
            elt.html(content.html);
        } else {
            if ("text" in content){
                elt.text(content.text);
            }
        }
    }
    if ("attr" in content){
        elt.attr(content.attr);
    }
    if ("style" in content){
        elt.style(content.style);
    }
    return elt;
}

function trans(x, y){
    return "translate(" + x + "," + y + ")";
}
function rot(angle){
    return "rotate(" + angle + ")";
}
function scale(sx, sy){
    return "scale(" + sx + "," + sy + ")";
}
function getTrans(elt){
    // call only if sure that elt transform attribute has the form translate(x,y)
    var d3Elt = d3.select(elt);
    var trafoString = d3Elt.attr("transform");
    var str = trafoString.slice(10, -1);
    if (str == "0") return {
        x: 0, y: 0
    };
    var comps;
    if (str.indexOf(",") >= 0){
        comps = str.split(",");
    } else {
        if (str.indexOf(" ") >= 0){
            comps = str.split(" ");
        } else {
            console.log("unexpected format error of translate transform");
            debugger; return null;
        }
    }
    comps = comps.map(function(s){
        var val = parseFloat(s);
        if (isNaN(val)){
            console.log("unexpected format error of translate transform");
            debugger;
        }
        return val;
    });
    if (comps.length != 2){
        console.log("unexpected format error of translate transform");
        debugger; return null;
    }
    return {
        x: comps[0], 
        y: comps[1] 
    }
}
function getScale(elt){
    // call only if sure that elt transform attribute has the form scale(x,y)
    var d3Elt = d3.select(elt);
    var trafoString = d3Elt.attr("transform");
    var str = trafoString.slice(6, -1);
    if (str == "1") return {
        x: 1, y: 1
    };
    var comps;
    if (str.indexOf(",") >= 0){
        comps = str.split(",");
    } else {
        if (str.indexOf(" ") >= 0){
            comps = str.split(" ");
        } else {
            var temp = parseFloat(str);
            if (isNaN(temp)){
                console.log("unexpected format error of scale transform");
                debugger; return null;
            } else {
                return {
                    x: temp, 
                    y: temp 
                };
            }

        }
    }
    comps = comps.map(function(s){
        var val = parseFloat(s);
        if (isNaN(val)){
            console.log("unexpected format error of scale transform");
            debugger;
        }
        return val;
    });
    if (comps.length != 2){
        console.log("unexpected format error of scale transform");
        debugger; return null;
    }
    return {
        x: comps[0], 
        y: comps[1] 
    };
}
function getViewBox(elt){
    var vb = elt.attr("viewBox");
    var temp = vb.split(" ");
    temp = temp.map(function(x){return parseFloat(x);});
    return temp;
}
function getRotation(elt){
    return parseFloat(elt.attr("transform").slice(7, -1))*Math.PI/180;
}
function anchestor(elt, times){
    while (times > 0){
        if (elt == null){
            return null;
        }
        elt = elt.parentNode;
        times--;
    }
    return elt;
}
function hasAnchestorWithClass(elt, className){
    while (true){
        if (elt == null){
            return false;
        }
        if (elt.constructor == HTMLDocument){
            return false;
        }
        var d3Elt = d3.select(elt);
        if (d3Elt == null){
            debugger;
        }
        if (!("classed" in d3Elt)){
            debugger;
        }
        if (d3Elt.classed(className)){
            return true;
        }
        elt = elt.parentNode;
    }
}
function getClassNameOfClosestAncestorWithClass(elt){
    while (true){
        if (elt == null){
            return "-";
        }
        if (elt.constructor == HTMLDocument){
            return "-";
        }
        var d3Elt = d3.select(elt);
        if (d3Elt == null){
            debugger;
        }
        var className = d3Elt.attr("class");
        if (className && className.length){
            return className;
        }
        elt = elt.parentNode;
    }
}
function kidClone(source, target){
    // clone all children of source (deep), and append them to target
    for (var i=0; i<source.childNodes.length; i++){
        var child = source.childNodes[i];
        var childClone = child.cloneNode(true);
        target.node().appendChild(childClone);
    }
}
function comp(f, g){
    return function(arg){
        return f(g(arg));
    }
}
function get(prop){
    return function(obj){
        return obj[prop];
    }
}

function removeDuplicates(arr){
    var o = {};
    for (var i=0; i<arr.length; i++){
        o[arr[i]] = null;
    }
    return d3.keys(o);
}
// http://stackoverflow.com/questions/13983864/how-to-make-a-d3-plugin
d3.selection.prototype.translate = d3.selection.enter.prototype.translate = function(x,y){
    return this.attr("transform", "translate(" + x + "," + y + ")");
}
d3.selection.prototype.scale = d3.selection.enter.prototype.scale = function(sx, sy){
    return this.attr("transform", "scale(" + sx + "," + sy + ")");
}
d3.selection.prototype.rotate = d3.selection.enter.prototype.rotate = function(angle){
    return this.attr("transform", "rotate(" + angle + ")");
}
d3.selection.prototype.attProp = d3.selection.enter.prototype.attProp = function(attName, propertyName){
    return this.attr(attName, function(d,i,j){ return d[propertyName]; });
}
