var queryStringObj = UTIL.GET();
var debugFlag = thisIsThomasKloeckersLocalMachine || !!(queryStringObj && ("debug" in queryStringObj));
//var useStaticData = thisIsThomasKloeckersLocalMachine || (!!(queryStringObj && ("useStaticData" in queryStringObj)));
var useStaticData = (!!(queryStringObj && ("useStaticData" in queryStringObj)));  // currently NOT USED!!
var useExternalServer = (!!(queryStringObj && ("externalServer" in queryStringObj)));

function setUpShowHideCheckbox(parentId, checkId, targetId){
    // checkId: id of the checkbox
    // parentId: id of the parent div of the checkbox
    // targetId: id of that which is shown or hidden based on checkbox status
    // div with id parentId must already exist
    // content of the div (checkbox and text) be generated, and so will the handler
    // always start with checkbox being checked (element should be initially shown to fit this choice),
    // so do not remember user choice of checkbox from last time.
    var parent = $("#" + parentId);
    var check = HtmlGen.checkbox(true, {id: checkId});
    var text = "show controls";
    parent.html(check + text);
    document.getElementById(checkId).checked = true;
    jQuery("#" + checkId).click(function(){
        var controls = jQuery("#" + targetId);
        if (this.checked){
            controls.show();
        } else {
            controls.hide();
        }
    });
}
function Progress(parent, opts){
    var radius = this.radius = "radius" in opts ? opts.radius : 55;
    var margin = this.margin = "margin" in opts ? opts.margin : 8;
    var innerPortion = this.innerPortion = "innerPortion" in opts ? opts.innerPortion : 0.8;
    var size = this.size = 2 * (radius + margin);
    var width = size;
    var height = size;
    this.parent = parent;
    var svg = this.svg = parent.append("svg").attr("width", width).attr("height", height);
    var rm = radius + margin;
    var g = this.g = svg.append("g").translate(rm, rm);
    var innerRadius = this.innerRadius = innerPortion * radius;
    var arc = this.arc = d3.svg.arc().outerRadius(radius).innerRadius(innerRadius);
}
Progress.prototype.set = function(value){
    var g = this.g;
    var arc = this.arc;
    g.selectAll("*").remove();
    var twoPi = 2 * Math.PI;
    var angle = value * twoPi;
    g.selectAll("path.arc").data([
        {startAngle:     0, endAngle: angle},
        {startAngle: angle, endAngle: twoPi}
    ]).enter().append("path")
        .attr("class", "arc")
        .attr("fill", function(d, i){ return ["steelblue", "#c5c5c5"][i]; })
        .attr("d", function(d, i){ return arc(d, i); });
    var txt = Math.round(100*value) + "%";
    g.append("text").text(txt).attr("text-anchor", "middle").attr("font-size", "22px").attr("y", 6)
}

/*
function getServletURL(){
    var baseURL = "../dts10";
    var startDateString = "2012-05-08 22";
    var endDateString = "2014-12-21 07";
    var qsParams = {
        start: startDateString,
        end  : endDateString,
        t0 : "POWF_M1_KW",
        t1 : "POWF_M2_KW",
        t2 : "POWF_M3_KW",
        t3 : "POWF_M4_KW",
        t4 : "POWF_M5_KW",
        t5 : "POWF_M6_KW",
        t6 : "POWF_M7_KW",
        t7 : "POWF_M8_KW",
        t8 : "POWF_M1PMPRN",
        t9 : "POWF_M2PMPRN",
        t10 : "POWF_M3PMPRN",
        t11 : "POWF_M4PMPRN",
        t12 : "POWF_M5PMPRN",
        t13 : "POWF_M6PMPRN",
        t14 : "POWF_M7PMPRN",
        t15 : "POWF_M8PMPRN",
        t16 : "POWF_M1_DIFF",
        t17 : "POWF_M2_DIFF",
        t18 : "POWF_M3_DIFF",
        t19 : "POWF_M4_DIFF",
        t20 : "POWF_M5_DIFF",
        t21 : "POWF_M6_DIFF",
        t22 : "POWF_M7_DIFF",
        t23 : "POWF_M8_DIFF",
        t24 : "POWF_MZFLOW",
        t25 : "AVST_STLEV",
        t26 : "BLST_STLEV1",
        t27 : "BLST_STLEV2",
        t28 : "NWST_STLEV",
        t29 : "WHWP_RTLEV1",
        t30 : "WHWP_RTLEV2",
        t31 : "WHWP_RTLEV3",
        t32 : "WHWP_RTLEV4",
        t33 : "WLST_STLEV"
    };
    var qsParams = {
        start: startDateString,
        end  : endDateString,
        what : "potomacEff"
    };
    return baseURL + HtmlGen.makeQueryString(qsParams);
}
*/
function mySqlHourlyDateStringFromDate(date){
    function X(a){
        if (a < 10){
            return "0" + a;
        }
        return a;
    }
    return [[date.getFullYear(), X(1+date.getMonth()), X(date.getDate())].join("-"), X(date.getHours())].join(" ");
}


/* old code to get the manufacturer curves
jQuery.ajax({
    url: "data/mfgrCharCurveData.json",
    dataType: "json",
    success: function(response) {
		mfgrCharCurve = response;
        //console.log("mfgrCharCurveData.json loaded");
    }, 
    error: function(response, error) {
        $("#loadInfo").html("error loading mfgrCharCurve");
        console.log("error");
    }
});
jQuery.ajax({
    url: "data/mfgrEffCurveData.json",
    dataType: "json",
    success: function(response) {
		mfgrEffCurve = response;
        //console.log("mfgrEffCurveData.json loaded");
    }, 
    error: function(response, error) {
        $("#loadInfo").html("error loading mfgrEffCurve");
        console.log("error");
    }
});
*/

MainController = {
    loadRecentData : true,
    serverAnswerCount : 0,
    initProgressBar: function(){
        var progressBarSuperContainer = d3.select("#progressbarParent");
        progressBarSuperContainer.html('<table  id="progressBarSuperContainer"><tr><td align="center" id="progressBarContainer"></td></tr><tr><td id="progressBarTableContainer"></td></tr></table>');
        var progressBarContainer = d3.select("#progressBarContainer");
        progressBarTableContainer = d3.select("#progressBarTableContainer");
        this.theProgressBar = new Progress(progressBarContainer, {});
    },
    bodyOnload: function (){
        TIMESERIES.html0 = $("#timeSeriesTab").html();
        body0 = $("body").html();
        //$("#loadInfo").html("loading ...");
        this.initProgressBar();
        this.loadCsvManifest();
    },
    updateLoadProgress: function(){
        var csvManifestData = this.csvManifestData;
        var totalLoadedCSVbytes = csvManifestData.calcTotalLoadedCSVbytes();
        var factor = this.sqlFinished ? 0.99 : 0.95;
        var totalPercentDone = factor * totalLoadedCSVbytes / csvManifestData.totalCSVfileSize;
        this.theProgressBar.set(totalPercentDone);
        var _now = new Date();
        var nowString = [_now.getHours(), _now.getMinutes(), _now.getSeconds(), _now.getMilliseconds()].join(":");
        console.log(nowString + " !>>> " + [totalLoadedCSVbytes, totalPercentDone].join(", "));
    },
    loadCsvManifest: function(){
        var THIS = this;
        d3.csv(Config.URLs.CSVmanifest, function(error, data){
            if (error){
                alert("fatal error: data manifest not found");
            } else {
                THIS.csvManifestData = data;
                THIS.afterCsvManifestLoaded();
            }
        });
    },
    afterCsvManifestLoaded: function(){
        var csvManifestData = this.csvManifestData;
        var loadErrorFlag = false;
        var THIS = this;
        var totalRequestCount = csvManifestData.length;
        if (this.loadRecentData){
        	totalRequestCount++;
        }
        csvManifestData.totalCSVfileSize = 0;
        csvManifestData.calcTotalLoadedCSVbytes = function(){
        	return this.reduce(function(accumulator, item){ return accumulator + item.loaded; }, 0);
        }
        csvManifestData.forEach(function(csvManifestDatum, csvIndex){
        	var d = csvManifestDatum;
        	d.loaded = 0;
        	var startDate = d.startDate;
        	var endDate = d.endDate;
        	var fileSize = parseInt(d.fileSize);
        	csvManifestData.totalCSVfileSize += fileSize;
        	var fileName = "data/" + d.fileName;
        	var newRequest = d3.csv(fileName, function(error, data){
        		if (error){
        			Error = error;
        			alert("failure to get cached CSV data from " + fileName);
        			loadErrorFlag = true;
        			return;
        		}
        		d.response = data;
        		++THIS.serverAnswerCount;
        		if (THIS.serverAnswerCount == totalRequestCount){
        			THIS.serverDataArrived();
        		}
        	});
        	d.req = newRequest;
        	d.xhr = null;
        	newRequest.on("progress", function(XHR){
        		if (d.xhr == null){
        			d.xhr = XHR;
        			XHR.addEventListener("progress", function(evt){
        				if (evt.lengthComputable){
        					var singlePercentComplete = evt.loaded / evt.total;
        					d.loaded = evt.loaded;
        					var str = Math.round(100 * singlePercentComplete) + "%";
                            var _now = new Date();
                            var nowString = [_now.getHours(), _now.getMinutes(), _now.getSeconds(), _now.getMilliseconds()].join(":");
                            console.log(nowString + " >>> CSV " + csvIndex + ": " + str + " (" + evt.loaded + " of " + evt.total + ")");
        					THIS.updateLoadProgress();
        				}
        			});
        		}
        	});
        });
        if (this.loadRecentData){
        	// want to request from database starting at 1 hour later than the latest cached CSV left off until now.
        	var lastCSV = csvManifestData[csvManifestData.length - 1];
        	var lastCSVendString = lastCSV.endDate;
        	var endYear = parseInt(lastCSVendString.slice(0, 4));
        	var endMonth = parseInt(lastCSVendString.slice(5, 7));
        	var endDay = parseInt(lastCSVendString.slice(8, 10));
        	var endHour = parseInt(lastCSVendString.slice(11, 13));
        	var oneHourLater = new Date(endYear, endMonth-1, endDay, endHour + 1);
        	var oneHourLaterString = mySqlHourlyDateStringFromDate(oneHourLater);
        	var currentDate = new Date();
        	var currentDateString = mySqlHourlyDateStringFromDate(currentDate);
        	var sqlUrl = Config.URLs.SQL + "?what=potomacEff&start=" + escape(oneHourLaterString) + "&end=" + escape(currentDateString);
            if (useExternalServer){
                sqlUrl += "&externalServer";
            }
        	var SQLREQ = d3.csv(sqlUrl, function(error, data){
        		++THIS.serverAnswerCount;
        		if (error){
        			Error = error;
        			console.error(error);
        			loadErrorFlag = true;
        			alert("loading of recent data failed. cached data ends " + lastCSVendString);
        			THIS.dataFromSQL = [];
        		} else {
         			THIS.sqlFinished = true;
        			THIS.updateLoadProgress();
        			THIS.dataFromSQL = data;
        		}
        		if (THIS.serverAnswerCount == totalRequestCount){
        			THIS.serverDataArrived();
        		}
        	});
        }

    },
    serverDataArrived : function(){
        console.log("data arrived");
        var dataFromSQL = this.dataFromSQL;
        var csvManifestData = this.csvManifestData;
        var THIS = this;
        window.setTimeout(function(){
            var data = [];
            csvManifestData.forEach(function(csvManifestDatum, csvIndex){
                data = data.concat(csvManifestDatum.response)
            });
            if (THIS.loadRecentData){
                data = data.concat(dataFromSQL);
            }
            THIS.parseServerData(data);
        },1);
    },
    parseServerData : function(data){
        API.init0(data);
        $("#loadInfo").html("loaded");
        DEBUG = [];
        for (var i=0; i<=9; i++){
            var elt = jQuery("#debug" + i);
            if (debugFlag){
                elt.show();
            }
            DEBUG[i] = elt;
        }
        theBody = d3.select("body");
        window.setTimeout(function(){ MainController.initialize(API.finalTableAll, true)}, 1);
    },
    initialize: function(response, firstRun){
        /*
        if (!firstRun){
            PROGBAR.start();
            PROGBAR.finish();
        }
        */
        document.getElementById("includeErrors").checked = false;
        TIMESERIES.includeErrors = false;
        setUpShowHideCheckbox("showHideSingleAndCombCheckParent", "showHideSingleAndCombCheck", "combinedAndSingleControls");
        setUpShowHideCheckbox("pumpComboComparisonCheckboxParent", "pumpComboComparisonCheck", "pccControls");
        setUpShowHideCheckbox("pumpComboAnalysisCheckboxParent", "pumpComboAnalysisCheck", "pumpCombinationAnalysisControls");
        API.init1(response, ERRORTAB.maxAbsError * ERRORTAB.maxAbsError, Config.minHours);
    	$("#tabs").tabs();
        tabObjects = {
            timeSeries                 : TIMESERIES,
            statistics                 : STATISTICS,
            charCurve                  : CHARCURVES,
            pumpCombinationPerformance : PUMP_COMB_PERF,
            pumpComboAnalysis          : PUMPCOMB_ANALYSIS,
            pumpComboComparison        : PUMPCOMB_COMPARISON,
            recommendations            : RECOMMENDATIONS,
            combinedSingle             : COMBINED_AND_SINGLE,
            ophrsEffByFlowRange        : OPHRSEFF,
            effCurve                   : EFFCURVES,
            Rebuild                    : REBUILD,
            dateRange                  : DATERANGE,
            error                      : ERRORTAB
        };
        resizeableObjectNames = [];
        resizeableObjects = {};
        for (var nameOfTab in tabObjects){
            chartSizeInit_preMake_newVersionWithBotRightCornerResize(nameOfTab);
            //chartSizeInit_preMake(nameOfTab);
        }
        chartSizeInit_PreMake_ophrsEffByFlowRange();
        TIMESERIES.make(true);
        OPHRSEFF.make();
        RECOMMENDATIONS.make();
        PUMP_COMB_PERF.make();
        PUMPCOMB_ANALYSIS.make();
        PUMPCOMB_COMPARISON.make();
        CHARCURVES.make();
        EFFCURVES.make();
        STATISTICS.make();
        COMBINED_AND_SINGLE.make();
        REBUILD.make();
        ERRORTAB.make();
        DATERANGE.make();
        
        var unused_blah = (nameOfTab ==  "charCurve" || nameOfTab ==  "effCurve" || nameOfTab == "timeSeries" || nameOfTab == "pumpCombinationPerformance"
                 || nameOfTab == "pumpComboAnalysis" || nameOfTab == "pumpComboComparison" || nameOfTab == "combinedSingle" || nameOfTab == "recommendations");
        
        for (var nameOfTab in tabObjects){
            chartSizeInit_postMake_newVersionWithBotRightCornerResize(nameOfTab);
            //chartSizeInit_postMake(nameOfTab);
        }
        chartSizeInit_PostMake_ophrsEffByFlowRange();
        $("#tabs").tabs({
            activate: function(event, ui){
                var activatedObjectName = ui.newPanel.attr("id").slice(0, -3);
                MainController.activeTab = activatedObjectName;
                var activatedObject = tabObjects[activatedObjectName];
                localStorage.setItem("potoEffD3_mostRecentTab", activatedObjectName);
                if ("onActivate" in activatedObject){
                    activatedObject.onActivate();
                }
                var deactivatedObject = tabObjects[ui.oldPanel.attr("id").slice(0, -3)];
                if ("onDeactivate" in deactivatedObject){
                    deactivatedObject.onDeactivate();
                }
                var sel = ui.newPanel.selector;
                if (sel = "#RebuildTab"){
                    var w = $("#rebuild table").width();
                    $("#rebuildCommentary").width(w);
                }
            },
            create: function(event, ui){}
        });
        $("#loadstate").html(""); $("#loadstate").remove();
        $("#errorLog").html(""); $("#errorLog").remove();
        $("#tabs").css("visibility", "visible");
		$("#tabs>ul.ui-tabs-nav li.ui-state-default>[id^='ui-id-']").css("margin-right",10);
		$("#combinedSingeKontent").height(690);                
        $("h3").append("  " + DATERANGE.dateStr(API.finalTable[0].date, "/") + " - " + DATERANGE.dateStr(API.finalTable[API.finalTable.length-1].date, "/"));
        TIMESERIES.onActivate();
        MainController.activeTab = "timeSeries";
        OPHRSEFF.onDeactivate();
        var mostRecentTab = localStorage.getItem("potoEffD3_mostRecentTab");
        if (mostRecentTab != null){
            jQuery("a[href=#" + mostRecentTab + "Tab]").trigger("click");
        }
        afterThoughts();
        attachWindowResizeHandler();
    }
};
function attachWindowResizeHandler(){
    d3Window = d3.select(window);
    d3Window.on("resize", function(){
        /*
        for (var i=0; i<resizeableObjectNames.length; i++){
            var objName = resizeableObjectNames[i];
            var resizerId = "chartResizerImg_" + objName;
            if (objName.indexOf(MainController.activeTab) == -1){
                jQuery("#" + resizerId).hide();
            } else {
                var OBJ = resizeableObjects[objName];
                if (OBJ){
                    if (MainController.activeTab == "ophrsEffByFlowRange"){
                        if (OPHRSEFF.oneOfTheBucketTabsIsVisible){
                            debugger;
                            if (objName.split("_")[2] == OPHRSEFF.indexOfVisibleBucketTab){
                                putResizerElementInPlace(OBJ, objName).show();
                            } else {
                                jQuery("#" + resizerId).hide();
                            }
                        }
                    } else {
                        putResizerElementInPlace(OBJ, objName);
                    }
                }
            }
        }*/
        var activeTabName = MainController.activeTab;
        var activeTabObj = tabObjects[activeTabName];
        if (activeTabObj.hasResizeableChart){
            //var resizerId = "chartResizerImg_" +
            putResizerElementInPlace(activeTabObj, activeTabName);
        } else {
            if (OPHRSEFF.summary1Resizer.style("display") != "none"){
                OPHRSEFF.putSummary1ResizerEltInPlace();
            }
            if (MainController.activeTab == "ophrsEffByFlowRange" && OPHRSEFF.oneOfTheBucketTabsIsVisible){
                var bucketIdx = parseInt(OPHRSEFF.indexOfVisibleBucketTab);
                OPHRSEFF.putBucketResizerEltInPlace(bucketIdx);
                var objName = "ophrsEffByFlowRange_flowHeadChart_" + bucketIdx;
                var obj = OPHRSEFF.flowRanges[bucketIdx].FHC;
                if (obj){
                    putResizerElementInPlace(obj, objName);
                }
            }
        }
    });
}
function makeLocalStorageVarName(nameOfTab){
    return "potoEffD3_" + nameOfTab + "_sizeFactor";
}
function makeLocalStorageVarName_X(nameOfTab){
    return "potoEffD3_" + nameOfTab + "_sizeFactor_X";
}
function makeLocalStorageVarName_Y(nameOfTab){
    return "potoEffD3_" + nameOfTab + "_sizeFactor_Y";
}
function chartSizeInit_preMake_newVersionWithBotRightCornerResize(nameOfTab){
    if (!tabObjects[nameOfTab].hasResizeableChart){
        return;
    }
    resizeableObjectNames.push(nameOfTab);
    resizeableObjects[nameOfTab] = tabObjects[nameOfTab];
    var varName_X = makeLocalStorageVarName_X(nameOfTab);
    var varName_Y = makeLocalStorageVarName_Y(nameOfTab);
    var varValue_X = localStorage.getItem(varName_X);
    var varValue_Y = localStorage.getItem(varName_Y);
    if (varValue_X == null){
        varValue_X = 1;
        localStorage.setItem(varName_X, 1);
    }
    if (varValue_Y == null){
        varValue_Y = 1;
        localStorage.setItem(varName_Y, 1);
    }
    tabObjects[nameOfTab].sizeFactor_X = parseFloat(varValue_X);
    tabObjects[nameOfTab].sizeFactor_Y = parseFloat(varValue_Y);
    tabObjects[nameOfTab].myName = nameOfTab;
    var resizerId = "chartResizerImg_" + nameOfTab;
    if (d3.selectAll("#" + resizerId).node() == null){
        theBody.append("img").attr("id", resizerId).attr("src", "img/corner.png").style("display", "none");
    }
}
function chartSizeInit_PreMake_ophrsEffByFlowRange(){
    var localStorageBucketName = Config.ophrsEffByFlowRange.bucketsName;
    var localStorageBucketValue = localStorage.getItem(localStorageBucketName);
    var numBuckets;
    if (localStorageBucketValue == null){
        numBuckets = Config.ophrsEffByFlowRange.defaultFlowBuckets.length - 1;
    } else {
        numBuckets =  JSON.parse(localStorageBucketValue).length - 1;
    }
    for (var i=0; i<numBuckets; i++){
        var nameOfFlowHeadChart = OPHRSEFF.nameOfFlowHeadChart(i);
        resizeableObjectNames.push(nameOfFlowHeadChart);
        var varName_X = OPHRSEFF.flowHeadChartLocalStorageVarNameForSizeFactor_X(i);
        var varName_Y = OPHRSEFF.flowHeadChartLocalStorageVarNameForSizeFactor_Y(i);
        var varValue_X = localStorage.getItem(varName_X);
        var varValue_Y = localStorage.getItem(varName_Y);
        if (varValue_X == null || isNaN(varValue_X) || varValue_X < 0){
            localStorage.setItem(varName_X, 1);
        }
        if (varValue_Y == null || isNaN(varValue_Y) || varValue_Y < 0){
            localStorage.setItem(varName_Y, 1);
        }
        var resizerId = OPHRSEFF.makeChartResizerIdForFlowHeadChart(i);
        if (d3.selectAll("#" + resizerId).node() == null){
            theBody.append("img").attr("id", resizerId).attr("src", "img/corner.png").style("display", "none").attr("class", "flowHeadChartResizer");
        }
    }
}
function chartSizeInit_PostMake_ophrsEffByFlowRange(){
    var numBuckets = OPHRSEFF.flowRanges.length;
    for (var i=0; i<numBuckets; i++){
        var nameOfFlowHeadChart = OPHRSEFF.nameOfFlowHeadChart(i);
        // resizeableObjects[nameOfFlowHeadChart] = OPHRSEFF.flowBuckets[i].FHC;   <-- later!!! they don't exist yet
        resizeableObjects[nameOfFlowHeadChart] = null;
    }
}
function chartSizeInit_postMake_newVersionWithBotRightCornerResize(nameOfTab){
    if (!tabObjects[nameOfTab].hasResizeableChart){
        return;
    }
}
function chartSizeInit_preMake(nameOfTab){
    if (!tabObjects[nameOfTab].hasResizeableChart){
        return;
    }
    var varName = makeLocalStorageVarName(nameOfTab);
    var varValue = localStorage.getItem(varName);
    if (varValue == null){
        varValue = 1;
        localStorage.setItem(varName, 1);
    }
    tabObjects[nameOfTab].sizeFactor = parseFloat(varValue);
    tabObjects[nameOfTab].myName = nameOfTab;
}
function chartSizeInit_postMake(nameOfTab){
    debugger;
    if (!tabObjects[nameOfTab].hasResizeableChart){
        return;
    }
    var idOfTab = nameOfTab + "Tab";
    var plusId = idOfTab + "Plus";
    var minusId = idOfTab + "Minus";
    var plusMinusId = idOfTab + "PlusMinus";
    var btns = HtmlGen.div(
        "chart size &nbsp; " + HtmlGen.eisBtn("+", {id: plusId}) + HtmlGen.eisBtn("&ndash;", {id: minusId}), {id: plusMinusId, "class": "chartSizeChange"}
    );
    var tabElt = jQuery("#" + idOfTab);
    var h3 = tabElt.find("h3");
    h3.append(btns);
    $("#" + plusId).click(makeIncreaseChartSizeHandler(nameOfTab));
    $("#" + minusId).click(makeDecreaseChartSizeHandler(nameOfTab));
}
function makeIncreaseChartSizeHandler(nameOfTab){
    return function(){
        var newSizeFactor = tabObjects[nameOfTab].sizeFactor * chartSizeChangeFactor;
        var varName = makeLocalStorageVarName(nameOfTab);
        localStorage.setItem(varName, newSizeFactor);
        tabObjects[nameOfTab].sizeFactor = newSizeFactor;
        tabObjects[nameOfTab].sizeChanged();
    }
}
function makeDecreaseChartSizeHandler(nameOfTab){
    return function(){
        var newSizeFactor = tabObjects[nameOfTab].sizeFactor / chartSizeChangeFactor;
        var varName = makeLocalStorageVarName(nameOfTab);
        localStorage.setItem(varName, newSizeFactor);
        tabObjects[nameOfTab].sizeFactor = newSizeFactor;
        tabObjects[nameOfTab].sizeChanged();
    }
}

/***************************************************************************************************************
************************************************ REBUILD *****************************************************
****************************************************************************************************************/
REBUILD = {
    adjustWidthTimer: null,
    dataRowFromPumpNum: function(pump){
        var result = [];
        var meanFlow = API.calcMeanAbsoluteFlowContrib(pump);
        var bepFlow = API.formulas.indivBepFlow[pump-1];
        var flowDiff = meanFlow - bepFlow;
        var meanEff = API.calcMeanSingleEff(pump);
        var bepEff = 100*API.formulas.indivBepEff[pump-1];
        var effDiff = bepEff - meanEff ;
        if (effDiff > 2){
            result.push(HtmlGen.span("!", {"class" : "red"}) + "&nbsp;" + pump);
        } else {
            result.push(pump);
        }
        var format = "%8.3f";
        result.push(flowFormat.sprintf(meanFlow));
        result.push(flowFormat.sprintf(bepFlow));
        result.push(flowDiffFormat.sprintf(flowDiff));
        result.push(effFormat.sprintf(meanEff));
        result.push(effFormat.sprintf(bepEff));
        result.push(effFormat.sprintf(effDiff));
        return result;
    },
    headings: ["pump", "mean flow", "BEP flow", "mean-BEP flow", "mean eff", "BEP eff", "BEP-mean eff"],
    make : function(){
        var html = HtmlGen.tableFromRowFunc(API.pumpsWithValidData.length, function(index){ return REBUILD.dataRowFromPumpNum(API.pumpsWithValidData[index])}, REBUILD.headings, {cellSpacing: 0, cellPadding: 1});
        $("#rebuild").html($(html));
        $("#rebuild table tr:odd").addClass("oddRow");
        $("#rebuild table tr:even:not(:eq(0))").addClass("evenRow");
        $("#rebuild table tr:eq(0)").addClass("headerRow");
    }
};

/***************************************************************************************************************
************************************************ DATERANGE *****************************************************
****************************************************************************************************************/
var DATERANGE = {
    Monday: true,
    Tuesday: true,
    Wednesday: true,
    Thursday: true,
    Friday: true,
    Saturday: true,
    Sunday: true,
    dateStr: function(obj, separator){
        if (arguments.length < 2){
            separator = "-";
        }
        return obj.year + separator + ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][obj.month] + separator + leftPadWithZero(obj.day);
    },
    make: function(){
        var kontent = $("#dateRangeKontent");
        var pSize = 25;
        var startEndDateInputTable = HtmlGen.table(
            HtmlGen.tr(
                HtmlGen.td("start date") +
                HtmlGen.td(HtmlGen.text({id: "startDatePicker", size: pSize}))
            ) +
            HtmlGen.tr(
                HtmlGen.td("end date") +
                HtmlGen.td(HtmlGen.text({id: "endDatePicker", size: pSize}))
            )
        );
        kontent.html(startEndDateInputTable);
        $("#startDatePicker").datepicker();
        $("#endDatePicker").datepicker();
        $("#startDatePicker").datepicker("option", "dateFormat", "yy-M-dd");
        $("#endDatePicker").datepicker("option", "dateFormat", "yy-M-dd");
        $("#startDatePicker").val(this.dateStr(API.finalTable[0].date));
        $("#endDatePicker").val(this.dateStr(API.finalTable[API.finalTable.length - 1].date));
        function aux(day){
            return HtmlGen.tr(HtmlGen.td(HtmlGen.checkbox(DATERANGE[day], {id: day})) + HtmlGen.td(day)); 
        }
        var weekdayTable = HtmlGen.table(aux("Monday") + aux("Tuesday") + aux("Wednesday") + aux("Thursday") + aux("Friday") + aux("Saturday") + aux("Sunday"));
        var weekdayButtonTable = HtmlGen.table(HtmlGen.tr(
            HtmlGen.td(HtmlGen.eisBtn("Weekdays", {id: "weekdays"})) + HtmlGen.td(HtmlGen.eisBtn("Weekends", {id: "weekends"})) + HtmlGen.td(HtmlGen.eisBtn("All", {id: "allOfTheWeek"}))
        ));
        var weekdayChooser = HtmlGen.singleCellTable(
            HtmlGen.labeledBorder(
                weekdayTable + weekdayButtonTable
                , "weekday selection"
            )
        );
        kontent.append(weekdayChooser);
        var btn = HtmlGen.btn("recompute", {id: "dateRangeRecomputeButton"});
        kontent.append(btn);
        $("#dateRangeRecomputeButton").click(dateRangeRecomputeClicked);
        $("#weekdays").click(weekdaysBtnClicked);
        $("#weekends").click(weekendsBtnClicked);
        $("#allOfTheWeek").click(allWeekBtnClicked);
        $("#Monday").change(function(){ DATERANGE.Monday = this.checked});
        $("#Tuesday").change(function(){ DATERANGE.Tuesday = this.checked});
        $("#Wednesday").change(function(){ DATERANGE.Wednesday = this.checked});
        $("#Thursday").change(function(){ DATERANGE.Thursday = this.checked});
        $("#Friday").change(function(){ DATERANGE.Friday = this.checked});
        $("#Saturday").change(function(){ DATERANGE.Saturday = this.checked});
        $("#Sunday").change(function(){ DATERANGE.Sunday = this.checked});
    }
};
function weekdaysBtnClicked(){
    $("#Monday")[0].checked = true;    DATERANGE.Monday = true;   
    $("#Tuesday")[0].checked = true;   DATERANGE.Tuesday = true;   
    $("#Wednesday")[0].checked = true; DATERANGE.Wednesday = true;   
    $("#Thursday")[0].checked = true;  DATERANGE.Thursday = true;   
    $("#Friday")[0].checked = true;    DATERANGE.Friday = true;   
    $("#Saturday")[0].checked = false; DATERANGE.Saturday = false;   
    $("#Sunday")[0].checked = false;   DATERANGE.Sunday = false;     
}
function weekendsBtnClicked(){
    $("#Monday")[0].checked = false;    DATERANGE.Monday = false;   
    $("#Tuesday")[0].checked = false;   DATERANGE.Tuesday = false;  
    $("#Wednesday")[0].checked = false; DATERANGE.Wednesday = false;
    $("#Thursday")[0].checked = false;  DATERANGE.Thursday = false; 
    $("#Friday")[0].checked = false;    DATERANGE.Friday = false;   
    $("#Saturday")[0].checked = true;   DATERANGE.Saturday = true; 
    $("#Sunday")[0].checked = true;     DATERANGE.Sunday = true;   
}
function allWeekBtnClicked(){
    $("#Monday")[0].checked = true;     DATERANGE.Monday = true;   
    $("#Tuesday")[0].checked = true;    DATERANGE.Tuesday = true;  
    $("#Wednesday")[0].checked = true;  DATERANGE.Wednesday = true;
    $("#Thursday")[0].checked = true;   DATERANGE.Thursday = true; 
    $("#Friday")[0].checked = true;     DATERANGE.Friday = true;   
    $("#Saturday")[0].checked = true;   DATERANGE.Saturday = true; 
    $("#Sunday")[0].checked = true;     DATERANGE.Sunday = true;   
}
function dateIsValid(theDate){
    if (isNaN(theDate.getYear())) return false;
    if (isNaN(theDate.getDay())) return false;
    if (isNaN(theDate.getDate())) return false;
    if (isNaN(theDate.getMonth())) return false;
    return true;
}
function dateRangeRecomputeClicked(){
    var oneDay = 3600 * 24 * 1000;
    var start = $("#startDatePicker").datepicker("getDate");
    var end = $("#endDatePicker").datepicker("getDate");
    if (!dateIsValid(start)){
        return;
    }
    if (!dateIsValid(end)){
        return;
    }
    var rowSelection = API.finalTableAll.select(function(row){
        var rowDate = row.jsDate();
        return rowDate - start >= 0 && end - rowDate >= -oneDay && $("#" + ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][rowDate.getDay()])[0].checked;
    });
    $("body").html("");
    $("body").html(body0);
    $(window).unbind("resize");
    $("#loadInfo").html("recalculating ...");
    window.setTimeout(function(){ MainController.initialize(rowSelection, false)}, 1);
}
/***************************************************************************************************************
************************************************ ERRORTAB ******************************************************
****************************************************************************************************************/
var ERRORTAB = {
    maxAbsError : 4.8,
    colChooserVisible : true,
    tableParent: null,
    whichColsToShow: function(){
        return this.availableHeadings.map(function(head){
            return document.getElementById("selectCol" + head).checked;
        })
    },
    makeMainTable: function(){
        var pmps = UTIL.range(1, API.numPumps);
        var availableHeadings = this.availableHeadings;
        var errRows = API.finalTable.select(API.isErrorRow);
        this.errorRows = errRows;
        var chosenCols = this.whichColsToShow();
        var headings = availableHeadings.selectBasedOneBooleanArray(chosenCols);
        var cellGens = {};
        cellGens["exclude"] = function(index){
            return HtmlGen.checkbox(true, {id: "excludeCheck" + index});
        }
        cellGens["date"] = function(index){
            var row = errRows[index];
            return row.datatime.slice(0, 16);
        }
        cellGens["error descr"] = function(index){
            var row = errRows[index];
            return row.errors.join("<br>")
        }
        cellGens["pumps"] = function(index){
            var row = errRows[index];
            return row.pumpsList;
        }
        pmps.map(function(pmp){
            cellGens["TDH-" + pmp] = function(index){
                var row = errRows[index];
                return "%7.1f".sprintf(row.head[pmp-1]);
            }
        });
        cellGens["avg TDH"] = function(index){
            var row = errRows[index];
            return "%7.1f".sprintf(row.avgHead);
        }
        pmps.map(function(pmp){
            cellGens["shaft-" + pmp] = function(index){
                var row = errRows[index];
                return "%8.0f".sprintf(row.shaft[pmp-1]);
            }
        });
        cellGens["total shaft"] = function(index){
            var row = errRows[index];
            return "%7.1f".sprintf(row.totalShaft);
        }
        cellGens["flow"] = function(index){
            var row = errRows[index];
            return "%7.1f".sprintf(row.flow);
        }
        cellGens["efficiency"] = function(index){
            var row = errRows[index];
            return "%7.2f".sprintf(100*row.eff);
        }
        cellGens["hydro pow"] = function(index){
            var row = errRows[index];
            return "%8.0f".sprintf(row.hydroPow);
        }
        cellGens["predicted flow"] = function(index){
            var row = errRows[index];
            return "%7.1f".sprintf(row.predictedFlow);
        }
        function rowMaker(index){
            var result = [];
            for (var i = 0; i < availableHeadings.length; i++){
                if (chosenCols[i]){
                    var heading = availableHeadings[i];
                    result.push(cellGens[heading](index));
                }
            }
            return result;
        }
        var tableAttributes = {id: "errorsTable", cellspacing: 0};
        this.tableParent.html( 
            HtmlGen.tableFromRowFunc(errRows.length, rowMaker, headings, tableAttributes));
        $("#errorsTable th").parent().addClass("headerRow");
        for (var errorRowIndex = 0; errorRowIndex < errRows.length; errorRowIndex++){
            $("#excludeCheck" + errorRowIndex).change(makeErrorExcludeCheckChange(errorRowIndex));
        }
        
    },
    make: function(){
        this.tableParent = $("#errorsTableParent");
        this.chooserElt = $("#errorsColChooserChecksTableParent");
        var pmps = API.pumpsWithValidData;
        var tdhHeaders = pmps.map(function(pmp) {return "TDH-" + pmp;} );
        var nonono = pmps.map(function(){ return false;});
        var yepyep = pmps.map(function(){ return true;});
        var shaftHeaders = pmps.map(function(pmp) {return "shaft-" + pmp;} );
        var headingsMisc = ["exclude", "date", "error descr", "pumps", "efficiency", "flow", "predicted flow", "hydro pow"];
        var defaultsMisc = [true,        true,      true,       true,       true,       true,     true,           true];
        var headingsTDH = ["avg TDH"].concat(tdhHeaders);
        var defaultsTDH = ["true"].concat(nonono);
        var headingsShaft = ["total shaft"].concat(shaftHeaders);
        var defaultsShaft = ["true"].concat(yepyep);
        var availableHeadings = headingsMisc.concat(headingsTDH).concat(headingsShaft);
        this.availableHeadings = availableHeadings;
        
        var miscColumnChoiceTableRows = headingsMisc.map(function(header, index){ return HtmlGen.tr(
                HtmlGen.td(HtmlGen.checkbox(defaultsMisc[index], {id: "selectCol" + header})) +
                HtmlGen.td(header)
        )});
        var tdhColumnChoiceTableRows = headingsTDH.map(function(header, index){ return HtmlGen.tr(
                HtmlGen.td(HtmlGen.checkbox(defaultsTDH[index], {id: "selectCol" + header})) +
                HtmlGen.td(header)
        )});
        var shaftColumnChoiceTableRows = headingsShaft.map(function(header, index){ return HtmlGen.tr(
                HtmlGen.td(HtmlGen.checkbox(defaultsShaft[index], {id: "selectCol" + header})) +
                HtmlGen.td(header)
        )});
        var miscCctr = miscColumnChoiceTableRows.nGroups(2);
        var tdhCctr = tdhColumnChoiceTableRows.nGroups(2);
        var shaftCctr = shaftColumnChoiceTableRows.nGroups(2);
        var miscColChoiceTable = HtmlGen.table(HtmlGen.tr(
            miscCctr.map(
                function(someRows){return HtmlGen.td(HtmlGen.table(someRows.join("")), {"valign": "top"})}
            ).join("")
        ), {id: "miscColChoiceTable"});
        var tdhColChoiceTable = HtmlGen.table(HtmlGen.tr(
            tdhCctr.map(
                function(someRows){return HtmlGen.td(HtmlGen.table(someRows.join("")), {"valign": "top"})}
            ).join("")
        ), {id: "tdhColChoiceTable"});
        var shaftColChoiceTable = HtmlGen.table(HtmlGen.tr(
            shaftCctr.map(
                function(someRows){return HtmlGen.td(HtmlGen.table(someRows.join("")), {"valign": "top"})}
            ).join("")
        ), {id: "shaftColChoiceTable"});
        var allTDH = HtmlGen.eisBtn("check all", {id: "checkAllTDH"});
        var noneTDH = HtmlGen.eisBtn("uncheck all", {id: "uncheckAllTDH"});
        var allNoneTDH = HtmlGen.table(HtmlGen.tr(HtmlGen.td(allTDH) + HtmlGen.td(noneTDH)));
        var allShaft = HtmlGen.eisBtn("check all", {id: "checkAllShaft"});
        var noneShaft = HtmlGen.eisBtn("uncheck all", {id: "uncheckAllShaft"});
        var allNoneShaft = HtmlGen.table(HtmlGen.tr(HtmlGen.td(allShaft) + HtmlGen.td(noneShaft)));
        var miscColChoiceTableLabeled = HtmlGen.labeledBorder(miscColChoiceTable, "Display Columns");
        var tdhColChoiceTableLabeled = HtmlGen.labeledBorder(tdhColChoiceTable + allNoneTDH, "TDH");
        var shaftColChoiceTableLabeled = HtmlGen.labeledBorder(shaftColChoiceTable + allNoneShaft, "Shaft KW");
        var colChoiceTable = HtmlGen.table(HtmlGen.tr(
            HtmlGen.td(miscColChoiceTableLabeled, {"valign": "top"})
            + HtmlGen.td(tdhColChoiceTableLabeled, {"valign": "top"})
            + HtmlGen.td(shaftColChoiceTableLabeled, {"valign": "top"})
        ));
        this.chooserElt.html(colChoiceTable);
        this.makeMainTable();
        for (var i = 0; i < availableHeadings.length; i++){
            var heading = availableHeadings[i];
            var id = "selectCol" + heading;
            $(document.getElementById(id)).change(function(){ ERRORTAB.makeMainTable()});
        }
        $("#checkAllTDH").click(checkAllTDHclick);
        $("#uncheckAllTDH").click(uncheckAllTDHclick);
        $("#checkAllShaft").click(checkAllShaftClick);
        $("#uncheckAllShaft").click(uncheckAllShaftClick);
        //
        var showHide = HtmlGen.eisBtn("Hide Column Selection", {id: "showHideErrorsColumnSelection"});
        var recalc = HtmlGen.eisBtn("recalculate", {id: "errorsRecalcBtn"});
        var onTop = HtmlGen.table(HtmlGen.tr(
            HtmlGen.td(showHide) + HtmlGen.td("", {"class": "spacer4"}) + HtmlGen.td(recalc, {id: "errorsRecalcBtnTd"})
        ));
        $("#errorsButtons").html(onTop);
        $("#showHideErrorsColumnSelection").click(showHideErrorsColumnSelectionClick);
        // slider setup
        $("#errorsSliderErrorThresh").slider({
            min: 1,
            max: 9.99999,
            value: ERRORTAB.maxAbsError,
            step: 0.01,
            slide: function(event, ui){
                var w = ui.value;
                $("#errorsSliderErrorThreshValue").html("%4.2f".sprintf(w));
                ERRORTAB.maxAbsError = w;
            }
        });
        $("#errorsSliderErrorThreshValue").html("%4.2f".sprintf(ERRORTAB.maxAbsError));
        $("#errorsRecalcBtn").click(errorRecalcClicked);
    }
};


function errorRecalcClicked(){
    //$("#errorsRecalcBtnTd").html("recalculating");
    $("body").html("");
    $("body").html(body0);
    $(window).unbind("resize");
    $("#loadInfo").html("recalculating ...");
    window.setTimeout(function(){ MainController.initialize(API.finalTable, false)}, 1);
}
function makeErrorExcludeCheckChange(errorRowIndex){
    return function(){
        //alert(ERRORTAB.errorRows[errorRowIndex].flow);
        alert("include / exclude not implemented");
    }
}
function showHideErrorsColumnSelectionClick(){
    if (ERRORTAB.colChooserVisible){
        ERRORTAB.chooserElt.hide();
        ERRORTAB.colChooserVisible = false;
        $("#showHideErrorsColumnSelection span").text("Show Column Selection");
    } else {
        ERRORTAB.chooserElt.show();
        ERRORTAB.colChooserVisible = true;
        $("#showHideErrorsColumnSelection span").text("Hide Column Selection");
    }
}
function checkAllTDHclick(){
    var elements = $("[id^=selectColTDH], [id='selectColavg TDH']");
    elements.each(function(index){ this.checked = true; });
    ERRORTAB.makeMainTable();
    
}
function uncheckAllTDHclick(){
    var elements = $("[id^=selectColTDH], [id='selectColavg TDH']");
    elements.each(function(index){ this.checked = false; });
    ERRORTAB.makeMainTable();
}
function checkAllShaftClick(){
    var elements = $("[id^=selectColshaft], [id='selectColtotal shaft']");
    elements.each(function(index){ this.checked = true; });
    ERRORTAB.makeMainTable();
}
function uncheckAllShaftClick(){
    var elements = $("[id^=selectColshaft], [id='selectColtotal shaft']");
    elements.each(function(index){ this.checked = false; });
    ERRORTAB.makeMainTable();
}
function arr2xy(arr){
    return {
        x: arr[0],
        y: arr[1]
    }
}
/***************************************************************************************************************
************************************************ TIMESERIES ****************************************************
****************************************************************************************************************/
TIMESERIES = {
    hasResizeableChart : true,
    dateFormatFun_1 : d3.time.format("%Y-%b-%d"),
    dateFormatFun_2 : d3.time.format("%H:%M"),
    aspect : 1.7,  // width/height  - used to calculate width from height
    height0 : 600,      // will be multiplied with sizeFactor, which exists for each tab and is stored in localStorage, default is 1. search for "chartSize" in data.js for details
    marginLR : 62,
    marginTop : 28,
    marginBot : 55,
    labelTopGap : 12,
    labelHorizGap : 12,
    pumpGroupLabelWidth : 12,
    pumpGroupLabelHeightPerLetter : 20,
    topCssClass : "timeseries",
    cssClasses : ["flow", "head", "eff"],
    xAxisCssClass : "xAxis",
    yAxisCssClass : "yAxis",
    yEffAxisCssClass : "yEffAxis",
    xAxisLabelCssClass : "xAxisLabel",
    yAxisLabelCssClass : "yAxisLabel",
    flowAxisLabelCssClass  : "flowAxisLabel",
    tdhAxisLabelCssClass   : "tdhAxisLabel",
    yEffAxisLabelCssClass : "yEffAxisLabel",
    xAxisLabelText : "time",
    yAxisLabelTextFlow : "flow (MGD)",
    yAxisLabelTextTDH : "TDH (psi)",
    yEffAxisLabelText : "efficiency (%)",
    FLOW_INDEX : 0,
    HEAD_INDEX : 1,
    EFF_INDEX  : 2,
    includeErrors: false,
    theTable: null,
    stopBackgroundChangeAt : 1500, // if more than 1500 data points, stop changing background for each pump combination - currently not used
    minPixelsForShowingPumpCombo : 12, // minimum pixel-width of pump combination in chart for getting labelled with a yellow label on top
    extendY : 0.11,            // extra space on top for labels (portion of non-extra)
    sliderId : "timeSeriesDateRangeSlider",
    valueList: null,
    fromIndex: 100,
    toIndex: 200,
    showDateRange: function(){
        $("#timeSeriesDateRangeSliderValue").html(this.valueList[this.fromIndex] + " - " + this.valueList[this.toIndex]);
    },
    calcValueList: function(){
        return this.theTable.map(function(row){ return row && toDateString(row.date); });
    },
    makeSlider: function(){
        this.valueList = this.calcValueList();
        var THIS = this;
        $("#timeSeriesDateRangeSlider").slider({
            //range: true,
            min: 1,
            max: THIS.valueList.length - 1,
            values: [THIS.fromIndex, THIS.toIndex],
            slide: function(event, ui){
                var w1 = Math.round(ui.values[0]);
                var w2 = Math.round(ui.values[1]);
                var v1 = Math.min(w1, w2);
                var v2 = Math.max(w1, w2);
                if (v1==v2){
                    if (v1>0){
                        v1--;
                    } else {
                        v2++;
                    }
                }
                if (document.getElementById("siameseHandles").checked){
                    // siamese - move the handles in tandem, keeping them in equal distance to each other
                    var whichHandle = ui.handle.id.substring(THIS.sliderId.length + 6) - 0;
                    var leftPos = parseFloat($("#" +THIS.sliderId + "Handle0").css("left"));
                    var rightPos = parseFloat($("#" +THIS.sliderId + "Handle1").css("left"));
                    var max = THIS.valueList.length - 1;
                    function rightHandleMove(leftIndex){
                        var movedBy = v2 - THIS.toIndex;
                        THIS.fromIndex += movedBy;
                        if (THIS.fromIndex < 1) THIS.fromIndex = 1;
                        if (THIS.fromIndex > max) THIS.fromIndex = max;
                        THIS.toIndex = v2;
                        $("#" + THIS.sliderId).slider("values", leftIndex, THIS.fromIndex);
                    }
                    function leftHandleMove(rightIndex){
                        var movedBy = v1 - THIS.fromIndex;
                        THIS.toIndex += movedBy;
                        if (THIS.toIndex < 1) THIS.toIndex = 1;
                        if (THIS.toIndex > max) THIS.toIndex = max;
                        THIS.fromIndex = v1;
                        $("#" + THIS.sliderId).slider("values", rightIndex, THIS.toIndex);
                    }
                    var handleMove = [leftHandleMove, rightHandleMove];
                    if (rightPos>=leftPos){
                        handleMove[whichHandle](1-whichHandle);
                    } else {
                        handleMove[1-whichHandle](1-whichHandle);
                    }
                } else {
                    // not siamese - do the usual thing of moving the two handles independently
                    THIS.fromIndex = v1;
                    THIS.toIndex = v2;
                }
                THIS.showDateRange();
                if (THIS.toIndex - THIS.fromIndex <= 2000){
                    THIS.makeChart(THIS.chartContainer);
                }
            },
            stop: function(event, ui){
                THIS.makeChart(THIS.chartContainer);
            }
        });
        var aa = $("#" + THIS.sliderId + " span");
        $(aa[0]).attr("id", THIS.sliderId + "Handle0");
        $(aa[1]).attr("id", THIS.sliderId + "Handle1");
        this.showDateRange();
    },
    makeChart: function(container){
        container = d3.select(container);
        var FLOW_INDEX = this.FLOW_INDEX;
        var HEAD_INDEX = this.HEAD_INDEX;
        var EFF_INDEX  = this.EFF_INDEX;
        var fromIndex = this.fromIndex;
        var   toIndex = this.toIndex;
        var startRow = this.theTable[fromIndex];
        var   endRow = this.theTable[toIndex];
        var R = UTIL.range(fromIndex, toIndex);
        var flowPoints = R.map(function(index){ var row = TIMESERIES.theTable[index]; return [row.jsDate(),     row.flow];   });
        var headPoints = R.map(function(index){ var row = TIMESERIES.theTable[index]; return [row.jsDate(),     row.avgHead];});
        var  effPoints = R.map(function(index){ var row = TIMESERIES.theTable[index]; return [row.jsDate(), 100*row.eff];    });
        var flows = flowPoints.map(entry1);
        var heads = headPoints.map(entry1);
        var effs = effPoints.map(entry1);
        var maxFlow = flows.max(); var minFlow = flows.min();
        var maxHead = heads.max(); var minHead = heads.min();
        var maxEff = effs.max(); var minEff = effs.min();
        maxEff += this.extendY * (maxEff - minEff);
        var yMinLeft = Math.min(minFlow, minHead);
        var yMaxLeft = Math.max(maxFlow, maxHead);
        yMaxLeft += this.extendY * (yMaxLeft - yMinLeft);
        
        var cssClasses = this.cssClasses;
        var topCssClass = this.topCssClass;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        var marginLR  = this.marginLR;
        var marginTop = this.marginTop;
        var marginBot = this.marginBot;
        var xDomain = [fromIndex, toIndex];
        var startDate = this.startDate = startRow.jsDate();
        var   endDate = this.endDate   =   endRow.jsDate();
        var xDomain_time = [startDate, endDate];
        var yDomain = [yMinLeft, yMaxLeft];
        var yDomain_Eff = [minEff, maxEff];
        var innerWidth = width - 2 * marginLR;
        var innerHeight = height - marginTop - marginBot;
        var xStart = marginLR;
        var xEnd = width - marginLR;     this.xEnd = xEnd;
        var yStart = height - marginBot; this.yStart = yStart;
        var yEnd = marginTop;
        var data = [
            flowPoints.map(arr2xy),
            headPoints.map(arr2xy),
            effPoints.map(arr2xy)
        ];
        //var x = d3.scale.linear().domain(xDomain).range([xStart, xEnd]);
        var x = d3.time.scale().domain(xDomain_time).range([xStart, xEnd]);
        var y = d3.scale.linear().domain(yDomain).range([yStart, yEnd]);
        var y_Eff = d3.scale.linear().domain(yDomain_Eff).range([yStart, yEnd]);
        this.x = x;
        this.y = y;
        this.y_Eff = y_Eff;
        this.xInv = x.invert;
        this.yInv = y.invert;
        this.yEffInv = y_Eff.invert;
        //var line = d3.svg.line().x(comp(x, get("x"))).y(comp(y, get("y")));
        //var deleteme = d3.time.scale().domain(xDomain).range(xDomain_time);
        //var xx = comp(x, deleteme);
        var line = d3.svg.line().x(function(d,i,j){
            return x(d.x);
        }).y(comp(y, get("y")));
        var line_Eff = d3.svg.line().x(comp(x, get("x"))).y(comp(y_Eff, get("y")));
        container.html("");
        var svg = container.append("svg").attr("width", width).attr("height", height).classed(topCssClass, true);
        this.svg = svg;
        // prepare mouse lookup
        var mouseIndexLookup = this.mouseIndexLookup = [];
        var maxMouseIndex = -1;
        for (var i=fromIndex; i<=toIndex; i++){
            var row = this.theTable[i];
            var idx = Math.round((row.jsDate() - startDate) / TIME_RESOLUTION);
            mouseIndexLookup[idx] = row;
            maxMouseIndex = Math.max(maxMouseIndex, idx);
        }
        // add shading indicating changes in active pump group
        var i = -1;
        var leftDate = null;
        var lastRunCode = -1;
        var currentRunCode = -1;
        var lastPumpList = null;
        var currentPumpList = null;
        var currentRow = null;
        var colorFlag = true;
        var mustCloseGroup;
        var start_i;
        var lastAppendedRect = null;
        while (true){
            i++;
            var finished = (i>maxMouseIndex);
            var valid = (mouseIndexLookup[i] != null);
            var groupWasStarted = (leftDate != null);
            if (valid){
                currentRow = mouseIndexLookup[i];
                currentRunCode = currentRow.runCode;
                currentPumpList = currentRow.pumpsList;
                if (groupWasStarted){
                    if (currentRunCode == lastRunCode){
                        // continuation of current group
                        mustCloseGroup = false;
                    } else {
                        // group ended 1 step previous
                        mustCloseGroup = true;
                    }
                } else {
                    // start of new group
                    mustCloseGroup = false;
                    start_i = i;
                    lastRunCode = currentRunCode;
                    leftDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
                }
            } else {
                mustCloseGroup = groupWasStarted;
            }
            if (mustCloseGroup){
                var change = !currentPumpList.equalShallow(lastPumpList);
                /*console.log("from " + start_i + " to " + (i-1) +
                    " pumps " + currentPumpList.join(", ") + "(previously " + (lastPumpList ? lastPumpList.join(", "): "NULL") + ")" +
                    "  change " + change + "  color flag " + colorFlag);*/
                rightDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
                var right = x(rightDate);
                if (change){
                    colorFlag = !colorFlag;
                    var left = x(leftDate);
                    var cssClass = (colorFlag ? "yin" : "yang");
                    lastAppendedRect = svg.append("rect").classed("pumpGroupBackground " + cssClass, true).attr({
                        x: left, width: right - left, y: yEnd, height: innerHeight
                    });
                    lastAppendedRect.datum(currentPumpList);
                } else {
                    lastAppendedRect.attr("width", right - lastAppendedRect.attr("x"));
                }
                lastPumpList = currentPumpList;
                leftDate = null;
            }
            if (finished){
                break;
            }
        }
        // plot the lines
        svg.selectAll("path").data(data).enter()
            .append("path")
            .attr({
                "fill": "none",
                "d": function(d,i,j){
                    if (i == EFF_INDEX){
                        return line_Eff(d,i,j);
                    }
                    return line(d,i,j);
                }
            }).attr("class", function(d, i, j){
                return topCssClass + " line " + cssClasses[i]
            });
        // make axes
        //var xScale = d3.scale.linear().domain(xDomain).range([0, innerWidth]);
        var xScale = d3.time.scale().domain(xDomain_time).range([0, innerWidth]);
        var yScale = d3.scale.linear().domain(yDomain).range([innerHeight, 0]);
        var yScale_Eff = d3.scale.linear().domain(yDomain_Eff).range([innerHeight, 0]);
        var xAxis = d3.svg.axis().scale(xScale).orient("bottom").ticks(10);
        var yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(10);
        var yAxis_Eff = d3.svg.axis().scale(yScale_Eff).orient("right").ticks(10);
        var axisCssClass = topCssClass + " axis";
        var xAxisGroup = this.xAxisGroup = svg.append("g").translate(xStart, yStart).attr("class", axisCssClass).call(xAxis);
        var yAxisGroup = this.yAxisGroup = svg.append("g").translate(xStart, yEnd).attr("class", axisCssClass).call(yAxis);
        var yEffAxisGroup = this.EffyAxisGroup = svg.append("g").translate(xEnd, yEnd).attr("class", axisCssClass).call(yAxis_Eff);
        xAxisGroup.classed(this.xAxisCssClass, true);
        yAxisGroup.classed(this.yAxisCssClass, true);
        yEffAxisGroup.classed(this.yEffAxisCssClass, true);
        // axis labels
        var xAxisLabelCssClass = this.xAxisLabelCssClass;
        var yAxisLabelCssClass = this.yAxisLabelCssClass;
        var yEffAxisLabelCssClass = this.yEffAxisLabelCssClass;
        var flowAxisLabelCssClass = this.flowAxisLabelCssClass;
        var tdhAxisLabelCssClass  = this.tdhAxisLabelCssClass;
        var xAxisLabelText    = this.xAxisLabelText;
        var yAxisLabelTextFlow   = this.yAxisLabelTextFlow;
        var yAxisLabelTextTDH   = this.yAxisLabelTextTDH;
        var yEffAxisLabelText = this.yEffAxisLabelText;
        xAxisGroup.append("text").classed(xAxisLabelCssClass, true).classed("axisLabel", true).text(xAxisLabelText).attr({
            x : innerWidth/2,
            y : marginBot - 8
        });
        yEffAxisGroup.append("text").classed(yEffAxisLabelCssClass, true).classed("axisLabel", true).text(yEffAxisLabelText).attr({
            x : this.labelTopGap,
            y : -marginLR + 21,
            "text-anchor" : "start",
            "transform": "rotate(90)"
        });
        var yAxisLabelGroup = this.yAxisLabelGroup = yAxisGroup.append("g").classed(yAxisLabelCssClass, true).rotate(90);
        var flowLabel = yAxisLabelGroup.append("text").classed("axisLabel", true).classed(flowAxisLabelCssClass, true).text(yAxisLabelTextFlow).attr({
            x : this.labelTopGap,
            y : marginLR - this.labelHorizGap,
            "text-anchor" : "start"
        });
        var tdhLabel = yAxisLabelGroup.append("text").classed("axisLabel", true).classed(tdhAxisLabelCssClass, true).text(yAxisLabelTextTDH).attr({
            x : 125,
            y : marginLR - this.labelHorizGap,
            "text-anchor" : "start"
        });
        // obstruct lines where data is invalid
        var i = -1;
        var leftDate = null;
        while (true){
            i++;
            var finished = (i>maxMouseIndex);
            var valid = (mouseIndexLookup[i] != null);
            var groupWasStarted = (leftDate != null);
            var mustCloseGroup = groupWasStarted && (finished || valid);
            if (mustCloseGroup){
                rightDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
                var left = x(leftDate);
                var right = x(rightDate);
                leftDate = null;
                svg.append("rect").classed("obstructor", true).attr({
                    x: left, width: right - left, y: yEnd, height: innerHeight
                });
            }
            if (finished){
                break;
            }
            if ((!valid) && (!groupWasStarted)){
                leftDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
            }
        }
        // mark errors
        if (includeErrors){
            var i = -1;
            var leftDate = null;
            while (true){
                i++;
                var finished = (i>maxMouseIndex);
                var dataAvailable = (mouseIndexLookup[i] != null);
                var hasError = dataAvailable && (mouseIndexLookup[i].errors.length > 0);
                var groupWasStarted = (leftDate != null);
                var mustCloseGroup = groupWasStarted && (finished || !(hasError));
                if (mustCloseGroup){
                    rightDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
                    var left = x(leftDate);
                    var right = x(rightDate);
                    leftDate = null;
                    svg.append("rect").classed("errorMarker", true).attr({
                        x: left, width: right - left, y: yEnd, height: innerHeight
                    });
                }
                if (finished){
                    break;
                }
                if (hasError && (!groupWasStarted)){
                    leftDate = UTIL.dateAdd(startDate, (i-0.5) * TIME_RESOLUTION);
                }
            }
        }
        // add pump group labels
        var minWidth4LblViz = this.minPixelsForShowingPumpCombo;
        var semiwi = this.pumpGroupLabelWidth/2;
        var pglh = this.pumpGroupLabelHeightPerLetter;
        var pumpGroupRegions = svg.selectAll("rect.pumpGroupBackground");
        pumpGroupRegions.each(function(d,i,j){
            var self = d3.select(this);
            var width = parseFloat(self.attr("width"));
            if (width < minWidth4LblViz) return;
            var left = parseFloat(self.attr("x"));
            var center = left + width/2;
            //console.log("pumps " + d.join(", ") + " left " + Math.round(left) + " width " + Math.round(width) + " center " + Math.round(center));
            var lblGroup = svg.append("g").classed("pumpGroupLabelGroup", true).translate(center, yEnd - 8);
            var lblRect = lblGroup.append("rect").classed("pumpGroupLabelRect", true).attr({
                x : -semiwi, y: 0, width: 2*semiwi, height: pglh * d.length
            });
            lblGroup.selectAll("honestPolitician").data(d).enter().append("text").text(function(d) { return d; }).attr("x", 0)
                .attr("y", function(d,i){ return (i+0.73) * pglh});
        });
        jQuery("#timeSeriesNumComboChanges").html((pumpGroupRegions[0].length-1) + " combination changes");
        // mouse lines
        var hMouseLine = this.hMouseLine = svg.append("line").attr("x1", xStart).attr("x2", xEnd).attr("y1", 0).attr("y2", 0)
            .attr("id", "timeSeries_H_MouseLine").classed("mouseLine horizontal inactive", true);
        var vMouseLine = this.vMouseLine = svg.append("line").attr("y1", yStart).attr("y2", yEnd).attr("x1", 0).attr("x2", 0)
            .attr("id", "timeSeries_V_MouseLine").classed("mouseLine vertical inactive", true);
        var leftMouseAxisRectWidth = marginLR - 5;
        var leftMouseAxisRectHeight = this.leftMouseAxisRectHeight = 18;
        var leftMouseAxisRect = this.leftMouseAxisRect = svg.append("rect").attr("x", marginLR-leftMouseAxisRectWidth).attr("width", leftMouseAxisRectWidth)
            .attr("y", 111).attr("height", leftMouseAxisRectHeight).classed("leftMouseAxisRect mouseAxisRect inactive", true);
        var leftMouseAxisText = this.leftMouseAxisText = svg.append("text").attr("x", xStart - 6).attr("y", 0)
            .classed("leftMouseAxisText mouseAxisText inactive", true);
        var rightMouseAxisRectWidth = this.rightMouseAxisRectWidth = leftMouseAxisRectWidth;
        var rightMouseAxisRectHeight = this.rightMouseAxisRectHeight = leftMouseAxisRectHeight;
        var rightMouseAxisRect = this.rightMouseAxisRect = svg.append("rect").attr("x", xEnd).attr("width", rightMouseAxisRectWidth)
            .attr("y", 111).attr("height", rightMouseAxisRectHeight).classed("rightMouseAxisRect mouseAxisRect inactive", true);
        var rightMouseAxisText = this.rightMouseAxisText = svg.append("text").attr("x", xEnd + 6).attr("y", 0)
            .classed("rightMouseAxisText mouseAxisText inactive", true);
        var bottomMouseAxisRectWidth = this.bottomMouseAxisRectWidth = 102;
        var bottomMouseAxisRectHeight = this.bottomMouseAxisRectHeight = 32;
        var bottomMouseAxisRect = this.bottomMouseAxisRect = svg.append("rect").attr("y", yStart).attr("width", bottomMouseAxisRectWidth).attr("height", bottomMouseAxisRectHeight)
            .classed("bottomMouseAxisRect mouseAxisRect inactive", true).attr("x", 0);
        var bottomMouseAxisText_1 = this.bottomMouseAxisText_1 = svg.append("text").attr("y", yStart + 14)
            .classed("bottomMouseAxisText mouseAxisText inactive", true);
        var bottomMouseAxisText_2 = this.bottomMouseAxisText_2 = svg.append("text").attr("y", yStart + 28)
            .classed("bottomMouseAxisText mouseAxisText inactive", true);
        this.attachMouseHandler();
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var m = d3.mouse(TIMESERIES.svg.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var mouseOverDate = TIMESERIES.xInv(xRaw);
            var mouseOverIndex = Math.round((mouseOverDate - TIMESERIES.startDate) / TIME_RESOLUTION);
            var mouseOverRow = TIMESERIES.mouseIndexLookup[mouseOverIndex];
            var hml = TIMESERIES.hMouseLine;
            var vml = TIMESERIES.vMouseLine;
            var lmar = TIMESERIES.leftMouseAxisRect;
            var lmat = TIMESERIES.leftMouseAxisText;
            var rmar = TIMESERIES.rightMouseAxisRect;
            var rmat = TIMESERIES.rightMouseAxisText;
            var bmar = TIMESERIES.bottomMouseAxisRect;
            var mbat1 = TIMESERIES.bottomMouseAxisText_1;
            var mbat2 = TIMESERIES.bottomMouseAxisText_2;
            var yInRange = ((yRaw - TIMESERIES.marginTop) * (TIMESERIES.yStart - yRaw) > 0);
            var xInRange = ((xRaw - TIMESERIES.marginLR ) * (TIMESERIES.xEnd  - xRaw) > 0);
            var mouseIsOverChart = xInRange && yInRange;
            hml.classed("inactive", !mouseIsOverChart);
            vml.classed("inactive", !mouseIsOverChart);
            lmar.classed("inactive", !mouseIsOverChart);
            lmat.classed("inactive", !mouseIsOverChart);
            rmar.classed("inactive", !mouseIsOverChart);
            rmat.classed("inactive", !mouseIsOverChart);
            bmar.classed("inactive", !mouseIsOverChart);
            mbat1.classed("inactive", !mouseIsOverChart);
            mbat2.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hml.attr("y1", yRaw).attr("y2", yRaw);
                vml.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                lmar.attr("y", yRaw - TIMESERIES.leftMouseAxisRectHeight/2);
                rmar.attr("y", yRaw - TIMESERIES.rightMouseAxisRectHeight/2);
                lmat.attr("y", yRaw + TIMESERIES.leftMouseAxisRectHeight/2 - 4).text("%5.1f".sprintf(TIMESERIES.yInv(yRaw)));
                rmat.attr("y", yRaw + TIMESERIES.rightMouseAxisRectHeight/2 - 4).text("%5.1f".sprintf(TIMESERIES.yEffInv(yRaw)));
                bmar.attr("x", xRaw - TIMESERIES.bottomMouseAxisRectWidth/2);
                mbat1.attr("x", xRaw).text(TIMESERIES.dateFormatFun_1(mouseOverDate));
                mbat2.attr("x", xRaw).text(TIMESERIES.dateFormatFun_2(mouseOverDate));
            }
            //DEBUG[0].html(mouseOverDate);
            //DEBUG[1].html("index " + mouseOverIndex + " x-pixel " + xRaw);
            //DEBUG[2].html("y pixel coordinate %6.1f".sprintf(yRaw));
            if (mouseOverRow && mouseIsOverChart){
                //XXX = mouseOverRow
                //DEBUG[3].html("tdh: %6.2f".sprintf(mouseOverRow.avgHead));
                //DEBUG[4].html("flow: %6.2f".sprintf(mouseOverRow.flow));
                //DEBUG[5].html("eff: %6.2f%".sprintf(mouseOverRow.eff*100));
                //DEBUG[6].html("pumps " + mouseOverRow.pumpsList.join(", "));
                makeMouseOverTable(mouseOverRow, -1, "timeSeriesHintTable", "timeSeriesMouseOverTableContainer", "timeSeriesHintHandle", "timeSeriesMinimizer", "timeHintCaption", "timeSeriesRight");
                TIMESERIES.errorElt.html("");
                for (var i=0; i<mouseOverRow.errors.length; i++){
                    TIMESERIES.errorElt.append(HtmlGen.div(mouseOverRow.errors[i]));
                }
            } else {
                jQuery("#timeSeriesHintTable").remove();
                jQuery("#timeHintCaption").html("no data");
                //DEBUG[3].html("no data");
                //DEBUG[4].html("no data");
                //DEBUG[5].html("no data");
                //DEBUG[6].html("no data");
            }
        });
    },
    make: function(firstRun){
        $("#timeSeriesTab").html("");
        $("#timeSeriesTab").html(this.html0);
        this.errorElt = $("#timeSeriesErrorContainer");
        this.chartContainer = $("#tdTimeSeriesChart")[0];
        document.getElementById("includeErrors").checked = this.includeErrors;
        if (this.includeErrors){
            // the user has just checked the "includeErrors" checkbox, must adjust fromIndex and toIndex, so the dates vary as little as possible.
            this.fromIndex = this.theTable[this.fromIndex].rowId;
            this.toIndex = this.theTable[this.toIndex].rowId;
            this.theTable = API.finalTable;
        } else {
            this.theTable = API.finalTable.select(API.isntErrorRow);
            for (var i=0; i<this.theTable.length; i++){
                //finalTable[this.theTable[i].rowId].noErrorId = i;
                this.theTable[i].noErrorId = i;
            }
            if (firstRun){
                // set fromIndex and toIndex to sensible defaults
                this.toIndex = this.theTable.length - 1;
                this.fromIndex = Math.max(0, this.toIndex - 81);
            } else {
                // if we get here, the user has just unchecked the "includeErrors" checkbox,
                // so we must "translate" fromIndex and toIndex into new values
                var from = this.fromIndex;
                while (from >= 1 && !("noErrorId" in API.finalTable[from])){ from--; }
                this.fromIndex = ("noErrorId" in API.finalTable[from]) ? API.finalTable[from].noErrorId : 0;
                var to = this.toIndex;
                while (to < API.finalTable.length && !("noErrorId" in API.finalTable[to])){ to++; }
                this.toIndex = ("noErrorId" in API.finalTable[to]) ? API.finalTable[to].noErrorId : this.theTable.length-1;
            }
        }
        this.makeSlider();
        this.makeChart(this.chartContainer);
        $("#includeErrors").change(function(){
            TIMESERIES.includeErrors = document.getElementById("includeErrors").checked;
            TIMESERIES.make(false);
            //chartSizeInit_postMake(TIMESERIES.myName);
        });
    },
    onActivate : function(){
        this.attachMouseHandler();
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        this.chartResizerElt.style("display", "none");
    }
}; // End of TIMESERIES
/***************************************************************************************************************
************************************************ STATISTICS ****************************************************
****************************************************************************************************************/
STATISTICS = {
    hasResizeableChart : false,
    make : function(){
        pumpComboOrderings = {
            numOpHoursOrdering  : {asc: API.numOpHoursOrdering.concat().reverse()  , desc: API.numOpHoursOrdering                  , defoult: "desc"}, 
            meanFlowOrdering    : {asc: API.meanFlowOrdering                       , desc: API.meanFlowOrdering.concat().reverse() , defoult: "asc"},
            meanShaftOrdering   : {asc: API.meanShaftOrdering                      , desc: API.meanShaftOrdering.concat().reverse(), defoult: "asc"},
            meanHeadOrdering    : {asc: API.meanHeadOrdering                       , desc: API.meanHeadOrdering.concat().reverse() , defoult: "asc"},
            meanEffOrdering     : {asc: API.meanEffOrdering.concat().reverse()     , desc: API.meanEffOrdering                     , defoult: "desc"},
            effAtOptimumOrdering: {asc: API.effAtOptimumOrdering.concat().reverse(), desc: API.effAtOptimumOrdering                , defoult: "desc"},
            optFlowOrdering     : {asc: API.optFlowOrdering                        , desc: API.optFlowOrdering.concat().reverse()  , defoult: "asc"}
        };
        this.makeOrderButtonHandler("numOpHoursOrdering", "desc").call($("#orderByNumOpHours"));
    },
    makeOrderButtonHandler: function(orderingName, direction){
        var ordObj = pumpComboOrderings[orderingName];
        var ordering = ordObj[direction];
        var className = (direction == "asc" ? "upTriag" : "downTriag");
        return function(){
            var statsEntry = function (i){ return formattedPmpGroupInfo(API.common[ordering[i]]); }
            var statsTable = HtmlGen.tableFromRowFunc(API.common.length, statsEntry, pumpGroupInfoTableHeadings, {cellSpacing: 0, cellPadding: 1});
            $("#statisticsTable").html($(statsTable));
            $("#statisticsTable table tr:odd").addClass("oddRow");
            $("#statisticsTable table tr:even:not(:eq(0))").addClass("evenRow");
            $("#statisticsTable table tr:eq(0)").addClass("headerRow");
            $("#statisticsButtons .outerEisButton").removeClass("chosen");
            $("#" + orderingName).addClass(className);
            $("#" + orderingName).parent().addClass("chosen");
            $("#statisticsTable th span").each(function(index){
                var th = $(this).parent();
                th.addClass("sortable");
                var myId = this.id;
                var dir = pumpComboOrderings[myId].defoult;
                if ($(this).hasClass("upTriag")) dir = "desc";
                if ($(this).hasClass("downTriag")) dir = "asc";
                th.click(STATISTICS.makeOrderButtonHandler(myId, dir));
            });
        }
    }
};

/***************************************************************************************************************
************************************************ PUMP_COMB_PERF ************************************************
****************************************************************************************************************/
PUMP_COMB_PERF = {
    hasResizeableChart : true,
    height0 : 600,
    aspect  : 1.7,
    marginLR : 62,
    marginTop : 28,
    marginBot : 55,
    labelTopGap : 12,
    labelHorizGap : 12,
    cssClasses : ["flow", "head", "eff"],
    xAxisCssClass : "xAxis",
    yAxisCssClass : "yAxis",
    yEffAxisCssClass : "yEffAxis",
    xAxisLabelCssClass : "xAxisLabel",
    yAxisLabelCssClass : "yAxisLabel",
    flowAxisLabelCssClass  : "flowAxisLabel",
    tdhAxisLabelCssClass   : "tdhAxisLabel",
    yEffAxisLabelCssClass : "yEffAxisLabel",
    xAxisLabelText : "rank",
    yAxisLabelTextFlow : "flow (MGD)",
    yAxisLabelTextTDH : "TDH (psi)",
    yEffAxisLabelText : "efficiency (%)",
    FLOW_INDEX : 0,
    HEAD_INDEX : 1,
    EFF_INDEX  : 2,
    code : null,
    btnCode : null,
    pumpsList : null,
    sortBy: "time",
    comparisonFunctions : {
        flow : function(arg1, arg2){
            var flowDiff = arg1.flow - arg2.flow;
            if (flowDiff != 0) return flowDiff;
            return arg1.timeIdx - arg2.timeIdx;
        },
        head : function(arg1, arg2){
            var headDiff = arg1.head - arg2.head;
            if (headDiff != 0) return headDiff;
            return arg1.timeIdx - arg2.timeIdx;
        },
        eff : function(arg1, arg2){
            var effDiff = arg2.eff - arg1.eff;
            if (effDiff != 0) return effDiff;
            return arg1.timeIdx - arg2.timeIdx;
        }
    },
    make : function(){
        makeComboChooserButtons(false, "pumpCombinPerfButtons", "pcp", this, "code", "btnCode");
        this.makeOrderByButtons();
        this.container = d3.select("#tdPumpCombinPerfChart");
        $("#pcpBtnCombo" + API.common[0]).trigger("click");
    },
    makeOrderByButtons : function(){
        var btn1 = HtmlGen.eisBtn("time", {id: "pcpSortByTime", "class": "big chosen"});
        var btn2 = HtmlGen.eisBtn("flow", {id: "pcpSortByFlow", "class": "big"});
        var btn3 = HtmlGen.eisBtn("head", {id: "pcpSortByHead", "class": "big"});
        var btn4 = HtmlGen.eisBtn("efficiency", {id: "pcpSortByEff", "class": "big"});
        $("#pumpCombinPerfOrderByButtons").html("sort by &nbsp; " + btn1 + btn4 + btn2 + btn3);
        $("#pcpSortByTime").click(function(){
            PUMP_COMB_PERF.sortBy = "time"; PUMP_COMB_PERF.makeChart();
            $("#pcpSortByTime").addClass("chosen"); $("#pcpSortByFlow").removeClass("chosen");
            $("#pcpSortByHead").removeClass("chosen"); $("#pcpSortByEff").removeClass("chosen");
        });
        $("#pcpSortByFlow").click(function(){
            PUMP_COMB_PERF.sortBy = "flow"; PUMP_COMB_PERF.makeChart();
            $("#pcpSortByTime").removeClass("chosen"); $("#pcpSortByFlow").addClass("chosen");
            $("#pcpSortByHead").removeClass("chosen"); $("#pcpSortByEff").removeClass("chosen");
        });
        $("#pcpSortByHead").click(function(){
            PUMP_COMB_PERF.sortBy = "head"; PUMP_COMB_PERF.makeChart();
            $("#pcpSortByTime").removeClass("chosen"); $("#pcpSortByFlow").removeClass("chosen");
            $("#pcpSortByHead").addClass("chosen"); $("#pcpSortByEff").removeClass("chosen");
        });
        $("#pcpSortByEff").click(function(){
            PUMP_COMB_PERF.sortBy = "eff"; PUMP_COMB_PERF.makeChart();
            $("#pcpSortByTime").removeClass("chosen"); $("#pcpSortByFlow").removeClass("chosen");
            $("#pcpSortByHead").removeClass("chosen"); $("#pcpSortByEff").addClass("chosen");
        });
    },
    prepareChart : function(code){
        var pumpsList = API.pumpsListFromCode(code);
        this.pumpsList = pumpsList;
        var R = API.formulas.rowsWherePumpsRunOnly[code];
        var points = R.map(function(rowIdx, i){
            var row = API.finalTable[rowIdx];
            return {
                flow    : row.flow,
                head    : row.avgHead,
                eff     : 100*row.eff,
                date    : row.jsDate(),
                row     : row,
                timeIdx : i
            };
        });
        var flows = points.map(function(point){ return point.flow; });
        var heads = points.map(function(point){ return point.head; });
        var effs  = points.map(function(point){ return point.eff;  });
        var maxFlow = flows.max(); var minFlow = flows.min();
        var maxHead = heads.max(); var minHead = heads.min();
        var maxEff  = effs.max();  var minEff  = effs.min();
        var yMinLeft = Math.min(minFlow, minHead);
        var yMaxLeft = Math.max(maxFlow, maxHead);
        this.minEff = minEff;
        this.maxEff = maxEff;
        this.yMinLeft = yMinLeft;
        this.yMaxLeft = yMaxLeft;
        this.sortedPoints = {
            time : points
        }
        for (var key in this.comparisonFunctions){
            var compFun = this.comparisonFunctions[key];
            var pointsShallowCopy = points.concat();
            pointsShallowCopy.sort(compFun);
            this.sortedPoints[key] = pointsShallowCopy;
        }
    },
    makeChart : function(code){
        if (arguments.length >= 1){
            this.prepareChart(code);
        } else {
            code = this.code;
        }
        var myName = this.myName;
        var cssClasses = this.cssClasses;
        var FLOW_INDEX = this.FLOW_INDEX;
        var HEAD_INDEX = this.HEAD_INDEX;
        var EFF_INDEX  = this.EFF_INDEX;
        var minEff = this.minEff;
        var maxEff = this.maxEff;
        var yMinLeft = this.yMinLeft;
        var yMaxLeft = this.yMaxLeft;
        var marginLR  = this.marginLR;
        var marginTop = this.marginTop;
        var marginBot = this.marginBot;
        var container = this.container;
        var height0 = this.height0;
        var height = this.height = height0 * this.sizeFactor_Y;
        var width = this.width = height0 * this.aspect * this.sizeFactor_X;
        var pointCount = this.sortedPoints.time.length;
        var xDomain = [0, pointCount - 1];
        var yDomain = [yMinLeft, yMaxLeft];
        var yDomain_Eff = [minEff, maxEff];
        var innerWidth = width - 2 * marginLR;
        var innerHeight = height - marginTop - marginBot;
        var xStart = marginLR;
        var xEnd   = width  - marginLR;  this.xEnd   = xEnd;
        var yStart = height - marginBot; this.yStart = yStart;
        var yEnd = marginTop;
        var points = this.sortedPoints[this.sortBy];
        var data = [
            points.map(function(point){ return point.flow; }),
            points.map(function(point){ return point.head; }),
            points.map(function(point){ return point.eff;  })
        ];
        var x = d3.scale.linear().domain(xDomain).range([xStart, xEnd]);
        var y = d3.scale.linear().domain(yDomain).range([yStart, yEnd]);
        var y_Eff = d3.scale.linear().domain(yDomain_Eff).range([yStart, yEnd]);
        this.x = x;
        this.y = y;
        this.y_Eff = y_Eff;
        this.xInv = x.invert;
        this.yInv = y.invert;
        this.yEffInv = y_Eff.invert;
        var line     = d3.svg.line().x(function(d,i,j){return x(i);}).y(function(d,i,j){return y    (d);});
        var line_Eff = d3.svg.line().x(function(d,i,j){return x(i);}).y(function(d,i,j){return y_Eff(d);});
        container.html("");
        var svg = this.svg = container.append("svg").classed("pumpCombinationPerformance", true).attr("width", width).attr("height", height);
        this.svg = svg;
        // plot the lines
        svg.selectAll("path").data(data).enter()
        	.append("path")
        	.attr({
        		"fill": "none",
        		"d": function(d,i,j){
        			if (i == EFF_INDEX){
        				return line_Eff(d,i,j);
        			}
        			return line(d,i,j);
        		}
        	}).attr("class", function(d, i, j){
        		return myName + " line " + cssClasses[i]
        	});
        // make axes
        var xScale = d3.scale.linear().domain(xDomain).range([0, innerWidth]);   this.xScale = xScale;
        var yScale = d3.scale.linear().domain(yDomain).range([innerHeight, 0]);
        var yScale_Eff = d3.scale.linear().domain(yDomain_Eff).range([innerHeight, 0]);
        var xAxis = d3.svg.axis().scale(xScale).orient("bottom").ticks(pointCount < 10 ? pointCount : 10);
        var yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(10);
        var yAxis_Eff = d3.svg.axis().scale(yScale_Eff).orient("right").ticks(10);
        var axisCssClass = myName + " axis";
        var xAxisGroup = this.xAxisGroup = svg.append("g").translate(xStart, yStart).attr("class", axisCssClass).call(xAxis);
        var yAxisGroup = this.yAxisGroup = svg.append("g").translate(xStart, yEnd).attr("class", axisCssClass).call(yAxis);
        var yEffAxisGroup = this.EffyAxisGroup = svg.append("g").translate(xEnd, yEnd).attr("class", axisCssClass).call(yAxis_Eff);
        xAxisGroup.classed(this.xAxisCssClass, true);
        yAxisGroup.classed(this.yAxisCssClass, true);
        yEffAxisGroup.classed(this.yEffAxisCssClass, true);
        // axis labels
        var xAxisLabelCssClass = this.xAxisLabelCssClass;
        var yAxisLabelCssClass = this.yAxisLabelCssClass;
        var yEffAxisLabelCssClass = this.yEffAxisLabelCssClass;
        var flowAxisLabelCssClass = this.flowAxisLabelCssClass;
        var tdhAxisLabelCssClass  = this.tdhAxisLabelCssClass;
        var xAxisLabelText    = this.xAxisLabelText;
        var yAxisLabelTextFlow   = this.yAxisLabelTextFlow;
        var yAxisLabelTextTDH   = this.yAxisLabelTextTDH;
        var yEffAxisLabelText = this.yEffAxisLabelText;
        xAxisGroup.append("text").classed(xAxisLabelCssClass, true).classed("axisLabel", true).text(xAxisLabelText).attr({
        	x : innerWidth/2,
        	y : marginBot - 8
        });
        yEffAxisGroup.append("text").classed(yEffAxisLabelCssClass, true).classed("axisLabel", true).text(yEffAxisLabelText).attr({
        	x : this.labelTopGap,
        	y : -marginLR + 21,
        	"text-anchor" : "start",
        	"transform": "rotate(90)"
        });
        var yAxisLabelGroup = this.yAxisLabelGroup = yAxisGroup.append("g").classed(yAxisLabelCssClass, true).rotate(90);
        var flowLabel = yAxisLabelGroup.append("text").classed("axisLabel", true).classed(flowAxisLabelCssClass, true).text(yAxisLabelTextFlow).attr({
        	x : this.labelTopGap,
        	y : marginLR - this.labelHorizGap,
        	"text-anchor" : "start"
        });
        var tdhLabel = yAxisLabelGroup.append("text").classed("axisLabel", true).classed(tdhAxisLabelCssClass, true).text(yAxisLabelTextTDH).attr({
        	x : 125,
        	y : marginLR - this.labelHorizGap,
        	"text-anchor" : "start"
        });
        // mouse lines
        var hMouseLine = this.hMouseLine = svg.append("line").attr("x1", xStart).attr("x2", xEnd).attr("y1", 0).attr("y2", 0)
        	.attr("id", myName + "_H_MouseLine").classed("mouseLine horizontal inactive", true);
        var vMouseLine = this.vMouseLine = svg.append("line").attr("y1", yStart).attr("y2", yEnd).attr("x1", 0).attr("x2", 0)
        	.attr("id", myName + "_V_MouseLine").classed("mouseLine vertical inactive", true);
        var leftMouseAxisRectWidth = marginLR - 5;
        var leftMouseAxisRectHeight = this.leftMouseAxisRectHeight = 18;
        var leftMouseAxisRect = this.leftMouseAxisRect = svg.append("rect").attr("x", marginLR-leftMouseAxisRectWidth).attr("width", leftMouseAxisRectWidth)
        	.attr("y", 111).attr("height", leftMouseAxisRectHeight).classed("leftMouseAxisRect mouseAxisRect inactive", true);
        var leftMouseAxisText = this.leftMouseAxisText = svg.append("text").attr("x", xStart - 6).attr("y", 0)
        	.classed("leftMouseAxisText mouseAxisText inactive", true);
        var rightMouseAxisRectWidth = this.rightMouseAxisRectWidth = leftMouseAxisRectWidth;
        var rightMouseAxisRectHeight = this.rightMouseAxisRectHeight = leftMouseAxisRectHeight;
        var rightMouseAxisRect = this.rightMouseAxisRect = svg.append("rect").attr("x", xEnd).attr("width", rightMouseAxisRectWidth)
        	.attr("y", 111).attr("height", rightMouseAxisRectHeight).classed("rightMouseAxisRect mouseAxisRect inactive", true);
        var rightMouseAxisText = this.rightMouseAxisText = svg.append("text").attr("x", xEnd + 6).attr("y", 0)
        	.classed("rightMouseAxisText mouseAxisText inactive", true);
        var bottomMouseAxisRectWidth = this.bottomMouseAxisRectWidth = 102;
        var bottomMouseAxisRectHeight = this.bottomMouseAxisRectHeight = 32;
        var bottomMouseAxisRect = this.bottomMouseAxisRect = svg.append("rect").attr("y", yStart).attr("width", bottomMouseAxisRectWidth).attr("height", bottomMouseAxisRectHeight)
        	.classed("bottomMouseAxisRect mouseAxisRect inactive", true).attr("x", 0);
        var bottomMouseAxisText_1 = this.bottomMouseAxisText_1 = svg.append("text").attr("y", yStart + 14)
        	.classed("bottomMouseAxisText mouseAxisText inactive", true);
        var bottomMouseAxisText_2 = this.bottomMouseAxisText_2 = svg.append("text").attr("y", yStart + 28)
        	.classed("bottomMouseAxisText mouseAxisText inactive", true);
        this.attachMouseHandler();
    },
    attachMouseHandler : function(){
        theBody.on("mousemove", function(){
            var OBJ = PUMP_COMB_PERF;
            var myName = OBJ.myName;
            var m = d3.mouse(OBJ.svg.node());
            var xRaw = m[0];
            var yRaw = m[1];
            var mouseOverIndex = OBJ.xInv(xRaw);
            var mouseOverIndex = Math.round(mouseOverIndex);
            var mouseOverRow = OBJ.sortedPoints[OBJ.sortBy][mouseOverIndex];
            var hml = OBJ.hMouseLine;
            var vml = OBJ.vMouseLine;
            var lmar = OBJ.leftMouseAxisRect;
            var lmat = OBJ.leftMouseAxisText;
            var rmar = OBJ.rightMouseAxisRect;
            var rmat = OBJ.rightMouseAxisText;
            var bmar = OBJ.bottomMouseAxisRect;
            var mbat1 = OBJ.bottomMouseAxisText_1;
            var mbat2 = OBJ.bottomMouseAxisText_2;
            var yInRange = ((yRaw - OBJ.marginTop) * (OBJ.yStart - yRaw) > 0);
            var xInRange = ((xRaw - OBJ.marginLR ) * (OBJ.xEnd  - xRaw) > 0);
            var mouseIsOverChart = xInRange && yInRange;
            hml.classed("inactive", !mouseIsOverChart);
            vml.classed("inactive", !mouseIsOverChart);
            lmar.classed("inactive", !mouseIsOverChart);
            lmat.classed("inactive", !mouseIsOverChart);
            rmar.classed("inactive", !mouseIsOverChart);
            rmat.classed("inactive", !mouseIsOverChart);
            bmar.classed("inactive", !mouseIsOverChart);
            mbat1.classed("inactive", !mouseIsOverChart);
            mbat2.classed("inactive", !mouseIsOverChart);
            if (mouseIsOverChart){
                hml.attr("y1", yRaw).attr("y2", yRaw);
                vml.attr("x1", xRaw).attr("x2", xRaw).attr("y2", yRaw);
                lmar.attr("y", yRaw - OBJ.leftMouseAxisRectHeight/2);
                rmar.attr("y", yRaw - OBJ.rightMouseAxisRectHeight/2);
                lmat.attr("y", yRaw + OBJ.leftMouseAxisRectHeight/2 - 4).text("%5.1f".sprintf(OBJ.yInv(yRaw)));
                rmat.attr("y", yRaw + OBJ.rightMouseAxisRectHeight/2 - 4).text("%5.1f".sprintf(OBJ.yEffInv(yRaw)));
                bmar.attr("x", xRaw - OBJ.bottomMouseAxisRectWidth/2);
                mbat1.attr("x", xRaw).text("lorem");
                mbat2.attr("x", xRaw).text("ipsum");
            }
            if (mouseOverRow && mouseIsOverChart){
                var row = mouseOverRow.row;
                // make the tank levels table
                var tanksHTML = HtmlGen.div("tank levels", {id: "tankLevelsHeadingPcp"}) + HtmlGen.twoColTable(API.tankNames, mapFormat(tankLevelFormat, row.tankLevels));
                $("#pumpCombinPerfRightTop").html(tanksHTML);
                $("#pumpCombinPerfRightTop").width($("#tankLevelsHeadingPcp").width());
                var chosenColumnIndex = -1;
                var tableId = myName + "HintTable";
                var containerId = "pumpCombinPerfRightBottom";
                var dragHandleId = myName + "HintHandle";
                var minimizerId = myName + "Minimizer";
                var captionId = myName + "HintCaption";
                makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId);
            } else {
            }
        });
    },
    comboChoiceDone: function(code){
        this.makeChart(code);
    },
    onActivate : function(){
        this.attachMouseHandler();
        //debugger;
        initChartResizing(this.myName);
    },
    onDeactivate : function(){
        //debugger
        this.chartResizerElt.style("display", "none");
    }
};
/***************************************************************************************************************
************************************************ UTILITIES *****************************************************
****************************************************************************************************************/
function makeMouseOverTable(row, chosenColumnIndex, tableId, containerId, dragHandleId, minimizerId, captionId, whatToBeDraggedId){
    if (!whatToBeDraggedId){
        whatToBeDraggedId = containerId;
    }
    var pumpsRow = row.pumpsList.concat(["total"]);
    var pumpsInd = row.pumpsList.map(function(p){ return p-1});
    var flowRow = pumpsInd.map(function(i){ return row.absFlowContrib(i);}); flowRow.push(row.flow);
    var headRow = row.head.pick(pumpsInd).concat(row.avgHead);
    var shaftRow = row.shaft.pick(pumpsInd).concat(row.totalShaft);
    var effRow = pumpsInd.map(function(i){ return 100*row.singleEff(i);}); effRow.push(100*row.eff);
    flowRow = mapFormat(flowFormat, flowRow);
    headRow = mapFormat(tdhFormat, headRow);
    shaftRow = mapFormat(shaftFormat, shaftRow);
    effRow = mapFormat(effFormat, effRow);
    var minimizer = dragHandleId ? HtmlGen.span(HtmlSymbols.triagBigUp, {id: minimizerId, "class": "miniMaxiTriag"}) : "";
    var dateDivAtts = dragHandleId ? {"class": "mouseOverDate dragHandle", id: dragHandleId} : {"class": "mouseOverDate dragHandle"}; 
    var dateDiv = HtmlGen.div(HtmlGen.span(toDateString(row.date), {id: captionId, "class" : "mouseOverDateSpan"}) + minimizer, dateDivAtts);
    var tableHtml = HtmlGen.tableFrom2DArrayWithHeaderCol(
		[pumpsRow, flowRow, headRow, shaftRow, effRow],
		["pump", "flow", "head", "shaft", "eff"],
		true,
        {id: tableId, "class": "mouseOverHintTable"}
    );
    var html = dateDiv + tableHtml;
    if (!containerId) return html;
    var container = $("#" + containerId);
    container.html(html);
    var tableElt = $("#" + tableId);
    if (chosenColumnIndex != -1){
        tableElt.find("tr td:nth-child(" +chosenColumnIndex+ ")").addClass("chosenColumn");
        tableElt.find("tr th:nth-child(" +chosenColumnIndex+ ")").addClass("chosenColumn");
    }
    tableElt.find("tr:odd").addClass("oddRow");
    tableElt.find("tr:even:not(:eq(0))").addClass("evenRow");
    tableElt.find("tr:eq(0)").addClass("headerRow");
    if (dragHandleId){
        $( "#"  + whatToBeDraggedId).draggable({ handle: "#" + dragHandleId });
        var toggleHintFun = function(){
            // http://stackoverflow.com/questions/178325/testing-if-something-is-hidden-with-jquery
            if ($("#" + tableId).toggle().css("display") == "none"){
                $("#" + minimizerId).html(HtmlSymbols.triagBigDown);
            } else {
                $("#" + minimizerId).html(HtmlSymbols.triagBigUp);
            }
        };
        $("#" + minimizerId).click(toggleHintFun);
    }
    var dragHandleElt = jQuery("#" + dragHandleId);
    var bigMysteryOfTheUniverse = 10;
    tableElt.width(dragHandleElt.width() + bigMysteryOfTheUniverse);
    // console.log("handle " + dragHandleElt.width() + " table " + tableElt.width()); ... mystery!
    return container.html();
}

function makeComboChooserButtons(checkBoxesExist, containerId, btnIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, OBJExtraArgument){
    // containerId,                 btnIdPrefix,       OBJ,         chosenCodeName, OBJExtraArgument
    // "casControlsBotMain",            "cas", COMBINED_AND_SINGLE,     "code"         undefined
    // "pcaControlsBotMain",            "pca", PUMPCOMB_ANALYSIS,       "code"         undefined
    // "pumpCombinPerfButtons",         "pcp", PUMP_COMB_PERF,          "code"         undefined
    // new for pump combo comparison:
    // pccControls1                      pcc1,   PUMPCOMB_COMPARISON,      code1             1
    // pccControls2                      pcc2,   PUMPCOMB_COMPARISON,      code2             2
    
    var btnRowElt = $("#" + containerId);
    //var buttons = HtmlGen.btn("pumps<br/>op hrs", {disabled: true, id: btnIdPrefix + "LeadingPseudoBtn",  "class": "combinationButton pseudoButton"});
    var buttons = HtmlGen.eisBtn(
        HtmlGen.table(HtmlGen.tr(HtmlGen.td("pumps")) + HtmlGen.tr(HtmlGen.td("op hrs")), {"class": "innerButtonTable"})
        , {id: btnIdPrefix + "LeadingPseudoBtn",  "class": "small pseudo"}
    );
    for (var i=0; i<API.common.length; i++){
        var code = API.common[i];
        var pumpsList = API.pumpsListFromCode(code);
        var oph = API.numOpHours[code];
        //var btnCaption = "" + pumpsList + "<br/>" + oph;
        var btnCaption = HtmlGen.table(HtmlGen.tr(HtmlGen.td(pumpsList)) + HtmlGen.tr(HtmlGen.td(oph)), {"class": "innerButtonTable"});
        //buttons += HtmlGen.btn(btnCaption, {id: btnIdPrefix + "BtnCombo" + code, "class": "combinationButton"});
        buttons += HtmlGen.eisBtn(btnCaption, {id: btnIdPrefix + "BtnCombo" + code, "class": "small"});
    }
    btnRowElt.html(buttons);
    var makeBtnColoringHandler = function(code, idPre){
        return function(){
            // OBJ[chosenCodeName]: here the code of the chosen combination is stored in the tab object (that is, in OBJ)
            OBJ[chosenCodeName] = code;
            if (OBJ[chosenBtnCodeName]){
                // unmark old chosen
                jQuery(idPre + OBJ[chosenBtnCodeName]).removeClass("chosen");
            }
            if (code){
                OBJ[chosenBtnCodeName] = code; // remember choice
            }
            jQuery(idPre + OBJ[chosenBtnCodeName]).addClass("chosen"); // mark new chosen
        }
    };
    var makeUpdateCheckboxesHandler = checkBoxesExist ?
        function(code){
            // let the checkboxes reflect the same choice as the button does
            var cbIdPre = btnIdPrefix + "PumpComboChooserCheckbox";
            return function(){
                setCheckboxesToCode(cbIdPre, code);
            }
        } : function(){return function(){}; };
    // hook up handlers for main buttons
    for (var i=0; i<API.common.length; i++){
        var code = API.common[i];
        var idPre = "#" + btnIdPrefix + "BtnCombo";
        var id = idPre + code;
        var btn = jQuery(id);
        var makeH = function(code, idPre){
            // coloringHandler takes care that newly clicked button get the active class ("chosen"), and previously clicked one loses it.
            // cbHandler takes care that check boxes (in case they exist) reflact same choice as button
            // lastly event is delegated back to OBJ (the tab object / main UI object)
            var coloringHandler = makeBtnColoringHandler(code, idPre); 
            var cbHandler = makeUpdateCheckboxesHandler(code);
            return function(){
                coloringHandler();
                cbHandler();
                OBJ.comboChoiceDone(code, OBJExtraArgument);
            };
        }
        btn.click(makeH(code, idPre));
    }
}

function makeTwoCombosInfoTable(code1, code2, containerId, handleId, minimizerId){
	var pumpsList1 = API.pumpsListFromCode(code1);
	var pumpsList2 = API.pumpsListFromCode(code2);
	var dragHandle =  HtmlGen.div(
		HtmlGen.span("summary") + HtmlGen.span(HtmlSymbols.triagBigUp, {id: minimizerId, "class": "miniMaxiTriag"}),
		{id: handleId, "class": "dragHandle"}
	);
    var c = "#" + containerId;
	if (API.numOpHours[code1] || API.numOpHours[code2]){
		$(c).html(dragHandle + HtmlGen.threeColTable(pumpGroupInfoTableHeadings2, formattedPmpGroupInfo(code1), formattedPmpGroupInfo(code2), null, {"class": "summary"}));
	} else {
		$(c).html(dragHandle + HtmlGen.threeColTable(pumpGroupInfoTableHeadings2.pick([0,1,13,14])
            , formattedPmpGroupInfo(code1).pick([0,1,13,14])
            , formattedPmpGroupInfo(code2).pick([0,1,13,14])
            , null, {"class": "summary"}
        ));
	}
	$(c + " table").attr("cellspacing", 0);
	// format the table
	$(c + " table tr:odd").addClass("oddRow");
	$(c + " table tr:even").addClass("evenRow");
	var rows = $(c + " table tr");
	var rowCount = rows.length;
	for (var row=0; row<rowCount; row++){
		var elt = $(rows[row]);
		elt.addClass("row" + row);
		var cells = elt.find("td");
		$(cells[0]).addClass("leftTableCol");
		$(cells[2]).addClass("rightTableCol");
	}
	$(c).draggable({handle: "#" + handleId});
	$("#" + minimizerId).click(function(){
		if ( $(c + " table").toggle().css("display") == "none" ){
			$("#" + minimizerId).html(HtmlSymbols.triagBigDown);
		} else {
			$("#" + minimizerId).html(HtmlSymbols.triagBigUp);
		}
        $("#" + handleId).width($(c + " table").width() + 3);
	});
}

function stretchButtonsAux(stretchTdId, hostObject){
    var stretchTd = jQuery("#" + stretchTdId);
    var objName = hostObject.myName;
    var stretchButtonId     = objName + "StretchHoriz";
    var antiStretchButtonId = objName + "AntiStretchHoriz";
    // &#8622; left right strike through
    stretchTd.html(
        HtmlGen.eisBtn("&#8596;", {id : stretchButtonId}) +
        HtmlGen.eisBtn("&#8597;", {id : antiStretchButtonId})
    );
    hostObject.stretchButton = jQuery("#" + stretchButtonId);
    hostObject.antiStretchButton = jQuery("#" + antiStretchButtonId);
    hostObject.stretchButton.addClass("stretchButton horizontal");
    hostObject.antiStretchButton.addClass("stretchButton vertical");
}
function makeComboChooserInterface(containerId, btnIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, OBJExtraArgument){
    var container = $("#" + containerId);
    var topId = containerId + "TopAux"; // container of upper part (with sort buttons and check boxes)
    var botId = containerId + "BotMain"; // container of lower part (with main content (all the buttons))
    var topDiv = HtmlGen.div("", {id: topId, "class": "pumpComboChoiceUpper"});
    var botDiv = HtmlGen.div("", {id: botId, "class": "pumpComboChoiceLower"});
    container.html(topDiv + botDiv);
    // make content of upper part
    makeComboChooserTopAux(topId, botId, btnIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, OBJExtraArgument);
    // make main content (lower)
    makeComboChooserButtons(true, botId, btnIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, OBJExtraArgument);
}
function makeComboChooserTopAux(containerId, btnContainerId, btnIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, OBJExtraArgument){
	var container = jQuery("#" + containerId);
    var opHrsOrdBtnId = btnIdPrefix + "OrderByOpHoursBtn";
    var flowOrdBtnId = btnIdPrefix + "OrderByMeanFlowBtn";
	var orderByOpHoursButtonHTML = HtmlGen.eisBtn("op hours", {id: opHrsOrdBtnId, "class": "big"});
	var orderByMeanFlowButtonHTML = HtmlGen.eisBtn("mean flow", {id: flowOrdBtnId, "class": "big"});
	container.html("order buttons by " + orderByOpHoursButtonHTML + orderByMeanFlowButtonHTML);
	jQuery("#" + opHrsOrdBtnId).addClass("chosen");
	// hook up handlers for top buttons
	jQuery("#" + opHrsOrdBtnId).click(makeReorderButtonsByOpHoursFun(opHrsOrdBtnId, flowOrdBtnId, btnContainerId, btnIdPrefix));
	jQuery("#" + flowOrdBtnId).click(makeReorderButtonsByFlowFun(opHrsOrdBtnId, flowOrdBtnId, btnContainerId, btnIdPrefix));
	// make custom combination check boxes
    var cbIdPre = btnIdPrefix + "PumpComboChooserCheckbox";
	var checkboxSpan = HtmlGen.span(
		API.pumpsWithValidData.map(function(pump){ return "&nbsp;" + pump + HtmlGen.checkbox(false, {id: cbIdPre + pump})}).join("")
	);
	container.append($(HtmlGen.span(" &nbsp;Custom Combination ") + checkboxSpan));
	// handlers for checkboxes
	for (var i=0; i<API.numPumps; i++){
		var pump = i+1;
		var id = cbIdPre + pump;
		var elt = document.getElementById(id);
		if (!elt) continue;
		jQuery(elt).click(makeCheckboxHandler(btnIdPrefix, cbIdPre, OBJ, chosenCodeName, chosenBtnCodeName, pump, OBJExtraArgument));
	}
}
function makeComboInfoTable(code, containerId, handleId, minimizerId){
	var pumpsList = API.pumpsListFromCode(code);
	var dragHandle =  HtmlGen.div(
		HtmlGen.span("summary") + HtmlGen.span(HtmlSymbols.triagBigUp, {id: minimizerId, "class": "miniMaxiTriag"}),
		{id: handleId, "class": "dragHandle"}
	);
    var c = "#" + containerId;
	if (API.numOpHours[code]){
		$(c).html(dragHandle + HtmlGen.twoColTable(pumpGroupInfoTableHeadings2, formattedPmpGroupInfo(code), null, {"class": "summary"}));
	} else {
		$(c).html(dragHandle + HtmlGen.twoColTable(pumpGroupInfoTableHeadings2.pick([0,1,13,14]), formattedPmpGroupInfo(code).pick([0,1,13,14]), null, {"class": "summary"}));
	}
	$(c + " table").attr("cellspacing", 0);
	// format the table
	$(c + " table tr:odd").addClass("oddRow");
	$(c + " table tr:even").addClass("evenRow");
	var rows = $(c + " table tr");
	var rowCount = rows.length;
	for (var row=0; row<rowCount; row++){
		var elt = $(rows[row]);
		elt.addClass("row" + row);
		var cells = elt.find("td");
		$(cells[0]).addClass("leftTableCol");
		$(cells[1]).addClass("rightTableColUNUSED");
	}
	$(c).draggable({handle: "#" + handleId});
	$("#" + minimizerId).click(function(){
		if ( $(c + " table").toggle().css("display") == "none" ){
			$("#" + minimizerId).html(HtmlSymbols.triagBigDown);
		} else {
			$("#" + minimizerId).html(HtmlSymbols.triagBigUp);
		}
        $("#" + handleId).width($(c + " table").width() + 3);
	});
}
function makeReorderButtonsByOpHoursFun(opHrsOrdBtnId, flowOrdBtnId, btnContainerId, btnIdPrefix){
    // generates the handler reordering pump combination chooser buttons by hours in operation
    return function(){
    	jQuery("#" + opHrsOrdBtnId).addClass("chosen");
    	jQuery("#" + flowOrdBtnId).removeClass("chosen");
    	var btnRowElt = jQuery("#" + btnContainerId);
    	var detached = [];
    	for (var i=0; i<API.common.length; i++){
    		var code = API.common[i];
            var idPre = "#" + btnIdPrefix + "BtnCombo";
            var id = idPre + code;
            var btn = jQuery(id);
    		detached.push(btn);
    	}
    	for (var i=0; i<API.common.length; i++){
    		btnRowElt.append(detached[i]);
    	}
    }
}
function makeReorderButtonsByFlowFun(opHrsOrdBtnId, flowOrdBtnId, btnContainerId, btnIdPrefix){
    // generates the handler reordering pump combination chooser buttons by mean flow
    return function(){
    	jQuery("#" + flowOrdBtnId).addClass("chosen");
    	jQuery("#" + opHrsOrdBtnId).removeClass("chosen");
    	var btnRowElt = jQuery("#" + btnContainerId);
    	var detached = [];
    	for (var i=0; i<API.common.length; i++){
    		var code = API.common[API.meanFlowOrdering[i]];
            var idPre = "#" + btnIdPrefix + "BtnCombo";
            var id = idPre + code;
            var btn = jQuery(id);
    		detached.push(btn);
    	}
    	for (var i=0; i<API.common.length; i++){
    		btnRowElt.append(detached[i]);
    	}
    }
}
function makeCheckboxHandler(btnIdPrefix, checksIdPrefix, OBJ, chosenCodeName, chosenBtnCodeName, pump, OBJExtraArgument){
    // generates the handler needed when a checkbox for a custom pump combination is checked or unchecked
    return function(){
        var code = getCodeFromCheckboxes(checksIdPrefix); // the check boxes represent a binary number retrieved here
        if (code==0) return;               // binary zero represents no pump running at all, an invalid choice, ignore that
        var btn = document.getElementById(btnIdPrefix + "BtnCombo" + code);
        if (btn){
            // change in checkbox based choice is the same as an existing button represents
            // -> simulate that button being clicked, that's it, nothing else to do.
            $(btn).trigger("click");
        } else {
            OBJ[chosenCodeName] = code;
            // the button does not exist.
            // so we want to show something about a custom combination which (probably) never ran.
            if (OBJ[chosenBtnCodeName]){
                // anyways, must remove "chosen-coloring" (active class) of previously "chosen" button, (in case there was one)
                jQuery("#" + btnIdPrefix + "BtnCombo" + OBJ[chosenBtnCodeName]).removeClass("chosen");
                // none of the buttons is marked, remember that.
                OBJ[chosenBtnCodeName] = null;
            }
            OBJ.comboChoiceDone(code, OBJExtraArgument);
        }
    }
}
function setCheckboxesToCode(idPrefix, code){
	var runBits = API.runBitsFromCode(code);
	for (var i=0; i<API.numPumps; i++){
		var pump = i+1;
		var id = idPrefix + pump;
		var elt = document.getElementById(id);
		if (!elt) continue;
		elt.checked = runBits[i];
	}
}
function getCodeFromCheckboxes(idPrefix, wind){
    if (arguments.length < 2){
        wind = window;
    }
	var code = 0;
	var powerOf2 = 1;
	for (var i=0; i<API.numPumps; i++){
		var pump = i+1;
		var id = idPrefix + pump;
		var elt = wind.document.getElementById(id);
		if (elt && elt.checked) {
			code += powerOf2;
		}
		powerOf2 *= 2;
	}
    return code;
}
function makePanDragBehavior(objectName, options){
    if (arguments.length < 2){
        options = {};
    }
    var groupName = "mainGroup";
    if ("groupName" in options){ groupName = options.groupName; }
    var OBJ = tabObjects[objectName];
    var behavior = d3.behavior.drag();
    behavior.on("dragstart", function(){
        var mouseStartXY = d3.mouse(OBJ[groupName].node());
        var startXraw = mouseStartXY[0];
        var startYraw = mouseStartXY[1];
        var xInv = OBJ.makeXtrafo().invert;
        var yInv = OBJ.makeYtrafo().invert;
        OBJ.dragStartData = {
            xMinExt : OBJ.xMin,
            xMaxExt : OBJ.xMax,
            yMinExt : OBJ.yMin,
            yMaxExt : OBJ.yMax,
            xInv    : xInv,
            yInv    : yInv,
            startX  : xInv(startXraw),
            startY  : yInv(startYraw)
        };
    });
    behavior.on("drag", function(){
        var mouseXY = d3.mouse(OBJ[groupName].node());
        var xRaw = mouseXY[0];
        var yRaw = mouseXY[1];
        var dsd = OBJ.dragStartData;
        var xInv = dsd.xInv;
        var yInv = dsd.yInv;
        var currentX = xInv(xRaw);
        var currentY = yInv(yRaw);
        var dX = currentX - dsd.startX;
        var dY = currentY - dsd.startY;
        var xLo = dsd.xMinExt - dX;
        var xHi = dsd.xMaxExt - dX;
        var yLo = dsd.yMinExt - dY;
        var yHi = dsd.yMaxExt - dY;
        OBJ.updateChart(xLo, xHi, yLo, yHi);
    });
    return behavior;
}
function mouseZoomInit(){
    var THIS = this;
    this.mainGroup.on("mousewheel.zoom", function(){
        var withControlKey = d3.event.ctrlKey;
        if (withControlKey){
            return;
        }
        d3.event.preventDefault();
        var m = d3.mouse(THIS.mainGroup.node());
        var xRaw = m[0];
        var yRaw = m[1];
        var theX = THIS.x.invert(xRaw);
        var theY = THIS.y.invert(yRaw);
        var zoomFactor = 1;
        var theFactor = 0.95;
        if (d3.event.wheelDelta > 0){
            zoomFactor = theFactor;
        }
        if (d3.event.wheelDelta < 0){
            zoomFactor = 1/theFactor;
        }
        var withAltKey = d3.event.altKey;
        if (withAltKey){
            THIS.stretchWithCenter(theX, theY, zoomFactor);
        } else {
            THIS.zoomWithCenter(theX, theY, zoomFactor);
        }
    });
    this.zoomWithCenter = function(centerX, centerY, zoomFactor){
        this.generalZoomWithCenter(centerX, centerY, zoomFactor, zoomFactor);
    };
    this.stretchWithCenter = function(centerX, centerY, zoomFactor){
        this.generalZoomWithCenter(centerX, centerY, zoomFactor, 1/zoomFactor);
    };
    this.generalZoomWithCenter = function(centerX, centerY, zoomFactorX, zoomFactorY){
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dxLeft  = centerX - x0;
        var dxRight = x1 - centerX;
        var y0 = this.yMin;
        var y1 = this.yMax;
        var dyBot = centerY - y0;
        var dyTop = y1 - centerY;
        var _x0 = centerX - zoomFactorX * dxLeft;
        var _x1 = centerX + zoomFactorX * dxRight;
        var _y0 = centerY - zoomFactorY * dyBot;
        var _y1 = centerY + zoomFactorY * dyTop;
        this.updateChart(_x0, _x1, _y0, _y1);
    };
    objName = this.myName;
    jQuery("table#" + objName + "ZoomTable").parent().hide();
    jQuery("#" + objName + "StretchButtonsParentTd").hide();
}
function zoomInit(){
    this.zoomIn = function(){
        var zoomEpsilon = this.zoomEpsilon;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 + zoomEpsilon * dx;
        var xHi = x1 - zoomEpsilon * dx;
        var y0 = this.yMin;
        var y1 = this.yMax;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        this.updateChart(xLo, xHi, yLo, yHi);
    };
    this.zoomOut = function(){
        var zoomEpsilon = this.zoomEpsilon;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 - zoomEpsilon * dx;
        var xHi = x1 + zoomEpsilon * dx;
        var y0 = this.yMin;
        var y1 = this.yMax;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        this.updateChart(xLo, xHi, yLo, yHi);
    };
    this.stretch = function(){
        // zoom in horizontally and zoom out vertically - i.e. horizontal stretch
        var zoomEpsilon = this.zoomEpsilon/2;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 + zoomEpsilon * dx;
        var xHi = x1 - zoomEpsilon * dx;
        var y0 = this.yMin;
        var y1 = this.yMax;
        var dy = y1 - y0;
        var yLo = y0 - zoomEpsilon * dy;
        var yHi = y1 + zoomEpsilon * dy;
        this.updateChart(xLo, xHi, yLo, yHi);
    };
    this.antiStretch = function(){
        // zoom out horizontally and zoom in vertically - i.e. vertical stretch
        var zoomEpsilon = this.zoomEpsilon/2;
        var x0 = this.xMin;
        var x1 = this.xMax;
        var dx = x1 - x0;
        var xLo = x0 - zoomEpsilon * dx;
        var xHi = x1 + zoomEpsilon * dx;
        var y0 = this.yMin;
        var y1 = this.yMax;
        var dy = y1 - y0;
        var yLo = y0 + zoomEpsilon * dy;
        var yHi = y1 - zoomEpsilon * dy;
        this.updateChart(xLo, xHi, yLo, yHi);
    };
    this.sizeChanged = function(){
        //console.log("sizeFactor " + this.sizeFactor);
        this.updateChart(this.xMin, this.xMax, this.yMin, this.yMax);
    };
}
function initChartResizing(objName){
    var OBJ = tabObjects[objName];
    var elt = putResizerElementInPlace(OBJ, objName);
    attachResizingHandler(OBJ, elt);
}
function putResizerElementInPlace(OBJ, objName){
    if (!OBJ){
        debugger;
    }
    if (!OBJ.svg){
        debugger;
    }
    var svgElt = jQuery(OBJ.svg.node());
    var o = svgElt.offset();
    var svgLeft = o.left;
    var svgTop = o.top;
    var svgWidth = svgElt.width();
    var svgHeight = svgElt.height();
    var svgRight = svgLeft + svgWidth;
    var svgBottom = svgTop + svgHeight;
    var eltLeft = svgRight - 19;
    var eltTop = svgBottom - 19;
    var resizerId = "chartResizerImg_" + objName;
    var elt = jQuery("img#" + resizerId);
    if (!elt.length){
        theBody.append("img").attr("id", resizerId).attr("src", "img/corner.png").style("display", "none").attr("class", "flowHeadChartResizer");
        elt = jQuery("img#" + resizerId);
    }
    elt.css({
        position : "absolute",
        cursor   : "se-resize",
        left     : eltLeft,
        top      : eltTop
    });
    return elt;
}
function attachResizingHandler(OBJ, elt){
    var delt = d3.select(elt[0]);
    delt.style("display", null);
    OBJ.chartResizerElt = delt;
    var behavior = d3.behavior.drag();
    behavior.on("dragstart", function(){
        var mouseStartXY = d3.mouse(OBJ.svg.node());
        //DEBUG[0].html(mouseStartXY.join(", "));
        OBJ.chartResizeStartX = mouseStartXY[0];
        OBJ.chartResizeStartY = mouseStartXY[1];
        OBJ.chartResizeStartWidth = OBJ.width;
        OBJ.chartResizeStartHeight = OBJ.height;
        OBJ.chartResizeResizerEltStartLeft = parseFloat(elt.css("left"));
        OBJ.chartResizeResizerEltStartTop = parseFloat(elt.css("top"));
    });
    behavior.on("drag", function(){
        var mouseXY = d3.mouse(OBJ.svg.node());
        //DEBUG[1].html(mouseXY.join(", "));
        var mouseX = mouseXY[0];
        var mouseY = mouseXY[1];
        var dx = mouseX - OBJ.chartResizeStartX;
        var dy = mouseY - OBJ.chartResizeStartY;
        var targetWidth = OBJ.chartResizeStartWidth + dx;
        var targetHeight = OBJ.chartResizeStartHeight + dy;
        var height0 = OBJ.height0;
        var width0 = height0 * OBJ.aspect;
        var factor_X = targetWidth / width0;
        var factor_Y = targetHeight / height0;
        OBJ.sizeFactor_X = factor_X;
        OBJ.sizeFactor_Y = factor_Y;
        //DEBUG[7].html("drag sizefa " + factor_X + " , " + factor_Y);
        localStorage.setItem(makeLocalStorageVarName_X(OBJ.myName), factor_X);
        localStorage.setItem(makeLocalStorageVarName_Y(OBJ.myName), factor_Y);
        //DEBUG[0].html("mouse start " + OBJ.chartResizeStartX + " , " + OBJ.chartResizeStartY);
        //DEBUG[1].html("start width, height " + OBJ.chartResizeStartWidth + " , " + OBJ.chartResizeStartHeight);
        //DEBUG[2].html("target width, height " + targetWidth + " , " + targetHeight);
        //DEBUG[3].html("factors " + factor_X + " , " + factor_Y);
        if (OBJ.myName == "timeSeries"){
            OBJ.makeChart(OBJ.chartContainer);
        } else {
            if (OBJ.myName == "pumpCombinationPerformance"){
                OBJ.makeChart();
            } else {
                if (OBJ.myName.indexOf("ophrsEffByFlowRange_fl") != -1){
                    if (OPHRSEFF.flowRanges[numberAtEndOfString(OBJ.myName)].FHC != OBJ){
                        debugger;
                    }
                }
                OBJ.updateChart(OBJ.xMin, OBJ.xMax, OBJ.yMin, OBJ.yMax);
                if (OBJ.myName == "combinedSingle"){
                    pegCombinedAndSingleMouseOverInfoToTopRightCornerOfSVG();
                }
            }
        }
        elt.css({
            left : OBJ.chartResizeResizerEltStartLeft + dx,
            top  : OBJ.chartResizeResizerEltStartTop + dy
        })
    });
    delt.call(behavior);
}
function makeYAxisRangeStuff(container, idPrefix, OBJ, handlerArgs, byDefaultManual, flagNameInObj){
    var localStorageVarName = "potoEffD3_" + idPrefix + "_yManual";
    var localStorageVarValue = localStorage.getItem(localStorageVarName);
    var manual;
    if (localStorageVarValue == null){
        manual = byDefaultManual;
        localStorage.setItem(localStorageVarName, byDefaultManual);
    } else {
        manual = !!JSON.parse(localStorageVarValue);
    }
    OBJ[flagNameInObj] = manual;
    var id1 = idPrefix + "_yAxisAutoBtn";
    var id2 = idPrefix + "_yAxisNotAutoBtn";
    var id3 = idPrefix + "_yAxisDoitBtn";
    var html = HtmlGen.table00(
        HtmlGen.tr(
            HtmlGen.td("y-axis range adjust&nbsp;") +
            HtmlGen.td(HtmlGen.eisBtn("auto", {id: id1})) +
            //HtmlGen.td(HtmlGen.eisBtn("manual (press button) " + HtmlSymbols.rightArrow + "&nbsp;", {id: id2})) +
            HtmlGen.td(HtmlGen.eisBtn("manual &nbsp;", {id: id2})) +
            HtmlGen.td("&nbsp;") +
            HtmlGen.td(HtmlGen.eisBtn("&nbsp;", {id: id3}))
        )
    );
    container.html(html);
    var btn1 = jQuery("#" + id1);
    var btn2 = jQuery("#" + id2);
    var btn3 = jQuery("#" + id3);
    OBJ["yAxisAutoBtn"   + ("what" in handlerArgs ? "_" + handlerArgs.what : "")] = btn1;
    OBJ["yAxisManualBtn" + ("what" in handlerArgs ? "_" + handlerArgs.what : "")] = btn2;
    OBJ["AxisActionBtn"  + ("what" in handlerArgs ? "_" + handlerArgs.what : "")] = btn3;
    btn1.on("click", makeYAxisAutoHandler(OBJ, handlerArgs, localStorageVarName));
    btn2.on("click", makeYAxisManualHandler(OBJ, handlerArgs, localStorageVarName));
    btn3.on("click", makeYAxisActionHandler(OBJ, handlerArgs));
    btn3.hide();
    if (manual){
        btn2.addClass("chosen");
    } else {
        btn1.addClass("chosen");
    }
}
function makeYAxisAutoHandler(OBJ, handlerArgs, localStorageVarName){
    return function(evt){
        localStorage.setItem(localStorageVarName, false);
        OBJ["yAxisAutoBtn"   + ("what" in handlerArgs ? "_" + handlerArgs.what : "")].addClass("chosen");
        OBJ["yAxisManualBtn" + ("what" in handlerArgs ? "_" + handlerArgs.what : "")].removeClass("chosen");
        OBJ.setYaxisAuto(handlerArgs);
    }
}
function makeYAxisManualHandler(OBJ, handlerArgs, localStorageVarName){
    return function(evt){
        localStorage.setItem(localStorageVarName, true);
        OBJ["yAxisAutoBtn"   + ("what" in handlerArgs ? "_" + handlerArgs.what : "")].removeClass("chosen");
        OBJ["yAxisManualBtn" + ("what" in handlerArgs ? "_" + handlerArgs.what : "")].addClass("chosen");
        OBJ.setYaxisManual(handlerArgs);
    }
}
function makeYAxisActionHandler(OBJ, handlerArgs){
    return function(evt){
        OBJ.setYaxisAction(handlerArgs);
    }
}
